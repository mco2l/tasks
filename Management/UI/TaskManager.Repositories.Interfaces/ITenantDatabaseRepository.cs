﻿using System;
using System.Collections.Generic;
using System.Text;
using TaskManager.DTO;

namespace TaskManager.Repositories.Interfaces
{
    public interface ITenantDatabaseRepository
    {
        TaskTypeSettingsDTO GetTaskTypeSettingsByTaskTypeId(long taskTypeId);
        CreateTaskDTO CreateTask(string tenantConnectionString, CreateTaskDTO createTaskDTO);

        List<ColumnPropertiesDTO> GetSettingsTaskByTaskId(string connectionString, long taskId);
        List<ColumnPropertiesDTO> GetSpecificSettingsForTaskByTaskId(string tenantConnectionString, string settingsTable, long taskId);
    }
}
