﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskManager.ErrorHandling
{
    public class ErrorClass
    {
        public bool HasError { get; set; } = false;
        public string CustomError { get; set; } = string.Empty;
        public string ExceptionMessage { get; set; } = string.Empty;

    }
}
