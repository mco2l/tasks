﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using Microsoft.Data.Sqlite;
using Microsoft.Extensions.Options;
using TaskManager.DTO;
using TaskManager.ErrorHandling;
using TaskManager.Middleware;
using TaskManager.Repositories.Interfaces;

namespace TaskManager.Repositories
{
    public class TenantDatabaseRepository : ITenantDatabaseRepository
    {
        private IMainDatabaseRepository _mainDatabaseRepository;
        private IOptions<TenantMiddlewareOptions> _options;

        public TenantDatabaseRepository(IMainDatabaseRepository mainDatabaseRepository, IOptions<TenantMiddlewareOptions> options)
        {
            _mainDatabaseRepository = mainDatabaseRepository;
            _options = options;
        }



        public CreateTaskDTO CreateTask(string tenantConnectionString, CreateTaskDTO createTaskDTO)
        {
            ErrorClass error = new ErrorClass();
            var count = 0;
            var propFail = "";

            CreateTaskDTO createdTask = new CreateTaskDTO()
            {
                CommonTaskFields = new List<ColumnPropertiesDTO>(),
                SettingsTaskFields = new List<ColumnPropertiesDTO>()             
            };

            SqliteTransaction tr1 = null;
            SqliteTransaction tr2 = null;

            try
            {
                //Generating unique id for this organization



                using (SqliteConnection con = new SqliteConnection(tenantConnectionString))
                {
                    con.Open();

                    using (SqliteCommand cmd = con.CreateCommand())
                    {


                        #region Insert Into SettingsTable     

                        tr1 = con.BeginTransaction();

                        var tenantSettingsTaskTable = "Settings_Task";
                        var columnNames = _mainDatabaseRepository.GetTableColumnNames(tenantConnectionString, tenantSettingsTaskTable);

                        StringBuilder sbColumns, sbColumnsValues;
                        BuildInsertQuery(columnNames, out sbColumns, out sbColumnsValues);

                        for (int i = 0; i < columnNames.Count; i++)
                        {
                            count = i;
                            propFail = createTaskDTO.CommonTaskFields.Where(r => r.FieldName == columnNames[i]).FirstOrDefault().FieldName;

                            var recordType = createTaskDTO.CommonTaskFields.Where(r => r.FieldName == columnNames[i]).FirstOrDefault().FieldDataType;

                            if (recordType == "BOOLEAN")
                            {
                                //If checkbox is unchecked the value is null, and so 0 is the value for False.
                                if (createTaskDTO.CommonTaskFields.Where(r => r.FieldName == columnNames[i]).FirstOrDefault().FieldValue == null)
                                    createTaskDTO.CommonTaskFields.Where(r => r.FieldName == columnNames[i]).FirstOrDefault().FieldValue = "0";

                                cmd.Parameters.AddWithValue($"@{columnNames[i]}", createTaskDTO.CommonTaskFields.Where(r => r.FieldName == columnNames[i]).FirstOrDefault().FieldValue);
                            }
                            else if (recordType == "TEXT" || recordType.Contains("TEXT"))
                            {
                                //if (columnNames[i] == "TenantGUID")
                                //{
                                //    cmd.Parameters.AddWithValue($"@{columnNames[i]}", tenantGUID);
                                //}
                                //else
                                //{
                                cmd.Parameters.AddWithValue($"@{columnNames[i]}", createTaskDTO.CommonTaskFields.Where(r => r.FieldName == columnNames[i]).FirstOrDefault().FieldValue);
                                //}
                            }
                            else if (recordType == "INTEGER")
                            {
                                if (columnNames[i] != "Id")
                                    cmd.Parameters.AddWithValue($"@{columnNames[i]}", int.Parse(createTaskDTO.CommonTaskFields.Where(r => r.FieldName == columnNames[i]).FirstOrDefault().FieldValue));
                            }

                        }

                        //SQL query to update data from table
                        string myQuery = $"INSERT INTO {tenantSettingsTaskTable} ({sbColumns.ToString()}) VALUES ({sbColumnsValues})";

                        cmd.CommandText = myQuery;


                        //cmd.Parameters.AddWithValue("@TenantId", record.Where(r => r.ColumnName == "TenantId").FirstOrDefault().ColumnValueNumber);
                        //cmd.Parameters.AddWithValue("@TenantName", record.Where(r => r.ColumnName == "TenantName").FirstOrDefault().ColumnValueString);
                        //cmd.Parameters.AddWithValue("@AccessKey", record.Where(r => r.ColumnName == "AccessKey").FirstOrDefault().ColumnValueString);
                        //cmd.Parameters.AddWithValue("@Disabled", record.Where(r => r.ColumnName == "Disabled").FirstOrDefault().ColumnValueBool);


                        //con.Open();//Open the connection with database

                        cmd.Transaction = tr1;

                        cmd.ExecuteNonQuery();//Executes the SQL query


                        //GetLastInsertedRowID
                        cmd.CommandText = "SELECT Last_Insert_Rowid()";

                        // The row ID is a 64-bit value - cast the Command result to an Int64.
                        Int64 LastRowID64 = (Int64)cmd.ExecuteScalar();

                        // Then grab the bottom 32-bits as the unique ID of the row. This wil actually be the TaskId
                        long LastRowID = (long)LastRowID64;


                        tr1.Commit();

                        #endregion


                        var recordProps = GetSettingsTaskByTaskId(tenantConnectionString, LastRowID);

                        foreach (var item in recordProps)
                        {
                            createdTask.CommonTaskFields.Add(item);
                        }

                        var taskTypeId = long.Parse(createdTask.CommonTaskFields.Where(t => t.FieldName == "TaskTypeId").FirstOrDefault().FieldValue);

                      


                        #region Insert Into Settings_{SpecificTaskTable}

                        tr2 = con.BeginTransaction();


                        var settingsSpecificTaskTable = _mainDatabaseRepository.GetSettingsTableNameForTaskTypeId(_options.Value.MainConnectionString, taskTypeId);

                        columnNames = _mainDatabaseRepository.GetTableColumnNames(tenantConnectionString, settingsSpecificTaskTable);

                        BuildInsertQuery(columnNames, out sbColumns, out sbColumnsValues);

                        cmd.Parameters.Clear();

                        for (int i = 0; i < columnNames.Count; i++)
                        {
                            count = i;
                            propFail = createTaskDTO.SettingsTaskFields.Where(r => r.FieldName == columnNames[i]).FirstOrDefault().FieldName;

                            var recordType = createTaskDTO.SettingsTaskFields.Where(r => r.FieldName == columnNames[i]).FirstOrDefault().FieldDataType;

                            if (recordType == "BOOLEAN")
                            {
                                //If checkbox is unchecked the value is null, and so 0 is the value for False.
                                if (createTaskDTO.SettingsTaskFields.Where(r => r.FieldName == columnNames[i]).FirstOrDefault().FieldValue == null)
                                    createTaskDTO.SettingsTaskFields.Where(r => r.FieldName == columnNames[i]).FirstOrDefault().FieldValue = "0";

                                cmd.Parameters.AddWithValue($"@{columnNames[i]}", createTaskDTO.SettingsTaskFields.Where(r => r.FieldName == columnNames[i]).FirstOrDefault().FieldValue);
                            }
                            else if (recordType == "TEXT" || recordType.Contains("TEXT"))
                            {
                                //if (columnNames[i] == "TenantGUID")
                                //{
                                //    cmd.Parameters.AddWithValue($"@{columnNames[i]}", tenantGUID);
                                //}
                                //else
                                //{
                                cmd.Parameters.AddWithValue($"@{columnNames[i]}", createTaskDTO.SettingsTaskFields.Where(r => r.FieldName == columnNames[i]).FirstOrDefault().FieldValue);
                                //}
                            }
                            else if (recordType == "INTEGER")
                            {
                                if (columnNames[i] == "TaskId")
                                {
                                    //Automatically set task id
                                    //createdTask.SettingsTaskFields.FirstOrDefault(s => s.FieldName == "TaskId").FieldValue = createdTask.CommonTaskFields.Where(t => t.FieldName == "Id").FirstOrDefault().FieldValue;

                                    cmd.Parameters.AddWithValue($"@{columnNames[i]}", int.Parse(createdTask.CommonTaskFields.Where(t => t.FieldName == "Id").FirstOrDefault().FieldValue));

                                }
                                else if (columnNames[i] != "Id")
                                    cmd.Parameters.AddWithValue($"@{columnNames[i]}", int.Parse(createTaskDTO.SettingsTaskFields.Where(r => r.FieldName == columnNames[i]).FirstOrDefault().FieldValue));
                            }

                        }

                        myQuery = $"INSERT INTO {settingsSpecificTaskTable} ({sbColumns.ToString()}) VALUES ({sbColumnsValues})";

                        cmd.CommandText = myQuery;

                        cmd.Transaction = tr2;

                        cmd.ExecuteNonQuery();//Executes the SQL query

                        tr2.Commit();


                        recordProps = GetSpecificSettingsForTaskByTaskId(tenantConnectionString, settingsSpecificTaskTable, LastRowID);

                        foreach (var item in recordProps)
                        {
                            createdTask.SettingsTaskFields.Add(item);
                        }


                        #endregion
                    }

                }
            }
            catch(SqliteException ex)
            {
                tr1.Rollback();
                tr2.Rollback();

                tr1.Dispose();
                tr2.Dispose();

                error.HasError = true;

                error.ExceptionMessage = ex.Message;

                error.CustomError = $"CreateTask";
            }
            catch (Exception ex)
            {
                error.HasError = true;

                error.ExceptionMessage = ex.Message;

                error.CustomError = $"CreateTask  {propFail}";
            }

            return createdTask;
        }

        private static void BuildInsertQuery(List<string> columnNames, out StringBuilder sbColumns, out StringBuilder sbColumnsValues)
        {

            //Building query column parameters
            /*$"INSERT INTO {tableName} ({sbColumns.ToString()}) VALUES ({sbColumnsValues})";*/
            sbColumns = new StringBuilder();
            for (int i = 0; i < columnNames.Count; i++)
            {
                if (columnNames[i] != "Id")
                {
                    sbColumns.Append($"{columnNames[i]}");

                    if (i < columnNames.Count - 1)
                        sbColumns.Append(",");
                }
            }



            //Building query parameters
            /*$"INSERT INTO {tableName} ({sbColumns.ToString()}) VALUES ({sbColumnsValues})";*/
            sbColumnsValues = new StringBuilder();
            for (int i = 0; i < columnNames.Count; i++)
            {
                if (columnNames[i] != "Id")
                {
                    sbColumnsValues.Append($"@{columnNames[i]}");

                    if (i < columnNames.Count - 1)
                        sbColumnsValues.Append(",");
                }
            }
        }

        public List<ColumnPropertiesDTO> GetSettingsTaskByTaskId(string connectionString, long taskId)
        {
            ErrorClass error = new ErrorClass();

            var tableName = "Settings_Task";


            List<ColumnPropertiesDTO> record = new List<ColumnPropertiesDTO>();


            try
            {
                using (SqliteConnection con = new SqliteConnection(connectionString))
                {
                    con.Open();

                    using (SqliteCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = $@"select * from {tableName} where Id=@Id";



                        cmd.Parameters.AddWithValue("@Id", taskId);
                        cmd.CommandType = System.Data.CommandType.Text;

                        using (SqliteDataReader reader = cmd.ExecuteReader())
                        {
                            foreach (DbDataRecord s in reader.Cast<DbDataRecord>())
                            {
                                var val = s;

                                for (int i = 0; i < s.FieldCount; i++)
                                {
                                    var recordDTO = new ColumnPropertiesDTO();
                                    recordDTO.FieldName = s.GetName(i);

                                    var dataType = s.GetDataTypeName(i).Trim();
                                    var fieldType = s.GetFieldType(i).Name;

                                    recordDTO.IsPrimaryKey = reader.GetString(5) == "1" ? true : false; //ordinal 5 indicates if is PK
                                    recordDTO.Required = reader.GetString(3) == "1" ? true : false; //ordinal 3 indicates if field is not null

                                    recordDTO.FieldType = fieldType;
                                    recordDTO.FieldDataType = dataType;
                                    recordDTO.FieldValue = s[i].ToString();

                                    record.Add(recordDTO);

                                }

                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                error.HasError = true;
                error.ExceptionMessage = ex.Message;
                error.CustomError = "GetSettingsTaskByTaskId";
            }

            return record;
        }



        public List<ColumnPropertiesDTO> GetSpecificSettingsForTaskByTaskId(string tenantConnectionString, string settingsTableName, long taskId)
        {
            ErrorClass error = new ErrorClass();

            List<ColumnPropertiesDTO> record = new List<ColumnPropertiesDTO>();

            try
            {
                using (SqliteConnection con = new SqliteConnection(tenantConnectionString))
                {
                    con.Open();

                    using (SqliteCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = $@"select * from {settingsTableName} where TaskId=@TaskId";



                        cmd.Parameters.AddWithValue("@TaskId", taskId);
                        cmd.CommandType = System.Data.CommandType.Text;

                        using (SqliteDataReader reader = cmd.ExecuteReader())
                        {
                            foreach (DbDataRecord s in reader.Cast<DbDataRecord>())
                            {
                                var val = s;

                                for (int i = 0; i < s.FieldCount; i++)
                                {
                                    var recordDTO = new ColumnPropertiesDTO();
                                    recordDTO.FieldName = s.GetName(i);

                                    var dataType = s.GetDataTypeName(i).Trim();
                                    var fieldType = s.GetFieldType(i).Name;

                                    recordDTO.IsPrimaryKey = reader.GetString(5) == "1" ? true : false; //ordinal 5 indicates if is PK
                                    recordDTO.Required = reader.GetString(3) == "1" ? true : false; //ordinal 3 indicates if field is not null

                                    recordDTO.FieldType = fieldType;
                                    recordDTO.FieldDataType = dataType;
                                    recordDTO.FieldValue = s[i].ToString();

                                    record.Add(recordDTO);

                                }

                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                error.HasError = true;
                error.ExceptionMessage = ex.Message;
                error.CustomError = "GetSpecificSettingsForTaskByTaskId " + settingsTableName;
            }

            return record;
        }

        public TaskTypeSettingsDTO GetTaskTypeSettingsByTaskTypeId(long taskTypeId)
        {
            ErrorClass error = new ErrorClass();

            TaskTypeSettingsDTO taskTypeSettings = new TaskTypeSettingsDTO();

            try
            {

            }
            catch (Exception ex)
            {

                error.HasError = true;

                error.ExceptionMessage = ex.Message;

                error.CustomError = "GetTaskTypeSettingsByTaskTypeId";
            }

            return taskTypeSettings;
        }
    }


}
