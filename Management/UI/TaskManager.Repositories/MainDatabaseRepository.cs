﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Data.Sqlite;
using TaskManager.DTO;
using TaskManager.ErrorHandling;
using TaskManager.Repositories.Interfaces;

namespace TaskManager.Repositories
{
    public class MainDatabaseRepository : IMainDatabaseRepository
    {
        private IHostingEnvironment _hostingEnvironment;

        public MainDatabaseRepository(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        public ErrorClass EnsureDatabaseCreated(string connectionString, string conntentRootPath)
        {
            ErrorClass error = new ErrorClass();

            try
            {

                string contentRootPath = conntentRootPath;

                var mainDatabasePath = Path.Combine(contentRootPath, "Main.db");

                if (!System.IO.File.Exists(mainDatabasePath))
                {
                    //string strConnection = @"Data Source=C:\Users\luis.coelho\Documents\visual studio 2017\Projects\SQLiteLabProject\SQLiteLabProject\main.db";
                    var rooDir = Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).FullName, "Database\\Main\\Main.sql");

                    //string strCommand = System.IO.File.ReadAllText(@"C:\Users\luis.coelho\Documents\visual studio 2017\Projects\SQLiteLabProject\Database\Main.sql"); // .sql file path
                    string strCommand = System.IO.File.ReadAllText(rooDir); // .sql file path



                    using (SqliteConnection objConnection = new SqliteConnection(connectionString))
                    {
                        using (SqliteCommand objCommand = objConnection.CreateCommand())
                        {
                            objConnection.Open();
                            objCommand.CommandText = strCommand;

                            objCommand.ExecuteNonQuery();
                            objConnection.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                error.HasError = true;
                error.ExceptionMessage = ex.Message;
                error.CustomError = "EnsureDatabaseCreated";
            }

            return error;
        }


        public List<SQLiteRecordDTO> GetOrganizationById(string connectionString, long TenantId)
        {
            ErrorClass error = new ErrorClass();

            List<SQLiteRecordDTO> record = new List<SQLiteRecordDTO>();

            try
            {
                using (SqliteConnection con = new SqliteConnection(connectionString))
                {
                    con.Open();

                    using (SqliteCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = @"select * from OrganizationSettings where TenantId=@TenantId";



                        cmd.Parameters.AddWithValue("@TenantId", TenantId);
                        cmd.CommandType = System.Data.CommandType.Text;

                        using (SqliteDataReader reader = cmd.ExecuteReader())
                        {
                            foreach (DbDataRecord s in reader.Cast<DbDataRecord>())
                            {
                                var val = s;

                                for (int i = 0; i < s.FieldCount; i++)
                                {
                                    var recordDTO = new SQLiteRecordDTO();
                                    recordDTO.ColumnName = s.GetName(i);

                                    var dataType = s.GetDataTypeName(i).Trim();
                                    var fieldType = s.GetFieldType(i).Name;

                                    recordDTO.FieldType = fieldType;
                                    recordDTO.DataType = dataType;
                                    recordDTO.ColumnValue = s[i].ToString();

                                    //switch (dataType)
                                    //{
                                    //    case "BOOLEAN":
                                    //        recordDTO.DataType = "checkbox";
                                    //        if (!string.IsNullOrEmpty(s[i].ToString()))
                                    //        {
                                    //            recordDTO.ColumnValue = (Int64)s[i] > 0 ? true : false;
                                    //        }
                                    //        else
                                    //        {
                                    //            recordDTO.ColumnValue = string.Empty;
                                    //        }


                                    //        break;
                                    //    case "INTEGER":
                                    //        recordDTO.DataType = "number";

                                    //        if (s.GetFieldType(i).Name == "Int64")
                                    //        {
                                    //            //recordDTO.ColumnValueString = s[i].ToString();
                                    //            recordDTO.ColumnValue = (Int64)s[i];
                                    //        }
                                    //        else if (s.GetFieldType(i).Name == "Int32")
                                    //        {
                                    //            //recordDTO.ColumnValueString = s[i].ToString();
                                    //            recordDTO.ColumnValue = (Int32)s[i];
                                    //        }
                                    //        else if (s.GetFieldType(i).Name == "Int16")
                                    //        {
                                    //            //recordDTO.ColumnValueString = s[i].ToString();
                                    //            recordDTO.ColumnValue = (Int16)s[i];
                                    //        }
                                    //        if (string.IsNullOrEmpty(s[i].ToString()))
                                    //        {
                                    //            recordDTO.ColumnValue = 0;
                                    //        }


                                    //        break;
                                    //    case "TEXT":
                                    //        recordDTO.DataType = "text";

                                    //        if (s.GetFieldType(i).Name == "DBNull")
                                    //        {
                                    //            //recordDTO.ColumnValueString = s[i].ToString();
                                    //            recordDTO.ColumnValue = (DBNull)s[i];
                                    //        }
                                    //        else if (s.GetFieldType(i).Name == "String")
                                    //        {
                                    //            //recordDTO.ColumnValueString = s[i].ToString();
                                    //            recordDTO.ColumnValue = (String)s[i];
                                    //        }
                                    //        else if(string.IsNullOrEmpty(s[i].ToString()))
                                    //        {
                                    //            recordDTO.ColumnValue = string.Empty;
                                    //        }


                                    //        break;
                                    //}

                                    //if (s.GetFieldType(i).Name == "Int64")
                                    //{
                                    //    recordDTO.DataType = "number";
                                    //    //recordDTO.ColumnValueString = s[i].ToString();
                                    //    recordDTO.ColumnValue = (Int64)s[i];
                                    //}
                                    //else if(s.GetFieldType(i).Name == "Int32")
                                    //{
                                    //    recordDTO.DataType = "number";
                                    //    //recordDTO.ColumnValueString = s[i].ToString();
                                    //    recordDTO.ColumnValue = (Int32)s[i];
                                    //}
                                    //else if (s.GetFieldType(i).Name == "Int16")
                                    //{
                                    //    recordDTO.DataType = "number";
                                    //    //recordDTO.ColumnValueString = s[i].ToString();
                                    //    recordDTO.ColumnValue = (Int16)s[i];
                                    //}
                                    //else if(s.GetFieldType(i).Name == "DBNull")
                                    //{
                                    //    recordDTO.DataType = "text";
                                    //    //recordDTO.ColumnValueString = s[i].ToString();
                                    //    recordDTO.ColumnValue = (DBNull)s[i];
                                    //}
                                    //else if(s.GetFieldType(i).Name == "String")
                                    //{
                                    //    recordDTO.DataType = "text";
                                    //    //recordDTO.ColumnValueString = s[i].ToString();
                                    //    recordDTO.ColumnValue = (String)s[i];
                                    //}
                                    //else if(s.GetFieldType(i).Name == "Boolean" || s.GetDataTypeName(i).Trim() == "Boolean")
                                    //{
                                    //    recordDTO.DataType = "checkbox";
                                    //    //recordDTO.ColumnValueString = s[i].ToString();
                                    //    recordDTO.ColumnValue = (Boolean)s[i];
                                    //}


                                    //if (s.GetDataTypeName(i).Trim() == "TEXT")
                                    //{
                                    //    recordDTO.DataType = "text";
                                    //    //recordDTO.ColumnValueString = s[i].ToString();
                                    //    recordDTO.ColumnValue = (String)s[i];
                                    //}
                                    //if (s.GetDataTypeName(i).Trim() == "INTEGER")
                                    //{
                                    //    recordDTO.DataType = "number";
                                    //    //recordDTO.ColumnValueNumber = int.Parse(s[i].ToString());
                                    //    recordDTO.ColumnValue = (Int64)s[i];
                                    //}
                                    //if (s.GetDataTypeName(i).Trim() == "BOOLEAN")
                                    //{
                                    //    recordDTO.DataType = "checkbox";

                                    //    if (!string.IsNullOrEmpty(s[i].ToString()))
                                    //    {
                                    //        if (s[i].ToString() == "1")
                                    //        {
                                    //            //recordDTO.ColumnValueBool = true;
                                    //            recordDTO.ColumnValue = true;
                                    //        }
                                    //        else if (s[i].ToString() == "0")
                                    //        {
                                    //            //recordDTO.ColumnValueBool = false;
                                    //            recordDTO.ColumnValue = false;
                                    //        }
                                    //    }
                                    //    else
                                    //    {
                                    //        //recordDTO.ColumnValueBool = false;
                                    //        recordDTO.ColumnValue = false;
                                    //    }

                                    //}

                                    record.Add(recordDTO);

                                }

                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                error.HasError = true;
                error.ExceptionMessage = ex.Message;
                error.CustomError = "GetOrganizationById";
            }



            return record;
        }


        public List<DbDataRecord> GetAllOrganizations(string connectionString)
        {
            List<Dictionary<string, string>> _table = new List<Dictionary<string, string>>();

            List<DbDataRecord> table = new List<DbDataRecord>();



            using (SqliteConnection con = new SqliteConnection(connectionString))
            {
                con.Open();

                string stm = $"SELECT * FROM OrganizationSettings";

                using (SqliteCommand cmd = new SqliteCommand(stm, con))
                {
                    using (SqliteDataReader rdr = cmd.ExecuteReader())
                    {
                        foreach (DbDataRecord s in rdr.Cast<DbDataRecord>())
                        {
                            var val = s;

                            table.Add(s);
                        }

                        //while (rdr.Read())
                        //{

                        //    ///


                        //    ///////////////////////////////////


                        //    //List<string> columnNames = new List<string>();

                        //    //for (int i = 0; i < rdr.FieldCount; i++)
                        //    //{
                        //    //    columnNames.Add(rdr.GetName(i));

                        //    //    Dictionary<string, string> record = new Dictionary<string, string>();

                        //    //    var columnName = rdr.GetName(i);

                        //    //    if (rdr[columnName] != null)
                        //    //    {
                        //    //        record.Add(columnName, rdr[columnName].ToString());

                        //    //        table.Add(record);

                        //    //    }
                        //    //}
                        //}
                    }
                }

                con.Close();
            }


            return table;
        }


        public List<SQLiteRecordDTO> UpdateOrganization(string connectionString, List<SQLiteRecordDTO> record)
        {
            ErrorClass error = new ErrorClass();

            try
            {

                var tableName = "OrganizationSettings";

                var columnNames = GetTableColumnNames(connectionString, "OrganizationSettings");

                using (SqliteConnection con = new SqliteConnection(connectionString))
                {
                    con.Open();

                    using (SqliteCommand cmd = con.CreateCommand())
                    {
                        StringBuilder sb = new StringBuilder();

                        for (int i = 0; i < columnNames.Count(); i++)
                        {
                            if (columnNames[i] != "TenantId")
                            {
                                sb.Append($"{columnNames[i]} = @{columnNames[i]}");

                                if (i < columnNames.Count() - 1)
                                    sb.Append(",");
                            }
                        }


                        for (int i = 0; i < columnNames.Count(); i++)
                        {
                            var recordType = record.Where(r => r.ColumnName == columnNames[i]).FirstOrDefault().DataType;

                            if (recordType == "BOOLEAN")
                            {
                                //If checkbox is unchecked the value is null, and so 0 is the value for False.
                                if (record.Where(r => r.ColumnName == columnNames[i]).FirstOrDefault().ColumnValue == null)
                                    record.Where(r => r.ColumnName == columnNames[i]).FirstOrDefault().ColumnValue = "0";

                                cmd.Parameters.AddWithValue($"@{columnNames[i]}", record.Where(r => r.ColumnName == columnNames[i]).FirstOrDefault().ColumnValue);
                            }
                            else if (recordType == "TEXT")
                            {
                                cmd.Parameters.AddWithValue($"@{columnNames[i]}", record.Where(r => r.ColumnName == columnNames[i]).FirstOrDefault().ColumnValue);
                            }
                            else if (recordType == "INTEGER")
                            {
                                cmd.Parameters.AddWithValue($"@{columnNames[i]}", int.Parse(record.Where(r => r.ColumnName == columnNames[i]).FirstOrDefault().ColumnValue));
                            }




                            //object test = 1;
                            //object test1 = "this is a string";
                            //object test2 = true;

                            //var colObj = record.Where(r => r.ColumnName == columnNames[i]).FirstOrDefault().ColumnValue;

                            //if (colObj is int)
                            //{
                            //    cmd.Parameters.AddWithValue($"@{columnNames[i]}", (int)record.Where(r => r.ColumnName == columnNames[i]).FirstOrDefault().ColumnValue);
                            //}
                            //else if(colObj is String)
                            //{
                            //    cmd.Parameters.AddWithValue($"@{columnNames[i]}", (String)record.Where(r => r.ColumnName == columnNames[i]).FirstOrDefault().ColumnValue);
                            //}
                            //else if(colObj is Boolean)
                            //{
                            //    cmd.Parameters.AddWithValue($"@{columnNames[i]}", (Boolean)record.Where(r => r.ColumnName == columnNames[i]).FirstOrDefault().ColumnValue);
                            //}


                        }

                        //SQL query to update data from table
                        string myQuery = $"UPDATE {tableName} SET {sb.ToString()} WHERE TenantId = @TenantId";

                        cmd.CommandText = myQuery;


                        //cmd.Parameters.AddWithValue("@TenantId", record.Where(r => r.ColumnName == "TenantId").FirstOrDefault().ColumnValueNumber);
                        //cmd.Parameters.AddWithValue("@TenantName", record.Where(r => r.ColumnName == "TenantName").FirstOrDefault().ColumnValueString);
                        //cmd.Parameters.AddWithValue("@AccessKey", record.Where(r => r.ColumnName == "AccessKey").FirstOrDefault().ColumnValueString);
                        //cmd.Parameters.AddWithValue("@Disabled", record.Where(r => r.ColumnName == "Disabled").FirstOrDefault().ColumnValueBool);

                        con.Open();//Open the connection with database

                        cmd.ExecuteNonQuery();//Executes the SQL query
                    }
                }

                int _tenantId = int.Parse(record.Where(r => r.ColumnName == "TenantId").FirstOrDefault().ColumnValue);
                var updatedRecord = GetOrganizationById(connectionString, _tenantId);

                return updatedRecord;
            }
            catch (Exception ex)
            {
                error.HasError = true;
                error.ExceptionMessage = ex.Message;
                error.CustomError = "UpdateOrganization";

                return record;
            }
        }



        public List<string> GetTableColumnNames(string connectionString, string tableName)
        {

            using (SqliteConnection con = new SqliteConnection(connectionString))
            {
                con.Open();

                using (SqliteCommand cmd = con.CreateCommand())
                {
                    //SQL query to update data from table
                    string myQuery = "PRAGMA table_info(" + tableName + ");";

                    cmd.CommandText = myQuery;

                    con.Open();//Open the connection with database

                    var tableNames = new List<string>();
                    using (SqliteDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            tableNames.Add(reader.GetString(1)); // read column name
                        }
                    }
                    return tableNames;
                }
            }
        }

        public List<ColumnPropertiesDTO> GetTableColumnProperties(string connectionString, string tableName)
        {
            using (SqliteConnection con = new SqliteConnection(connectionString))
            {


                using (SqliteCommand cmd = con.CreateCommand())
                {
                    //SQL query to update data from table
                    string myQuery = "PRAGMA table_info(" + tableName + ");";

                    cmd.CommandText = myQuery;

                    con.Open();//Open the connection with database

                    var columnsProperties = new List<ColumnPropertiesDTO>();

                    using (SqliteDataReader reader = cmd.ExecuteReader())
                    {
                        //foreach (DbDataRecord item in reader.Cast<DbDataRecord>())
                        //{
                        //    for (int i = 0; i < item.FieldCount; i++)
                        //    {
                        //        columnsProperties.Add(new ColumnPropertiesDTO()
                        //        {
                        //            FieldName = item.GetName(i),
                        //            FieldDataType = item.GetDataTypeName(i).Trim(),
                        //            FieldType = item.GetFieldType(i).Name,
                        //            FieldValue = item[i].ToString()

                        //        });
                        //    }
                        //}


                        while (reader.Read())
                        {
                            columnsProperties.Add(new ColumnPropertiesDTO()
                            {
                                FieldValue = "",
                                FieldName = reader.GetString(1), //get column name
                                FieldDataType = reader.GetString(2), //ordinal 2 - column data type (i.e. INTEGER; BOOLEAN; TEXT),,
                                FieldType = "",
                                IsPrimaryKey = reader.GetString(5) == "1" ? true : false, //ordinal 5 indicates if is PK
                                Required = reader.GetString(3) == "1" ? true : false //ordinal 3 indicates if field is not null
                            });
                        }
                    }
                    return columnsProperties;
                }
            }
        }



        public List<SQLiteRecordDTO> CreateOrganization(string connectionString, List<SQLiteRecordDTO> record)
        {
            ErrorClass error = new ErrorClass();

            string tenantGUID = Guid.NewGuid().ToString();

            try
            {
                //Generating unique id for this organization


                var tableName = "OrganizationSettings";

                var columnNames = GetTableColumnNames(connectionString, "OrganizationSettings");

                using (SqliteConnection con = new SqliteConnection(connectionString))
                {


                    using (SqliteCommand cmd = con.CreateCommand())
                    {
                        StringBuilder sbColumns = new StringBuilder();

                        for (int i = 0; i < columnNames.Count(); i++)
                        {
                            if (columnNames[i] != "TenantId")
                            {
                                sbColumns.Append($"{columnNames[i]}");

                                if (i < columnNames.Count() - 1)
                                    sbColumns.Append(",");
                            }
                        }



                        StringBuilder sbColumnsValues = new StringBuilder();

                        for (int i = 0; i < columnNames.Count(); i++)
                        {
                            if (columnNames[i] != "TenantId")
                            {
                                sbColumnsValues.Append($"@{columnNames[i]}");

                                if (i < columnNames.Count() - 1)
                                    sbColumnsValues.Append(",");
                            }

                        }



                        for (int i = 0; i < columnNames.Count(); i++)
                        {
                            var recordType = record.Where(r => r.ColumnName == columnNames[i]).FirstOrDefault().DataType;

                            if (recordType == "BOOLEAN")
                            {
                                //If checkbox is unchecked the value is null, and so 0 is the value for False.
                                if (record.Where(r => r.ColumnName == columnNames[i]).FirstOrDefault().ColumnValue == null)
                                    record.Where(r => r.ColumnName == columnNames[i]).FirstOrDefault().ColumnValue = "0";

                                cmd.Parameters.AddWithValue($"@{columnNames[i]}", record.Where(r => r.ColumnName == columnNames[i]).FirstOrDefault().ColumnValue);
                            }
                            else if (recordType == "TEXT" || recordType.Contains("TEXT"))
                            {
                                if (columnNames[i] == "TenantGUID")
                                {
                                    cmd.Parameters.AddWithValue($"@{columnNames[i]}", tenantGUID);
                                }
                                else
                                {
                                    cmd.Parameters.AddWithValue($"@{columnNames[i]}", record.Where(r => r.ColumnName == columnNames[i]).FirstOrDefault().ColumnValue);
                                }
                            }
                            else if (recordType == "INTEGER")
                            {
                                if (columnNames[i] != "TenantId")
                                    cmd.Parameters.AddWithValue($"@{columnNames[i]}", int.Parse(record.Where(r => r.ColumnName == columnNames[i]).FirstOrDefault().ColumnValue));
                            }

                        }

                        //SQL query to update data from table
                        string myQuery = $"INSERT INTO {tableName} ({sbColumns.ToString()}) VALUES ({sbColumnsValues})";

                        cmd.CommandText = myQuery;


                        //cmd.Parameters.AddWithValue("@TenantId", record.Where(r => r.ColumnName == "TenantId").FirstOrDefault().ColumnValueNumber);
                        //cmd.Parameters.AddWithValue("@TenantName", record.Where(r => r.ColumnName == "TenantName").FirstOrDefault().ColumnValueString);
                        //cmd.Parameters.AddWithValue("@AccessKey", record.Where(r => r.ColumnName == "AccessKey").FirstOrDefault().ColumnValueString);
                        //cmd.Parameters.AddWithValue("@Disabled", record.Where(r => r.ColumnName == "Disabled").FirstOrDefault().ColumnValueBool);


                        con.Open();//Open the connection with database

                        cmd.ExecuteNonQuery();//Executes the SQL query
                    }
                }

                //int _tenantId = int.Parse(record.Where(r => r.ColumnName == "TenantId").FirstOrDefault().ColumnValue);



                var _tenantId = GetAllOrganizations(connectionString).FirstOrDefault(o => o["TenantGUID"].ToString() == tenantGUID)["TenantId"];


                //C#7 feature :) no need to decalre resTenantId!
                int.TryParse(_tenantId.ToString(), out int resTenantId);

                var createdRecord = GetOrganizationById(connectionString, resTenantId);

                var tenantConString = createdRecord.Where(o => o.ColumnName == "TenantConnectionString").FirstOrDefault().ColumnValue;

                CreateTenantDatabase(tenantConString);

                return createdRecord;
            }
            catch (Exception ex)
            {
                error.HasError = true;
                error.ExceptionMessage = ex.Message;
                error.CustomError = "UpdateOrganization";

                return record;
            }
        }

        public bool CreateTenantDatabase(string tenantConnectionString)
        {

            bool success = false;

            try
            {
                string contentRootPath = _hostingEnvironment.ContentRootPath;

                //var TenantDbName = createdOrganization.Where(r => r.ColumnName == "TenantDatabase").FirstOrDefault().ColumnValue;

                var splitDbName = tenantConnectionString.Split(new string[] { @"\" }, StringSplitOptions.RemoveEmptyEntries);
                var TenantDbName = splitDbName[tenantConnectionString.Split(new string[] { @"\" }, StringSplitOptions.RemoveEmptyEntries).Count() - 1];


                var tenantDatabasePath = Path.Combine(contentRootPath, $"{TenantDbName}");

                string strConnection = $"Filename={tenantDatabasePath}";

                if (!System.IO.File.Exists(tenantDatabasePath))
                {
                    //string strConnection = @"Data Source=C:\Users\luis.coelho\Documents\visual studio 2017\Projects\SQLiteLabProject\SQLiteLabProject\main.db";

                    string strCommand = System.IO.File.ReadAllText(@"C:\Users\luis.coelho\Documents\visual studio 2017\Projects\SQLiteLabProject\Database\Tenant.sql"); // .sql file path

                    using (SqliteConnection objConnection = new SqliteConnection(strConnection))
                    {
                        using (SqliteCommand objCommand = objConnection.CreateCommand())
                        {
                            objConnection.Open();
                            objCommand.CommandText = strCommand;

                            objCommand.ExecuteNonQuery();
                            objConnection.Close();
                        }
                    }
                }

                success = true;

            }
            catch (Exception)
            {

            }

            return success;

        }


        public string GetSettingsTableNameForTaskTypeId(string connectionString, long taskTypeId)
        {
            ErrorClass error = new ErrorClass();

            string settingsTable = string.Empty;


            try
            {
                using (SqliteConnection con = new SqliteConnection(connectionString))
                {
                    con.Open();

                    using (SqliteCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = @"select * from TaskTypes where TaskTypeId=@TaskTypeId";
                        cmd.Parameters.AddWithValue("@TaskTypeId", taskTypeId);
                        cmd.CommandType = System.Data.CommandType.Text;

                        using (SqliteDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                settingsTable = reader["SettingsTable"].ToString();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                error.HasError = true;
                error.ExceptionMessage = ex.Message;
                error.CustomError = "GetOrganizationById";
            }

            return settingsTable;
        }


        public List<SQLiteRecordDTO> GetAllTaskTypes(string connectionString)
        {
            ErrorClass error = new ErrorClass();

            List<SQLiteRecordDTO> record = new List<SQLiteRecordDTO>();

            try
            {
                using (SqliteConnection con = new SqliteConnection(connectionString))
                {
                    con.Open();

                    using (SqliteCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = @"select * from TaskTypes";


                        using (SqliteDataReader reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                var recordDTO = new SQLiteRecordDTO();
                                recordDTO.Id = reader["TaskTypeId"].ToString();
                                recordDTO.ColumnValue = reader["Description"].ToString();
                                record.Add(recordDTO);

                                //foreach (DbDataRecord s in reader.Cast<DbDataRecord>())
                                //{

                                //    recordDTO.ColumnName = s.GetName(0);
                                //    recordDTO.ColumnValue = s[0].ToString();
                                //    record.Add(recordDTO);
                                //    break;
                                //}

                            }

                            //foreach (DbDataRecord s in reader.Cast<DbDataRecord>())
                            //{
                            //    var val = s;




                            //    for (int i = 0; i < s.FieldCount; i++)
                            //    {
                            //        var recordDTO = new SQLiteRecordDTO();
                            //        recordDTO.ColumnName = s.GetName(i);

                            //        var dataType = s.GetDataTypeName(i).Trim();
                            //        var fieldType = s.GetFieldType(i).Name;

                            //        recordDTO.FieldType = fieldType;
                            //        recordDTO.DataType = dataType;
                            //        recordDTO.ColumnValue = s[i].ToString();

                            //        record.Add(recordDTO);

                            //    }

                            //}

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                error.HasError = true;
                error.ExceptionMessage = ex.Message;
                error.CustomError = "GetOrganizationById";
            }

            return record;
        }

        public Dictionary<string, List<TaskTypeSettingsDTO>> GetTaskTypeSettingsByTaskTypeId(string connectionString, long taskTypeId)
        {
            ErrorClass error = new ErrorClass();


            Dictionary<string, List<TaskTypeSettingsDTO>> taskTypeSettings = new Dictionary<string, List<TaskTypeSettingsDTO>>();

            try
            {
                using (SqliteConnection con = new SqliteConnection(connectionString))
                {
                    con.Open();

                    using (SqliteCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = @"select * from TaskTypeSettings where TaskTypeId=@TaskTypeId";



                        cmd.Parameters.AddWithValue("@TaskTypeId", taskTypeId);
                        cmd.CommandType = System.Data.CommandType.Text;

                        using (SqliteDataReader reader = cmd.ExecuteReader())
                        {

                            foreach (DbDataRecord s in reader.Cast<DbDataRecord>())
                            {
                                string settingName = reader["SettingName"].ToString();


                                List<TaskTypeSettingsDTO> fieldProps = new List<TaskTypeSettingsDTO>();

                                var val = s;

                                for (int i = 0; i < s.FieldCount; i++)
                                {
                                    var recordDTO = new TaskTypeSettingsDTO();

                                    recordDTO.IsCore = reader["IsCore"].ToString() == "1" ? true : false;

                                    recordDTO.MaxLength = int.TryParse(reader["MaxLength"].ToString(), out int _max) ? _max : 250;
                                    recordDTO.MinLength = int.TryParse(reader["MinLength"].ToString(), out int _min) ? _min : 0;
                                    recordDTO.TaskTypeId = int.TryParse(reader["TaskTypeId"].ToString(), out int _taskTypeId) ? _taskTypeId : -1;
                                    recordDTO.Type = int.TryParse(reader["Type"].ToString(), out int _type) ? _type : -1;

                                    //recordDTO.ColumnName = s.GetName(i);

                                    //var dataType = s.GetDataTypeName(i).Trim();
                                    //var fieldType = s.GetFieldType(i).Name;

                                    //recordDTO.FieldType = fieldType;
                                    //recordDTO.DataType = dataType;
                                    //recordDTO.ColumnValue = s[i].ToString();

                                    fieldProps.Add(recordDTO);
                                }

                                taskTypeSettings.Add(settingName, fieldProps);
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                error.HasError = true;
                error.ExceptionMessage = ex.Message;
                error.CustomError = "GetTaskTypeSettingsByTaskTypeId";
            }



            return taskTypeSettings;
        }
    }
}
