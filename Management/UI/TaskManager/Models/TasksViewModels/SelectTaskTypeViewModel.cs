﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace TaskManager.Models.TasksViewModels
{
    public class SelectTaskTypeViewModel
    {
        [Display(Name = "Task Type")]
        public int TaskTypeId { get; set; }

        public List<SelectListItem> TaskTypesList { get; set; }
    }
}
