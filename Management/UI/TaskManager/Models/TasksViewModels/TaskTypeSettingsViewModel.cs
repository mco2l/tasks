﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskManager.Models.TasksViewModels
{
    public class TaskTypeSettingsViewModel
    {
        public long TaskTypeId { get; set; }
        public string SettingName { get; set; }
        public long Type { get; set; }
        public int MinLength { get; set; }
        public int MaxLength { get; set; }
        public bool IsCore { get; set; }
    }
}
