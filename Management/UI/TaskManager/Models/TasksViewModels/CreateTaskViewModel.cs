﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using TaskManager.DTO;

namespace TaskManager.Models.TasksViewModels
{
    public class CreateTaskViewModel
    {
        public long TaskTypeId { get; set; }

        public SettingsTaskViewModel SettingsTask { get; set; }

        public List<TaskTypeSettingsViewModel> TaskTypeSettings { get; set; }


        /// <summary>
        /// Common fields to all task types
        /// </summary>
        public List<ColumnPropertiesDTO> CommonTaskFormFields { get; set; }


        /// <summary>
        /// Specific fields for settings table
        /// </summary>
        public List<ColumnPropertiesDTO> SettingsTableFormFields { get; set; }
    }
}
