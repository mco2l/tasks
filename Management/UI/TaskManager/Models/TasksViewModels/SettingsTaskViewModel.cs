﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskManager.Models.TasksViewModels
{
    public class SettingsTaskViewModel
    {
        public long Id { get; set; }
        public string ColumnName { get; set; }
        public string ColumnValue { get; set; }

    }
}
