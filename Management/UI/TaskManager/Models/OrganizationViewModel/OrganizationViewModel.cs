﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;

namespace TaskManager.Models.OrganizationViewModel
{
    public class OrganizationViewModel
    {
        public List<DbDataRecord> OrganizationsViewModel { get; set; }
    }
}
