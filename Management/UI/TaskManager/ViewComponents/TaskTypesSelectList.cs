﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using TaskManager.DTO;
using TaskManager.Models.TasksViewModels;
using TaskManager.Services.Interfaces;

namespace TaskManager.ViewComponents
{
    [ViewComponent(Name = "TaskTypesSelectList")]
    public class TaskTypesSelectList : ViewComponent
    {
        private ITenantDatabaseService _tenantDatabaseService;
        SelectTaskTypeViewModel vm;

        public TaskTypesSelectList(ITenantDatabaseService tenantDatabaseService)
        {
            _tenantDatabaseService = tenantDatabaseService;




        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            vm = new SelectTaskTypeViewModel();

            vm.TaskTypesList = new List<Microsoft.AspNetCore.Mvc.Rendering.SelectListItem>();

            List<TaskTypesDTO> dto = _tenantDatabaseService.GetTaskTypes(Startup.MainConnectionString);

            foreach (var task in dto)
            {
                //vm.TaskTypesList.Add(new SelectListItem() { Text = task.Description, Value = task.TaskTypeId.ToString() });
                vm.TaskTypesList.Add(new SelectListItem() { Text = task.ColumnValue, Value = task.TaskTypeId.ToString() });
            }

            return View(vm);
        }
    }
}
