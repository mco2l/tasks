﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace TaskManager.TagHelpers
{
    public class DynamicFormFieldTagHelper : TagHelper
    {

        /*TAG HELPER ATTRIBUTES*/
        public int ColumnIndex { get; set; }

        public string ColumnName { get; set; }
        public string ColumnValue { get; set; }

        public string FieldType { get; set; }

        public string FieldDataType { get; set; }

        public bool IsEditable { get; set; }

        public bool IsSettingsTableField { get; set; }

        public bool IsCommonTaskField { get; set; }

        public bool IsRequired { get; set; }

        public DynamicFormFieldTagHelper()
        {

        }


        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            StringBuilder sb = new StringBuilder();

            
            string _id = string.Empty;
            string _name = string.Empty;

            //build id an name for input fields. Since the default aspnet binding model is not used, we need to build the correct ids and names

            if(IsSettingsTableField)
            {
                //id = "SettingsTableFormFields_0__FieldValue" name = "SettingsTableFormFields[0].FieldValue" --> if used asp-for tag helper

                _id = $"SettingsTableFormFields_{ColumnIndex}__FieldValue";
                _name = $"SettingsTableFormFields[{ColumnIndex}].FieldValue";

            }
            else if (IsCommonTaskField)
            {
                //id = "SettingsTableFormFields_0__FieldValue" name = "SettingsTableFormFields[0].FieldValue"

                _id = $"CommonTaskFormFields_{ColumnIndex}__FieldValue";
                _name = $"CommonTaskFormFields[{ColumnIndex}].FieldValue";
            }
            else
            {
                _id = $"z{ColumnIndex}__ColumnValue";
                _name = $"[{ColumnIndex}].ColumnValue";
            }


            if (FieldDataType == "BOOLEAN")
            {
                int boolParsing;
                var result = int.TryParse(ColumnValue, out boolParsing);

                if (result && boolParsing > 0)
                {
                    sb.AppendFormat("<input id='{0}' name='{1}' value='{2}' class='form-control' type='checkbox' checked='checked' {3} {4} />",
                        _id,
                        _name,
                        (ColumnValue != null ? ColumnValue : ""),
                        IsEditable == false ? "readonly disabled" : "",
                        IsRequired == true ? "required = required" : "");

                }
                else
                {
                    //string sb1 = string.Empty;

                    //if (IsEditable)
                    //    sb1 = $@"<input id=""{ _id }"" name=""{_name}"" value=""{ColumnValue}"" class=""form-control"" type=""checkbox"" onclick=""handleCheckboxClick('{_id}')""  />";
                    //else
                    //    sb1 = $@"<input id=""{ _id }"" name=""{_name}"" value=""{ColumnValue}"" class=""form-control"" type=""checkbox"" onclick=""handleCheckboxClick('{_id}')""  disabled/>";

                    //sb.Append(sb1);


                    sb.AppendFormat("<input id='{0}' name='{1}' value='{2}' class='form-control' type='checkbox' onclick='handleCheckboxClick('{3}')' {4} {5}",
                        _id,
                        _name,
                        ColumnValue,
                        _id,
                        IsRequired == true ? "required = 'required'" : "",
                        IsEditable == false ? "readonly='readonly' disabled='disabled'" : "");


                 
                }

            }
            else if (FieldDataType == "INTEGER")
            {

                sb.AppendFormat("<input id='{0}' name='{1}' value='{2}' class='form-control' type='number' {3} {4}/>",
                    _id,
                    _name,
                    ColumnValue != null ? ColumnValue : "",
                    IsEditable == false ? "readonly" : "",
                    IsRequired == true ? "required = required" : "");
            }
            else if (FieldDataType == "TEXT" || FieldDataType.Contains("TEXT"))
            {
                sb.AppendFormat("<input id='{0}' name='{1}' value='{2}' class='form-control' type='text' {3} {4}/>", 
                    _id,
                    _name,
                    ColumnValue != null ? ColumnValue : "",
                    IsEditable == false ? "readonly" : "",
                    IsRequired == true ? "required = required" : "");
            }



            output.Content.SetHtmlContent(sb.ToString());

            output.TagMode = TagMode.StartTagAndEndTag;

        }
    }
}
