﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using TaskManager.Data;
using TaskManager.Middleware;
using TaskManager.Repositories;
using TaskManager.Repositories.Interfaces;
using TaskManager.Services;
using TaskManager.Services.Interfaces;

namespace TaskManager
{
    public class Startup
    {
        public static string MainConnectionString { get; private set; }

        /// <summary>
        /// Needs to be concatenated with the specific tenant database name.
        /// </summary>
        public static string TenantPartialConnectionString { get; private set; }

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            if (env.IsDevelopment())
            {
                // For more details on using the user secret store see https://go.microsoft.com/fwlink/?LinkID=532709
                builder.AddUserSecrets<Startup>();
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();


            MainConnectionString = Configuration.GetConnectionString("DefaultConnection");

            TenantPartialConnectionString = Configuration.GetConnectionString("TenantsConnection");
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();

            // Add framework services.
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlite(MainConnectionString));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddMvc();

            // Add application services.
            services.AddTransient<IEmailSender, AuthMessageSender>();
            services.AddTransient<ISmsSender, AuthMessageSender>();

            services.AddTransient<IMainDatabaseService, MainDatabaseService>();

            services.AddTransient<IMainDatabaseRepository, MainDatabaseRepository>();

            services.AddTransient<ITenantDatabaseRepository, TenantDatabaseRepository>();

            services.AddTransient<ITenantDatabaseService, TenantDatabaseService>();

            services.AddTransient<IRolesService, RolesService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IMainDatabaseService mainDatabaseService, IRolesService rolesService)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                app.UseBrowserLink();

                
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseIdentity();

            // Add external authentication middleware below. To configure them please see https://go.microsoft.com/fwlink/?LinkID=532715
            app.UseTenantMiddleware(Startup.MainConnectionString, Startup.TenantPartialConnectionString);

            mainDatabaseService.EnsureCreateDatabase(MainConnectionString, env.ContentRootPath);



            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });


            rolesService.InitializeAsync();
        }
    }
}
