﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using TaskManager.DTO;
using TaskManager.Middleware;
using TaskManager.Models.TasksViewModels;
using TaskManager.Services.Interfaces;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TaskManager.Controllers
{
    public class TasksController : Controller
    {
        private IMainDatabaseService _mainDatabaseService;
        private IOptions<TenantMiddlewareOptions> _options;
        private ITenantDatabaseService _tenantDatabaseService;

        public TasksController(IMainDatabaseService mainDatabaseService, ITenantDatabaseService tenantDatabaseService, IOptions<TenantMiddlewareOptions> options)
        {
            _mainDatabaseService = mainDatabaseService;
            _tenantDatabaseService = tenantDatabaseService;
            _options = options;
        }



        #region VIEWS

        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">TaskId</param>
        /// <returns></returns>
        public IActionResult Display(long id)
        {
            var taskProps = _tenantDatabaseService.GetSettingsTaskByTaskId(_options.Value.TenantConnectionString, id);

            CreateTaskViewModel taskToDisplay = new CreateTaskViewModel()
            {
                CommonTaskFormFields = new List<ColumnPropertiesDTO>(),
                SettingsTableFormFields = new List<ColumnPropertiesDTO>()
            };

            foreach (var item in taskProps)
            {
                taskToDisplay.CommonTaskFormFields.Add(item);
            }


            taskToDisplay.TaskTypeId = long.Parse(taskToDisplay.CommonTaskFormFields.Where(f => f.FieldName == "TaskTypeId").SingleOrDefault().FieldValue);

            var settingsTable = _mainDatabaseService.GetSettingsTableNameForTaskTypeId(_options.Value.MainConnectionString, taskToDisplay.TaskTypeId);

            taskToDisplay.SettingsTableFormFields = _tenantDatabaseService.GetSpecificSettingsForTaskByTaskId(_options.Value.TenantConnectionString, settingsTable, id);

            return View(taskToDisplay);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">TaskId</param>
        /// <returns></returns>
        public IActionResult Edit(long id)
        {
            var commonTaskProps = _tenantDatabaseService.GetSettingsTaskByTaskId(_options.Value.TenantConnectionString, id);

          var _taskTypeId = long.Parse(commonTaskProps.Where(f => f.FieldName == "TaskTypeId").SingleOrDefault().FieldValue);

            var settingsTable = _mainDatabaseService.GetSettingsTableNameForTaskTypeId(_options.Value.MainConnectionString, _taskTypeId);

            var specificTaskProps = _tenantDatabaseService.GetSpecificSettingsForTaskByTaskId(_options.Value.TenantConnectionString, settingsTable, id);
            
            CreateTaskViewModel taskToDisplay = new CreateTaskViewModel()
            {
                CommonTaskFormFields = commonTaskProps,
                SettingsTableFormFields = specificTaskProps,
                TaskTypeId = _taskTypeId
            };
          
            return View(taskToDisplay);
        }

        public IActionResult List()
        {
            CreateTaskViewModel vm = new CreateTaskViewModel();

            return View(vm);
        }


        public IActionResult New()
        {
            CreateTaskViewModel vm = new CreateTaskViewModel();

            return View(vm);
        }

        #endregion



        #region CRUD OPERATIONS
        public IActionResult CreateTask(CreateTaskViewModel vm)
        {
            if(ModelState.IsValid)
            {
                CreateTaskDTO createTaskDTO = new CreateTaskDTO()
                {
                    TaskTypeId = vm.TaskTypeId,
                    CommonTaskFields = vm.CommonTaskFormFields,
                    SettingsTaskFields = vm.SettingsTableFormFields
                };

                var createdTaskResult = _tenantDatabaseService.CreateTask(_options.Value.TenantConnectionString, createTaskDTO);

                return RedirectToAction("Display", new { taskId = createdTaskResult.CommonTaskFields[0].FieldValue });
            }

            return View("New", vm);
        }


        #endregion



        public IActionResult SelectTaskType(long taskTypeId)
        {
            CreateTaskViewModel vm = new CreateTaskViewModel()
            {
                TaskTypeId = taskTypeId,
                TaskTypeSettings = new List<TaskTypeSettingsViewModel>(),
                SettingsTask = new SettingsTaskViewModel(),
                CommonTaskFormFields = new List<ColumnPropertiesDTO>()
            };


            if(taskTypeId>0)
            {
                /*Get common tasks table*/
                vm.CommonTaskFormFields = _mainDatabaseService.GetTableColumnProperties(_options.Value.TenantConnectionString, "Settings_Task");

                /*Get specific tasks table*/
                var _taskTypeSettings = _mainDatabaseService.GetTaskTypeSettingsByTaskTypeId(Startup.MainConnectionString, taskTypeId);

                foreach (var kvp in _taskTypeSettings)
                {
                    var ttsModel = new TaskTypeSettingsViewModel();

                    foreach (var value in kvp.Value)
                    {
                        vm.TaskTypeSettings.Add(new TaskTypeSettingsViewModel()
                        {
                            IsCore = value.IsCore,
                            MaxLength = value.MaxLength,
                            MinLength = value.MinLength,
                            SettingName = kvp.Key.ToString(),
                            TaskTypeId = value.TaskTypeId,
                            Type = value.Type
                        });
                    }
                }
            }

            return View("New", vm);
        }
    }
}
