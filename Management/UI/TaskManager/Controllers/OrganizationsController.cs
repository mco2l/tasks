using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using TaskManager.Data;
using TaskManager.DTO;
using TaskManager.Middleware;
using TaskManager.Services.Interfaces;

namespace TaskManager.Controllers
{
    public class OrganizationsController : Controller
    {
        private IMainDatabaseService _mainDatabaseService;
        private IOptions<TenantMiddlewareOptions> _options;

        public OrganizationsController(IMainDatabaseService mainDatabaseService, IOptions<TenantMiddlewareOptions> options)
        {
            _mainDatabaseService = mainDatabaseService;
            _options = options;



        }


        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Display(int id)
        {
            var record = _mainDatabaseService.GetOrganizationById(Startup.MainConnectionString, id);

            return View(record);
        }

        [Authorize(Roles = "SuperUser, Administrator")]
        public IActionResult Edit(string id)
        {
            int _id;

            var record = new List<SQLiteRecordDTO>();

            var parseResult = int.TryParse(id, out _id);

            if (parseResult)
            {
                record = _mainDatabaseService.GetOrganizationById(Startup.MainConnectionString, _id);
            }

            return View(record);
        }

        public IActionResult List()
        {
            List<DbDataRecord> table = new List<DbDataRecord>();


            table = _mainDatabaseService.GetAllOrganizations(Startup.MainConnectionString);

            if (_options.Value.TenantId >0)
            {
               foreach(var record in table)
                {
                    for(int i=0;i<record.FieldCount;i++)
                    {
                        int.TryParse(record["TenantId"].ToString(), out int res);

                        var _tenantId = res;
                        if(_tenantId == _options.Value.TenantId)
                        {
                            var singleOrganization = table.Where(o => o == record).FirstOrDefault() ;

                            table.Clear();
                            table.Add(singleOrganization);

                            return View(table);
                        }
                    }
                }
            }
          


            return View(table);
        }


        [Authorize(Roles = "SuperUser, Administrator")]
        public IActionResult New()
        {
            List<string> columnNames = _mainDatabaseService.GetTableColumnNames(Startup.MainConnectionString, "OrganizationSettings");

            List<ColumnPropertiesDTO> props = _mainDatabaseService.GetTableColumnProperties(Startup.MainConnectionString, "OrganizationSettings");

            var record = new List<SQLiteRecordDTO>();

            for (int i = 0; i < props.Count(); i++)
            {
                record.Add(new SQLiteRecordDTO()
                {
                    ColumnName = props[i].FieldName,
                    ColumnValue = "",
                    DataType = props[i].FieldDataType,
                    FieldType = props[i].FieldType,
                    IsEditable = !props[i].IsPrimaryKey

                });
            }

            return View(record);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "SuperUser, Administrator")]
        public IActionResult UpdateOrganization(List<SQLiteRecordDTO> record)
        {
            List<SQLiteRecordDTO> upd = _mainDatabaseService.UpdateOrganization(Startup.MainConnectionString, record);

            int _tenantId = int.Parse(upd.Where(r => r.ColumnName == "TenantId").FirstOrDefault().ColumnValue);

            return RedirectToAction("Display", new { id = _tenantId });
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "SuperUser, Administrator")]
        public IActionResult CreateOrganization(List<SQLiteRecordDTO> record)
        {
            try
            {
                List<SQLiteRecordDTO> createdOrganization = _mainDatabaseService.CreateOrganization(Startup.MainConnectionString, record);


                int _tenantId;


                var res = int.TryParse(createdOrganization.Where(r => r.ColumnName == "TenantId").FirstOrDefault().ColumnValue, out _tenantId);


                return RedirectToAction("Display", new { id = _tenantId });
            }
            catch (Exception ex)
            {

                return View("New");
            }

        }

    }
}