﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using TaskManager.DTO;
using TaskManager.ErrorHandling;
using TaskManager.Repositories.Interfaces;
using TaskManager.Services.Interfaces;


namespace TaskManager.Services
{
    public class MainDatabaseService : IMainDatabaseService
    {
        private IMainDatabaseRepository _repository;

        public MainDatabaseService(IMainDatabaseRepository repository)
        {
            _repository = repository;
        }

        public ErrorClass EnsureCreateDatabase(string connectionString, string dataBaseScriptPath)
        {
            ErrorClass error = new ErrorClass();

            try
            {
                _repository.EnsureDatabaseCreated(connectionString, dataBaseScriptPath);
            }
            catch (Exception ex)
            {
                error.HasError = true;
                error.ExceptionMessage = ex.Message;
                error.CustomError = "EnsureCreatedDatabase";
            }

            return error;
        }

        public List<DbDataRecord> GetAllOrganizations(string connectionString)
        {
            return _repository.GetAllOrganizations(connectionString);
        }

        public List<SQLiteRecordDTO> GetOrganizationById(string connectionString, long id)
        {
            return _repository.GetOrganizationById(connectionString, id);
        }

        public List<SQLiteRecordDTO> UpdateOrganization(string connectionString, List<SQLiteRecordDTO> record)
        {
            return _repository.UpdateOrganization(connectionString, record);
        }


        public List<string> GetTableColumnNames(string connectionString, string tableName)
        {
            return _repository.GetTableColumnNames(connectionString, tableName);
        }

        public List<ColumnPropertiesDTO> GetTableColumnProperties(string connectionString, string tableName)
        {
            return _repository.GetTableColumnProperties(connectionString, tableName);
        }

        public List<SQLiteRecordDTO> CreateOrganization(string mainConnectionString, List<SQLiteRecordDTO> record)
        {
            return _repository.CreateOrganization(mainConnectionString, record);
        }


        public bool CreateTenantDatabase(string tenantConnectionString)
        {
            return _repository.CreateTenantDatabase(tenantConnectionString);
        }

        public Dictionary<string, List<TaskTypeSettingsDTO>> GetTaskTypeSettingsByTaskTypeId(string connectionString, long taskTypeId)
        {
            return _repository.GetTaskTypeSettingsByTaskTypeId(connectionString, taskTypeId);
        }

        public string GetSettingsTableNameForTaskTypeId(string connectionString, long taskTypeId)
        {
            return _repository.GetSettingsTableNameForTaskTypeId(connectionString, taskTypeId);
        }
    }
}
