﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Options;
using TaskManager.DTO;
using TaskManager.ErrorHandling;
using TaskManager.Repositories.Interfaces;
using TaskManager.Services.Interfaces;

namespace TaskManager.Services
{
    public class TenantDatabaseService : ITenantDatabaseService
    {
        private ITenantDatabaseRepository _tenantDatabaseRepository;
        private IMainDatabaseRepository _mainDatabaseRpository;

        public TenantDatabaseService(ITenantDatabaseRepository tenantDatabaseRepository, IMainDatabaseRepository mainDatabaseRpository)
        {
            _tenantDatabaseRepository = tenantDatabaseRepository;
            _mainDatabaseRpository = mainDatabaseRpository;
        }

        public List<TaskTypesDTO> GetTaskTypes(string connectionString)
        {
            var taskTypes = new List<TaskTypesDTO>();

            //TODO: Get available task types
            List<SQLiteRecordDTO> records = _mainDatabaseRpository.GetAllTaskTypes(connectionString);

            foreach(var record in records)
            {
                taskTypes.Add(new TaskTypesDTO()
                {
                    TaskTypeId = long.TryParse(record.Id, out long taskTypeId) ? taskTypeId : -1,
                    ColumnValue = record.ColumnValue
                });
            }

            return taskTypes;
        }

        public TaskTypeSettingsDTO GetTaskTypeSettingsByTaskTypeId(long taskTypeId)
        {
            ErrorClass error = new ErrorClass();


            TaskTypeSettingsDTO taskTypeSettings = new TaskTypeSettingsDTO();

            try
            {
                taskTypeSettings = _tenantDatabaseRepository.GetTaskTypeSettingsByTaskTypeId(taskTypeId);
            }
            catch (Exception ex)
            {

                error.HasError = true;

                error.ExceptionMessage = ex.Message;

                error.CustomError = "GetTaskTypeSettingsByTaskTypeId";
            }

            return taskTypeSettings;
        }

        public CreateTaskDTO CreateTask(string tenantConnectionString, CreateTaskDTO createTaskDTO)
        {

            ErrorClass error = new ErrorClass();

            var _createdTaskDTO = new CreateTaskDTO();

            try
            {
                _createdTaskDTO = _tenantDatabaseRepository.CreateTask(tenantConnectionString, createTaskDTO);
            }
            catch (Exception ex)
            {

                error.HasError = true;

                error.ExceptionMessage = ex.Message;

                error.CustomError = "GetTaskTypeSettingsByTaskTypeId";
            }

            return _createdTaskDTO;
        }

        public List<ColumnPropertiesDTO> GetSettingsTaskByTaskId(string tenantConnectionString, long taskId)
        {
            ErrorClass error = new ErrorClass();


            try
            {
                return _tenantDatabaseRepository.GetSettingsTaskByTaskId(tenantConnectionString, taskId);
            }
            catch (Exception ex)
            {

                error.HasError = true;

                error.ExceptionMessage = ex.Message;

                error.CustomError = "GetSettingsTaskByTaskId";
            }

            return new List<ColumnPropertiesDTO>();
        }

        public List<ColumnPropertiesDTO> GetSpecificSettingsForTaskByTaskId(string tenantConnectionString, string settingsTable, long taskId)
        {
            return _tenantDatabaseRepository.GetSpecificSettingsForTaskByTaskId(tenantConnectionString, settingsTable, taskId);
        }
    }
}
