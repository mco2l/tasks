﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskManager.DTO
{
    public class TaskTypesDTO
    {
        public long TaskTypeId { get; set; }
        public string ColumnName { get; set; }
        public string ColumnValue { get; set; }

    }
}
