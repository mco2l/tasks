﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskManager.DTO
{
    public class SQLiteRecordDTO
    {
        public string Id { get; set; }
        public string ColumnName { get; set; }

        public string ColumnValue { get; set; }

        public string DataType { get; set; }

        public string FieldType { get; set; }

        public bool IsEditable { get; set; }



        //public string ColumnValueString { get; set; }
        //public bool ColumnValueBool { get; set; }

        //public int ColumnValueNumber { get; set; }

    }
}
