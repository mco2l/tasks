﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskManager.DTO
{
    public class ColumnPropertiesDTO
    {
        public string FieldName { get; set; }
        public string FieldDataType { get; set; }
        public string FieldType { get; set; }
        public bool IsPrimaryKey { get; set; } = false;
        public bool Required { get; set; } = true;

        public string  FieldValue { get; set; }
    }
}
