﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskManager.DTO
{
    public class CreateTaskDTO
    {
        public long TaskTypeId { get; set; }

        public List<ColumnPropertiesDTO> CommonTaskFields { get; set; }
        public List<ColumnPropertiesDTO> SettingsTaskFields { get; set; }
    }
}
