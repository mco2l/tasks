﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using TaskSchedulerManager.Repositories.Interfaces;

namespace TaskSchedulerManager.Repositories
{
    public abstract class GenericRepository<C, T> : IGenericRepository<T> where T : class where C : DbContext, new()
    {

        private C _entities = new C();
        public C Context
        {

            get { return _entities; }
            set { _entities = value; }
        }

        public virtual IEnumerable<T> GetAll()
        {
            LoadNavigationProperties();

            IEnumerable<T> query = _entities.Set<T>().ToList();
            return query;
        }

        public IEnumerable<T> FindBy(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {

            IEnumerable<T> query = _entities.Set<T>().Where(predicate);
            return query;
        }

        public virtual void Add(T entity)
        {
            _entities.Set<T>().Add(entity);
        }

        public virtual void Delete(T entity)
        {
            _entities.Set<T>().Remove(entity);
        }

        public virtual void Edit(T entity)
        {
            _entities.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Save()
        {
            _entities.SaveChanges();
        }



        #region aux method to load navigation properties
        private void LoadNavigationProperties()
        {
            #region  Dinamically load navigation properties for the entity


            //As this is a generic repository, navigation properties will differ from entity to entity and so they 
            //need to be loaded dinamically rather than statically as following: DbSet.Include("TaskTypeSettings").AsQueryable(); 

            var entityType = typeof(T);

            var navProperties = entityType.GetProperties()
                     .Where(p => (typeof(IEnumerable).IsAssignableFrom(p.PropertyType) && p.PropertyType != typeof(string)) || p.PropertyType.Namespace == entityType.Namespace)
                     .Select(p => p)
                     .ToList();

            if (navProperties != null)
            {
                if (navProperties.Any())
                {

                    for (int i = 0; i < navProperties.Count(); i++)
                    {
                        _entities.Set<T>().Include(navProperties[i].Name).Load();
                        //DbSet.Include($"{navProperties[i].Name.ToString()}"); //.AsNoTracking(); //AsNotTracking avoid the error when updating OrganizationSettings.
                    }
                }
            }
            #endregion

        }
        #endregion
    }
}
