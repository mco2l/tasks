﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using TaskSchedulerManager.Data.MainDb.Models;
using TaskSchedulerManager.Repositories.Interfaces;

namespace TaskSchedulerManager.Repositories
{
    public class TaskTypesRepository : GenericRepository<MainContext, TaskTypes>, ITaskTypesRepository
    {
       

        public TaskTypes GetSingle(long taskTypeId)
        {
            return GetAll().FirstOrDefault(x => x.TaskTypeId == taskTypeId);
        }

    }
}
