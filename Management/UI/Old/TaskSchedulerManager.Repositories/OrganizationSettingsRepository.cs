﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaskSchedulerManager.Data.MainDb.Models;
using TaskSchedulerManager.Repositories.Interfaces;

namespace TaskSchedulerManager.Repositories
{
    public class OrganizationSettingsRepository : GenericRepository<MainContext, OrganizationSettings>, IOrganizationSettingsRepository
    {
        public OrganizationSettings GetSingle(long orgSettingId)
        {

            var query = GetAll().FirstOrDefault(x => x.TenantId == orgSettingId);
            return query;
        }
    }
}
