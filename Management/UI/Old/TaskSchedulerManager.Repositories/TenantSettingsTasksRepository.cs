﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using TaskSchedulerManager.Data.TenantDb.Models;
using TaskSchedulerManager.Repositories.Interfaces;

namespace TaskSchedulerManager.Repositories
{
    public class TenantSettingsTasksRepository : ITenantSettingsTasksRepository
    {
        private TenantContext _tenantContext;

        TenantContext ITenantSettingsTasksRepository.SetTenantContext(TenantContext context)
        {
            _tenantContext = context;

            return _tenantContext;
        }


        public void Add(SettingsTask entity)
        {
            _tenantContext.SettingsTask.Add(entity);

            Save();
        }

        public void Add(SettingsRsoMasterUpload entity)
        {
            throw new NotImplementedException();
        }

        public void Add(SettingsRsoSage50 entity)
        {
            throw new NotImplementedException();
        }

        public void Add(SettingsRsoScanDownload entity)
        {
            throw new NotImplementedException();
        }

        public void Add(SettingsRsoScanUpload entity)
        {
            throw new NotImplementedException();
        }

        public void Add(SettingsRsoXero entity)
        {
            throw new NotImplementedException();
        }

        public void Add(SettingsSage200Posting entity)
        {
            throw new NotImplementedException();
        }

        public void Add(SettingsSage50Posting entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(SettingsTask entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(SettingsRsoMasterUpload entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(SettingsRsoSage50 entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(SettingsRsoScanDownload entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(SettingsRsoScanUpload entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(SettingsRsoXero entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(SettingsSage200Posting entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(SettingsSage50Posting entity)
        {
            throw new NotImplementedException();
        }

        public void Edit(SettingsTask entity)
        {
            throw new NotImplementedException();
        }

        public void Edit(SettingsRsoMasterUpload entity)
        {
            throw new NotImplementedException();
        }

        public void Edit(SettingsRsoSage50 entity)
        {
            throw new NotImplementedException();
        }

        public void Edit(SettingsRsoScanDownload entity)
        {
            throw new NotImplementedException();
        }

        public void Edit(SettingsRsoScanUpload entity)
        {
            throw new NotImplementedException();
        }

        public void Edit(SettingsRsoXero entity)
        {
            throw new NotImplementedException();
        }

        public void Edit(SettingsSage200Posting entity)
        {
            throw new NotImplementedException();
        }

        public void Edit(SettingsSage50Posting entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<SettingsTask> FindBy(Expression<Func<SettingsTask, bool>> predicate)
        {
            return _tenantContext.SettingsTask.Where(predicate);
        }

        public IEnumerable<SettingsRsoMasterUpload> FindBy(Expression<Func<SettingsRsoMasterUpload, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<SettingsRsoSage50> FindBy(Expression<Func<SettingsRsoSage50, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<SettingsRsoScanDownload> FindBy(Expression<Func<SettingsRsoScanDownload, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<SettingsRsoScanUpload> FindBy(Expression<Func<SettingsRsoScanUpload, bool>> predicate)
        {
            return _tenantContext.Settings_RSO_ScanUpload.Where(predicate);
        }

        public IEnumerable<SettingsRsoXero> FindBy(Expression<Func<SettingsRsoXero, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<SettingsSage200Posting> FindBy(Expression<Func<SettingsSage200Posting, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<SettingsSage50Posting> FindBy(Expression<Func<SettingsSage50Posting, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<SettingsTask> GetAll()
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            _tenantContext.SaveChanges();
        }

        IEnumerable<SettingsRsoMasterUpload> IGenericRepository<SettingsRsoMasterUpload>.GetAll()
        {
            throw new NotImplementedException();
        }

        IEnumerable<SettingsRsoSage50> IGenericRepository<SettingsRsoSage50>.GetAll()
        {
            throw new NotImplementedException();
        }

        IEnumerable<SettingsRsoScanDownload> IGenericRepository<SettingsRsoScanDownload>.GetAll()
        {
            throw new NotImplementedException();
        }

        IEnumerable<SettingsRsoScanUpload> IGenericRepository<SettingsRsoScanUpload>.GetAll()
        {
            throw new NotImplementedException();
        }

        IEnumerable<SettingsRsoXero> IGenericRepository<SettingsRsoXero>.GetAll()
        {
            throw new NotImplementedException();
        }

        IEnumerable<SettingsSage200Posting> IGenericRepository<SettingsSage200Posting>.GetAll()
        {
            throw new NotImplementedException();
        }

        IEnumerable<SettingsSage50Posting> IGenericRepository<SettingsSage50Posting>.GetAll()
        {
            throw new NotImplementedException();
        }

     
    }
}
