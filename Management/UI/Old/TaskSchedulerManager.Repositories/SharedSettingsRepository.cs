﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Mvc.Rendering;
using TaskSchedulerManager.Data.TenantDb.Models;
using TaskSchedulerManager.Repositories.Interfaces;

namespace TaskSchedulerManager.Repositories
{
    public class SharedSettingsRepository : GenericRepository<TenantContext, SettingsSharedFieldMapping>, ISharedSettingsRepository
    {

        public SharedSettingsRepository()
        {

        }

        public List<SelectListItem> GetSharedSettingsByName(string sharedSettingName, TenantContext tenantContext)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            switch (sharedSettingName)
            {
                case "LineFieldMappingId":
                case "HeaderFieldMappingId":
                case "FieldMappingId":
                    foreach (var item in tenantContext.SettingsSharedFieldMapping.ToList())
                    {
                        items.Add(new SelectListItem { Text = item.Description, Value = item.Id.ToString() });
                    }
                    break;

                case "DebugLogSettingId":

                    foreach (var item in tenantContext.SettingsSharedDebugLog.Where(log => log.Enabled == true).ToList())
                    {
                        items.Add(new SelectListItem { Text = item.Path, Value = item.Id.ToString() });
                    }
                    break;

                case "AdestWebServiceSettingId":
                    foreach (var item in tenantContext.SettingsSharedAdestWebService.ToList())
                    {
                        items.Add(new SelectListItem { Text = item.Uri, Value = item.Id.ToString() });
                    }
                    break;

                case "RSOSettingId":
                    foreach (var item in tenantContext.SettingsSharedRso.ToList())
                    {
                        items.Add(new SelectListItem { Text = item.Uri, Value = item.Id.ToString() });
                    }
                    break;
                case "BatchListId":
                    foreach (var item in tenantContext.SettingsSharedBatchList.ToList())
                    {
                        items.Add(new SelectListItem { Text = item.Path, Value = item.Id.ToString() });
                    }
                    break;
                case "DownloadSettingId":
                    foreach (var item in tenantContext.SettingsSharedDownloadSettings.ToList())
                    {
                        items.Add(new SelectListItem { Text = item.Path, Value = item.Id.ToString() });
                    }
                    break;

                case "EmailSuccessSettingId":
                case "EmailFailureSettingId":
                    foreach (var item in tenantContext.SettingsSharedEmail.ToList())
                    {
                        items.Add(new SelectListItem { Text = item.EmailTo, Value = item.Id.ToString() });
                    }
                    break;
                default:
                    break;
            }

            if(!items.Any())
            {
                items.Add(new SelectListItem { Text = "No values found", Value = "-1" });
            }
            return items;
        }
    }
}
