﻿using System;
using System.Collections.Generic;
using System.Text;
using TaskSchedulerManager.Data.TenantDb.Models;
using TaskSchedulerManager.Repositories.Interfaces;

namespace TaskSchedulerManager.Repositories
{
    public class TenantRepository : ITenantRepository
    {
        public Dictionary<string, string> GetRecordById(TenantContext context, string tableName, int recordId)
        {
            throw new NotImplementedException();
        }

        public bool InsertSingleRecord(TenantContext context, string tableName, Dictionary<string, string> record)
        {
            throw new NotImplementedException();
        }

        public bool UpdateRecord(TenantContext context, string tableName, Dictionary<string, string> record)
        {
            throw new NotImplementedException();
        }
    }
}
