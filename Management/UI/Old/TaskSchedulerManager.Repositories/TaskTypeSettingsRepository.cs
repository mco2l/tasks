﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Mvc.Rendering;
using TaskSchedulerManager.Data.MainDb.Models;
using TaskSchedulerManager.Repositories.Interfaces;

namespace TaskSchedulerManager.Repositories
{
    public class TaskTypeSettingsRepository : GenericRepository<MainContext, TaskTypeSettings>, ITaskTypeSettingsRepository
    {
       
        public IEnumerable<TaskTypeSettings> GetAllByTaskTypeID(long taskTypeId)
        {
            return FindBy(tts => tts.TaskTypeId == taskTypeId);
        }

      
    }
}
