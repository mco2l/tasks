﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Mvc.Rendering;
using TaskSchedulerManager.Data.TenantDb.Models;
using TaskSchedulerManager.Domain;

namespace TaskSchedulerManager.Services.Interfaces
{
    public interface ITasksService
    {
        IEnumerable<TaskTypesDTO> GetAll();

        IEnumerable<TaskTypeSettingsDTO> GetTaskTypeSettings(long taskTypeId);


        List<SelectListItem> GetSharedSettingsByName(string sharedSettingName, TenantContext tenantContext);

        bool CreateTask(CreateTaskDTO createTaskDto, TenantContext tenantContext);


        SettingsTask GetSettingTask(long taskId, TenantContext tenantContext);
    }
}
