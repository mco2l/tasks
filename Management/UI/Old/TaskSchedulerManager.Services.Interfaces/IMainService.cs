﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;
using TaskSchedulerManager.Data.MainDb.Models;
using TaskSchedulerManager.Domain;

namespace TaskSchedulerManager.Services.Interfaces
{
    public interface IMainService
    {

        IEnumerable<OrganizationSettingsDTO> GetTenantsList();

        void AddOrganization(OrganizationSettingsDTO org);


        OrganizationSettingsDTO GetOrganizationById(long id);

        string GetOrganizationName(long id);

        void UpdateOrganization(OrganizationSettingsDTO org);
        void DeleteOrganization(long id);
    }
}
