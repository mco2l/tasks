﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace TaskSchedulerManager.Services.Interfaces
{
    public interface IRolesService
    {
        void InitializeAsync();
    }
}
