﻿using System;
using System.Collections.Generic;
using System.Text;
using TaskSchedulerManager.Data.MainDb.Models;

namespace TaskSchedulerManager.Services.Interfaces
{
    public interface IDynamicFormFieldService
    {

        /// <summary>
        /// Get the name of the Settings table. i.e. Settings_RSO_Sage50
        /// </summary>
        /// <param name="taskTypeId"></param>
        /// <returns></returns>
        string GetSettingsTableNameByTaskTypeId(string mainContextConnectionString, int taskTypeId);


        /// <summary>
        /// Retrieve field value from settings table.
        /// </summary>
        /// <param name="settingsTableName">The settings table containing the field to read</param>
        /// <param name="settingName">The name of the field to read the value</param>
        /// <returns></returns>
        string GetSettingsFieldValueBySettingName(string mainContextConnectionString, string settingsTableName, string settingName);
    }
}
