﻿using System;
using System.Collections.Generic;

namespace TaskSchedulerManager.Data.MainDb.Models
{
    public partial class OrganizationSettings
    {
        public long TenantId { get; set; }
        public string TenantName { get; set; }
        public string AccessKey { get; set; }
        public bool Disabled { get; set; }


        public string TenantAlias { get; set; }

        public string TenantDomain { get; set; }

        public string TenantDatabase { get; set; }

        public string Notes { get; set; }
    }
}
