﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace TaskSchedulerManager.Data.MainDb.Models
{
    public partial class MainContext : DbContext
    {
        public virtual DbSet<OrganizationSettings> OrganizationSettings { get; set; }
        public virtual DbSet<TaskTypeSettings> TaskTypeSettings { get; set; }
        public virtual DbSet<TaskTypes> TaskTypes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            #warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
            optionsBuilder.UseSqlite(@"Data Source=E:\Adest Dev\TaskSchedulerManager\TaskSchedulerManager.Data\MainDb\Main.db");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<OrganizationSettings>(entity =>
            {
                entity.HasKey(e => e.TenantId)
                    .HasName("PK_OrganizationSettings");

                entity.Property(e => e.AccessKey)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 100)");

                entity.Property(e => e.Disabled).HasColumnType("BOOLEAN");

                entity.Property(e => e.TenantName)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 50)");
            });

            modelBuilder.Entity<TaskTypeSettings>(entity =>
            {
                entity.Property(e => e.IsCore).HasColumnType("BOOLEAN");

                entity.Property(e => e.MaxLength).HasColumnType("INTEGER (0, 0)");

                entity.Property(e => e.MinLength).HasColumnType("INTEGER (1, 50)");

                entity.Property(e => e.SettingName)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 50)");

                entity.Property(e => e.Type).HasColumnType("INTEGER (1, 10)");

                entity.HasOne(d => d.TaskType)
                    .WithMany(p => p.TaskTypeSettings)
                    .HasForeignKey(d => d.TaskTypeId)
                    .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<TaskTypes>(entity =>
            {
                entity.HasKey(e => e.TaskTypeId)
                    .HasName("PK_TaskTypes");

                entity.Property(e => e.AssemblyName)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 50)");

                entity.Property(e => e.AssemblyType)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 50)");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 50)");

                entity.Property(e => e.SettingsTable)
                    .IsRequired()
                    .HasColumnType("TEXT (40)");
            });
        }
    }
}