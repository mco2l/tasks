﻿using System;
using System.Collections.Generic;

namespace TaskSchedulerManager.Data.MainDb.Models
{
    public partial class TaskTypes
    {
        public TaskTypes()
        {
            TaskTypeSettings = new HashSet<TaskTypeSettings>();
        }

        public long TaskTypeId { get; set; }
        public string Description { get; set; }
        public string AssemblyName { get; set; }
        public string AssemblyType { get; set; }
        public string SettingsTable { get; set; }

        public virtual ICollection<TaskTypeSettings> TaskTypeSettings { get; set; }
    }
}
