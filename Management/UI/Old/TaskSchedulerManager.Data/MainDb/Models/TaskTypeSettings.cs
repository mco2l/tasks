﻿using System;
using System.Collections.Generic;

namespace TaskSchedulerManager.Data.MainDb.Models
{
    public partial class TaskTypeSettings
    {
        public long Id { get; set; }
        public long TaskTypeId { get; set; }
        public string SettingName { get; set; }
        public long Type { get; set; }
        public long? MinLength { get; set; }
        public long? MaxLength { get; set; }
        public string IsCore { get; set; }

        public virtual TaskTypes TaskType { get; set; }
    }
}
