﻿using System;
using System.Collections.Generic;

namespace TaskSchedulerManager.Data.TenantDb.Models
{
    public partial class SettingsSharedFieldMappingFields
    {
        public long Id { get; set; }
        public long? MappingId { get; set; }
        public string FromField { get; set; }
        public string ToField { get; set; }
        public string DefaultValue { get; set; }

        public virtual SettingsSharedFieldMapping Mapping { get; set; }
    }
}
