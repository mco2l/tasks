﻿using System;
using System.Collections.Generic;

namespace TaskSchedulerManager.Data.TenantDb.Models
{
    public partial class SettingsRsoScanUpload
    {
        public long Id { get; set; }
        public long TaskId { get; set; }
        public string AutoDetection { get; set; }
        public string AutoBuyerId { get; set; }
        public string BatchPath { get; set; }
        public long? MaxFileSize { get; set; }
        public string DefaultDocumentSystemName { get; set; }
        public string OverMaxSizeFilePath { get; set; }
        public string BackupPath { get; set; }
        public string AutoCreateFolder { get; set; }
        public long? BatchListId { get; set; }
        public long? RsosettingId { get; set; }

        public virtual SettingsSharedBatchList BatchList { get; set; }
        public virtual SettingsSharedRso Rsosetting { get; set; }
        public virtual SettingsTask Task { get; set; }
    }
}
