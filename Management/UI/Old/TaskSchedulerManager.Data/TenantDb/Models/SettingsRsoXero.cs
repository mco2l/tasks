﻿using System;
using System.Collections.Generic;

namespace TaskSchedulerManager.Data.TenantDb.Models
{
    public partial class SettingsRsoXero
    {
        public long Id { get; set; }
        public long? TaskId { get; set; }
        public string OrganizationName { get; set; }
        public string ConsumerKey { get; set; }
        public string ConsumerSecret { get; set; }
        public string AppName { get; set; }
        public string Buyer { get; set; }
        public string UseXeroFailureSupplier { get; set; }
        public string XeroFailureSupplier { get; set; }
        public long DownloadSettingId { get; set; }
        public long RsosettingId { get; set; }
        public long? DebugLogSettingId { get; set; }
        public bool InvoiceLogEnabled { get; set; }
        public string InvoiceLogPath { get; set; }

        public virtual SettingsSharedDebugLog DebugLogSetting { get; set; }
        public virtual SettingsSharedDownloadSettings DownloadSetting { get; set; }
        public virtual SettingsSharedRso Rsosetting { get; set; }
        public virtual SettingsTask Task { get; set; }
    }
}
