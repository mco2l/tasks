﻿using System;
using System.Collections.Generic;

namespace TaskSchedulerManager.Data.TenantDb.Models
{
    public partial class SettingsSharedFieldMapping
    {
        public SettingsSharedFieldMapping()
        {
            SettingsRsoMasterUpload = new HashSet<SettingsRsoMasterUpload>();
            SettingsRsoSage50HeaderFieldMapping = new HashSet<SettingsRsoSage50>();
            SettingsRsoSage50LineFieldMapping = new HashSet<SettingsRsoSage50>();
            SettingsRsoScanDownload = new HashSet<SettingsRsoScanDownload>();
            SettingsSage200PostingHeaderFieldMapping = new HashSet<SettingsSage200Posting>();
            SettingsSage200PostingLineFieldMapping = new HashSet<SettingsSage200Posting>();
            SettingsSage50PostingHeaderFieldMapping = new HashSet<SettingsSage50Posting>();
            SettingsSage50PostingLineFieldMapping = new HashSet<SettingsSage50Posting>();
            SettingsSharedFieldMappingFields = new HashSet<SettingsSharedFieldMappingFields>();
        }

        public long Id { get; set; }
        public string Description { get; set; }

        public virtual ICollection<SettingsRsoMasterUpload> SettingsRsoMasterUpload { get; set; }
        public virtual ICollection<SettingsRsoSage50> SettingsRsoSage50HeaderFieldMapping { get; set; }
        public virtual ICollection<SettingsRsoSage50> SettingsRsoSage50LineFieldMapping { get; set; }
        public virtual ICollection<SettingsRsoScanDownload> SettingsRsoScanDownload { get; set; }
        public virtual ICollection<SettingsSage200Posting> SettingsSage200PostingHeaderFieldMapping { get; set; }
        public virtual ICollection<SettingsSage200Posting> SettingsSage200PostingLineFieldMapping { get; set; }
        public virtual ICollection<SettingsSage50Posting> SettingsSage50PostingHeaderFieldMapping { get; set; }
        public virtual ICollection<SettingsSage50Posting> SettingsSage50PostingLineFieldMapping { get; set; }
        public virtual ICollection<SettingsSharedFieldMappingFields> SettingsSharedFieldMappingFields { get; set; }
    }
}
