﻿using System;
using System.Collections.Generic;

namespace TaskSchedulerManager.Data.TenantDb.Models
{
    public partial class SettingsSharedRso
    {
        public SettingsSharedRso()
        {
            SettingsRsoMasterUpload = new HashSet<SettingsRsoMasterUpload>();
            SettingsRsoSage50 = new HashSet<SettingsRsoSage50>();
            SettingsRsoScanDownload = new HashSet<SettingsRsoScanDownload>();
            SettingsRsoScanUpload = new HashSet<SettingsRsoScanUpload>();
            SettingsRsoXero = new HashSet<SettingsRsoXero>();
        }

        public long Id { get; set; }
        public string ApiKey { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Uri { get; set; }
        public string UsingLineTaxFields { get; set; }

        public virtual ICollection<SettingsRsoMasterUpload> SettingsRsoMasterUpload { get; set; }
        public virtual ICollection<SettingsRsoSage50> SettingsRsoSage50 { get; set; }
        public virtual ICollection<SettingsRsoScanDownload> SettingsRsoScanDownload { get; set; }
        public virtual ICollection<SettingsRsoScanUpload> SettingsRsoScanUpload { get; set; }
        public virtual ICollection<SettingsRsoXero> SettingsRsoXero { get; set; }
    }
}
