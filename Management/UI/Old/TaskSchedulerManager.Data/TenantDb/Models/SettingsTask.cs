﻿using System;
using System.Collections.Generic;

namespace TaskSchedulerManager.Data.TenantDb.Models
{
    public partial class SettingsTask
    {
        public SettingsTask()
        {
            SettingsRsoMasterUpload = new HashSet<SettingsRsoMasterUpload>();
            SettingsRsoSage50 = new HashSet<SettingsRsoSage50>();
            SettingsRsoScanDownload = new HashSet<SettingsRsoScanDownload>();
            SettingsRsoScanUpload = new HashSet<SettingsRsoScanUpload>();
            SettingsRsoXero = new HashSet<SettingsRsoXero>();
            SettingsSage200Posting = new HashSet<SettingsSage200Posting>();
            SettingsSage50Posting = new HashSet<SettingsSage50Posting>();
            SettingsTaskSubMainTask = new HashSet<SettingsTaskSub>();
            SettingsTaskSubSubTask = new HashSet<SettingsTaskSub>();
        }

        public long Id { get; set; }
        public long TaskTypeId { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public long Interval { get; set; }
        public long TotalRunPerTimeRange { get; set; }
        public string Disabled { get; set; }
        public string EnableTracing { get; set; }
        public long MaxLogFiles { get; set; }
        public long EmailSuccessSettingId { get; set; }
        public long EmailFailureSettingId { get; set; }
        public string RunOnStart { get; set; }
        public long? MaxFailuresAllowed { get; set; }
        public long? IntervalFailureRetry { get; set; }

        public virtual ICollection<SettingsRsoMasterUpload> SettingsRsoMasterUpload { get; set; }
        public virtual ICollection<SettingsRsoSage50> SettingsRsoSage50 { get; set; }
        public virtual ICollection<SettingsRsoScanDownload> SettingsRsoScanDownload { get; set; }
        public virtual ICollection<SettingsRsoScanUpload> SettingsRsoScanUpload { get; set; }
        public virtual ICollection<SettingsRsoXero> SettingsRsoXero { get; set; }
        public virtual ICollection<SettingsSage200Posting> SettingsSage200Posting { get; set; }
        public virtual ICollection<SettingsSage50Posting> SettingsSage50Posting { get; set; }
        public virtual ICollection<SettingsTaskSub> SettingsTaskSubMainTask { get; set; }
        public virtual ICollection<SettingsTaskSub> SettingsTaskSubSubTask { get; set; }
        public virtual SettingsSharedEmail EmailFailureSetting { get; set; }
        public virtual SettingsSharedEmail EmailSuccessSetting { get; set; }
    }
}
