﻿using System;
using System.Collections.Generic;

namespace TaskSchedulerManager.Data.TenantDb.Models
{
    public partial class SettingsRsoScanDownload
    {
        public long Id { get; set; }
        public long? TaskId { get; set; }
        public string DownloadPath { get; set; }
        public long ImageLocationIndex { get; set; }
        public string CreateBuyerFolder { get; set; }
        public bool UsePermaLink { get; set; }
        public long? RsosettingId { get; set; }
        public long FieldMappingId { get; set; }

        public virtual SettingsSharedFieldMapping FieldMapping { get; set; }
        public virtual SettingsSharedRso Rsosetting { get; set; }
        public virtual SettingsTask Task { get; set; }
    }
}
