﻿using System;
using System.Collections.Generic;

namespace TaskSchedulerManager.Data.TenantDb.Models
{
    public partial class SettingsTaskSub
    {
        public long Id { get; set; }
        public long MainTaskId { get; set; }
        public long SubTaskId { get; set; }
        public long Order { get; set; }

        public virtual SettingsTask MainTask { get; set; }
        public virtual SettingsTask SubTask { get; set; }
    }
}
