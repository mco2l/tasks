﻿using System;
using System.Collections.Generic;

namespace TaskSchedulerManager.Data.TenantDb.Models
{
    public partial class User
    {
        public long UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string IsAdmin { get; set; }
        public long? DelegateTo { get; set; }
        public string DelegateFromDate { get; set; }
        public string DelegateToDate { get; set; }
        public long? LockCount { get; set; }
    }
}
