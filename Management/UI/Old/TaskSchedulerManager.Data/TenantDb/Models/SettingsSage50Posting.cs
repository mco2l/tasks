﻿using System;
using System.Collections.Generic;

namespace TaskSchedulerManager.Data.TenantDb.Models
{
    public partial class SettingsSage50Posting
    {
        public long Id { get; set; }
        public long? TaskId { get; set; }
        public string Company { get; set; }
        public string DatabasePath { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public long AdestWebServiceSettingId { get; set; }
        public long DebugLogSettingId { get; set; }
        public long HeaderFieldMappingId { get; set; }
        public long? LineFieldMappingId { get; set; }

        public virtual SettingsSharedAdestWebService AdestWebServiceSetting { get; set; }
        public virtual SettingsSharedDebugLog DebugLogSetting { get; set; }
        public virtual SettingsSharedFieldMapping HeaderFieldMapping { get; set; }
        public virtual SettingsSharedFieldMapping LineFieldMapping { get; set; }
        public virtual SettingsTask Task { get; set; }
    }
}
