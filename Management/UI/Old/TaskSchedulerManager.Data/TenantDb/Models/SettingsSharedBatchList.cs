﻿using System;
using System.Collections.Generic;

namespace TaskSchedulerManager.Data.TenantDb.Models
{
    public partial class SettingsSharedBatchList
    {
        public SettingsSharedBatchList()
        {
            SettingsRsoScanUpload = new HashSet<SettingsRsoScanUpload>();
        }

        public long Id { get; set; }
        public string Path { get; set; }
        public string DocumentType { get; set; }
        public string BuyersName { get; set; }

        public virtual ICollection<SettingsRsoScanUpload> SettingsRsoScanUpload { get; set; }
    }
}
