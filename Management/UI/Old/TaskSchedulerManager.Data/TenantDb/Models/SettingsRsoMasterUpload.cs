﻿using System;
using System.Collections.Generic;

namespace TaskSchedulerManager.Data.TenantDb.Models
{
    public partial class SettingsRsoMasterUpload
    {
        public long Id { get; set; }
        public long TaskId { get; set; }
        public long UploadType { get; set; }
        public bool ClearBeforeUpload { get; set; }
        public string CsvSeparator { get; set; }
        public string DataFilePath { get; set; }
        public string DataFilePattern { get; set; }
        public string DataFilePatternExt { get; set; }
        public string DataFilePatternMask { get; set; }
        public string BackupPath { get; set; }
        public string ErrorPath { get; set; }
        public bool SkipFirstLine { get; set; }
        public Boolean IsSplitFiles { get; set; }
        public string DbconnectionString { get; set; }
        public string DbcommandType { get; set; }
        public string DbcommandText { get; set; }
        public long RsosettingId { get; set; }
        public long FieldMappingId { get; set; }

        public virtual SettingsSharedFieldMapping FieldMapping { get; set; }
        public virtual SettingsSharedRso Rsosetting { get; set; }
        public virtual SettingsTask Task { get; set; }
    }
}
