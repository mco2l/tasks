﻿using System;
using System.Collections.Generic;

namespace TaskSchedulerManager.Data.TenantDb.Models
{
    public partial class SettingsRsoSage50
    {
        public long Id { get; set; }
        public long? TaskId { get; set; }
        public string Company { get; set; }
        public string DatabasePath { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string AttachmentPath { get; set; }
        public long RsosettingId { get; set; }
        public long HeaderFieldMappingId { get; set; }
        public long LineFieldMappingId { get; set; }
        public long DownloadSettingId { get; set; }

        public virtual SettingsSharedDownloadSettings DownloadSetting { get; set; }
        public virtual SettingsSharedFieldMapping HeaderFieldMapping { get; set; }
        public virtual SettingsSharedFieldMapping LineFieldMapping { get; set; }
        public virtual SettingsSharedRso Rsosetting { get; set; }
        public virtual SettingsTask Task { get; set; }
    }
}
