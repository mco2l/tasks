﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using TaskSchedulerManager.Data.MainDb.Models;

namespace TaskSchedulerManager.Data.Migrations
{
    [DbContext(typeof(MainContext))]
    [Migration("20170312115807_OrganizationSettingsMigration")]
    partial class OrganizationSettingsMigration
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.1");

            modelBuilder.Entity("TaskSchedulerManager.Data.MainDb.Models.OrganizationSettings", b =>
                {
                    b.Property<long>("TenantId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AccessKey")
                        .IsRequired()
                        .HasColumnType("TEXT (1, 100)");

                    b.Property<bool>("Disabled")
                        .HasColumnType("BOOLEAN");

                    b.Property<string>("Notes");

                    b.Property<string>("TenantAlias");

                    b.Property<string>("TenantDatabase");

                    b.Property<string>("TenantDomain");

                    b.Property<string>("TenantName")
                        .IsRequired()
                        .HasColumnType("TEXT (1, 50)");

                    b.HasKey("TenantId")
                        .HasName("PK_OrganizationSettings");

                    b.ToTable("OrganizationSettings");
                });

            modelBuilder.Entity("TaskSchedulerManager.Data.MainDb.Models.TaskTypes", b =>
                {
                    b.Property<long>("TaskTypeId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AssemblyName")
                        .IsRequired()
                        .HasColumnType("TEXT (1, 50)");

                    b.Property<string>("AssemblyType")
                        .IsRequired()
                        .HasColumnType("TEXT (1, 50)");

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasColumnType("TEXT (1, 50)");

                    b.Property<string>("SettingsTable")
                        .IsRequired()
                        .HasColumnType("TEXT (40)");

                    b.HasKey("TaskTypeId")
                        .HasName("PK_TaskTypes");

                    b.ToTable("TaskTypes");
                });

            modelBuilder.Entity("TaskSchedulerManager.Data.MainDb.Models.TaskTypeSettings", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("IsCore")
                        .HasColumnType("BOOLEAN");

                    b.Property<long?>("MaxLength")
                        .HasColumnType("INTEGER (0, 0)");

                    b.Property<long?>("MinLength")
                        .HasColumnType("INTEGER (1, 50)");

                    b.Property<string>("SettingName")
                        .IsRequired()
                        .HasColumnType("TEXT (1, 50)");

                    b.Property<long>("TaskTypeId");

                    b.Property<long>("Type")
                        .HasColumnType("INTEGER (1, 10)");

                    b.HasKey("Id");

                    b.HasIndex("TaskTypeId");

                    b.ToTable("TaskTypeSettings");
                });

            modelBuilder.Entity("TaskSchedulerManager.Data.MainDb.Models.TaskTypeSettings", b =>
                {
                    b.HasOne("TaskSchedulerManager.Data.MainDb.Models.TaskTypes", "TaskType")
                        .WithMany("TaskTypeSettings")
                        .HasForeignKey("TaskTypeId");
                });
        }
    }
}
