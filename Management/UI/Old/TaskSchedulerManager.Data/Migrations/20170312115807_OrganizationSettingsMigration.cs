﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TaskSchedulerManager.Data.Migrations
{
    public partial class OrganizationSettingsMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "OrganizationSettings",
                columns: table => new
                {
                    TenantId = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    AccessKey = table.Column<string>(type: "TEXT (1, 100)", nullable: false),
                    Disabled = table.Column<bool>(type: "BOOLEAN", nullable: false),
                    Notes = table.Column<string>(nullable: true),
                    TenantAlias = table.Column<string>(nullable: true),
                    TenantDatabase = table.Column<string>(nullable: true),
                    TenantDomain = table.Column<string>(nullable: true),
                    TenantName = table.Column<string>(type: "TEXT (1, 50)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrganizationSettings", x => x.TenantId);
                });

            migrationBuilder.CreateTable(
                name: "TaskTypes",
                columns: table => new
                {
                    TaskTypeId = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    AssemblyName = table.Column<string>(type: "TEXT (1, 50)", nullable: false),
                    AssemblyType = table.Column<string>(type: "TEXT (1, 50)", nullable: false),
                    Description = table.Column<string>(type: "TEXT (1, 50)", nullable: false),
                    SettingsTable = table.Column<string>(type: "TEXT (40)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaskTypes", x => x.TaskTypeId);
                });

            migrationBuilder.CreateTable(
                name: "TaskTypeSettings",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    IsCore = table.Column<string>(type: "BOOLEAN", nullable: true),
                    MaxLength = table.Column<long>(type: "INTEGER (0, 0)", nullable: true),
                    MinLength = table.Column<long>(type: "INTEGER (1, 50)", nullable: true),
                    SettingName = table.Column<string>(type: "TEXT (1, 50)", nullable: false),
                    TaskTypeId = table.Column<long>(nullable: false),
                    Type = table.Column<long>(type: "INTEGER (1, 10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaskTypeSettings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TaskTypeSettings_TaskTypes_TaskTypeId",
                        column: x => x.TaskTypeId,
                        principalTable: "TaskTypes",
                        principalColumn: "TaskTypeId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TaskTypeSettings_TaskTypeId",
                table: "TaskTypeSettings",
                column: "TaskTypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "OrganizationSettings");

            migrationBuilder.DropTable(
                name: "TaskTypeSettings");

            migrationBuilder.DropTable(
                name: "TaskTypes");
        }
    }
}
