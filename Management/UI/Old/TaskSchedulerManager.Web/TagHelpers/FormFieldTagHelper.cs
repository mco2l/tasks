﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.TagHelpers;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskSchedulerManager.Web.Models.TasksViewModels;

namespace TaskSchedulerManager.Web.TagHelpers
{




    //[HtmlTargetElement("input", Attributes = ForAttributeName)]
    [HtmlTargetElement("input", Attributes = ForAttributeName)]
    public class FormFieldTagHelper : InputTagHelper
    {
        private const string ForAttributeName = "asp-for";

        [HtmlAttributeName("field")]
        public TaskTypeSettingsViewModel Field { set; get; }

        [HtmlAttributeName("allowEdit")]
        public bool? AllowEdit { set; get; }


        public FormFieldTagHelper(IHtmlGenerator generator) : base(generator)
        {
        }

        public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            if (Field != null)
            {

                if (Field.SettingName.ToLower() == "taskid")
                {
                    output.Attributes.Add(new TagHelperAttribute("readonly"));
                }

                if (!string.IsNullOrEmpty(Field.IsCore))
                {
                    output.Attributes.Add(new TagHelperAttribute("required"));
                }


                output.Attributes.Add(new TagHelperAttribute("type", Field.FieldType));

                output.Attributes.Add(new TagHelperAttribute("class", "form-control"));


            }
            else
            {


                output.Attributes.Add(new TagHelperAttribute("class", "form-control"));

                if (AllowEdit.HasValue)
                    if (!AllowEdit.Value)
                        output.Attributes.Add(new TagHelperAttribute("readonly"));

                //output.Attributes.Add(new TagHelperAttribute("disabled", true));

                //string fieldName = "SettingsTask_Disabled";
                //output.Attributes.Add("onfocus", $"document.getElementById('{fieldName}').setAttribute('disabled', 'disabled')");


                //onfocus = "document.getElementById('SettingsTask_Disabled').setAttribute('disabled', 'disabled');"
            }


            base.Process(context, output);
        }
    }



}
