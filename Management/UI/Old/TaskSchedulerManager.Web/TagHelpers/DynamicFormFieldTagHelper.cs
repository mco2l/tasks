﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Razor.TagHelpers;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using TaskSchedulerManager.Data.MainDb.Models;
using TaskSchedulerManager.Data.TenantDb.Models;
using TaskSchedulerManager.Services.Interfaces;
using TaskSchedulerManager.Web.Models.TasksViewModels;

namespace TaskSchedulerManager.Web.TagHelpers
{
    //[HtmlTargetElement("dynamic-form-field")]
    public class DynamicFormFieldTagHelper : TagHelper
    {
        private IDynamicFormFieldService _dynamicFormFieldService;

        public DynamicFormFieldTagHelper(IDynamicFormFieldService dynamicFormFieldService)
        {
            _dynamicFormFieldService = dynamicFormFieldService;
        }

        public CreateTaskViewModel Info { get; set; }

        public string SettingName { get; set; }

        public TenantContext TenantContext { get; set; }

        public bool ReadFieldValue { get; set; }

        public string MainConnectionString { get; set; }

        public string TenantConnectionString { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {

            StringBuilder sb = new StringBuilder();

            var attributes = BuildInputAttributes(Info.TaskTypeId, SettingName);

            //asp-for is parsed into html generating the following attributes

            //id: SettingsTask_SettingsRsoMasterUpload_DbcommandText
            //name: SettingsTask.SettingsRsoMasterUpload.DbcommandText
            //value: SettingsTask.SettingsRsoMasterUpload.DbcommandText

            sb.AppendFormat("<input id='{0}' name='{1}' value='{2}' class='form-control'/>",
                            attributes["id"], 
                            attributes["name"], 
                            attributes.ContainsKey("value")?attributes["value"]:"");

            output.Content.SetHtmlContent(sb.ToString());

            output.TagMode = TagMode.StartTagAndEndTag;
        }


        private Dictionary<string, string> BuildInputAttributes(long taskTypeId, string settingName)
        {
            Dictionary<string, string> attributes = new Dictionary<string, string>();

            var settingsTableName = string.Empty;

           



            //TODO: Move this code to Services and Repository 
            using (var db = new MainContext())
            {
                settingsTableName = db.TaskTypes.Where(t => t.TaskTypeId == taskTypeId).FirstOrDefault().SettingsTable;


                //var _settingsTableName = settingsTableName.ToLower().Replace("_", "").Trim();

                attributes.Add("id", $"SettingsTask_{settingsTableName}_{settingName}");

                attributes.Add("name", $"SettingsTask.{settingsTableName}.{settingName}");




                if(ReadFieldValue == true)
                {
                    //option 1
                    using (SqliteConnection con = new SqliteConnection(TenantContext.Database.GetDbConnection().ConnectionString))
                    {
                        con.Open();

                        string stm = $"SELECT * FROM {settingsTableName}";

                        using (SqliteCommand cmd = new SqliteCommand(stm, con))
                        {
                            using (SqliteDataReader rdr = cmd.ExecuteReader())
                            {
                                while (rdr.Read())
                                {

                                    attributes.Add("value", rdr[settingName].ToString());
                                    break;
                                }
                            }
                        }

                        con.Close();
                    }
                }
               


                ////option2
                //using (var command = TenantContext.Database.GetDbConnection().CreateCommand())
                //{
                //    command.CommandText = $"SELECT * FROM {settingsTableName}";

                //    TenantContext.Database.OpenConnection();

                //    using (var res = command.ExecuteReader())
                //    {
                //        // do something with result

                //        for(int i=0; i< res.FieldCount ; i++)
                //        {
                //            while (res.Read())
                //            {
                //                attributes.Add("value", res[settingName].ToString());
                //            }

                         
                //        }                        
                //    }
                //}



                //foreach (var table in TenantContext.Model.GetEntityTypes())
                //{
              
                //    var _settingsTableName = settingsTableName.ToLower().Replace("_", "").Trim();

                //    var dbSets = TenantContext.GetType().GetProperties();

                //    foreach (var ctxProp in dbSets)
                //    {
                //        if(ctxProp.Name.ToLower() == _settingsTableName)
                //        {
                //            var dbSetProp1 = dbSets.SingleOrDefault(x => x.PropertyType.GetGenericArguments()[0].Name == _settingsTableName);

                //            var tableName = ctxProp.Name;

                //            var type = Type.GetType(tableName);
                //            var dbset = TenantContext.Set(type);

                //        }
                //    }

                //}


            }

            return attributes;
        }
    }
}
