﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using TaskSchedulerManager.Services.Interfaces;
using TaskSchedulerManager.Middleware;
using Microsoft.Extensions.Options;

namespace TaskSchedulerManager.Web.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private IMainService _mainService;
        private IOptions<TenantMiddlewareOptions> _options;

        public HomeController(IMainService mainService, IOptions<TenantMiddlewareOptions> options)
        {
            _mainService = mainService;

            _options = options;
        }


        public IActionResult Index()
        {

            ViewBag.Organization = _options.Value.TenantName;
            ViewBag.TenantContext = _options.Value.TenantDbContext;

           

            return View();
        }


        [Authorize(Roles = "SuperUser")]
        public IActionResult About()
        {


            ViewData["Message"] = "Your application description page.";

           
            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
