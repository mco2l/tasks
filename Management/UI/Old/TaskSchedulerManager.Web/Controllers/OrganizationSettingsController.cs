using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using TaskSchedulerManager.Web.Models.OrganizationSettingsViewModels;
using TaskSchedulerManager.Services.Interfaces;
using TaskSchedulerManager.Domain;
using NToastNotify;
using Microsoft.Extensions.Options;
using TaskSchedulerManager.Middleware;

namespace TaskSchedulerManager.Web.Controllers
{
    [Authorize(Roles = "SuperUser")]
    public class OrganizationSettingsController : Controller
    {
        private IMainService _mainService;
        private CreateOrganizationSettingsViewModel _vm;
        private IToastNotification _toastNotification;
        private IOptions<TenantMiddlewareOptions> _options;

        public OrganizationSettingsController(IMainService mainService, IToastNotification toastNotification, IOptions<TenantMiddlewareOptions> options)
        {
            _mainService = mainService;

            _vm = new CreateOrganizationSettingsViewModel();

            _toastNotification = toastNotification;

            _options = options;
        }
        public IActionResult Index()
        {

            return View();
        }



        public IActionResult Display(long id)
        {
            var dto = _mainService.GetOrganizationById(id);

            _vm.AccessKey = dto.AccessKey;
            _vm.Disabled = dto.Disabled;
            _vm.Notes = dto.Notes;
            _vm.TenantAlias = dto.TenantAlias;
            _vm.TenantDatabase = dto.TenantDatabase;
            _vm.TenantDomain = dto.TenantDomain;
            _vm.TenantId = dto.TenantId;
            _vm.TenantName = dto.TenantName;

            return View(_vm);
        }




        public IActionResult New()
        {
            return View(_vm);
        }

        [HttpPost]
        public IActionResult New(CreateOrganizationSettingsViewModel vm)
        {
            try
            {
                OrganizationSettingsDTO dto = new OrganizationSettingsDTO()
                {
                    AccessKey = vm.AccessKey,
                    Disabled = vm.Disabled,
                    Notes = vm.Notes,
                    TenantAlias = vm.TenantAlias,
                    TenantDatabase = vm.TenantDatabase,
                    TenantDomain = vm.TenantDomain,
                    TenantId = vm.TenantId,
                    TenantName = vm.TenantName
                };

                _mainService.AddOrganization(dto);



                _toastNotification.AddToastMessage("New Organization", $"A new organization {dto.TenantAlias} was created with success!", ToastEnums.ToastType.Success, new ToastOption()
                {
                    PositionClass = NToastNotify.Constants.ToastPositions.TopCenter,
                });

                return RedirectToAction("Display", new { id = dto.TenantId });
            }
            catch (Exception ex)
            {
                _toastNotification.AddToastMessage("Error", "Couldn't create organization this time...", ToastEnums.ToastType.Error, new ToastOption()
                {
                    PositionClass = NToastNotify.Constants.ToastPositions.TopCenter
                });

                return RedirectToAction("New");
            }


        }




        public IActionResult Edit(int id)
        {
            var dto = _mainService.GetOrganizationById(id);

            var vm = new CreateOrganizationSettingsViewModel()
            {
                AccessKey = dto.AccessKey,
                Disabled = dto.Disabled,
                Notes = dto.Notes,
                TenantAlias = dto.TenantAlias,
                TenantDatabase = dto.TenantDatabase,
                TenantDomain = dto.TenantDomain,
                TenantId = dto.TenantId,
                TenantName = dto.TenantName
            };

            return View(vm);
        }

        [HttpPost]
        public IActionResult Edit(CreateOrganizationSettingsViewModel org)
        {

            try
            {
                var dto = new OrganizationSettingsDTO()
                {
                    AccessKey = org.AccessKey,
                    Disabled = org.Disabled,
                    Notes = org.Notes,
                    TenantAlias = org.TenantAlias,
                    TenantDatabase = org.TenantDatabase,
                    TenantDomain = org.TenantDomain,
                    TenantId = org.TenantId,
                    TenantName = org.TenantName
                };

                _mainService.UpdateOrganization(dto);

                _toastNotification.AddToastMessage("Update", $"Organization {org.TenantAlias} updated with success!", ToastEnums.ToastType.Success, new ToastOption()
                {
                    PositionClass = NToastNotify.Constants.ToastPositions.TopCenter
                });

                return RedirectToAction("Display", new { id = org.TenantId });
            }
            catch (Exception ex)
            {
                _toastNotification.AddToastMessage("Error", "Couldn't update this time... Try again later.", ToastEnums.ToastType.Error, new ToastOption()
                {
                    PositionClass = NToastNotify.Constants.ToastPositions.TopCenter
                });

                return RedirectToAction("Edit", new { id = org.TenantId });
            }
        }



        public IActionResult Delete(long id)
        {
            try
            {
                _mainService.DeleteOrganization(id);

                _toastNotification.AddToastMessage("Delete", "Organization deleted with success!!", ToastEnums.ToastType.Success, new ToastOption()
                {
                    PositionClass = NToastNotify.Constants.ToastPositions.TopCenter
                });

            }
            catch (Exception ex)
            {
                _toastNotification.AddToastMessage("Error", "Couldn't delete this time... Try again later.", ToastEnums.ToastType.Error, new ToastOption()
                {
                    PositionClass = NToastNotify.Constants.ToastPositions.TopCenter
                });
            }

            return RedirectToAction("List");
        }




        public IActionResult List()
        {
            var allOrganizations = _mainService.GetTenantsList();

            List<CreateOrganizationSettingsViewModel> list = new List<CreateOrganizationSettingsViewModel>();


            if (!_options.Value.GrantSuperUserAccess)
            {
                allOrganizations = allOrganizations.Where(org => org.TenantName == _options.Value.TenantName);
            }
            

            foreach (var dto in allOrganizations)
            {
                list.Add(new CreateOrganizationSettingsViewModel()
                {
                    AccessKey = dto.AccessKey,
                    Disabled = dto.Disabled,
                    Notes = dto.Notes,
                    TenantAlias = dto.TenantAlias,
                    TenantDatabase = dto.TenantDatabase,
                    TenantDomain = dto.TenantDomain,
                    TenantId = dto.TenantId,
                    TenantName = dto.TenantName
                });
            }

            return View(list);
        }
    }
}