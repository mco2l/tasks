using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TaskSchedulerManager.Services.Interfaces;
using TaskSchedulerManager.Web.Models.TasksViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using NToastNotify;
using TaskSchedulerManager.Domain;
using System.Reflection;
using Microsoft.Extensions.Options;
using TaskSchedulerManager.Middleware;
using TaskSchedulerManager.Data.TenantDb.Models;

namespace TaskSchedulerManager.Web.Controllers
{
    public class TasksController : Controller
    {
        private ITasksService _tasksService;
        CreateTaskViewModel vm;
        private IToastNotification _toastNotification;
        private TenantContext _tenantContext;

        public TasksController(ITasksService tasksService, IToastNotification toastNotification, IOptions<TenantMiddlewareOptions> options)
        {
            _tasksService = tasksService;
            _toastNotification = toastNotification;
            _tenantContext = options.Value.TenantDbContext;

            vm = new CreateTaskViewModel();
        }


        public IActionResult Index()
        {
            var dto = _tasksService.GetAll();

            var viewModel = new List<TaskTypesViewModel>();

            foreach (var t in dto)
            {
                var taskViewModel = new TaskTypesViewModel()
                {
                    AssemblyName = t.AssemblyName,
                    AssemblyType = t.AssemblyType,
                    Description = t.Description,
                    SettingsTable = t.SettingsTable,
                    TaskTypeId = t.TaskTypeId
                };

                taskViewModel.TaskTypeSettings = new List<TaskTypeSettingsViewModel>();

                foreach (var setting in t.TaskTypeSettings)
                {
                    taskViewModel.TaskTypeSettings.Add(new TaskTypeSettingsViewModel()
                    {
                        Id = setting.Id,
                        IsCore = setting.IsCore,
                        MaxLength = setting.MaxLength,
                        MinLength = setting.MinLength,
                        SettingName = setting.SettingName,
                        TaskTypeId = setting.TaskTypeId,
                        FieldType = TaskTypeSettingsViewModel.GetFieldType(setting.Type),
                        Type = setting.Type
                    });
                }

                viewModel.Add(taskViewModel);
            }



            return View(viewModel);
        }



        public IActionResult New()
        {

            return View(vm);
        }


        public IActionResult SelectTaskType(int taskTypeId)
        {
            CreateTaskViewModel vm = new CreateTaskViewModel()
            {
                EditableContent = true,

                TaskTypeId = taskTypeId,

                //Common form fields for all tasks
                SettingsTask = new SettingsTaskViewModel(),


                TaskTypeSettings = new List<TaskTypeSettingsViewModel>()
            };



            var taskTypeSettings = _tasksService.GetTaskTypeSettings(taskTypeId);

            foreach (var setting in taskTypeSettings)
            {
                vm.TaskTypeSettings.Add(new TaskTypeSettingsViewModel()
                {
                    Id = setting.Id,
                    IsCore = setting.IsCore,
                    MaxLength = setting.MaxLength,
                    MinLength = setting.MinLength,
                    SettingName = setting.SettingName,
                    TaskTypeId = setting.TaskTypeId,
                    FieldType = TaskTypeSettingsViewModel.GetFieldType(setting.Type),
                    Type = setting.Type
                });
            }

            return View("New", vm);
        }


        [HttpPost]
        public IActionResult CreateTask(CreateTaskViewModel task)
        {
            bool createTaskResult = false;

            try
            {
                if (!ModelState.IsValid)
                {
                    _toastNotification.AddToastMessage("Create Task", $"Model State not valid!", ToastEnums.ToastType.Error, new ToastOption()
                    {
                        PositionClass = NToastNotify.Constants.ToastPositions.TopCenter
                    });

                    return RedirectToAction("New");
                }
                else
                {

                    CreateTaskDTO createTaskDto = new CreateTaskDTO();

                    createTaskDto.SettingsTask = new SettingsTaskDTO()
                    {
                        Disabled = task.SettingsTask.Disabled,
                        EmailFailureSettingId = task.SettingsTask.EmailFailureSettingId,
                        EmailSuccessSettingId = task.SettingsTask.EmailSuccessSettingId,
                        EnableTracing = task.SettingsTask.EnableTracing,
                        EndTime = task.SettingsTask.EndTime,
                        Id = task.SettingsTask.Id,
                        Interval = task.SettingsTask.Interval,
                        IntervalFailureRetry = task.SettingsTask.IntervalFailureRetry,
                        MaxFailuresAllowed = task.SettingsTask.MaxFailuresAllowed,
                        MaxLogFiles = task.SettingsTask.MaxLogFiles,
                        RunOnStart = task.SettingsTask.RunOnStart,
                        StartTime = task.SettingsTask.StartTime,
                        TaskTypeId = task.SettingsTask.TaskTypeId
                    };



                    switch (task.SettingsTask.TaskTypeId.ToString())
                    {
                        case "1":
                            createTaskDto.SettingsTask.SettingsSage200Posting = new Sage200_Posting_DTO
                            {
                                TaskId = task.SettingsTask.Settings_Sage200_Posting.TaskId,
                                DebugLogSettingId = task.SettingsTask.Settings_Sage200_Posting.DebugLogSettingId,
                                AdestWebServiceSettingId = task.SettingsTask.Settings_Sage200_Posting.AdestWebServiceSettingId,
                                Company = task.SettingsTask.Settings_Sage200_Posting.Company,
                                HeaderFieldMappingId = task.SettingsTask.Settings_Sage200_Posting.HeaderFieldMappingId,
                                Id = task.SettingsTask.Settings_Sage200_Posting.Id,
                                LineFieldMappingId = task.SettingsTask.Settings_Sage200_Posting.LineFieldMappingId,
                                Password = task.SettingsTask.Settings_Sage200_Posting.Password,
                                UserName = task.SettingsTask.Settings_Sage200_Posting.UserName
                            };
                            break;
                        case "2":
                            createTaskDto.SettingsTask.SettingsRsoMasterUpload = new RSO_MasterUpload_DTO()
                            {
                                BackupPath = task.SettingsTask.Settings_RSO_MasterUpload.BackupPath,
                                ClearBeforeUpload = task.SettingsTask.Settings_RSO_MasterUpload.ClearBeforeUpload,
                                CsvSeparator = task.SettingsTask.Settings_RSO_MasterUpload.CsvSeparator,
                                DataFilePath = task.SettingsTask.Settings_RSO_MasterUpload.DataFilePath,
                                DataFilePattern = task.SettingsTask.Settings_RSO_MasterUpload.DataFilePattern,
                                DataFilePatternExt = task.SettingsTask.Settings_RSO_MasterUpload.DataFilePatternExt,
                                DataFilePatternMask = task.SettingsTask.Settings_RSO_MasterUpload.DataFilePatternMask,
                                DbcommandText = task.SettingsTask.Settings_RSO_MasterUpload.DbcommandText,
                                DbcommandType = task.SettingsTask.Settings_RSO_MasterUpload.DbcommandType,
                                DbconnectionString = task.SettingsTask.Settings_RSO_MasterUpload.DbconnectionString,
                                ErrorPath = task.SettingsTask.Settings_RSO_MasterUpload.ErrorPath,
                                FieldMappingId = task.SettingsTask.Settings_RSO_MasterUpload.FieldMappingId,
                                Id = task.SettingsTask.Settings_RSO_MasterUpload.Id,
                                IsSplitFiles = task.SettingsTask.Settings_RSO_MasterUpload.IsSplitFiles,
                                RsosettingId = task.SettingsTask.Settings_RSO_MasterUpload.RsosettingId,
                                SkipFirstLine = task.SettingsTask.Settings_RSO_MasterUpload.SkipFirstLine,
                                TaskId = task.SettingsTask.Settings_RSO_MasterUpload.TaskId,
                                UploadType = task.SettingsTask.Settings_RSO_MasterUpload.UploadType
                            };
                            break;
                        case "3":
                            createTaskDto.SettingsTask.SettingsRsoScanUpload = new RSO_ScanUpload_DTO
                            {
                                TaskId = task.SettingsTask.Settings_RSO_ScanUpload.TaskId,
                                RsosettingId = task.SettingsTask.Settings_RSO_ScanUpload.RsosettingId,
                                Id = task.SettingsTask.Settings_RSO_ScanUpload.Id,
                                AutoBuyerId = task.SettingsTask.Settings_RSO_ScanUpload.AutoBuyerId,
                                AutoCreateFolder = task.SettingsTask.Settings_RSO_ScanUpload.AutoCreateFolder,
                                AutoDetection = task.SettingsTask.Settings_RSO_ScanUpload.AutoDetection,
                                BackupPath = task.SettingsTask.Settings_RSO_ScanUpload.BackupPath,
                                BatchListId = task.SettingsTask.Settings_RSO_ScanUpload.BatchListId,
                                BatchPath = task.SettingsTask.Settings_RSO_ScanUpload.BatchPath,
                                DefaultDocumentSystemName = task.SettingsTask.Settings_RSO_ScanUpload.DefaultDocumentSystemName,
                                MaxFileSize = task.SettingsTask.Settings_RSO_ScanUpload.MaxFileSize,
                                OverMaxSizeFilePath = task.SettingsTask.Settings_RSO_ScanUpload.OverMaxSizeFilePath
                            };
                            break;
                        case "4":
                            createTaskDto.SettingsTask.SettingsRsoScanDownload = new RSO_ScanDownload_DTO
                            {
                                Id = task.SettingsTask.Settings_RSO_ScanDownload.Id,
                                RsosettingId = task.SettingsTask.Settings_RSO_ScanDownload.RsosettingId,
                                CreateBuyerFolder = task.SettingsTask.Settings_RSO_ScanDownload.CreateBuyerFolder,
                                DownloadPath = task.SettingsTask.Settings_RSO_ScanDownload.DownloadPath,
                                FieldMappingId = task.SettingsTask.Settings_RSO_ScanDownload.FieldMappingId,
                                ImageLocationIndex = task.SettingsTask.Settings_RSO_ScanDownload.ImageLocationIndex,
                                TaskId = task.SettingsTask.Settings_RSO_ScanDownload.TaskId,
                                UsePermaLink = task.SettingsTask.Settings_RSO_ScanDownload.UsePermaLink

                            };
                            break;
                        case "5":
                            createTaskDto.SettingsTask.SettingsRsoSage50 = new RSO_Sage50_DTO
                            {
                                AttachmentPath = task.SettingsTask.Settings_RSO_Sage50.AttachmentPath,
                                Company = task.SettingsTask.Settings_RSO_Sage50.Company,
                                DatabasePath = task.SettingsTask.Settings_RSO_Sage50.DatabasePath,
                                TaskId = task.SettingsTask.Settings_RSO_Sage50.TaskId,
                                RsosettingId = task.SettingsTask.Settings_RSO_Sage50.RsosettingId,
                                Id = task.SettingsTask.Settings_RSO_Sage50.Id,
                                DownloadSettingId = task.SettingsTask.Settings_RSO_Sage50.DownloadSettingId,
                                HeaderFieldMappingId = task.SettingsTask.Settings_RSO_Sage50.HeaderFieldMappingId,
                                LineFieldMappingId = task.SettingsTask.Settings_RSO_Sage50.LineFieldMappingId,
                                Password = task.SettingsTask.Settings_RSO_Sage50.Password,
                                UserName = task.SettingsTask.Settings_RSO_Sage50.UserName

                            };
                            break;
                        case "6":
                            createTaskDto.SettingsTask.SettingsSage50Posting = new Sage50_Posting_DTO
                            {
                                UserName = task.SettingsTask.Settings_Sage50_Posting.UserName,
                                Password = task.SettingsTask.Settings_Sage50_Posting.Password,
                                LineFieldMappingId = task.SettingsTask.Settings_Sage50_Posting.LineFieldMappingId,
                                Id = task.SettingsTask.Settings_Sage50_Posting.Id,
                                HeaderFieldMappingId = task.SettingsTask.Settings_Sage50_Posting.HeaderFieldMappingId,
                                Company = task.SettingsTask.Settings_Sage50_Posting.Company,
                                AdestWebServiceSettingId = task.SettingsTask.Settings_Sage50_Posting.AdestWebServiceSettingId,
                                DebugLogSettingId = task.SettingsTask.Settings_Sage50_Posting.DebugLogSettingId,
                                TaskId = task.SettingsTask.Settings_Sage50_Posting.TaskId,
                                DatabasePath = task.SettingsTask.Settings_Sage50_Posting.DatabasePath
                            };
                            break;
                        case "7":
                            createTaskDto.SettingsTask.SettingsRsoXero = new RSO_Xero_DTO
                            {
                                Id = task.SettingsTask.Settings_RSO_Xero.Id,
                                AppName = task.SettingsTask.Settings_RSO_Xero.AppName,
                                Buyer = task.SettingsTask.Settings_RSO_Xero.Buyer,
                                ConsumerKey = task.SettingsTask.Settings_RSO_Xero.ConsumerKey,
                                ConsumerSecret = task.SettingsTask.Settings_RSO_Xero.ConsumerSecret,
                                DebugLogSettingId = task.SettingsTask.Settings_RSO_Xero.DebugLogSettingId,
                                DownloadSettingId = task.SettingsTask.Settings_RSO_Xero.DownloadSettingId,
                                InvoiceLogEnabled = task.SettingsTask.Settings_RSO_Xero.InvoiceLogEnabled,
                                InvoiceLogPath = task.SettingsTask.Settings_RSO_Xero.InvoiceLogPath,
                                OrganizationName = task.SettingsTask.Settings_RSO_Xero.OrganizationName,
                                RsosettingId = task.SettingsTask.Settings_RSO_Xero.RsosettingId,
                                TaskId = task.SettingsTask.Settings_RSO_Xero.TaskId,
                                UseXeroFailureSupplier = task.SettingsTask.Settings_RSO_Xero.UseXeroFailureSupplier,
                                XeroFailureSupplier = task.SettingsTask.Settings_RSO_Xero.XeroFailureSupplier
                            };
                            break;
                        case "8":
                            //TODO: SettingsIndex
                            break;

                    }

                    createTaskResult = _tasksService.CreateTask(createTaskDto, _tenantContext);

                }
            }
            catch (Exception ex)
            {
                _toastNotification.AddToastMessage("Create Task", $"{ex.Message}", ToastEnums.ToastType.Error, new ToastOption()
                {
                    PositionClass = NToastNotify.Constants.ToastPositions.TopCenter
                });
            }

            if (createTaskResult)
            {
                _toastNotification.AddToastMessage("Create Task", $"A new task was created with success!", ToastEnums.ToastType.Success, new ToastOption()
                {
                    PositionClass = NToastNotify.Constants.ToastPositions.TopCenter
                });

                return RedirectToAction("Display", task.SettingsTask.Id);
            }
            else
            {
                _toastNotification.AddToastMessage("Create Task", $"An error occurred creating the task!", ToastEnums.ToastType.Error, new ToastOption()
                {
                    PositionClass = NToastNotify.Constants.ToastPositions.TopCenter
                });

                return RedirectToAction("SelectTaskType", task.TaskTypeId);
            }


        }



        public IActionResult Display(int id)
        {
         

            try
            {
                vm = GetTaskDetails(id);

                vm.EditableContent = false;

                var taskTypeSettings = _tasksService.GetTaskTypeSettings(vm.TaskTypeId);

                vm.TaskTypeSettings = new List<TaskTypeSettingsViewModel>();

                foreach (var setting in taskTypeSettings)
                {
                    vm.TaskTypeSettings.Add(new TaskTypeSettingsViewModel()
                    {
                        Id = setting.Id,
                        IsCore = setting.IsCore,
                        MaxLength = setting.MaxLength,
                        MinLength = setting.MinLength,
                        SettingName = setting.SettingName,
                        TaskTypeId = setting.TaskTypeId,
                        FieldType = TaskTypeSettingsViewModel.GetFieldType(setting.Type),
                        Type = setting.Type
                    });
                }
            }
            catch (Exception ex)
            {
                _toastNotification.AddToastMessage("Ooops! Something wrong with your request!", $"{ex.Message}", ToastEnums.ToastType.Warning, new ToastOption()
                {
                    PositionClass = NToastNotify.Constants.ToastPositions.TopCenter
                });
            }


            return View(vm);
        }

        private CreateTaskViewModel GetTaskDetails(int id)
        {

            vm = new CreateTaskViewModel();

        

            //Get SettingsTaskById
            var settingTask = _tasksService.GetSettingTask(id, _tenantContext);


            //Cast to SettingsTaskViewModel
            vm.SettingsTask = new SettingsTaskViewModel()
            {
                Disabled = string.Equals(settingTask.Disabled.ToLower(), "false") ? false : true,
                EmailFailureSettingId = settingTask.EmailFailureSettingId,
                EmailSuccessSettingId = settingTask.EmailSuccessSettingId,
                EnableTracing = string.Equals(settingTask.EnableTracing.ToLower(), "false") ? false : true,
                EndTime = settingTask.EndTime,
                Id = settingTask.Id,
                Interval = settingTask.Interval,
                IntervalFailureRetry = settingTask.IntervalFailureRetry,
                MaxFailuresAllowed = settingTask.MaxFailuresAllowed,
                MaxLogFiles = settingTask.MaxLogFiles,
                RunOnStart = string.Equals(settingTask.RunOnStart.ToLower(), "false") ? false : true,
                StartTime = settingTask.StartTime,
                TaskTypeId = settingTask.TaskTypeId
            };

            vm.TaskTypeId = vm.SettingsTask.TaskTypeId;


            switch (vm.TaskTypeId.ToString())
            {
                case "1":
                    vm.SettingsTask.Settings_Sage200_Posting = new Sage200_Posting_ViewModel
                    {
                        TaskId = settingTask.SettingsSage200Posting.FirstOrDefault().TaskId.Value,
                        DebugLogSettingId = settingTask.SettingsSage200Posting.FirstOrDefault().DebugLogSettingId,
                        AdestWebServiceSettingId = settingTask.SettingsSage200Posting.FirstOrDefault().AdestWebServiceSettingId,
                        Company = settingTask.SettingsSage200Posting.FirstOrDefault().Company,
                        HeaderFieldMappingId = settingTask.SettingsSage200Posting.FirstOrDefault().HeaderFieldMappingId,
                        Id = settingTask.SettingsSage200Posting.FirstOrDefault().Id,
                        LineFieldMappingId = settingTask.SettingsSage200Posting.FirstOrDefault().LineFieldMappingId,
                        Password = settingTask.SettingsSage200Posting.FirstOrDefault().Password,
                        UserName = settingTask.SettingsSage200Posting.FirstOrDefault().UserName
                    };
                    break;
                case "2":
                    vm.SettingsTask.Settings_RSO_MasterUpload = new RSO_MasterUpload_ViewModel
                    {
                        BackupPath = settingTask.SettingsRsoMasterUpload.FirstOrDefault().BackupPath,
                        ClearBeforeUpload = settingTask.SettingsRsoMasterUpload.FirstOrDefault().ClearBeforeUpload,
                        CsvSeparator = settingTask.SettingsRsoMasterUpload.FirstOrDefault().CsvSeparator,
                        DataFilePath = settingTask.SettingsRsoMasterUpload.FirstOrDefault().DataFilePath,
                        DataFilePattern = settingTask.SettingsRsoMasterUpload.FirstOrDefault().DataFilePattern,
                        DataFilePatternExt = settingTask.SettingsRsoMasterUpload.FirstOrDefault().DataFilePatternExt,
                        DataFilePatternMask = settingTask.SettingsRsoMasterUpload.FirstOrDefault().DataFilePatternMask,
                        DbcommandText = settingTask.SettingsRsoMasterUpload.FirstOrDefault().DbcommandText,
                        DbcommandType = settingTask.SettingsRsoMasterUpload.FirstOrDefault().DbcommandType,
                        DbconnectionString = settingTask.SettingsRsoMasterUpload.FirstOrDefault().DbconnectionString,
                        ErrorPath = settingTask.SettingsRsoMasterUpload.FirstOrDefault().ErrorPath,
                        FieldMappingId = settingTask.SettingsRsoMasterUpload.FirstOrDefault().FieldMappingId,
                        Id = settingTask.SettingsRsoMasterUpload.FirstOrDefault().Id,
                        IsSplitFiles = settingTask.SettingsRsoMasterUpload.FirstOrDefault().IsSplitFiles,
                        RsosettingId = settingTask.SettingsRsoMasterUpload.FirstOrDefault().RsosettingId,
                        SkipFirstLine = settingTask.SettingsRsoMasterUpload.FirstOrDefault().SkipFirstLine,
                        TaskId = settingTask.SettingsRsoMasterUpload.FirstOrDefault().TaskId,
                        UploadType = settingTask.SettingsRsoMasterUpload.FirstOrDefault().UploadType
                    };
                    break;
                case "3":
                    vm.SettingsTask.Settings_RSO_ScanUpload = new RSO_ScanUpload_ViewModel
                    {
                        TaskId = settingTask.SettingsRsoScanUpload.FirstOrDefault().TaskId,
                        RsosettingId = settingTask.SettingsRsoScanUpload.FirstOrDefault().RsosettingId,
                        Id = settingTask.SettingsRsoScanUpload.FirstOrDefault().Id,
                        AutoBuyerId = settingTask.SettingsRsoScanUpload.FirstOrDefault().AutoBuyerId,
                        AutoCreateFolder = settingTask.SettingsRsoScanUpload.FirstOrDefault().AutoCreateFolder,
                        AutoDetection = settingTask.SettingsRsoScanUpload.FirstOrDefault().AutoDetection,
                        BackupPath = settingTask.SettingsRsoScanUpload.FirstOrDefault().BackupPath,
                        BatchListId = settingTask.SettingsRsoScanUpload.FirstOrDefault().BatchListId,
                        BatchPath = settingTask.SettingsRsoScanUpload.FirstOrDefault().BatchPath,
                        DefaultDocumentSystemName = settingTask.SettingsRsoScanUpload.FirstOrDefault().DefaultDocumentSystemName,
                        MaxFileSize = settingTask.SettingsRsoScanUpload.FirstOrDefault().MaxFileSize,
                        OverMaxSizeFilePath = settingTask.SettingsRsoScanUpload.FirstOrDefault().OverMaxSizeFilePath
                    };
                    break;
                case "4":
                    vm.SettingsTask.Settings_RSO_ScanDownload = new RSO_ScanDownload_ViewModel
                    {
                        Id = settingTask.SettingsRsoScanDownload.FirstOrDefault().Id,
                        RsosettingId = settingTask.SettingsRsoScanDownload.FirstOrDefault().RsosettingId,
                        CreateBuyerFolder = settingTask.SettingsRsoScanDownload.FirstOrDefault().CreateBuyerFolder,
                        DownloadPath = settingTask.SettingsRsoScanDownload.FirstOrDefault().DownloadPath,
                        FieldMappingId = settingTask.SettingsRsoScanDownload.FirstOrDefault().FieldMappingId,
                        ImageLocationIndex = settingTask.SettingsRsoScanDownload.FirstOrDefault().ImageLocationIndex,
                        TaskId = settingTask.SettingsRsoScanDownload.FirstOrDefault().TaskId.Value,
                        UsePermaLink = settingTask.SettingsRsoScanDownload.FirstOrDefault().UsePermaLink

                    };
                    break;
                case "5":
                    vm.SettingsTask.Settings_RSO_Sage50 = new RSO_Sage50_ViewModel
                    {
                        AttachmentPath = settingTask.SettingsRsoSage50.FirstOrDefault().AttachmentPath,
                        Company = settingTask.SettingsRsoSage50.FirstOrDefault().Company,
                        DatabasePath = settingTask.SettingsRsoSage50.FirstOrDefault().DatabasePath,
                        TaskId = settingTask.SettingsRsoSage50.FirstOrDefault().TaskId.Value,
                        RsosettingId = settingTask.SettingsRsoSage50.FirstOrDefault().RsosettingId,
                        Id = settingTask.SettingsRsoSage50.FirstOrDefault().Id,
                        DownloadSettingId = settingTask.SettingsRsoSage50.FirstOrDefault().DownloadSettingId,
                        HeaderFieldMappingId = settingTask.SettingsRsoSage50.FirstOrDefault().HeaderFieldMappingId,
                        LineFieldMappingId = settingTask.SettingsRsoSage50.FirstOrDefault().LineFieldMappingId,
                        Password = settingTask.SettingsRsoSage50.FirstOrDefault().Password,
                        UserName = settingTask.SettingsRsoSage50.FirstOrDefault().UserName

                    };
                    break;
                case "6":
                    vm.SettingsTask.Settings_Sage50_Posting = new Sage50_Posting_ViewModel
                    {
                        UserName = settingTask.SettingsSage50Posting.FirstOrDefault().UserName,
                        Password = settingTask.SettingsSage50Posting.FirstOrDefault().Password,
                        LineFieldMappingId = settingTask.SettingsSage50Posting.FirstOrDefault().LineFieldMappingId,
                        Id = settingTask.SettingsSage50Posting.FirstOrDefault().Id,
                        HeaderFieldMappingId = settingTask.SettingsSage50Posting.FirstOrDefault().HeaderFieldMappingId,
                        Company = settingTask.SettingsSage50Posting.FirstOrDefault().Company,
                        AdestWebServiceSettingId = settingTask.SettingsSage50Posting.FirstOrDefault().AdestWebServiceSettingId,
                        DebugLogSettingId = settingTask.SettingsSage50Posting.FirstOrDefault().DebugLogSettingId,
                        TaskId = settingTask.SettingsSage50Posting.FirstOrDefault().TaskId.Value,
                        DatabasePath = settingTask.SettingsSage50Posting.FirstOrDefault().DatabasePath
                    };
                    break;
                case "7":
                    vm.SettingsTask.Settings_RSO_Xero = new RSO_Xero_ViewModel
                    {
                        Id = settingTask.SettingsRsoXero.FirstOrDefault().Id,
                        AppName = settingTask.SettingsRsoXero.FirstOrDefault().AppName,
                        Buyer = settingTask.SettingsRsoXero.FirstOrDefault().Buyer,
                        ConsumerKey = settingTask.SettingsRsoXero.FirstOrDefault().ConsumerKey,
                        ConsumerSecret = settingTask.SettingsRsoXero.FirstOrDefault().ConsumerSecret,
                        DebugLogSettingId = settingTask.SettingsRsoXero.FirstOrDefault().DebugLogSettingId,
                        DownloadSettingId = settingTask.SettingsRsoXero.FirstOrDefault().DownloadSettingId,
                        InvoiceLogEnabled = settingTask.SettingsRsoXero.FirstOrDefault().InvoiceLogEnabled,
                        InvoiceLogPath = settingTask.SettingsRsoXero.FirstOrDefault().InvoiceLogPath,
                        OrganizationName = settingTask.SettingsRsoXero.FirstOrDefault().OrganizationName,
                        RsosettingId = settingTask.SettingsRsoXero.FirstOrDefault().RsosettingId,
                        TaskId = settingTask.SettingsRsoXero.FirstOrDefault().TaskId.Value,
                        UseXeroFailureSupplier = string.Equals(settingTask.SettingsRsoXero.FirstOrDefault().UseXeroFailureSupplier.ToLower(), "false") ? false : true,
                        XeroFailureSupplier = settingTask.SettingsRsoXero.FirstOrDefault().XeroFailureSupplier
                    };
                    break;
                case "8":
                    //TODO: SettingsIndex
                    break;

            }

            return vm;
        }

        public IActionResult Edit(int id)
        {
            vm = GetTaskDetails(id);

            vm.EditableContent = true;

            return View(vm);
        }


        [HttpPost]
        public IActionResult Save(CreateTaskViewModel task)
        {
            if (ModelState.IsValid)
            {
                _toastNotification.AddToastMessage("Save Task", $"Task updated with success!", ToastEnums.ToastType.Success, new ToastOption()
                {
                    PositionClass = NToastNotify.Constants.ToastPositions.TopCenter
                });
            }
            else
            {
                _toastNotification.AddToastMessage("Create Task", $"Failed to update task!", ToastEnums.ToastType.Error, new ToastOption()
                {
                    PositionClass = NToastNotify.Constants.ToastPositions.TopCenter
                });
            }

            return RedirectToAction("Display", task.SettingsTask.Id);
        }
    }
}