﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskSchedulerManager.Services.Interfaces;
using TaskSchedulerManager.Web.Models.TasksViewModels;

namespace TaskSchedulerManager.Web.ViewComponents
{
     [ViewComponent(Name = "TaskTypesSelectList")]
    public class TaskTypesSelectList : ViewComponent
    {
        private ITasksService _tasksService;
        SelectTaskTypeViewModel vm;

        public TaskTypesSelectList(ITasksService tasksService)
        {
            _tasksService = tasksService;



            
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            vm = new SelectTaskTypeViewModel();

            vm.TaskTypesList = new List<Microsoft.AspNetCore.Mvc.Rendering.SelectListItem>();

            var dto = _tasksService.GetAll();

            foreach (var task in dto)
            {
                vm.TaskTypesList.Add(new SelectListItem() { Text = task.Description, Value = task.TaskTypeId.ToString() });
            }

            return View(vm);
        }
    }
}
