﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using TaskSchedulerManager.Web.Services;
using TaskSchedulerManager.Data.Identity.Models;
using TaskSchedulerManager.Services.Interfaces;
using TaskSchedulerManager.Services;
using TaskSchedulerManager.Repositories;
using TaskSchedulerManager.Repositories.Interfaces;
using NToastNotify;
using TaskSchedulerManager.Data.MainDb.Models;
using TaskSchedulerManager.Middleware;
using TaskSchedulerManager.Data.TenantDb.Models;

namespace TaskSchedulerManager.Web
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            if (env.IsDevelopment())
            {
                // For more details on using the user secret store see https://go.microsoft.com/fwlink/?LinkID=532709
                builder.AddUserSecrets<Startup>();
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddOptions();

            // Add framework services.
            services.AddDbContext<MainContext>(options =>
               options.UseSqlite(Configuration.GetConnectionString("DefaultConnection")));

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlite(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>(config =>
            {
                config.SignIn.RequireConfirmedEmail = true;
                config.Lockout.MaxFailedAccessAttempts = 3;
                config.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(60);
                config.User.RequireUniqueEmail = true;
            })
            .AddEntityFrameworkStores<ApplicationDbContext>()
            .AddDefaultTokenProviders();


            services.AddDbContext<TenantContext>(options =>
             options.UseSqlite(Configuration.GetConnectionString("DefaultConnection")));


            services.AddNToastNotify(new ToastOption()
            {
                ProgressBar = false,
                PositionClass = NToastNotify.Constants.ToastPositions.BottomCenter
            });

            services.AddMvc();


            // Add application services.
            services.AddTransient<IEmailSender, AuthMessageSender>();
            services.AddTransient<ISmsSender, AuthMessageSender>();

            services.AddTransient<IRolesService, RolesService>();

            services.AddTransient<IMainService, MainService>();
            services.AddTransient<ITasksService, TasksService>();

            services.AddTransient<IOrganizationSettingsRepository, OrganizationSettingsRepository>();
            services.AddTransient<ITaskTypesRepository, TaskTypesRepository>();
            services.AddTransient<ITaskTypeSettingsRepository, TaskTypeSettingsRepository>();
            services.AddTransient<ISharedSettingsRepository, SharedSettingsRepository>();

            services.AddTransient<ITenantSettingsTasksRepository, TenantSettingsTasksRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IRolesService rolesService)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseIdentity();

            // Add external authentication middleware below. To configure them please see https://go.microsoft.com/fwlink/?LinkID=532715

            app.UseTenantMiddleware();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            rolesService.InitializeAsync();
        }
    }
}
