﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskSchedulerManager.Web.Models.TasksViewModels
{
    public class TaskTypesViewModel
    {
        public TaskTypesViewModel()
        {
            TaskTypeSettings = new HashSet<TaskTypeSettingsViewModel>();
        }


        public long TaskTypeId { get; set; }
        public string Description { get; set; }
        public string AssemblyName { get; set; }
        public string AssemblyType { get; set; }
        public string SettingsTable { get; set; }

        public virtual ICollection<TaskTypeSettingsViewModel> TaskTypeSettings { get; set; }
    }
}
