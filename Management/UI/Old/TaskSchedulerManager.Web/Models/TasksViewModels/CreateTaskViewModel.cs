﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Options;
using TaskSchedulerManager.Services.Interfaces;

namespace TaskSchedulerManager.Web.Models.TasksViewModels
{
    public class CreateTaskViewModel
    {

        private ITasksService _tasksService;

        public long TaskTypeId { get; set; }


        public bool EditableContent { get; set; }

        public bool ReadFieldValue { get; set; }

        public SettingsTaskViewModel SettingsTask { get; set; }

        public List<TaskTypeSettingsViewModel> TaskTypeSettings { get; set; }

        public List<SelectListItem> TryGetFKFieldListOfValues(ITasksService tasksService, IOptions<TaskSchedulerManager.Middleware.TenantMiddlewareOptions> options, string settingName)
        {
            _tasksService = tasksService;

            List<SelectListItem> items = new List<SelectListItem>();

            switch (settingName)
            {
                case "FieldMappingId":
                case "LineFieldMappingId":
                case "HeaderFieldMappingId":
                case "DebugLogSettingId":
                case "AdestWebServiceSettingId":
                case "RSOSettingId":
                case "BatchListId":
                case "DownloadSettingId":
                case "EmailSuccessSettingId":
                case "EmailFailureSettingId":


                    items = _tasksService.GetSharedSettingsByName(settingName, options.Value.TenantDbContext);

                    break;

                default:
                    break;
            }



            return items;
        }
    }
}
