﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskSchedulerManager.Web.Models.TasksViewModels
{
    public class SettingsTaskViewModel
    {
        public SettingsTaskViewModel()
        {
            Settings_RSO_MasterUpload = new RSO_MasterUpload_ViewModel();
            Settings_RSO_Sage50 = new RSO_Sage50_ViewModel();
            Settings_RSO_ScanDownload = new RSO_ScanDownload_ViewModel();
            Settings_RSO_ScanUpload = new RSO_ScanUpload_ViewModel();
            Settings_RSO_Xero = new RSO_Xero_ViewModel();
            Settings_Sage200_Posting = new Sage200_Posting_ViewModel();
            Settings_Sage50_Posting = new Sage50_Posting_ViewModel();
            //SettingsTaskSubMainTask = new HashSet<SettingsTaskSub>();
            //SettingsTaskSubSubTask = new HashSet<SettingsTaskSub>();
        }

        public long Id { get; set; }
        public long TaskTypeId { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public long Interval { get; set; }
        public long TotalRunPerTimeRange { get; set; }
        public bool Disabled { get; set; }
        public bool EnableTracing { get; set; }
        public long MaxLogFiles { get; set; }
        public long EmailSuccessSettingId { get; set; }
        public long EmailFailureSettingId { get; set; }
        public bool RunOnStart { get; set; }
        public long? MaxFailuresAllowed { get; set; }
        public long? IntervalFailureRetry { get; set; }

        public virtual RSO_MasterUpload_ViewModel Settings_RSO_MasterUpload { get; set; }
        public virtual RSO_Sage50_ViewModel Settings_RSO_Sage50 { get; set; }
        public virtual RSO_ScanDownload_ViewModel Settings_RSO_ScanDownload { get; set; }
        public virtual RSO_ScanUpload_ViewModel Settings_RSO_ScanUpload { get; set; }
        public virtual RSO_Xero_ViewModel Settings_RSO_Xero { get; set; }
        public virtual Sage200_Posting_ViewModel Settings_Sage200_Posting { get; set; }
        public virtual Sage50_Posting_ViewModel Settings_Sage50_Posting { get; set; }
        //public virtual ICollection<SettingsTaskSub> SettingsTaskSubMainTask { get; set; }
        //public virtual ICollection<SettingsTaskSub> SettingsTaskSubSubTask { get; set; }
        //public virtual SettingsSharedEmail EmailFailureSetting { get; set; }
        //public virtual SettingsSharedEmail EmailSuccessSetting { get; set; }
    }
}
