﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskSchedulerManager.Web.Models.TasksViewModels
{
    public class RSO_ScanUpload_ViewModel
    {
        public long Id { get; set; }
        public long TaskId { get; set; }
        public string AutoDetection { get; set; }
        public string AutoBuyerId { get; set; }
        public string BatchPath { get; set; }
        public long? MaxFileSize { get; set; }
        public string DefaultDocumentSystemName { get; set; }
        public string OverMaxSizeFilePath { get; set; }
        public string BackupPath { get; set; }
        public string AutoCreateFolder { get; set; }
        public long? BatchListId { get; set; }
        public long? RsosettingId { get; set; }
    }
}
