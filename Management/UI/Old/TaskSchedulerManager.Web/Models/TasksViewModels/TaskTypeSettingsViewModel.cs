﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Options;
using TaskSchedulerManager.Services.Interfaces;

namespace TaskSchedulerManager.Web.Models.TasksViewModels
{
    public class TaskTypeSettingsViewModel
    {


     
        
        public long Id { get; set; }


        public long TaskTypeId { get; set; }

      
        public string SettingName { get; set; }


        public long Type { get; set; }

        
        public string FieldType { get; set; }


        public long? MinLength { get; set; }

        public long? MaxLength { get; set; }


        public string IsCore { get; set; }


        public List<SelectListItem> ListItems { get; set; }

        public virtual TaskTypesViewModel TaskType { get; set; }




        public static string GetFieldType(long fieldTypeId)
        {
            string fieldType = string.Empty;

            switch (fieldTypeId)
            {
                case 1:
                    fieldType = "text";
                    break;
                case 2:
                    fieldType = "number";
                    break;
                case 3:
                    fieldType = "datetime-local";
                    break;
                case 4:
                    fieldType = "boolean";
                    break;
            }

            return fieldType;
        }

      
    }
}
