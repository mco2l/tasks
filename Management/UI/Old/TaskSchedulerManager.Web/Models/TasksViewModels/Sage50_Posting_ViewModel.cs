﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskSchedulerManager.Web.Models.TasksViewModels
{
    public class Sage50_Posting_ViewModel
    {
        public long Id { get; set; }
        public long TaskId { get; set; }
        public string Company { get; set; }
        public string DatabasePath { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public long AdestWebServiceSettingId { get; set; }
        public long DebugLogSettingId { get; set; }
        public long HeaderFieldMappingId { get; set; }
        public long? LineFieldMappingId { get; set; }
    }
}
