﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Mvc.Rendering;
using TaskSchedulerManager.Data.MainDb.Models;
using TaskSchedulerManager.Domain;


namespace TaskSchedulerManager.Repositories.Interfaces
{
    public interface ITaskTypesRepository : IGenericRepository<TaskTypes>
    {

        TaskTypes GetSingle(long taskTypeId);
      
    }
}
