﻿using System;
using System.Collections.Generic;
using System.Text;
using TaskSchedulerManager.Data.MainDb.Models;

namespace TaskSchedulerManager.Repositories.Interfaces
{
    public interface IOrganizationSettingsRepository : IGenericRepository<OrganizationSettings>
    {
        OrganizationSettings GetSingle(long orgSettingId);

    }
}
