﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Mvc.Rendering;
using TaskSchedulerManager.Data.TenantDb.Models;

namespace TaskSchedulerManager.Repositories.Interfaces
{
    public interface ISharedSettingsRepository : IGenericRepository<SettingsSharedFieldMapping>
    {
        List<SelectListItem> GetSharedSettingsByName(string sharedSettingName, TenantContext tenantContext);
    }
}
