﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Mvc.Rendering;
using TaskSchedulerManager.Data.MainDb.Models;

namespace TaskSchedulerManager.Repositories.Interfaces
{
    public interface ITaskTypeSettingsRepository : IGenericRepository<TaskTypeSettings>
    {
        IEnumerable<TaskTypeSettings> GetAllByTaskTypeID(long taskTypeId);

       
    }
}
