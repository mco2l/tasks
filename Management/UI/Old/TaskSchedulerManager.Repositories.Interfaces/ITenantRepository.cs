﻿using System;
using System.Collections.Generic;
using System.Text;
using TaskSchedulerManager.Data.TenantDb.Models;

namespace TaskSchedulerManager.Repositories.Interfaces
{
    public interface ITenantRepository
    {
        bool InsertSingleRecord(TenantContext context, string tableName, Dictionary<string, string> record);

        bool UpdateRecord(TenantContext context, string tableName, Dictionary<string, string> record);

        Dictionary<string, string> GetRecordById(TenantContext context, string tableName, int recordId);
    }
}
