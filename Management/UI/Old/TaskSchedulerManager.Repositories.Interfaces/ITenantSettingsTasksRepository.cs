﻿using System;
using System.Collections.Generic;
using System.Text;
using TaskSchedulerManager.Data.TenantDb.Models;

namespace TaskSchedulerManager.Repositories.Interfaces
{
    public interface ITenantSettingsTasksRepository :
                                                    IGenericRepository<SettingsTask>,
                                                    IGenericRepository<SettingsRsoMasterUpload>,
                                                    IGenericRepository<SettingsRsoSage50>,
                                                    IGenericRepository<SettingsRsoScanDownload>,
                                                    IGenericRepository<SettingsRsoScanUpload>,
                                                    IGenericRepository<SettingsRsoXero>,
                                                    IGenericRepository<SettingsSage200Posting>,
                                                    IGenericRepository<SettingsSage50Posting>

    {
        TenantContext SetTenantContext(TenantContext context);
       
    }
}
