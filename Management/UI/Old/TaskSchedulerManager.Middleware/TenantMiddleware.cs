﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TaskSchedulerManager.Data.Identity.Models;
using TaskSchedulerManager.Data.TenantDb.Models;
using TaskSchedulerManager.Services.Interfaces;

namespace TaskSchedulerManager.Middleware
{
    public class TenantMiddleware
    {
        private RequestDelegate _next;
        private IOptions<TenantMiddlewareOptions> _options;
        private UserManager<ApplicationUser> _userManager;
        private IMainService _mainService;
        private RoleManager<IdentityRole> _roleManager;
        private TenantMiddlewareOptions _optionsAccessor;

        public TenantMiddleware(RequestDelegate next, IOptions<TenantMiddlewareOptions> options)
        {
            _next = next;
            _options = options;
            _optionsAccessor = options.Value;
        }

        public Task Invoke(HttpContext httpContext, UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager, IMainService mainService)
        {

            if (httpContext != null)
            {
                _mainService = mainService;

                _userManager = userManager;

                var userId = _userManager.GetUserId(httpContext.User);

                if (userId != null)
                {
                    Task<ApplicationUser> user = _userManager.FindByIdAsync(userId);

                    if (user.Result != null)
                    {
                        if (user.Result.TenantId.HasValue && user.Result.TenantId.Value != -1)
                        {
                            var tenantID = user.Result.TenantId.Value;

                            var organization = _mainService.GetOrganizationById(tenantID);

                            
                            if (!string.IsNullOrEmpty(organization.TenantDatabase))
                            {
                                _options.Value.TenantDbContext = new TenantContext("TenantDb_" + organization.TenantDatabase);

                                //Ensure the database exists
                                _options.Value.TenantDbContext.Database.EnsureCreatedAsync();

                                _options.Value.TenantName = organization.TenantName;
                               
                                
                            }
                            else
                            {
                                //remove users that have no tenant
                                foreach (var login in user.Result.Logins)
                                {
                                    userManager.RemoveLoginAsync(user.Result, login.LoginProvider, login.ProviderKey);
                                }

                                var roles = _userManager.GetRolesAsync(user.Result);

                                foreach (var item in roles.Result)
                                {
                                    // item should be the name of the role
                                    var result =  userManager.RemoveFromRoleAsync(user.Result, item);
                                }



                                userManager.DeleteAsync(user.Result);
                            }

                            //might be a user, or admin
                            _options.Value.GrantSuperUserAccess = false;
                        }
                        else
                        {
                            _roleManager = roleManager;

                            var roles = _userManager.GetRolesAsync(user.Result);

                            if(roles.Result.Contains("SuperUser"))
                            {
                                //only super user. No need to define context.
                                _options.Value.TenantName = "Glantus";
                                _options.Value.GrantSuperUserAccess = true;

                            }

                        }

                    }
                }
                else
                {
                    //reset middleware options for logged out users
                    _options.Value.TenantName = string.Empty;
                    _options.Value.TenantDbContext = null;
                    _options.Value.GrantSuperUserAccess = false;
                }
            }

            return _next(httpContext);
        }
    }



    public static class MiddlewareExtensions
    {
        public static IApplicationBuilder UseTenantMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<TenantMiddleware>();
        }
    }
}
