﻿using TaskSchedulerManager.Data.MainDb.Models;
using TaskSchedulerManager.Data.TenantDb.Models;

namespace TaskSchedulerManager.Middleware
{
    public class TenantMiddlewareOptions
    {
        public MainContext MainDbContext { get; set; }

        public TenantContext TenantDbContext { get; set; }
        public string TenantName { get; set; }
        public bool GrantSuperUserAccess { get; set; }
    }
}