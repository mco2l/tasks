﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Microsoft.AspNetCore.Mvc.Rendering;
using TaskSchedulerManager.Data.MainDb.Models;
using TaskSchedulerManager.Data.TenantDb.Models;
using TaskSchedulerManager.Domain;
using TaskSchedulerManager.Repositories.Interfaces;
using TaskSchedulerManager.Services.Interfaces;

namespace TaskSchedulerManager.Services
{
    public class TasksService : ITasksService
    {
        private ITaskTypesRepository _repository;
        private ITaskTypeSettingsRepository _repositoryTaskTypeSettings;
        private ISharedSettingsRepository _sharedSettingsRepository;
        private ITenantSettingsTasksRepository _tenantRepository;

        public TasksService(ITaskTypesRepository repository,
                            ITaskTypeSettingsRepository repositoryTaskTypeSettings,
                            ISharedSettingsRepository sharedSettingsRepository,
                            ITenantSettingsTasksRepository tenantRepository)
        {
            _repository = repository;
            _repositoryTaskTypeSettings = repositoryTaskTypeSettings;
            _sharedSettingsRepository = sharedSettingsRepository;

            _tenantRepository = tenantRepository;
        }
        public IEnumerable<TaskTypesDTO> GetAll()
        {
            var allTaskTypes = _repository.GetAll();

            var dto = new List<TaskTypesDTO>();

            foreach (var t in allTaskTypes)
            {
                var taskDTO = new TaskTypesDTO()
                {
                    AssemblyName = t.AssemblyName,
                    AssemblyType = t.AssemblyType,
                    Description = t.Description,
                    SettingsTable = t.SettingsTable,
                    TaskTypeId = t.TaskTypeId
                };

                taskDTO.TaskTypeSettings = new List<TaskTypeSettingsDTO>();

                foreach (var setting in t.TaskTypeSettings)
                {
                    taskDTO.TaskTypeSettings.Add(new TaskTypeSettingsDTO()
                    {
                        Id = setting.Id,
                        IsCore = setting.IsCore,
                        MaxLength = setting.MaxLength,
                        MinLength = setting.MinLength,
                        SettingName = setting.SettingName,
                        TaskTypeId = setting.TaskTypeId,
                        Type = setting.Type
                    });
                }

                dto.Add(taskDTO);
            }

            return dto;
        }

        public IEnumerable<TaskTypeSettingsDTO> GetTaskTypeSettings(long taskTypeId)
        {

            var taskTypeSettings = _repositoryTaskTypeSettings.GetAllByTaskTypeID(taskTypeId);

            List<TaskTypeSettingsDTO> listTaskTypeSettingsDTO = new List<TaskTypeSettingsDTO>();

            foreach (var setting in taskTypeSettings)
            {
                var taskTypeSettingDTO = new TaskTypeSettingsDTO()
                {
                    Id = setting.Id,
                    IsCore = setting.IsCore,
                    MaxLength = setting.MaxLength,
                    MinLength = setting.MinLength,
                    SettingName = setting.SettingName,
                    TaskTypeId = setting.TaskTypeId,
                    Type = setting.Type
                };

                listTaskTypeSettingsDTO.Add(taskTypeSettingDTO);
            }

            return listTaskTypeSettingsDTO;
        }

        public List<SelectListItem> GetSharedSettingsByName(string sharedSettingName, TenantContext tenantContext)
        {
            return _sharedSettingsRepository.GetSharedSettingsByName(sharedSettingName, tenantContext);
        }

        public bool CreateTask(CreateTaskDTO createTaskDto, TenantContext tenantContext)
        {
            bool operationResult = false;

            try
            {
                _tenantRepository.SetTenantContext(tenantContext);

                //casting from DTO to Model
                SettingsTask task = new SettingsTask()
                {
                    //Id = createTaskDto.SettingsTask.Id,

                    Disabled = createTaskDto.SettingsTask.Disabled.ToString(),
                    EmailFailureSettingId = createTaskDto.SettingsTask.EmailFailureSettingId,
                    EmailSuccessSettingId = createTaskDto.SettingsTask.EmailSuccessSettingId,
                    EnableTracing = createTaskDto.SettingsTask.EnableTracing.ToString(),
                    EndTime = createTaskDto.SettingsTask.EndTime,
                    Interval = createTaskDto.SettingsTask.Interval,
                    IntervalFailureRetry = createTaskDto.SettingsTask.IntervalFailureRetry,
                    MaxFailuresAllowed = createTaskDto.SettingsTask.MaxFailuresAllowed,
                    MaxLogFiles = createTaskDto.SettingsTask.MaxLogFiles,
                    RunOnStart = createTaskDto.SettingsTask.RunOnStart.ToString(),
                    StartTime = createTaskDto.SettingsTask.StartTime,
                    TaskTypeId = createTaskDto.SettingsTask.TaskTypeId,
                    TotalRunPerTimeRange = createTaskDto.SettingsTask.TotalRunPerTimeRange
                };

                switch (task.TaskTypeId.ToString())
                {
                    case "1":
                        task.SettingsSage200Posting = new List<SettingsSage200Posting>()
                        {
                            new SettingsSage200Posting()
                            {
                                TaskId = createTaskDto.SettingsTask.SettingsSage200Posting.TaskId.Value,
                                DebugLogSettingId = createTaskDto.SettingsTask.SettingsSage200Posting.DebugLogSettingId.Value,
                                AdestWebServiceSettingId = createTaskDto.SettingsTask.SettingsSage200Posting.AdestWebServiceSettingId,
                                Company = createTaskDto.SettingsTask.SettingsSage200Posting.Company,
                                HeaderFieldMappingId = createTaskDto.SettingsTask.SettingsSage200Posting.HeaderFieldMappingId,
                                Id = createTaskDto.SettingsTask.SettingsSage200Posting.Id,
                                LineFieldMappingId = createTaskDto.SettingsTask.SettingsSage200Posting.LineFieldMappingId,
                                Password = createTaskDto.SettingsTask.SettingsSage200Posting.Password,
                                UserName = createTaskDto.SettingsTask.SettingsSage200Posting.UserName
                            }

                        };
                        break;

                    case "2":
                        task.SettingsRsoMasterUpload = new List<SettingsRsoMasterUpload>()
                        {
                            new SettingsRsoMasterUpload()
                            {
                                 BackupPath = createTaskDto.SettingsTask.SettingsRsoMasterUpload.BackupPath,
                            ClearBeforeUpload = createTaskDto.SettingsTask.SettingsRsoMasterUpload.ClearBeforeUpload,
                            CsvSeparator = createTaskDto.SettingsTask.SettingsRsoMasterUpload.CsvSeparator,
                            DataFilePath = createTaskDto.SettingsTask.SettingsRsoMasterUpload.DataFilePath,
                            DataFilePattern = createTaskDto.SettingsTask.SettingsRsoMasterUpload.DataFilePattern,
                            DataFilePatternExt = createTaskDto.SettingsTask.SettingsRsoMasterUpload.DataFilePatternExt,
                            DataFilePatternMask = createTaskDto.SettingsTask.SettingsRsoMasterUpload.DataFilePatternMask,
                            DbcommandText = createTaskDto.SettingsTask.SettingsRsoMasterUpload.DbcommandText,
                            DbcommandType = createTaskDto.SettingsTask.SettingsRsoMasterUpload.DbcommandType,
                            DbconnectionString = createTaskDto.SettingsTask.SettingsRsoMasterUpload.DbconnectionString,
                            ErrorPath = createTaskDto.SettingsTask.SettingsRsoMasterUpload.ErrorPath,
                            FieldMappingId = createTaskDto.SettingsTask.SettingsRsoMasterUpload.FieldMappingId,
                            Id = createTaskDto.SettingsTask.SettingsRsoMasterUpload.Id,
                            IsSplitFiles = createTaskDto.SettingsTask.SettingsRsoMasterUpload.IsSplitFiles,
                            RsosettingId = createTaskDto.SettingsTask.SettingsRsoMasterUpload.RsosettingId,
                            SkipFirstLine = createTaskDto.SettingsTask.SettingsRsoMasterUpload.SkipFirstLine,
                            TaskId = createTaskDto.SettingsTask.SettingsRsoMasterUpload.TaskId,
                            UploadType = createTaskDto.SettingsTask.SettingsRsoMasterUpload.UploadType
                            }

                        };
                        break;

                    case "3":
                        task.SettingsRsoScanUpload = new List<SettingsRsoScanUpload>()
                        {
                            new SettingsRsoScanUpload()
                            {
                                TaskId = createTaskDto.SettingsTask.SettingsRsoScanUpload.TaskId.Value,
                                RsosettingId = createTaskDto.SettingsTask.SettingsRsoScanUpload.RsosettingId,
                                Id = createTaskDto.SettingsTask.SettingsRsoScanUpload.Id,
                                AutoBuyerId = createTaskDto.SettingsTask.SettingsRsoScanUpload.AutoBuyerId,
                                AutoCreateFolder = createTaskDto.SettingsTask.SettingsRsoScanUpload.AutoCreateFolder,
                                AutoDetection = createTaskDto.SettingsTask.SettingsRsoScanUpload.AutoDetection,
                                BackupPath = createTaskDto.SettingsTask.SettingsRsoScanUpload.BackupPath,
                                BatchListId = createTaskDto.SettingsTask.SettingsRsoScanUpload.BatchListId,
                                BatchPath = createTaskDto.SettingsTask.SettingsRsoScanUpload.BatchPath,
                                DefaultDocumentSystemName = createTaskDto.SettingsTask.SettingsRsoScanUpload.DefaultDocumentSystemName,
                                MaxFileSize = createTaskDto.SettingsTask.SettingsRsoScanUpload.MaxFileSize,
                                OverMaxSizeFilePath = createTaskDto.SettingsTask.SettingsRsoScanUpload.OverMaxSizeFilePath
                            }
                        };
                        break;

                    case "4":
                        task.SettingsRsoScanDownload = new List<SettingsRsoScanDownload>
                        {
                            new SettingsRsoScanDownload()
                            {
                                Id = createTaskDto.SettingsTask.SettingsRsoScanDownload.Id,
                            RsosettingId = createTaskDto.SettingsTask.SettingsRsoScanDownload.RsosettingId,
                            CreateBuyerFolder = createTaskDto.SettingsTask.SettingsRsoScanDownload.CreateBuyerFolder,
                            DownloadPath = createTaskDto.SettingsTask.SettingsRsoScanDownload.DownloadPath,
                            FieldMappingId = createTaskDto.SettingsTask.SettingsRsoScanDownload.FieldMappingId,
                            ImageLocationIndex = createTaskDto.SettingsTask.SettingsRsoScanDownload.ImageLocationIndex,
                            TaskId = createTaskDto.SettingsTask.SettingsRsoScanDownload.TaskId,
                            UsePermaLink = createTaskDto.SettingsTask.SettingsRsoScanDownload.UsePermaLink
                            }
                        };
                        break;

                    case "5":
                        task.SettingsRsoSage50 = new List<SettingsRsoSage50>
                        {
                            new SettingsRsoSage50()
                            {
                                AttachmentPath = createTaskDto.SettingsTask.SettingsRsoSage50.AttachmentPath,
                                Company = createTaskDto.SettingsTask.SettingsRsoSage50.Company,
                                DatabasePath = createTaskDto.SettingsTask.SettingsRsoSage50.DatabasePath,
                                TaskId = createTaskDto.SettingsTask.SettingsRsoSage50.TaskId,
                                RsosettingId = createTaskDto.SettingsTask.SettingsRsoSage50.RsosettingId,
                                Id = createTaskDto.SettingsTask.SettingsRsoSage50.Id,
                                DownloadSettingId = createTaskDto.SettingsTask.SettingsRsoSage50.DownloadSettingId,
                                HeaderFieldMappingId = createTaskDto.SettingsTask.SettingsRsoSage50.HeaderFieldMappingId,
                                LineFieldMappingId = createTaskDto.SettingsTask.SettingsRsoSage50.LineFieldMappingId,
                                Password = createTaskDto.SettingsTask.SettingsRsoSage50.Password,
                                UserName = createTaskDto.SettingsTask.SettingsRsoSage50.UserName
                            }

                        };
                        break;

                    case "6":
                        task.SettingsSage50Posting = new List<SettingsSage50Posting>
                        {
                            new SettingsSage50Posting()
                            {
                                UserName = createTaskDto.SettingsTask.SettingsSage50Posting.UserName,
                                Password = createTaskDto.SettingsTask.SettingsSage50Posting.Password,
                                LineFieldMappingId = createTaskDto.SettingsTask.SettingsSage50Posting.LineFieldMappingId,
                                Id = createTaskDto.SettingsTask.SettingsSage50Posting.Id,
                                HeaderFieldMappingId = createTaskDto.SettingsTask.SettingsSage50Posting.HeaderFieldMappingId,
                                Company = createTaskDto.SettingsTask.SettingsSage50Posting.Company,
                                AdestWebServiceSettingId = createTaskDto.SettingsTask.SettingsSage50Posting.AdestWebServiceSettingId,
                                DebugLogSettingId = createTaskDto.SettingsTask.SettingsSage50Posting.DebugLogSettingId,
                                TaskId = createTaskDto.SettingsTask.SettingsSage50Posting.TaskId,
                                DatabasePath = createTaskDto.SettingsTask.SettingsSage50Posting.DatabasePath
                            }

                        };
                        break;

                    case "7":
                        task.SettingsRsoXero = new List<SettingsRsoXero>
                        {
                            new SettingsRsoXero()
                            {
                                Id = createTaskDto.SettingsTask.SettingsRsoXero.Id,
                                AppName = createTaskDto.SettingsTask.SettingsRsoXero.AppName,
                                Buyer = createTaskDto.SettingsTask.SettingsRsoXero.Buyer,
                                ConsumerKey = createTaskDto.SettingsTask.SettingsRsoXero.ConsumerKey,
                                ConsumerSecret = createTaskDto.SettingsTask.SettingsRsoXero.ConsumerSecret,
                                DebugLogSettingId = createTaskDto.SettingsTask.SettingsRsoXero.DebugLogSettingId,
                                DownloadSettingId = createTaskDto.SettingsTask.SettingsRsoXero.DownloadSettingId,
                                InvoiceLogEnabled = createTaskDto.SettingsTask.SettingsRsoXero.InvoiceLogEnabled,
                                InvoiceLogPath = createTaskDto.SettingsTask.SettingsRsoXero.InvoiceLogPath,
                                OrganizationName = createTaskDto.SettingsTask.SettingsRsoXero.OrganizationName,
                                RsosettingId = createTaskDto.SettingsTask.SettingsRsoXero.RsosettingId,
                                TaskId = createTaskDto.SettingsTask.SettingsRsoXero.TaskId,
                                UseXeroFailureSupplier = createTaskDto.SettingsTask.SettingsRsoXero.UseXeroFailureSupplier.ToString(),
                                XeroFailureSupplier = createTaskDto.SettingsTask.SettingsRsoXero.XeroFailureSupplier
                            }

                        };
                        break;

                    case "8":
                        //TODO: SettingsIndex
                        break;

                }




                _tenantRepository.Add(task);

                operationResult = true;
            }
            catch (Exception ex)
            {

                operationResult = false;
            }


            return operationResult;
        }



        public SettingsTask GetSettingTask(long taskId, TenantContext tenantContext)
        {
            _tenantRepository.SetTenantContext(tenantContext);

            var param = Expression.Parameter(typeof(SettingsTask), "p");

            var body = Expression.Equal(Expression.PropertyOrField(param, "Id"), Expression.Constant(taskId));

            var lambda = Expression.Lambda<Func<SettingsTask, bool>>(body, param);

            SettingsTask settingsTask = _tenantRepository.FindBy(lambda).FirstOrDefault();



           

          


            switch (settingsTask.TaskTypeId.ToString())
            {
                case "1":
                    var param1 = Expression.Parameter(typeof(SettingsSage200Posting), "p");

                    var body1 = Expression.Equal(Expression.PropertyOrField(param1, "TaskId"), Expression.Constant(taskId));

                    var lambda1 = Expression.Lambda<Func<SettingsSage200Posting, bool>>(body1, param1);

                    settingsTask.SettingsSage200Posting = new List<SettingsSage200Posting>();
                    
                    var res1 = _tenantRepository.FindBy(lambda1).FirstOrDefault();

                    settingsTask.SettingsSage200Posting.Add(res1);

                    break;
                case "2":
                    var param2 = Expression.Parameter(typeof(SettingsRsoMasterUpload), "p");

                    var body2 = Expression.Equal(Expression.PropertyOrField(param2, "TaskId"), Expression.Constant(taskId));

                    var lambda2 = Expression.Lambda<Func<SettingsRsoMasterUpload, bool>>(body2, param2);

                    settingsTask.SettingsRsoMasterUpload = new List<SettingsRsoMasterUpload>();

                    var res2 = _tenantRepository.FindBy(lambda2).FirstOrDefault();

                    settingsTask.SettingsRsoMasterUpload.Add(res2);

                    break;
                case "3":
                    var param3 = Expression.Parameter(typeof(SettingsRsoScanUpload), "p");

                    var body3 = Expression.Equal(Expression.PropertyOrField(param3, "TaskId"), Expression.Constant(taskId));

                    var lambda3 = Expression.Lambda<Func<SettingsRsoScanUpload, bool>>(body3, param3);


                    settingsTask.SettingsRsoScanUpload = new List<SettingsRsoScanUpload>();

                    var res3 = _tenantRepository.FindBy(lambda3).FirstOrDefault();

                    settingsTask.SettingsRsoScanUpload.Add(res3);

                    break;
                case "4":
                    var param4 = Expression.Parameter(typeof(SettingsRsoScanDownload), "p");

                    var body4 = Expression.Equal(Expression.PropertyOrField(param4, "TaskId"), Expression.Constant(taskId));


                    var lambda4 = Expression.Lambda<Func<SettingsRsoScanDownload, bool>>(body4, param4);

                    settingsTask.SettingsRsoScanDownload = new List<SettingsRsoScanDownload>();

                    var res4 = _tenantRepository.FindBy(lambda4).FirstOrDefault();

                    settingsTask.SettingsRsoScanDownload.Add(res4);

                    break;
                case "5":
                    var param5 = Expression.Parameter(typeof(SettingsRsoSage50), "p");

                    var body5 = Expression.Equal(Expression.PropertyOrField(param5, "TaskId"), Expression.Constant(taskId));


                    var lambda5 = Expression.Lambda<Func<SettingsRsoSage50, bool>>(body5, param5);

                    settingsTask.SettingsRsoSage50 = new List<SettingsRsoSage50>();

                    var res5 = _tenantRepository.FindBy(lambda5).FirstOrDefault();

                    settingsTask.SettingsRsoSage50.Add(res5);

                    break;
                case "6":

                    var param6 = Expression.Parameter(typeof(SettingsSage50Posting), "p");

                    var body6 = Expression.Equal(Expression.PropertyOrField(param6, "TaskId"), Expression.Constant(taskId));

                    var lambda6 = Expression.Lambda<Func<SettingsSage50Posting, bool>>(body6, param6);

                    settingsTask.SettingsSage50Posting = new List<SettingsSage50Posting>();

                    var res6 = _tenantRepository.FindBy(lambda6).FirstOrDefault();

                    settingsTask.SettingsSage50Posting.Add(res6);

                    break;
                case "7":
                    var param7 = Expression.Parameter(typeof(SettingsRsoXero), "p");

                    var body7 = Expression.Equal(Expression.PropertyOrField(param7, "TaskId"), Expression.Constant(taskId));


                    var lambda7 = Expression.Lambda<Func<SettingsRsoXero, bool>>(body7, param7);


                    settingsTask.SettingsRsoXero = new List<SettingsRsoXero>();

                    var res7 = _tenantRepository.FindBy(lambda7).FirstOrDefault();

                    settingsTask.SettingsRsoXero.Add(res7);

                    break;
                case "8":
                    //TODO: SettingsIndex
                    break;
            }


            

           



            return settingsTask;
        }

    }
}
