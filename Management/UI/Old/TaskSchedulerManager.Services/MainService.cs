﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Mvc.Rendering;
using TaskSchedulerManager.Data.MainDb.Models;
using TaskSchedulerManager.Repositories.Interfaces;
using TaskSchedulerManager.Services.Interfaces;
using TaskSchedulerManager.Domain;

namespace TaskSchedulerManager.Services
{
    public class MainService : IMainService
    {
        private IOrganizationSettingsRepository _repository;

        public MainService(IOrganizationSettingsRepository repository)
        {
            _repository = repository;
        }
        public void AddOrganization(OrganizationSettingsDTO org)
        {
            //map dto to model class
            OrganizationSettings orgSettings = new OrganizationSettings()
            {
                AccessKey = org.AccessKey,
                Disabled = org.Disabled,
                Notes = org.Notes,
                TenantAlias = org.TenantAlias,
                TenantDatabase = org.TenantDatabase,
                TenantDomain = org.TenantDomain,
                TenantId = org.TenantId,
                TenantName = org.TenantName
            };

            _repository.Add(orgSettings);         

            _repository.Save();

            org.TenantId = orgSettings.TenantId;
        }

        public void DeleteOrganization(long id)
        {
            var organization = _repository.GetSingle(id);

            _repository.Delete(organization);

            _repository.Save();
        }

        public OrganizationSettingsDTO GetOrganizationById(long id)
        {
            var org = _repository.GetSingle(id);

            if(org==null)
            {
                return new OrganizationSettingsDTO();
            }

            OrganizationSettingsDTO dto = new OrganizationSettingsDTO()
            {
                AccessKey = org.AccessKey,
                Disabled = org.Disabled,
                Notes = org.Notes,
                TenantAlias = org.TenantAlias,
                TenantDatabase = org.TenantDatabase,
                TenantDomain = org.TenantDomain,
                TenantId = org.TenantId,
                TenantName = org.TenantName
            };

            return dto;
        }

        public string GetOrganizationName(long id)
        {
            return _repository.GetSingle(id)?.TenantName;
        }

        public IEnumerable<OrganizationSettingsDTO> GetTenantsList()
        {
            List<OrganizationSettingsDTO> list = new List<OrganizationSettingsDTO>();

            var orgs = _repository.GetAll();

            foreach(var org in orgs)
            {
                OrganizationSettingsDTO dto = new OrganizationSettingsDTO()
                {
                    AccessKey = org.AccessKey,
                    Disabled = org.Disabled,
                    Notes = org.Notes,
                    TenantAlias = org.TenantAlias,
                    TenantDatabase = org.TenantDatabase,
                    TenantDomain = org.TenantDomain,
                    TenantId = org.TenantId,
                    TenantName = org.TenantName
                };

                list.Add(dto);
            }

            return list;
        }

        public void UpdateOrganization(OrganizationSettingsDTO org)
        {

            OrganizationSettings orgSettings = new OrganizationSettings()
            {
                AccessKey = org.AccessKey,
                Disabled = org.Disabled,
                Notes = org.Notes,
                TenantAlias = org.TenantAlias,
                TenantDatabase = org.TenantDatabase,
                TenantDomain = org.TenantDomain,
                TenantId = org.TenantId,
                TenantName = org.TenantName
            };

            _repository.Edit(orgSettings);
            

            _repository.Save();

           
        }
    }
}
