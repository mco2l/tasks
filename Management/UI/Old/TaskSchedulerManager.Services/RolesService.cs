﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TaskSchedulerManager.Services.Interfaces;

namespace TaskSchedulerManager.Services
{
    public class RolesService : IRolesService
    {
        private enum RolesEnum
        {
            SuperUser,
            Administrator,
            User,
            Anonymous
        }


        private RoleManager<IdentityRole> _roleManager;

        public RolesService(RoleManager<IdentityRole> roleManager)
        {
            _roleManager = roleManager;
        }
        public async void InitializeAsync()
        {
            foreach (RolesEnum roleName in Enum.GetValues(typeof(RolesEnum)))
            {
                if (!await _roleManager.RoleExistsAsync(roleName.ToString()))
                {
                    var role = new IdentityRole(roleName.ToString());
                    await _roleManager.CreateAsync(role);
                }
            }
        }
    }
}
