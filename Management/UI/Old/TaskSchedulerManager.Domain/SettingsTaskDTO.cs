﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskSchedulerManager.Domain
{
    public class SettingsTaskDTO
    {
        public SettingsTaskDTO()
        {
            SettingsRsoMasterUpload = new RSO_MasterUpload_DTO();
            SettingsRsoSage50 = new RSO_Sage50_DTO();
            SettingsRsoScanDownload = new RSO_ScanDownload_DTO();
            SettingsRsoScanUpload = new RSO_ScanUpload_DTO();
            SettingsRsoXero = new RSO_Xero_DTO();
            SettingsSage200Posting = new Sage200_Posting_DTO();
            SettingsSage50Posting = new Sage50_Posting_DTO();


            //SettingsTaskSubMainTask = new HashSet<SettingsTaskSub>();
            //SettingsTaskSubSubTask = new HashSet<SettingsTaskSub>();
        }

        public long Id { get; set; }
        public long TaskTypeId { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public long Interval { get; set; }
        public long TotalRunPerTimeRange { get; set; }
        public bool Disabled { get; set; }
        public bool EnableTracing { get; set; }
        public long MaxLogFiles { get; set; }
        public long EmailSuccessSettingId { get; set; }
        public long EmailFailureSettingId { get; set; }
        public bool RunOnStart { get; set; }
        public long? MaxFailuresAllowed { get; set; }
        public long? IntervalFailureRetry { get; set; }

        public virtual RSO_MasterUpload_DTO SettingsRsoMasterUpload { get; set; }
        public virtual RSO_Sage50_DTO SettingsRsoSage50 { get; set; }
        public virtual RSO_ScanDownload_DTO SettingsRsoScanDownload { get; set; }
        public virtual RSO_ScanUpload_DTO SettingsRsoScanUpload { get; set; }
        public virtual RSO_Xero_DTO SettingsRsoXero { get; set; }
        public virtual Sage200_Posting_DTO SettingsSage200Posting { get; set; }
        public virtual Sage50_Posting_DTO SettingsSage50Posting { get; set; }


        //public virtual ICollection<SettingsTaskSub> SettingsTaskSubMainTask { get; set; }
        //public virtual ICollection<SettingsTaskSub> SettingsTaskSubSubTask { get; set; }
        //public virtual SettingsSharedEmail EmailFailureSetting { get; set; }
        //public virtual SettingsSharedEmail EmailSuccessSetting { get; set; }
    }
}
