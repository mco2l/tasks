﻿namespace TaskSchedulerManager.Domain
{
    public class RSO_Sage50_DTO
    {
        public long Id { get; set; }
        public long? TaskId { get; set; }
        public string Company { get; set; }
        public string DatabasePath { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string AttachmentPath { get; set; }
        public long RsosettingId { get; set; }
        public long HeaderFieldMappingId { get; set; }
        public long LineFieldMappingId { get; set; }
        public long DownloadSettingId { get; set; }
    }
}