﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskSchedulerManager.Domain
{
    public class TaskTypesDTO
    {
        public TaskTypesDTO()
        {
            TaskTypeSettings = new HashSet<TaskTypeSettingsDTO>();
        }

        public long TaskTypeId { get; set; }
        public string Description { get; set; }
        public string AssemblyName { get; set; }
        public string AssemblyType { get; set; }
        public string SettingsTable { get; set; }

        public virtual ICollection<TaskTypeSettingsDTO> TaskTypeSettings { get; set; }
    }
}
