﻿namespace TaskSchedulerManager.Domain
{
    public class RSO_Xero_DTO
    {
        public long Id { get; set; }
        public long? TaskId { get; set; }
        public string OrganizationName { get; set; }
        public string ConsumerKey { get; set; }
        public string ConsumerSecret { get; set; }
        public string AppName { get; set; }
        public string Buyer { get; set; }
        public bool UseXeroFailureSupplier { get; set; }
        public string XeroFailureSupplier { get; set; }
        public long DownloadSettingId { get; set; }
        public long RsosettingId { get; set; }
        public long? DebugLogSettingId { get; set; }
        public bool InvoiceLogEnabled { get; set; }
        public string InvoiceLogPath { get; set; }
    }
}