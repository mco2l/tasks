﻿namespace TaskSchedulerManager.Domain
{
    public class RSO_ScanDownload_DTO
    {
        public long Id { get; set; }
        public long? TaskId { get; set; }
        public string DownloadPath { get; set; }
        public long ImageLocationIndex { get; set; }
        public string CreateBuyerFolder { get; set; }
        public bool UsePermaLink { get; set; }
        public long? RsosettingId { get; set; }
        public long FieldMappingId { get; set; }
    }
}