﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskSchedulerManager.Domain
{
    public class CreateTaskDTO
    {

        public SettingsTaskDTO SettingsTask { get; set; }

        public List<TaskTypeSettingsDTO> TaskTypeSettings { get; set; }
    }
}
