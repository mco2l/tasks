﻿using System;
using System.Collections.Generic;
using System.Text;
using TaskManager.DTO;

namespace TaskManager.Services.Interfaces
{
    public interface ITenantDatabaseService
    {
        TaskTypeSettingsDTO GetTaskTypeSettingsByTaskTypeId(long taskTypeId);

        List<TaskTypesDTO> GetTaskTypes(string connectionString);

        CreateTaskDTO CreateTask(string tenantConnectionString, CreateTaskDTO createTaskDTO);

        List<ColumnPropertiesDTO> GetSettingsTaskByTaskId(string tenantConnectionString, long taskId);

        List<ColumnPropertiesDTO> GetSpecificSettingsForTaskByTaskId(string tenantConnectionString, string settingsTable, long taskId);
    }
}
