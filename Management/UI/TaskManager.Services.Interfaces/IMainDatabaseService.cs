﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using TaskManager.DTO;
using TaskManager.ErrorHandling;

namespace TaskManager.Services.Interfaces
{
    public interface IMainDatabaseService
    {
        ErrorClass EnsureCreateDatabase(string connectionString, string dataBaseScriptPath);
        List<DbDataRecord> GetAllOrganizations(string connectionString);

        List<SQLiteRecordDTO> GetOrganizationById(string connectionString, long id);
        List<SQLiteRecordDTO> UpdateOrganization(string connectionString, List<SQLiteRecordDTO> record);



        List<string> GetTableColumnNames(string connectionString, string tableName);

        List<ColumnPropertiesDTO> GetTableColumnProperties(string connectionString, string tableName);
        List<SQLiteRecordDTO> CreateOrganization(string mainConnectionString, List<SQLiteRecordDTO> record);
        bool CreateTenantDatabase(string tenantConnectionString);
        Dictionary<string, List<TaskTypeSettingsDTO>> GetTaskTypeSettingsByTaskTypeId(string connectionString, long taskTypeId);

        string GetSettingsTableNameForTaskTypeId(string connectionString, long taskTypeId);
    }
}
