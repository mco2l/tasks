﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskManager.Middleware
{
    public class TenantMiddlewareOptions
    {
        public string MainConnectionString{ get; set; }
        public long TenantId { get; set; }
        public string TenantConnectionString { get; set; }
        public string TenantName { get; set; }
        public string TenantDbName { get; set; }
        public bool GrantSuperUserAccess { get; set; }
    }
}
