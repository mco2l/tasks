﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using TaskManager.Data;

using TaskManager.Services.Interfaces;

namespace TaskManager.Middleware
{


    public class TenantMiddleware
    {
        private RequestDelegate _next;
        private IOptions<TenantMiddlewareOptions> _options;
        private UserManager<ApplicationUser> _userManager;
        private RoleManager<IdentityRole> _roleManager;
        private IMainDatabaseService _mainDatabaseService;


        public TenantMiddleware(RequestDelegate next, IOptions<TenantMiddlewareOptions> options)
        {
            _next = next;
            _options = options;


        }

        public Task Invoke(HttpContext httpContext, UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager, IMainDatabaseService mainDatabaseService)
        {

            if (httpContext != null)
            {
                _userManager = userManager;
                _roleManager = roleManager;
                _mainDatabaseService = mainDatabaseService;


                var userId = _userManager.GetUserId(httpContext.User);

                if (userId != null)
                {
                    Task<ApplicationUser> AppUser = _userManager.FindByIdAsync(userId);

                    if (AppUser.Result != null)
                    {
                        #region Check if user is SuperUser
                        _roleManager = roleManager;

                        var roles = _userManager.GetRolesAsync(AppUser.Result);

                        if (roles.Result.Contains("SuperUser"))
                        {
                            //only super user. No need to define context.
                            _options.Value.TenantName = "Glantus";
                            _options.Value.GrantSuperUserAccess = true;

                        }
                        #endregion

                        if (AppUser.Result.TenantId > 0)
                        {
                            var tenantID = AppUser.Result.TenantId;


                            var organization = _mainDatabaseService.GetOrganizationById(MiddlewareExtensions.MainConnectionString, tenantID);

                            /*SET MIDDLEWARE OPTIONS*/
                            _options.Value.MainConnectionString = MiddlewareExtensions.MainConnectionString;
                            _options.Value.TenantConnectionString = MiddlewareExtensions.TenantConnectionString + organization.Find(o => o.ColumnName == "TenantConnectionString").ColumnValue;
                            _options.Value.TenantName = organization.Find(o => o.ColumnName == "TenantName").ColumnValue;

                            int.TryParse(organization.Find(o => o.ColumnName == "TenantId").ColumnValue, out int res);

                            _options.Value.TenantId = res;
                        }
                        else
                        {
                            _options.Value.MainConnectionString = MiddlewareExtensions.MainConnectionString;
                            _options.Value.TenantId = AppUser.Result.TenantId;
                        }
                    }

                }
                else
                {
                    //reset middleware options for logged out users
                    _options.Value.TenantName = string.Empty;
                    _options.Value.TenantConnectionString = null;
                    _options.Value.GrantSuperUserAccess = false;
                    _options.Value.MainConnectionString = string.Empty;
                }
            }


            return _next(httpContext);
        }
    }

    public static class MiddlewareExtensions
    {
        public static string MainConnectionString;
        public static string TenantConnectionString;

        public static IApplicationBuilder UseTenantMiddleware(this IApplicationBuilder builder, string mainConString, string tenantConnString)
        {
            MainConnectionString = mainConString;

            TenantConnectionString = tenantConnString;

            return builder.UseMiddleware<TenantMiddleware>();
        }
    }
}
