﻿using System;

namespace TaskRunner.Core
{
    public sealed class ExceptionEventArgs : EventArgs
    {
        public string Title { get; set; }
        public Exception Exception { get; set; }
        public string MethodName { get; set; }

        public ExceptionEventArgs(string methodName, string title, Exception exception)
        {
            MethodName = methodName;
            Title = title;
            Exception = exception;
        }
    }
}
