﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using TaskRunner.Core.Enums;

namespace TaskRunner.Core.Logging
{
    public sealed class LogHelper
    {
        #region log file functions

        public static void WriteToLogFile(string applicationName, string message)
        {
            WriteToLogFile(new LogEntry() { ApplicationName = applicationName, Message = message, LogMessageType = AdestLogMessageType.Information });
        }

        public static void WriteErrorToLogFile(string applicationName, string methodName, string message, Exception ex)
        {
            WriteToLogFile(new LogEntry() { ApplicationName = applicationName, MethodName = methodName, Message = message, Exception = ex, LogMessageType = AdestLogMessageType.Error });
        }

        public static void WriteToLogFile(LogEntry logEntry)
        {
            bool isException = (logEntry.LogMessageType == AdestLogMessageType.Error) ? true : false;
            string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), @"Adest\Logs");
            string fileName = string.Concat(logEntry.ApplicationName, "_", System.DateTime.Now.ToString("ddMMyyyy"), isException ? "_Error" : string.Empty, ".txt");
            string logFileName = Path.Combine(path, fileName);

            try
            {
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);
            }
            catch (IOException exception)
            {
                exception = new IOException(exception.Message, logEntry.Exception);
                WriteToEventViewer(logEntry.ApplicationName, logEntry.MethodName, logEntry.Message, exception, AdestLogMessageType.Error);
                throw;
            }
            catch (Exception exception)
            {
                exception = new Exception(exception.Message, logEntry.Exception); //Use message from exception caused by directory failure and stored message exception in inner exception.
                WriteToEventViewer(logEntry.ApplicationName, logEntry.MethodName, logEntry.Message, exception, AdestLogMessageType.Error);
                throw;
            }

            try
            {
                if (isException)
                {
                    string exceptionGuid = Guid.NewGuid().ToString();
                    WriteToLogFile(logEntry.ApplicationName, string.Concat("An exception occurred in Method/Function: ", logEntry.MethodName, " - Please check error log ( ", Path.GetFileName(logFileName), " ) for Exception ID: ", exceptionGuid));
                }

                using (StreamWriter sw = new StreamWriter(logFileName, true, Encoding.UTF8))
                {
                    sw.Write(GetExceptionDetails(logEntry));
                }
            }
            catch (IOException exception)
            {
                exception = new IOException(exception.Message, logEntry.Exception); //Use message from exception caused by directory failure and stored message exception in inner exception.
                WriteToEventViewer(logEntry.ApplicationName, logEntry.MethodName, logEntry.Message, exception, AdestLogMessageType.Error);
                throw;
            }
            catch (Exception exception)
            {
                exception = new Exception(exception.Message, logEntry.Exception); //Use message from exception caused by directory failure and stored message exception in inner exception.
                WriteToEventViewer(logEntry.ApplicationName, logEntry.MethodName, logEntry.Message, exception, AdestLogMessageType.Error);
                throw;
            }
        }

        #endregion

        #region event viewer helper functions

        public static void WriteToEventViewer(string applicationName, string message, AdestLogMessageType logMessageType)
        {
            WriteToEventViewer(new LogEntry() { ApplicationName = applicationName, Message = message, Exception = null, LogMessageType = logMessageType });
        }

        public static void WriteToEventViewer(string applicationName, string methodName, string message, AdestLogMessageType logMessageType)
        {
            WriteToEventViewer(new LogEntry() { ApplicationName = applicationName, MethodName = methodName, Message = message, Exception = null, LogMessageType = logMessageType });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationName"></param>
        /// <param name="message"></param>
        /// <param name="ex"></param>
        /// <param name="logMessageType"></param>
        /// <remarks>
        /// Overloaded method without the MethodName parameter as sometimes, it is not required to log where the error came from as it is obvious. 
        /// </remarks>
        public static void WriteToEventViewer(string applicationName, string message, Exception ex, AdestLogMessageType logMessageType)
        {
            WriteToEventViewer(new LogEntry() { ApplicationName = applicationName, Message = message, Exception = ex, LogMessageType = logMessageType });
        }

        public static void WriteToEventViewer(string applicationName, string methodName, string message, Exception ex, AdestLogMessageType logMessageType)
        {
            WriteToEventViewer(new LogEntry() { ApplicationName = applicationName, MethodName = methodName, Message = message, Exception = ex, LogMessageType = logMessageType });
        }

        public static void WriteToEventViewer(LogEntry logEntry)
        {
            if (!EventLog.SourceExists(logEntry.ApplicationName))
            {
                EventLog.CreateEventSource(logEntry.ApplicationName, "Adest");
            }

            string temp = EventLog.LogNameFromSourceName(logEntry.ApplicationName, Environment.MachineName);
            if (string.Compare(temp, "Adest") != 0)
            {
                EventLog.DeleteEventSource(logEntry.ApplicationName);
                EventLog.CreateEventSource(logEntry.ApplicationName, "Adest");
            }

            EventLogEntryType eventLogEntryType;

            switch (logEntry.LogMessageType)
            {
                case AdestLogMessageType.Error:
                    eventLogEntryType = EventLogEntryType.Error;
                    break;
                case AdestLogMessageType.Information:
                    eventLogEntryType = EventLogEntryType.Information;
                    break;
                case AdestLogMessageType.Warning:
                    eventLogEntryType = EventLogEntryType.Warning;
                    break;
                default:
                    eventLogEntryType = EventLogEntryType.Error;
                    break;
            }

            EventLog.WriteEntry(logEntry.ApplicationName, GetExceptionDetails(logEntry), eventLogEntryType, 0, 0);
        }

        #endregion

        #region helper functions

        private static string GetExceptionDetails(LogEntry logEntry)
        {
            StringBuilder sb = new StringBuilder();

            if (logEntry.Exception != null)
            {
                if (!string.IsNullOrEmpty(logEntry.ExceptionGuid))
                    sb.AppendLine("Exception ID: " + logEntry.ExceptionGuid);

                sb.AppendLine("Method/Function: " + logEntry.MethodName);
                sb.AppendLine("DateTime: " + DateTime.Now.ToLongDateString());
                sb.AppendLine("Message: " + logEntry.Message);
                sb.AppendLine(logEntry.Exception.Message);

                if (logEntry.LogType == AdestLogType.File)
                    sb.AppendLine("***************************************************************************************");
            }
            else
            {
                sb.AppendLine(logEntry.Message);
            }

            return sb.ToString();
        }

        #endregion

        #region debug log

        public static List<LogTrace> debugLogTrace = new List<LogTrace>();

        public static string SerializeDebugLog()
        {
            string xmlEncodedLog = string.Empty;
            using (StringWriter sw = new StringWriter())
            {
                var xs = new XmlSerializer(debugLogTrace.GetType());
                xs.Serialize(sw, debugLogTrace);
                xmlEncodedLog = sw.ToString();
            }

            //LogHelper.WriteToLogFile("DataAccesslayer", xmlEncodedLog);
            System.Diagnostics.EventLog appLog = new System.Diagnostics.EventLog();
            appLog.Source = "Adest";
            appLog.WriteEntry(xmlEncodedLog);
            debugLogTrace.Clear();
            return xmlEncodedLog;
        }

        public struct LogTrace
        {
            public DateTime _date { get; set; }
            public string _methodName { get; set; }
            //public string _className { get; set; }
            //public int _lineNumber { get; set; }
            public string _logMessage { get; set; }
        }

        public static void ConcatLogTrace(string log)
        {
            StackTrace st = new StackTrace(true);

            var logTrace = new LogTrace()
            {
                _date = DateTime.Now,
                _methodName = st.GetFrame(2).GetMethod().Name,
                //_className = st.GetFrame(1).GetMethod().DeclaringType.Name,
                //_lineNumber = st.GetFrame(1).GetFileLineNumber(),
                _logMessage = log
            };

            debugLogTrace.Add(logTrace);
        }

        #endregion
    }
}
