﻿using System;
using System.Text;

namespace TaskRunner.Core.Extensions
{
    public static class ExceptionExtensions
    {
        private static readonly string ExceptionTextPattern = "Exception Message   : '{0}'\r\nException Type      : '{1}'\r\nException Source    : '{2}'\r\nException StackTrace: '{3}'";

        public static string ToText(this Exception exception)
        {
            if (exception == null)
            {
                return String.Empty;
            }

            StringBuilder builder = new StringBuilder();

            ExceptionExtensions.ConstructMessage(builder, 0, exception);

            return builder.ToString();
        }

        private static void ConstructMessage(StringBuilder builder, int innerExceptionLevel, Exception exception)
        {
            if (innerExceptionLevel > 0)
            {
                builder.Append('\t', innerExceptionLevel);
            }

            builder.AppendLine(
                String.Format(
                    ExceptionTextPattern,
                    exception.Message,
                    exception.GetType().ToString(),
                    exception.Source,
                    exception.StackTrace));

            if (exception.InnerException != null)
            {
                ExceptionExtensions.ConstructMessage(builder, innerExceptionLevel + 1, exception.InnerException);
            }
        }

        //public static string ToTextWithDetails(this Exception exception)
        //{
        //    StringBuilder sb = new StringBuilder();

        //    try
        //    {
        //        if (exception != null)
        //        {
        //            if (exception.InnerException != null)
        //            {
        //                while (exception != null)
        //                {
        //                    sb.AppendLine(string.Concat("Exception Message   : ", exception.Message));
        //                    sb.AppendLine(string.Concat("Exception Type      : ", exception.GetType().ToString()));
        //                    sb.AppendLine(string.Concat("Exception Source    : ", exception.Source));
        //                    if (exception.TargetSite != null) sb.AppendLine(string.Concat("Exception TargetSite: ", exception.TargetSite.ToString().Trim()));
        //                    if (!string.IsNullOrEmpty(exception.StackTrace)) sb.AppendLine(string.Concat("Exception StackTrace: ", exception.StackTrace.Trim()));
        //                    exception = exception.InnerException;
        //                }
        //            }
        //            else
        //            {
        //                sb.AppendLine(string.Concat("Exception Message   : ", exception.Message));
        //                sb.AppendLine(string.Concat("Exception Type      : ", exception.GetType().ToString()));
        //                sb.AppendLine(string.Concat("Exception Source    : ", exception.Source));
        //                if (exception.TargetSite != null) sb.AppendLine(string.Concat("Exception TargetSite: ", exception.TargetSite.ToString().Trim()));
        //                if (!string.IsNullOrEmpty(exception.StackTrace)) sb.AppendLine(string.Concat("Exception StackTrace: ", exception.StackTrace.Trim()));
        //            }
        //        }
        //        return sb.ToString();
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        sb = null;
        //    }
        //}
    }
}
