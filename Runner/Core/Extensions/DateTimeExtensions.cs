﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TaskRunner.Core.Extensions
{
    public static class DateTimeExtensions
    {
        public static string ToISODateTime(this DateTime datetime)
        {
            return datetime.ToString("yyyyMMdd HH:mm:ss");
        }

        public static string ToISODate(this DateTime datetime)
        {
            return datetime.ToString("yyyyMMdd");
        }
    }
}
