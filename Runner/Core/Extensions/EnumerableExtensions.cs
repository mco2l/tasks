﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskRunner.Core.Extensions
{
    public static class EnumerableExtensions
    {
        public static string ToComma<T, TU>(this IEnumerable<T> source, Func<T, TU> func)
        {
            return string.Join(",", source.Select(s => func(s).ToString()).ToArray());
        }

        public static string ToSeparator<T, TU>(this IEnumerable<T> source, Func<T, TU> func, string separator, bool endWithSeparator, List<int> ignoreList)
        {
            string separatedString = string.Join(separator, source.Select(x =>
            {
                var value = func(x);
                return value == null ? string.Empty : value.ToString();
            }));

            separatedString += (endWithSeparator ? separator : string.Empty);

            int skipIndex = 0;

            for (int index = 0; index < ignoreList.Count; index++)
            {
                separatedString = separatedString.Remove(ignoreList[index] - skipIndex);
                skipIndex = +1;
            }

            return separatedString;
        }

        public static string ToSeparator<T, TU>(this IEnumerable<T> source, Func<T, TU> func, string separator, bool endWithSeparator)
        {
            string separatedString = string.Join(separator, source.Select(x =>
            {
                var value = func(x);
                return value == null ? string.Empty : value.ToString();
            }));

            separatedString += (endWithSeparator ? separator : string.Empty);

            return separatedString;
        }

        public static string ToSeparator<T, TU, TZ>(this IEnumerable<T> source, Func<T, TU> func, TZ defaultValue, string separator, bool endWithSeparator)
        {
            string separatedString = string.Join(separator, source.Select(x =>
            {
                var value = func(x);
                return value == null ? defaultValue.ToString() : value.ToString();
            }));

            separatedString += (endWithSeparator ? separator : string.Empty);

            return separatedString;
        }


    }
}
