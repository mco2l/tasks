﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace TaskRunner.Core.Extensions
{
    public static class DataRowExtensions
    {
        public static string ToSeparator(this DataRow dataRow, string separator, bool endWithSeparator)
        {
            return ToSeparator(dataRow, separator, endWithSeparator, new List<int>());
        }

        public static string ToSeparator(this DataRow dataRow, string separator, bool endWithSeparator, List<int> ignoreIndexList)
        {
            StringBuilder sb = new StringBuilder();

            for (int index = 0; index < dataRow.ItemArray.Length; ++index)
            {
                if (!ignoreIndexList.Contains(index))
                {
                    sb.Append(FormatValue(dataRow.ItemArray[index]) + separator);
                }
            }

            if (!endWithSeparator)
                sb.Remove(sb.Length - 1, 1);

            return sb.ToString();
        }

        private static string FormatValue(object value)
        {
            if (Object.ReferenceEquals(value, null) || DBNull.Value.Equals(value))
                return string.Empty;

            Type valueType = value.GetType();

            if (valueType == typeof(DateTime))
                return System.Convert.ToDateTime(value).ToString("yyyy-MM-dd");

            if (valueType.IsPrimitive && valueType != typeof(string))
                return value.ToString();

            return value.ToString();
        }
    }
}