﻿using System;
using System.Text.RegularExpressions;

namespace TaskRunner.Core.Extensions
{
    public static class StringExtensions
    {
        public static T ConvertTo<T>(this string text)
        {
            return (T)Convert.ChangeType(text, typeof(T));
        }

        public static string ReplaceCaseInsensitive(this string text, string oldValue, string newValue)
        {
            if (string.IsNullOrEmpty(text))
                return string.Empty;

            return Regex.Replace(text, Regex.Escape(oldValue), newValue, RegexOptions.IgnoreCase);
            //return Regex.Replace(source, oldValue, newValue, RegexOptions.IgnoreCase);
        }

        public static string FixQuotes(this string text)
        {
            if (string.IsNullOrEmpty(text))
                return string.Empty;

            return text.Replace(@"'", @"''");
        }

        //Replace any kind of white spaces i.e. tabs, newline, spaces, etc...
        public static string ReplaceWhiteSpaces(this string text, string replaceValue)
        {
            if (string.IsNullOrEmpty(text))
                return string.Empty;

            return Regex.Replace(text, @"\s+", " ");
        }
    }
}
