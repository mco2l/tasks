﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;

namespace TaskRunner.Core.Extensions
{
    public static class DataTableExtensions
    {
        public static List<T> ToList<T>(this DataTable table)
        {
            List<T> list = new List<T>();

            Type pocoType = typeof(T);
            PropertyInfo[] pocoClass = pocoType.GetProperties();
            List<DataColumn> columns = table.Columns.Cast<DataColumn>().ToList();
            T pocoObject;

            foreach (DataRow item in table.Rows)
            {
                pocoObject = (T)Activator.CreateInstance(pocoType);
                foreach (PropertyInfo pc in pocoClass)
                {
                    DataColumn d = columns.Find(c => c.ColumnName.ToUpperInvariant() == pc.Name.ToUpperInvariant());
                    if (d != null && item[pc.Name] != System.DBNull.Value)
                        pc.SetValue(pocoObject, item[pc.Name], null);
                }
                list.Add(pocoObject);
            }
            return list;
        }

        public static string ToSeparator(this DataTable table, string separator, bool endWithSeparator)
        {
            return ToSeparator(table, separator, false, endWithSeparator);
        }

        public static string ToSeparator(this DataTable table, string separator, bool includeHeaderRow, bool endWithSeparator)
        {
            StringBuilder sb = new StringBuilder();

            if (includeHeaderRow)
            {
                StringBuilder sbColumns = new StringBuilder();
                foreach (DataColumn dc in table.Columns)
                {
                    sbColumns.Append(dc.ColumnName + separator);
                }
                if (!endWithSeparator)
                    sbColumns.Remove(sbColumns.Length - separator.Length, separator.Length);
            }

            foreach (DataRow dr in table.Rows)
            {
                sb.AppendLine(dr.ToSeparator(separator, endWithSeparator));
            }

            return sb.ToString();
        }
    }
}
