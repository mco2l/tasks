﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using TaskRunner.Core.Extensions;

namespace TaskRunner.Core
{
    public class TaskBase
    {
        private static readonly string ExceptionTextPattern = "Exception Message   : '{0}'\r\nException Type      : '{1}'\r\nException Source    : '{2}'\r\nException StackTrace: '{3}'";
        private bool IsOldLogFilesDeleted;
        private bool IsOldErrorLogFilesDeleted;

        #region properties

        public string LogPath { get; private set; }
        public string LogFilename { get; private set; }
        public string LogErrorFilename { get; private set; }
        public string CurrentFilename { get; set; }
        public string CurrentHashkey { get; set; }
        public StringBuilder LogText { get; set; }
        public bool LogTextOutputted { get; set; }
        public bool IsSuccess { get; set; }
        public bool IsProcessing { get; set; }
        public bool IsDataProcessed { get; set; }
        public Dictionary<string, string> AdditionalTags { get; set; }

        #endregion

        protected bool Initialize(string tenantDB, int taskID)
        {
            if (String.IsNullOrEmpty(tenantDB) || taskID == 0)
                throw new ArgumentNullException("Task", "Task tenant database must be set and task ID cannot be 0.");

            //string taskName = string.Concat(task.AssemblyType, "_", task.Id.ToString(), "_", DateTime.Now.ToShortDateString().ToString().Replace(@"/", string.Empty));

            IsOldLogFilesDeleted = false;
            IsOldErrorLogFilesDeleted = false;
            AdditionalTags = new Dictionary<string, string>();
            LogTextOutputted = false;
            LogText = new StringBuilder();
            CurrentFilename = string.Empty;
            CurrentHashkey = string.Empty;

            if (!CreateFolder(LogPath))
            {
                return false;
            }
            else
            {
                //LogFilename = GetNextLogPathFilename(taskName);
                //LogErrorFilename = Path.Combine(Path.GetDirectoryName(LogFilename), Path.GetFileNameWithoutExtension(LogFilename) + "_Error" + Path.GetExtension(LogFilename));
                return true;
            }
        }

        #region log methods

        protected void WriteLogFile(string text)
        {
            WriteLogFile(text, false, false);
        }

        protected void WriteLogFile(string text, bool includeDateTime, bool enableTracing)
        {
            if (enableTracing)
                WriteLogFileNow(text, includeDateTime);
        }

        private void WriteLogFileNow(string text, bool includeDateTime)
        {
            if (!IsOldLogFilesDeleted)
            {
                IsOldLogFilesDeleted = true;

                //Always delete one less than defined as the next step will be to create a new file i.e. MaxLogFiles=100, 
                //so delete 99, as the next file created will be the 100th one in the folder.
                //IOHelper.ClearOlderFiles(Path.GetDirectoryName(LogFilename), "*.*", CurrentTask.MaxLogFiles - 1, false);
            }

            if (LogText.Length > 0)
            {
                text = LogText.ToString() + ((includeDateTime ? (DateTime.Now.TimeOfDay.ToString() + " - ") : string.Empty) + text);
                LogText = new StringBuilder();
                LogTextOutputted = true;
                includeDateTime = false; //make sure to reset the includeDateTime so that it doesn't get outputted twice.
            }

            Stream stream = null;
            try
            {
                if (text.EndsWith("\r\n"))
                    text = text.TrimEnd(new char[] { '\r', '\n' });
                stream = new FileStream(LogFilename, FileMode.Append);
                using (StreamWriter sw = new StreamWriter(stream))
                {
                    stream = null;
                    if (string.IsNullOrEmpty(text))
                    {
                        sw.WriteLine(string.Empty);
                        Debug.WriteLine(string.Empty);
                    }
                    else
                    {
                        sw.WriteLine((includeDateTime ? (DateTime.Now.TimeOfDay.ToString() + " - ") : string.Empty) + text);
                        Debug.WriteLine(DateTime.Now.TimeOfDay.ToString() + " - " + text);
                    }
                }
            }
            finally
            {
                if (stream != null)
                    stream.Dispose();
            }
        }

        #endregion

        #region error handling methods

        protected void WriteErrorLogFile(string methodName, string title)
        {
            //if (CurrentTask.EnableTracing)
            //    WriteErrorLogFileNow(methodName, title, null, string.Empty, string.Empty, string.Empty);
        }

        protected void WriteErrorLogFile(string methodName, string title, Exception ex)
        {
            WriteErrorLogFileNow(methodName, title, ex.ToText(), string.Empty, string.Empty, string.Empty);
        }

        protected void WriteErrorLogFile(string methodName, string title, string exceptionMessage, string exceptionType, string exceptionSource, string exceptionStackTrace)
        {
            WriteErrorLogFileNow(methodName, title, exceptionMessage, exceptionType, exceptionSource, exceptionStackTrace);
        }

        private void WriteErrorLogFileNow(string methodName, string title, string exceptionMessage, string exceptionType, string exceptionSource, string exceptionStackTrace)
        {
            //Exception ID        :
            //Method/Function     :
            //Date/Time           :
            //Title               :
            //Message Source      :
            //Message Exception   :
            //Exception StackTrace:
            //***************************************************

            if (!IsOldErrorLogFilesDeleted)
            {
                IsOldErrorLogFilesDeleted = true;

                //Always delete one less than defined as the next step will be to create a new file i.e. MaxLogFiles=100, 
                //so delete 99, as the next file created will be the 100th one in the folder.
                //IOHelper.ClearOlderFiles(Path.GetDirectoryName(LogErrorFilename), "*.*", CurrentTask.MaxLogFiles - 1, false);
            }

            if (!string.IsNullOrEmpty(LogText.ToString()))
            {
                WriteLogFileNow(LogText.ToString(), false);
            }

            Stream stream = null;

            try
            {
                stream = new FileStream(LogErrorFilename, FileMode.Append);
                using (StreamWriter sw = new StreamWriter(stream))
                {
                    stream = null;
                    string exceptionGuid = Guid.NewGuid().ToString();

                    //Add entry to log file with GUID reference so you can find the error easily.
                    WriteLogFile("An exception occurred in Method/Function: " + methodName + " - Please check error log ( " + Path.GetFileName(LogErrorFilename) + " ) for Exception ID: " + exceptionGuid, true, true);

                    sw.WriteLine("Exception ID        : " + exceptionGuid);
                    sw.WriteLine("Method/Function     : " + methodName);
                    sw.WriteLine("DateTime            : " + System.DateTime.Now.ToString());
                    sw.WriteLine("Title               : " + title);
                    if (string.IsNullOrEmpty(exceptionType) && string.IsNullOrEmpty(exceptionSource) && string.IsNullOrEmpty(exceptionStackTrace))
                    {
                        sw.WriteLine(exceptionMessage);
                    }
                    else
                    {
                        sw.WriteLine(String.Format(ExceptionTextPattern, exceptionMessage, exceptionType, exceptionSource, exceptionStackTrace));
                    }
                    sw.WriteLine("***************************************************************************************");
                }
                IsDataProcessed = true; //Always Set IsDataProcessed when an error is logged.
            }
            finally
            {
                if (stream != null)
                    stream.Dispose();
            }
        }

        protected void MoveToExceptionFolder(string path, string dataFile, string imageFile, Exception ex)
        {
            MoveToExceptionFolderNow(path, dataFile, imageFile, ex.Message, ex.GetType().ToString(), ex.Source, ex.ToText());
        }

        protected void MoveToExceptionFolder(string path, string dataFile, string imageFile, string exceptionMessage)
        {
            MoveToExceptionFolderNow(path, dataFile, imageFile, exceptionMessage, string.Empty, string.Empty, string.Empty);
        }

        protected void MoveToExceptionFolder(string path, string dataFile, string imageFile, string exceptionMessage, string exceptionType, string exceptionSource, string exceptionStackTrace)
        {
            MoveToExceptionFolderNow(path, dataFile, imageFile, exceptionMessage, exceptionType, exceptionSource, exceptionStackTrace);
        }

        private void MoveToExceptionFolderNow(string path, string dataFile, string imageFile, string exceptionMessage, string exceptionType, string exceptionSource, string exceptionStackTrace)
        {
            try
            {
                string newPath = Path.Combine(path, System.DateTime.Now.ToString("ddMMyyyy"));
                if (!Directory.Exists(newPath))
                    Directory.CreateDirectory(newPath);

                string dataPath = Path.Combine(newPath, "Data");
                if (!Directory.Exists(dataPath))
                    Directory.CreateDirectory(dataPath);

                string imagePath = Path.Combine(newPath, "Images");
                if (!Directory.Exists(imagePath))
                    Directory.CreateDirectory(imagePath);

                string exceptionPath = Path.Combine(newPath, "Exceptions");
                if (!Directory.Exists(exceptionPath))
                    Directory.CreateDirectory(exceptionPath);

                Stream stream = null;
                string exceptionFileName = System.IO.Path.GetFileNameWithoutExtension(dataFile);
                try
                {
                    stream = new FileStream(Path.Combine(exceptionPath, exceptionFileName + ".dat"), FileMode.Append);
                    using (StreamWriter sw = new StreamWriter(stream))
                    {
                        stream = null;
                        if (string.IsNullOrEmpty(exceptionType) && string.IsNullOrEmpty(exceptionSource) && string.IsNullOrEmpty(exceptionStackTrace))
                        {
                            sw.WriteLine(exceptionMessage);
                        }
                        else
                        {
                            sw.WriteLine(String.Format(ExceptionTextPattern, exceptionMessage, exceptionType, exceptionSource, exceptionStackTrace));
                        }
                    }
                }
                finally
                {
                    if (stream != null)
                        stream.Dispose();
                }

                if (!File.Exists(Path.Combine(dataPath, Path.GetFileName(dataFile)))) File.Move(dataFile, Path.Combine(dataPath, Path.GetFileName(dataFile)));
                if (!File.Exists(Path.Combine(imagePath, Path.GetFileName(imageFile)))) File.Move(imageFile, Path.Combine(imagePath, Path.GetFileName(imageFile)));

            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region helper methods

        protected void AddUpdateAdditionalTag(string key, string value)
        {
            if (object.ReferenceEquals(AdditionalTags, null)) //This should never be called but is there as a precaution.
                AdditionalTags = new Dictionary<string, string>();

            key = key.ToLowerInvariant();
            if (AdditionalTags.ContainsKey(key))
            {
                AdditionalTags[key] = value;
            }
            else
            {
                AdditionalTags.Add(key, value);
            }
        }

        protected string GetNextLogPathFilename(string rootFileName)
        {
            int logCount = 1;
            string filename = Path.Combine(LogPath, string.Concat(new string[] { rootFileName, "_", logCount.ToString(), ".txt" }));

            //string filename = Path.Combine(LogPath, string.Concat(new string[] { rootFileName, "_", Guid.NewGuid().ToString(), ".txt" }));

            while (File.Exists(filename))
            {
                logCount += 1;
                filename = Path.Combine(LogPath, string.Concat(new string[] { rootFileName, "_", logCount.ToString(), ".txt" }));
            };

            return filename;
        }

        protected bool CreateFolder(string folder)
        {
            try
            {
                if (!Directory.Exists(folder))
                    Directory.CreateDirectory(folder);
                return true;
            }
            catch (IOException ex)
            {
                OnExceptionRaised(new ExceptionEventArgs("CreatePath", "An unhandled IO exception occurred while attempting to create the following folder: " + folder, ex));
                return false;
            }
            catch (Exception ex)
            {
                OnExceptionRaised(new ExceptionEventArgs("CreatePath", "An unhandled exception occurred while attempting to create the following folder: " + folder, ex));
                return false;
            }
        }

        #endregion

        #region events

        public event EventHandler<ExceptionEventArgs> ExceptionRaised;
        protected virtual void OnExceptionRaised(ExceptionEventArgs e)
        {
            EventHandler<ExceptionEventArgs> handler = ExceptionRaised;

            if (handler != null)
                handler(this, e);
        }

        #endregion

    }
}
