﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskRunner.Core.Enums
{

    public enum ContentType
    {
        Json,
        Xml
    }

    public enum AdestLogType
    {
        File,
        EventViewer
    }

    public enum AdestLogMessageType
    {
        Error,
        Warning,
        Information
    }

    public enum UploadTypeEnum
    {
        File,
        Database
    }

    public enum ConnectionTypeEnum
    {
        Sql,
        Odbc
    }

    public enum MasterDataTypeEnum
    {
        NominalCode,
        Currency,
        Supplier,
        SupplierBank,
        Project,
        Custom
    }

}
