﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Data.OleDb;
using TaskRunner.Core.Extensions;
using TaskRunner.Core;

namespace TaskRunner.Core
{
    public sealed class TaskHelpers
    {
        public enum DbTypeEnum
        {
            Sql,
            Odbc
        }


        public static Dictionary<string, object> GetData(int taskID, int uploadType, Dictionary<string, object> parameters)
        {
            Dictionary<string, object> csvInfo = new Dictionary<string, object>();
            string csvData = string.Empty;
            string filename = string.Empty;

            if (uploadType == 0)
            {
                if (!parameters.ContainsKey("FilePath"))
                    throw new ArgumentException("FilePath argument is missing.");

                if (!parameters.ContainsKey("Pattern"))
                    throw new ArgumentException("Pattern argument is missing.");

                filename = GetCsvFilename((string)parameters["FilePath"], (string)parameters["Pattern"]);
                csvData = GetDataFromFile(filename, (string)parameters["Pattern"]);
            }
            else
            {
                if (!parameters.ContainsKey("ConnectionString"))
                    throw new ArgumentException("ConnectionString argument is missing.");

                if (!parameters.ContainsKey("CommandType"))
                    throw new ArgumentException("CommandType argument is missing.");

                if (!parameters.ContainsKey("CommandText"))
                    throw new ArgumentException("CommandText argument is missing.");

                if (!parameters.ContainsKey("CsvSeparator"))
                    throw new ArgumentException("CsvSeparator argument is missing.");

                filename = Path.GetFileNameWithoutExtension("DatabaseData.txt") + taskID.ToString() + Path.GetExtension("DatabaseData.txt");

                if (parameters.ContainsKey("PreCommandText") && !string.IsNullOrEmpty(parameters["PreCommandText"].ToString()))
                {
                    //pre-query using the PreCommand and will include the header row so that columns name can be processed in the post query.                  
                    csvData = GetDataFromDatabase((DbTypeEnum)parameters["ConnectionType"], (string)parameters["ConnectionString"], (CommandType)parameters["CommandType"], (string)parameters["PreCommandText"], "\t", true);

                    if (!string.IsNullOrEmpty(csvData))
                    {
                        try
                        {
                            //Post-query
                            //Remember that a schema.ini must be created in order to be processed successfully.
                            csvData = GetDataFromText(csvData, filename, (string)parameters["CommandText"], (string)parameters["CsvSeparator"]);
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }
                }
                else
                {
                    csvData = GetDataFromDatabase((DbTypeEnum)parameters["ConnectionType"], (string)parameters["ConnectionString"], (CommandType)parameters["CommandType"], (string)parameters["CommandText"], (string)parameters["CsvSeparator"], false);
                }
            }

            int totalLines = 0;

            if (!string.IsNullOrEmpty(csvData))
            {
                totalLines = csvData.Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).Length;
            }

            csvInfo.Add("Total", totalLines);
            csvInfo.Add("Data", csvData);
            csvInfo.Add("Filename", filename);
            csvInfo.Add("HashKey", string.IsNullOrEmpty(csvData) ? string.Empty : EncryptionHelper.GetMD5Hash(csvData));

            return csvInfo;
        }

        public static string GetDataFromText(string csvData, string tempFilename, string commandText, string csvSeparator)
        {
            if (commandText.IndexOf("<DataFile>", StringComparison.InvariantCultureIgnoreCase) < 0)
                throw new ArgumentException("The <DataFile> is missing from the CommandText");

            string tempCsv = string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "\\Adest\\TaskRunner\\Data\\", tempFilename.Replace(".txt", ".csv"));

            //Output csv to temp file
            try
            {
                File.WriteAllText(tempCsv, csvData);
            }
            catch (IOException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }

            //Process temp file using ODBC Text

            string full = Path.GetFullPath(tempCsv);
            string file = Path.GetFileName(full);
            string dir = Path.GetDirectoryName(full);

            string cnString = "Provider=Microsoft.Jet.OLEDB.4.0;"
              + "Data Source=" + dir + ";"
              + "Extended Properties=\"text;HDR=YES;FMT=Delimited\"";

            //create the database query
            string query = commandText.Replace("<DataFile>", file);

            //create a DataTable to hold the query results
            DataTable dt = new DataTable();

            //create an OleDbDataAdapter to execute the query
            OleDbDataAdapter da = new OleDbDataAdapter(query, cnString);

            try
            {
                //fill the DataTable
                da.Fill(dt);
                return dt.ToSeparator(csvSeparator, false);
            }
            catch (InvalidOperationException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                //delete temp file
                if (File.Exists(tempCsv))
                {
                    File.Delete(tempCsv);
                }
                dt.Dispose();
                da.Dispose();
            }
        }

        private static string GetDataFromDatabase(DbTypeEnum dbType, string connectionString, CommandType commandType, string commandText, string csvSeparator, bool includeColumnHeader)
        {
            switch (dbType)
            {
                case DbTypeEnum.Sql:
                    return GetDataFromSQLDatabase(connectionString, commandType, commandText, csvSeparator, includeColumnHeader);
                case DbTypeEnum.Odbc:
                    return GetDataFromODBCDatabase(connectionString, commandType, commandText, csvSeparator, includeColumnHeader);
                default:
                    return GetDataFromSQLDatabase(connectionString, commandType, commandText, csvSeparator, includeColumnHeader);
            }
        }

        private static DataTable GetDataFromDatabase(DbTypeEnum dbType, string connectionString, CommandType commandType, string commandText)
        {
            DataTable dt = null;
            switch (dbType)
            {
                case DbTypeEnum.Sql:
                    dt = GetDataFromSQLDatabase(connectionString, commandType, commandText);
                    break;
                case DbTypeEnum.Odbc:
                    dt = GetDataFromODBCDatabase(connectionString, commandType, commandText);
                    break;
                default:
                    dt = GetDataFromSQLDatabase(connectionString, commandType, commandText);
                    break;
            }
            return dt;
        }

        private static DataTable GetDataFromSQLDatabase(string connectionString, CommandType commandType, string commandText)
        {
            DataTable dt = new DataTable("DataTable");

            try
            {
                using (SqlConnection cn = new SqlConnection(connectionString))
                {
                    cn.Open();
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = cn;
                        cmd.CommandType = commandType;
                        cmd.CommandText = commandText;
                        SqlDataReader dr = cmd.ExecuteReader();
                        if (dr.HasRows)
                        {
                            dt.Load(dr);
                            //int fieldCount = dr.FieldCount - 1;
                            //string separator = string.Empty;
                            //while (dr.Read())
                            //{
                            //    StringBuilder sbLine = new StringBuilder();
                            //    for (int i = 0; i <= fieldCount; i++)
                            //    {
                            //        //If you are dealing with the last field, do not add a separator if there is a value, otherwise add one.
                            //        separator = (i != fieldCount || (i == fieldCount && (dr.IsDBNull(i) || string.IsNullOrEmpty(dr[i].ToString())))) ? csvSeparator : string.Empty;
                            //        //separator = (i != fieldCount) ? csvSeparator : string.Empty;
                            //        sbLine.Append(dr[i].ToString() + separator);
                            //    }
                            //    sbData.AppendLine(sbLine.ToString());
                            //    sbLine = null;
                            //}
                        }
                        else
                        {
                            //log?
                        }
                    }
                }
                return dt;
            }
            catch (SqlException)
            {
                //log
                throw;
            }
            catch (Exception)
            {
                //log
                throw;
            }
        }

        private static DataTable GetDataFromODBCDatabase(string connectionString, CommandType commandType, string commandText)
        {
            DataTable dt = new DataTable("DataTable");

            try
            {
                using (OdbcConnection cn = new OdbcConnection(connectionString))
                {
                    cn.Open();
                    using (OdbcCommand cmd = new OdbcCommand())
                    {
                        cmd.Connection = cn;
                        cmd.CommandType = commandType;
                        cmd.CommandText = commandText.ReplaceWhiteSpaces(" ");
                        OdbcDataReader dr = cmd.ExecuteReader();
                        if (dr.HasRows)
                        {
                            dt.Load(dr);
                            //int fieldCount = dr.FieldCount - 1;
                            //string separator = string.Empty;
                            //while (dr.Read())
                            //{
                            //    StringBuilder sbLine = new StringBuilder();
                            //    for (int i = 0; i <= fieldCount; i++)
                            //    {
                            //        //If you are dealing with the last field, do not add a separator if there is a value, otherwise add one.
                            //        separator = (i != fieldCount || (i == fieldCount && (dr.IsDBNull(i) || string.IsNullOrEmpty(dr[i].ToString())))) ? csvSeparator : string.Empty;
                            //        //separator = (i != fieldCount) ? csvSeparator : string.Empty;
                            //        sbLine.Append(dr[i].ToString() + separator);
                            //    }
                            //    sbData.AppendLine(sbLine.ToString());
                            //    sbLine = null;
                            //}
                        }
                        else
                        {
                            //log?
                        }
                    }
                }
                return dt;
            }
            catch (SqlException)
            {
                //log
                throw;
            }
            catch (Exception)
            {
                //log
                throw;
            }
        }

        private static string GetDataFromSQLDatabase(string connectionString, CommandType commandType, string commandText, string csvSeparator, bool includeColumnHeader)
        {
            StringBuilder sbData = new StringBuilder();
            try
            {
                using (SqlConnection cn = new SqlConnection(connectionString))
                {
                    cn.Open();
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = cn;
                        cmd.CommandType = commandType;
                        cmd.CommandText = commandText;
                        SqlDataReader dr = cmd.ExecuteReader();
                        if (dr.HasRows)
                        {
                            int fieldCount = dr.FieldCount - 1;
                            string separator = string.Empty;

                            if (includeColumnHeader)
                            {
                                StringBuilder sbColumns = new StringBuilder();
                                for (int i = 0; i < fieldCount; i++)
                                {
                                    separator = (i != fieldCount) ? csvSeparator : string.Empty;
                                    sbColumns.Append(dr.GetName(i) + separator);
                                }
                                sbData.AppendLine(sbColumns.ToString());
                            }

                            //DataTable dt = new DataTable();
                            //dt.Load(dr);
                            //int numRows = dt.Rows.Count;

                            while (dr.Read())
                            {
                                StringBuilder sbLine = new StringBuilder();
                                for (int i = 0; i <= fieldCount; i++)
                                {
                                    //If you are dealing with the last field, do not add a separator if there is a value, otherwise add one.
                                    separator = (i != fieldCount || (i == fieldCount && (dr.IsDBNull(i) || string.IsNullOrEmpty(dr[i].ToString())))) ? csvSeparator : string.Empty;
                                    //separator = (i != fieldCount) ? csvSeparator : string.Empty;
                                    sbLine.Append(dr[i].ToString() + separator);
                                }
                               
                                sbData.AppendLine(sbLine.ToString());
                                sbLine = null;
                            }
                        }
                        else
                        {
                            //log?
                        }
                    }
                }
                return sbData.ToString();
            }
            catch (SqlException)
            {
                //log
                throw;
            }
            catch (Exception)
            {
                //log
                throw;
            }
        }

        private static string GetDataFromODBCDatabase(string connectionString, CommandType commandType, string commandText, string csvSeparator, bool includeColumnHeader)
        {
            StringBuilder sbData = new StringBuilder();
            try
            {
                using (OdbcConnection cn = new OdbcConnection(connectionString))
                {
                    cn.Open();
                    using (OdbcCommand cmd = new OdbcCommand())
                    {
                        cmd.Connection = cn;
                        cmd.CommandType = commandType;
                        cmd.CommandText = commandText.ReplaceWhiteSpaces(" ");
                        OdbcDataReader dr = cmd.ExecuteReader();
                        if (dr.HasRows)
                        {
                            int fieldCount = dr.FieldCount - 1;
                            string separator = string.Empty;

                            if (includeColumnHeader)
                            {
                                StringBuilder sbColumns = new StringBuilder();
                                for (int i = 0; i <= fieldCount; i++)
                                {
                                    separator = (i != fieldCount) ? csvSeparator : string.Empty;
                                    sbColumns.Append(dr.GetName(i) + separator);
                                }
                                sbData.AppendLine(sbColumns.ToString());
                            }

                            while (dr.Read())
                            {
                                StringBuilder sbLine = new StringBuilder();
                                for (int i = 0; i <= fieldCount; i++)
                                {
                                    //If you are dealing with the last field, do not add a separator if there is a value, otherwise add one.
                                    separator = (i != fieldCount || (i == fieldCount && (dr.IsDBNull(i) || string.IsNullOrEmpty(dr[i].ToString())))) ? csvSeparator : string.Empty;
                                    //separator = (i != fieldCount) ? csvSeparator : string.Empty;
                                    sbLine.Append(dr[i].ToString() + separator);
                                }
                                sbData.AppendLine(sbLine.ToString());
                                sbLine = null;
                            }
                        }
                        else
                        {
                            //log?
                        }
                    }
                }
                return sbData.ToString();
            }
            catch (SqlException)
            {
                //log
                throw;
            }
            catch (Exception)
            {
                //log
                throw;
            }
        }

        private static string GetDataFromFile(string filename, string pattern)
        {
            string data = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(filename))
                {
                    //return File.ReadAllText(filename);                    
                    using (StreamReader sr = new StreamReader(filename, Encoding.Default))
                    {
                        data = sr.ReadToEnd();
                    }
                    return data;
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                data = null;
            }
            return string.Empty;
        }

        private static string GetCsvFilename(string csvFilePath, string pattern)
        {
            try
            {
                string filename = string.Empty;
                pattern = IOHelper.WildcardToRegex(pattern);
                string[] files = Directory.GetFiles(csvFilePath).Where(path => Regex.Match(path, pattern, RegexOptions.IgnoreCase).Success).ToArray<string>();
                if (files.Length > 0)
                {
                    filename = files[0];
                }
                return filename;
            }
            catch (IOException ex)
            {
                throw new Exception("An unhandled IO exception has occured while attempting to obtain a csv file in " + csvFilePath, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("An unhandled exception has occured while attempting to obtain a csv file in " + csvFilePath, ex);
            }
        }

        public static void MoveFileToBackupPath(string backupPath, string filename, string hashKey)
        {
            try
            {
                if (!Directory.Exists(backupPath))
                    Directory.CreateDirectory(backupPath);

                string backupFilename = Path.Combine(backupPath, Path.GetFileNameWithoutExtension(filename) + "_" + hashKey + Path.GetExtension(filename));
                if (File.Exists(backupFilename))
                {
                    File.Delete(backupFilename); //Even though the files are identical in regards to their data, this always ensure that only the latest one is available.
                }
                if (File.Exists(filename))
                    File.Move(filename, backupFilename);
            }
            catch (IOException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static void MoveFileToErrorPath(string errorPath, string filename, string hashKey)
        {
            try
            {
                if (!Directory.Exists(errorPath))
                    Directory.CreateDirectory(errorPath);

                string faultyFilename = Path.Combine(errorPath, Path.GetFileNameWithoutExtension(filename) + "_" + hashKey + Path.GetExtension(filename));
                if (File.Exists(faultyFilename))
                {
                    File.Delete(faultyFilename); //Even though the files are identical in regards to their data, this always ensure that only the latest one is available.
                }
                if (File.Exists(filename))
                    File.Move(filename, faultyFilename);
            }
            catch (IOException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static bool IsHashFileExists(string filename, string hashKey)
        {
            string TaskRunnerPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "Adest\\TaskRunner\\Processed");

            try
            {
                if (!Directory.Exists(TaskRunnerPath))
                    Directory.CreateDirectory(TaskRunnerPath);

                filename = Path.Combine(TaskRunnerPath, string.Concat(Path.GetFileNameWithoutExtension(filename), "_", hashKey, ".txt"));

                if (!File.Exists(filename))
                {
                    return true; //If file doesn't exists, can't be the same
                }
                else
                {
                    return false; //if exact file exists, it means the very same filename containing the same data was previously processed.
                }
            }
            catch (IOException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static void CreateHashFile(string filename, string hashKey)
        {
            string TaskRunnerPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "Adest\\TaskRunner\\Processed");

            try
            {
                if (!Directory.Exists(TaskRunnerPath))
                    Directory.CreateDirectory(TaskRunnerPath);

                string[] previousFiles = Directory.GetFiles(TaskRunnerPath, Path.GetFileNameWithoutExtension(filename) + "*.txt");

                foreach (string previousFile in previousFiles)
                {
                    if (File.Exists(previousFile))
                        File.Delete(previousFile);
                }

                filename = Path.Combine(TaskRunnerPath, string.Concat(Path.GetFileNameWithoutExtension(filename), "_", hashKey, ".txt"));

                File.WriteAllText(filename, string.Empty);
            }
            catch (IOException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static void DeleteFile(string filename)
        {
            if (File.Exists(filename))
            {
                try
                {
                    File.Delete(filename);
                }
                catch (IOException)
                {
                    //log
                    throw;
                }
                catch (Exception)
                {
                    //log
                    throw;
                }
            }
        }
    }
}
