﻿using System;
using System.ComponentModel;
using System.Xml;
using System.Xml.Serialization;

namespace TaskRunner.Core
{
    [Serializable]
    public sealed class LogPath
    {
        public LogPath()
        {
            MaxLogFiles = 500;
        }

        [XmlIgnore]
        public int Id { get; set; }

        [XmlAttribute("Id")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public string IdString
        {
            get { return Id == int.MinValue ? string.Empty : XmlConvert.ToString(Id); }
            set { Id = string.IsNullOrEmpty(value) ? int.MinValue : XmlConvert.ToInt32(value); }
        }

        [XmlAttribute]
        public string Path { get; set; }

        [XmlIgnore]
        public int MaxLogFiles { get; set; }

        [XmlAttribute("MaxLogFiles")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public string MaxLogFilesString
        {
            get { return MaxLogFiles.ToString(); }
            set { MaxLogFiles = string.IsNullOrEmpty(value) ? 500 : Convert.ToInt32(value); }
        }
    }
}
