﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace TaskRunner.Core
{
    public sealed class EncryptionHelper
    {
        private static string salt = "LBYsraa;289H3ogq52W|97+P9J84.#";
        private static string inhouseKey = "!gV0[7VoFI6$#447Q/O(j[anj?{v:m";

        public static string EncryptToString<T>(string value, string password) where T : SymmetricAlgorithm, new()
        {
            return Convert.ToBase64String(Encrypt<T>(value, password));
        }

        public static byte[] EncryptToByte<T>(byte[] value, string password) where T : SymmetricAlgorithm, new()
        {
            return Encrypt<T>(Convert.ToBase64String(value), password);
        }

        /// <summary>
        /// http://www.superstarcoders.com/blogs/posts/symmetric-encryption-in-c-sharp.aspx
        /// </summary>
        public static byte[] Encrypt<T>(string value, string password)
              where T : SymmetricAlgorithm, new()
        {
            DeriveBytes rgb = new Rfc2898DeriveBytes(password, Encoding.Unicode.GetBytes(salt));

            SymmetricAlgorithm algorithm = new T();

            byte[] rgbKey = rgb.GetBytes(algorithm.KeySize >> 3);
            byte[] rgbIV = rgb.GetBytes(algorithm.BlockSize >> 3);

            ICryptoTransform transform = algorithm.CreateEncryptor(rgbKey, rgbIV);

            using (MemoryStream buffer = new MemoryStream())
            {
                using (CryptoStream stream = new CryptoStream(buffer, transform, CryptoStreamMode.Write))
                {
                    using (StreamWriter writer = new StreamWriter(stream, Encoding.Unicode))
                    {
                        writer.Write(value);
                    }
                }

                return buffer.ToArray();
            }
        }

        public static string Decrypt<T>(string text, string password)
           where T : SymmetricAlgorithm, new()
        {
            DeriveBytes rgb = new Rfc2898DeriveBytes(password, Encoding.Unicode.GetBytes(salt));

            SymmetricAlgorithm algorithm = new T();

            byte[] rgbKey = rgb.GetBytes(algorithm.KeySize >> 3);
            byte[] rgbIV = rgb.GetBytes(algorithm.BlockSize >> 3);

            ICryptoTransform transform = algorithm.CreateDecryptor(rgbKey, rgbIV);

            using (MemoryStream buffer = new MemoryStream(Convert.FromBase64String(text)))
            {
                using (CryptoStream stream = new CryptoStream(buffer, transform, CryptoStreamMode.Read))
                {
                    using (StreamReader reader = new StreamReader(stream, Encoding.Unicode))
                    {
                        return reader.ReadToEnd();
                    }
                }
            }
        }

        public static byte[] DecryptFromByte<T>(byte[] value, string password)
        where T : SymmetricAlgorithm, new()
        {
            return Convert.FromBase64String(Decrypt<T>(Convert.ToBase64String(value), password));
        }

        public static string DecryptFromString<T>(string value, string password)
        where T : SymmetricAlgorithm, new()
        {
            return Decrypt<T>(value, password);
        }

        public static string AdEncrypt(string value)
        {
            return EncryptToString<AesManaged>(value, inhouseKey);
        }

        public static string AdDecrypt(string value)
        {
            return DecryptFromString<AesManaged>(value, inhouseKey);
        }

        public static string GetMD5Hash(string input)
        {
            MD5CryptoServiceProvider x = new MD5CryptoServiceProvider();
            byte[] bs = Encoding.UTF8.GetBytes(input);
            bs = x.ComputeHash(bs);
            StringBuilder s = new StringBuilder();
            foreach (byte b in bs)
            {
                s.Append(b.ToString("x2").ToLower());
            }
            return s.ToString();
        }
    }
}
