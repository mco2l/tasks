﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Linq;

namespace TaskRunner.Core
{
    public sealed class IOHelper
    {
        public static string WildcardToRegex(string pattern)
        {
            return "^" + Regex.Escape(pattern).Replace("\\*", ".*").Replace("\\?", ".") + "$";
        }

        public static bool IsFileLocked(string filename)
        {
            try
            {
                using (FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.ReadWrite, FileShare.None))
                {
                }
                return false;
            }
            catch (IOException)
            {
                return true;
            }
            catch (Exception)
            {
                return true;
            }
        }

        /// <summary>
        /// Load a file to byte array.
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static byte[] LoadFileToArray(string fileName)
        {
            FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            byte[] data = new byte[Convert.ToInt32(fs.Length)];
            fs.Read(data, 0, Convert.ToInt32(fs.Length));
            fs.Close();
            return data;
        }

        /// <summary>Procedure which allows you to output a byte array into a file.</summary>
        /// <returns>
        /// Returns true if successful.
        /// </returns>
        /// <param name="fileName">The full path and file name for the file that you want the data output to.</param>
        /// <param name="data">The byte array containing the data that you want to output to a file.</param>
        public static bool SaveArrayToFile(byte[] data, string fileName, bool overwrite)
        {
            if (!System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(fileName)))
                System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(fileName));
            if (System.IO.File.Exists(fileName) & overwrite)
            {
                System.IO.File.Delete(fileName);
            }
            else if (System.IO.File.Exists(fileName) & !overwrite)
            {
                throw new System.IO.IOException(fileName.Trim() + " already exists.");
            }

            FileStream fs = new FileStream(fileName, FileMode.CreateNew);
            fs.Write(data, 0, data.Length);
            fs.Close();

            return true;
        }

        /// <summary>
        /// Checks if a specific extension is in a list semi-colon separated string
        /// </summary>
        /// <param name="extension"></param>
        /// <param name="excludeList"></param>
        /// <returns></returns>
        public static bool IsExtensionInList(string extension, string excludeList)
        {
            //If the extension is part of the exclusion list, it should not be processed, so return the false. 
            string[] extensions = excludeList.Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            return extensions.Any(m => m.ToLowerInvariant() == extension.ToLowerInvariant());
        }

        public static string FormatBytes(double totalBytes)
        {
            const Double ONE_KB = 1024;
            const Double ONE_MB = ONE_KB * 1024;
            const Double ONE_GB = ONE_MB * 1024;
            const Double ONE_TB = ONE_GB * 1024;
            const Double ONE_PB = ONE_TB * 1024;
            const Double ONE_EB = ONE_PB * 1024;
            const Double ONE_ZB = ONE_EB * 1024;
            const Double ONE_YB = ONE_ZB * 1024;

            //See how big the value is.
            if (totalBytes <= 999)
            {
                //Format in bytes.
                return totalBytes.ToString("0") + " bytes";
            }
            else if (totalBytes <= ONE_KB * 999)
            {
                //Format in KB.
                return ThreeNonZeroDigits(totalBytes / ONE_KB) + " " + "Kb";
            }
            else if (totalBytes <= ONE_MB * 999)
            {
                //Format in MB.
                return ThreeNonZeroDigits(totalBytes / ONE_MB) + " " + "Mb";
            }
            else if (totalBytes <= ONE_GB * 999)
            {
                //Format in GB.
                return ThreeNonZeroDigits(totalBytes / ONE_GB) + " " + "Gb";
            }
            else if (totalBytes <= ONE_TB * 999)
            {
                //Format in TB.
                return ThreeNonZeroDigits(totalBytes / ONE_TB) + " " + "Tb";
            }
            else if (totalBytes <= ONE_PB * 999)
            {
                //Format in PB.
                return ThreeNonZeroDigits(totalBytes / ONE_PB) + " " + "Pb";
            }
            else if (totalBytes <= ONE_EB * 999)
            {
                //Format in EB.
                return ThreeNonZeroDigits(totalBytes / ONE_EB) + " " + "Eb";
            }
            else if (totalBytes <= ONE_ZB * 999)
            {
                //Format in ZB.
                return ThreeNonZeroDigits(totalBytes / ONE_ZB) + " " + "Zb";
            }
            else
            {
                //Format in YB.
                return ThreeNonZeroDigits(totalBytes / ONE_YB) + " " + "Yb";
            }
        }

        private static string ThreeNonZeroDigits(double value)
        {
            if (value >= 100)
            {
                return Convert.ToInt32(value).ToString();
            }
            else if (value >= 10)
            {
                return value.ToString("0.0");
            }
            else
            {
                return value.ToString("0.00");
            }

        }

        public static void ClearOlderFiles(string path, string pattern, int maxFilesAllowed, bool throwExceptionOnDelete)
        {
            FileInfo[] files = new DirectoryInfo(path).GetFiles(pattern);

            if (files.Length > 0 && files.Length > (maxFilesAllowed))
            {
                int difference = files.Length - (maxFilesAllowed);
                files = files.OrderBy(m => m.LastWriteTime).ToArray<FileInfo>().Take(difference).ToArray<FileInfo>();
                foreach (var file in files)
                {
                    try
                    {
                        File.Delete(file.FullName);
                    }
                    catch (Exception)
                    {
                        if (throwExceptionOnDelete)
                            throw;
                    }
                }
            }

            files = null;
        }

    }
}
