﻿using System;
using TaskRunner.Core.Enums;

namespace TaskRunner.Core.Logging
{
    public sealed class LogEntry
    {
        public string Message { get; set; }
        public Exception Exception { get; set; }
        public string ExceptionGuid { get; set; }
        public AdestLogMessageType LogMessageType { get; set; }
        public AdestLogType LogType { get; set; }
        public string MethodName { get; set; }       
        public string ApplicationName { get; set; }
    }
}
