﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskRunner.Core
{
    public interface ITenant
    {
        string MasterDatabase { set; }
        int TenantID { set; }
        void LoadTenant();
        
    }
}
