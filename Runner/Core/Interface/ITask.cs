﻿using System.Collections.Generic;

namespace TaskRunner.Core
{
    public interface ITask
    {
        void RunTask(string tenantDB, int taskID);
        void SetTaskSettings(string tenantDB, int taskID);
        bool IsProcessing { get; }
        bool IsSuccess { get; }
        bool IsDataProcessed { get; }
        string LogFilename { get;   }
        string LogErrorFilename { get; }
        Dictionary<string, string> AdditionalTags { get; set; }
    }
}
