﻿using System;
using System.Collections.Generic;

namespace TaskRunner.Model
{
    public partial class SettingsRsoSage50
    {
        public int Id { get; set; }
        public int? TaskId { get; set; }
        public string Company { get; set; }
        public string DatabasePath { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string AttachmentPath { get; set; }
        public int RsosettingId { get; set; }
        public int HeaderFieldMappingId { get; set; }
        public int LineFieldMappingId { get; set; }
        public int DownloadSettingId { get; set; }

        public virtual SettingsSharedDownloadSettings DownloadSetting { get; set; }
        public virtual SettingsSharedFieldMapping HeaderFieldMapping { get; set; }
        public virtual SettingsSharedFieldMapping LineFieldMapping { get; set; }
        public virtual SettingsSharedRso Rsosetting { get; set; }
        public virtual SettingsTask Task { get; set; }
    }
}
