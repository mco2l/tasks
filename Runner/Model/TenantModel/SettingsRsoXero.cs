﻿using System;
using System.Collections.Generic;

namespace TaskRunner.Model
{
    public partial class SettingsRsoXero
    {
        public int Id { get; set; }
        public int? TaskId { get; set; }
        public string OrganizationName { get; set; }
        public string ConsumerKey { get; set; }
        public string ConsumerSecret { get; set; }
        public string AppName { get; set; }
        public string Buyer { get; set; }
        public string UseXeroFailureSupplier { get; set; }
        public string XeroFailureSupplier { get; set; }
        public int DownloadSettingId { get; set; }
        public int RsosettingId { get; set; }
        public int? DebugLogSettingId { get; set; }
        public string InvoiceLogEnabled { get; set; }
        public string InvoiceLogPath { get; set; }
        public bool ClearBeforeUpload { get; set; }
        public virtual SettingsSharedDebugLog DebugLogSetting { get; set; }
        public virtual SettingsSharedDownloadSettings DownloadSetting { get; set; }
        public virtual SettingsSharedRso Rsosetting { get; set; }
        public virtual SettingsTask Task { get; set; }
    }
}
