﻿using System;
using System.Collections.Generic;

namespace TaskRunner.Model
{
    public partial class SettingsSage200Posting
    {
        public int Id { get; set; }
        public int? TaskId { get; set; }
        public string Company { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int AdestWebServiceSettingId { get; set; }
        public int? DebugLogSettingId { get; set; }
        public int HeaderFieldMappingId { get; set; }
        public int? LineFieldMappingId { get; set; }

        public virtual SettingsSharedAdestWebService AdestWebServiceSetting { get; set; }
        public virtual SettingsSharedDebugLog DebugLogSetting { get; set; }
        public virtual SettingsSharedFieldMapping HeaderFieldMapping { get; set; }
        public virtual SettingsSharedFieldMapping LineFieldMapping { get; set; }
        public virtual SettingsTask Task { get; set; }
    }
}
