﻿using System;
using System.Collections.Generic;

namespace TaskRunner.Model
{
    public partial class SettingsSharedEmail
    {
        public SettingsSharedEmail()
        {
            SettingsTaskEmailFailureSetting = new HashSet<SettingsTask>();
            SettingsTaskEmailSuccessSetting = new HashSet<SettingsTask>();
        }

        public int Id { get; set; }
        public string EmailTo { get; set; }
        public string Cc { get; set; }
        public string Bcc { get; set; }
        public string Subject { get; set; }
        public string BodyTextFile { get; set; }
        public string IsHtmlBody { get; set; }
        public string Priority { get; set; }
        public string Sensitivity { get; set; }
        public string EmailFrom { get; set; }
        public string FromDisplayName { get; set; }
        public string ReplyTo { get; set; }

        public virtual ICollection<SettingsTask> SettingsTaskEmailFailureSetting { get; set; }
        public virtual ICollection<SettingsTask> SettingsTaskEmailSuccessSetting { get; set; }
    }
}
