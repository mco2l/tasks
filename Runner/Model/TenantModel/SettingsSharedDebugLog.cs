﻿using System;
using System.Collections.Generic;

namespace TaskRunner.Model
{
    public partial class SettingsSharedDebugLog
    {
        public SettingsSharedDebugLog()
        {
            SettingsRsoXero = new HashSet<SettingsRsoXero>();
            SettingsSage200Posting = new HashSet<SettingsSage200Posting>();
            SettingsSage50Posting = new HashSet<SettingsSage50Posting>();
        }

        public int Id { get; set; }
        public string Enabled { get; set; }
        public string Path { get; set; }

        public virtual ICollection<SettingsRsoXero> SettingsRsoXero { get; set; }
        public virtual ICollection<SettingsSage200Posting> SettingsSage200Posting { get; set; }
        public virtual ICollection<SettingsSage50Posting> SettingsSage50Posting { get; set; }
    }
}
