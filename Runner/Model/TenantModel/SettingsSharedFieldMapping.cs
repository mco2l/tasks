﻿using System;
using System.Collections.Generic;

namespace TaskRunner.Model
{
    public partial class SettingsSharedFieldMapping
    {
        public SettingsSharedFieldMapping()
        {
            SettingsRsoMasterUpload = new HashSet<SettingsRsoMasterUpload>();
            SettingsRsoSage50HeaderFieldMapping = new HashSet<SettingsRsoSage50>();
            SettingsRsoSage50LineFieldMapping = new HashSet<SettingsRsoSage50>();
            SettingsRsoScanDownloadFieldMapping = new HashSet<SettingsRsoScanDownload>();
            SettingsSage200PostingHeaderFieldMapping = new HashSet<SettingsSage200Posting>();
            SettingsSage200PostingLineFieldMapping = new HashSet<SettingsSage200Posting>();
            SettingsSage50PostingHeaderFieldMapping = new HashSet<SettingsSage50Posting>();
            SettingsSharedFieldMappingFields = new HashSet<SettingsSharedFieldMappingFields>();
            SettingsIndexHeaderFieldMapping = new HashSet<SettingsIndex>();
            SettingsIndexLineFieldMapping = new HashSet<SettingsIndex>();
        }

        public int Id { get; set; }
        public string Description { get; set; }

        public virtual ICollection<SettingsRsoMasterUpload> SettingsRsoMasterUpload { get; set; }
        public virtual ICollection<SettingsRsoSage50> SettingsRsoSage50HeaderFieldMapping { get; set; }
        public virtual ICollection<SettingsRsoSage50> SettingsRsoSage50LineFieldMapping { get; set; }
        public virtual ICollection<SettingsRsoScanDownload> SettingsRsoScanDownloadFieldMapping { get; set; }
        public virtual ICollection<SettingsSage200Posting> SettingsSage200PostingHeaderFieldMapping { get; set; }
        public virtual ICollection<SettingsSage200Posting> SettingsSage200PostingLineFieldMapping { get; set; }
        public virtual ICollection<SettingsSage50Posting> SettingsSage50PostingHeaderFieldMapping { get; set; }
        public virtual ICollection<SettingsSage50Posting> SettingsSage50PostingLineFieldMapping { get; set; }
        public virtual ICollection<SettingsSharedFieldMappingFields> SettingsSharedFieldMappingFields { get; set; }
        public virtual ICollection<SettingsIndex> SettingsIndexHeaderFieldMapping { get; set; }
        public virtual ICollection<SettingsIndex> SettingsIndexLineFieldMapping { get; set; }
    }
}
