﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace TaskRunner.Model
{
    public partial class SettingsTask
    {
        public SettingsTask()
        {
            SettingsRsoMasterUpload = new HashSet<SettingsRsoMasterUpload>();
            SettingsRsoSage50 = new HashSet<SettingsRsoSage50>();
            SettingsRsoScanDownload = new HashSet<SettingsRsoScanDownload>();
            SettingsRsoScanUpload = new HashSet<SettingsRsoScanUpload>();
            SettingsRsoXero = new HashSet<SettingsRsoXero>();
            SettingsSage200Posting = new HashSet<SettingsSage200Posting>();
            SettingsSage50Posting = new HashSet<SettingsSage50Posting>();
            SettingsTaskSubMainTask = new HashSet<SettingsTaskSub>();
            SettingsTaskSubSubTask = new HashSet<SettingsTaskSub>();
        }

        public int Id { get; set; }
        public int TaskTypeId { get; set; }
        public string Description { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public long Interval { get; set; }
        public long TotalRunPerTimeRange { get; set; }
        public bool Disabled { get; set; }
        public bool EnableTracing { get; set; }
        public long MaxLogFiles { get; set; }
        public int EmailSuccessSettingId { get; set; }
        public int EmailFailureSettingId { get; set; }
        public bool RunOnStart { get; set; }
        public long? MaxFailuresAllowed { get; set; }
        public long? IntervalFailureRetry { get; set; }

        public virtual ICollection<SettingsRsoMasterUpload> SettingsRsoMasterUpload { get; set; }
        public virtual ICollection<SettingsRsoSage50> SettingsRsoSage50 { get; set; }
        public virtual ICollection<SettingsRsoScanDownload> SettingsRsoScanDownload { get; set; }
        public virtual ICollection<SettingsRsoScanUpload> SettingsRsoScanUpload { get; set; }
        public virtual ICollection<SettingsRsoXero> SettingsRsoXero { get; set; }
        public virtual ICollection<SettingsSage200Posting> SettingsSage200Posting { get; set; }
        public virtual ICollection<SettingsSage50Posting> SettingsSage50Posting { get; set; }
        public virtual ICollection<SettingsTaskSub> SettingsTaskSubMainTask { get; set; }
        public virtual ICollection<SettingsTaskSub> SettingsTaskSubSubTask { get; set; }
        public virtual SettingsSharedEmail EmailFailureSetting { get; set; }
        public virtual SettingsSharedEmail EmailSuccessSetting { get; set; }
        public virtual ICollection<SettingsIndex> SettingsIndex { get; set; }
    }
}
