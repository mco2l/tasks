﻿using System;
using System.Collections.Generic;

namespace TaskRunner.Model
{
    public partial class SettingsRsoMasterUpload
    {
        public int Id { get; set; }
        public int TaskId { get; set; }
        public int UploadType { get; set; }
        public string ClearBeforeUpload { get; set; }
        public string CsvSeparator { get; set; }
        public string DataFilePath { get; set; }
        public string DataFilePattern { get; set; }
        public string DataFilePatternExt { get; set; }
        public string DataFilePatternMask { get; set; }
        public string BackupPath { get; set; }
        public string ErrorPath { get; set; }
        public string SkipFirstLine { get; set; }
        public string IsSplitFiles { get; set; }
        public bool IsFileShared { get; set; }
        public string DbconnectionType { get; set; }
        public string DbconnectionString { get; set; }
        public string DbcommandType { get; set; }
        public string DbcommandText { get; set; }
        public int RsosettingId { get; set; }
        public int FieldMappingId { get; set; }

        public virtual SettingsSharedFieldMapping FieldMapping { get; set; }
        public virtual SettingsSharedRso Rsosetting { get; set; }
        public virtual SettingsTask Task { get; set; }
    }
}
