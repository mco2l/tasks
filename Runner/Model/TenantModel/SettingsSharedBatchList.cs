﻿using System;
using System.Collections.Generic;

namespace TaskRunner.Model
{
    public partial class SettingsSharedBatchList
    {
        public SettingsSharedBatchList()
        {
            SettingsRsoScanUpload = new HashSet<SettingsRsoScanUpload>();
        }

        public int Id { get; set; }
        public string Path { get; set; }
        public string DocumentType { get; set; }
        public string BuyersName { get; set; }

        public virtual ICollection<SettingsRsoScanUpload> SettingsRsoScanUpload { get; set; }
    }
}
