﻿using System;
using System.Collections.Generic;

namespace TaskRunner.Model
{
    public partial class SettingsIndex
    {
        public long Id { get; set; }
        public int? TaskId { get; set; }
        public int? IdentityServerEndpoint { get; set; }
        public int? Apiendpoint { get; set; }
        public int? HeaderFieldMappingId { get; set; }
        public int? LineFieldMappingId { get; set; }
        public string IndexDataFilePath { get; set; }

        public virtual SettingsSharedAdestWebService ApiendpointNavigation { get; set; }
        public virtual SettingsSharedFieldMapping HeaderFieldMapping { get; set; }
        public virtual SettingsSharedAdestWebService IdentityServerEndpointNavigation { get; set; }
        public virtual SettingsSharedFieldMapping LineFieldMapping { get; set; }
        public virtual SettingsTask Task { get; set; }
    }
}
