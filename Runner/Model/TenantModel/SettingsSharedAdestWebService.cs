﻿using System;
using System.Collections.Generic;

namespace TaskRunner.Model
{
    public partial class SettingsSharedAdestWebService
    {
        public SettingsSharedAdestWebService()
        {
            SettingsSage200Posting = new HashSet<SettingsSage200Posting>();
            SettingsSage50Posting = new HashSet<SettingsSage50Posting>();
            SettingsIndexApiendpointNavigation = new HashSet<SettingsIndex>();
            SettingsIndexIdentityServerEndpointNavigation = new HashSet<SettingsIndex>();
        }

        public int Id { get; set; }
        public string Uri { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string SearchCompany { get; set; }

        public virtual ICollection<SettingsSage200Posting> SettingsSage200Posting { get; set; }
        public virtual ICollection<SettingsSage50Posting> SettingsSage50Posting { get; set; }
        public virtual ICollection<SettingsIndex> SettingsIndexApiendpointNavigation { get; set; }
        public virtual ICollection<SettingsIndex> SettingsIndexIdentityServerEndpointNavigation { get; set; }
    }
}
