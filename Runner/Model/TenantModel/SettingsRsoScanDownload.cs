﻿using System;
using System.Collections.Generic;

namespace TaskRunner.Model
{
    public partial class SettingsRsoScanDownload
    {
        public int Id { get; set; }
        public int? TaskId { get; set; }
        public string DownloadPath { get; set; }
        public int ImageLocationIndex { get; set; }
        public bool CreateBuyerFolder { get; set; }
        public bool UsePermaLink { get; set; }
        public int RsosettingId { get; set; }
        public int FieldMappingId { get; set; }

        public virtual SettingsSharedFieldMapping FieldMapping { get; set; }
        public virtual SettingsSharedRso Rsosetting { get; set; }
        public virtual SettingsTask Task { get; set; }
    }
}
