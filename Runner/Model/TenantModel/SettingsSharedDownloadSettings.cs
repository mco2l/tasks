﻿using System;
using System.Collections.Generic;

namespace TaskRunner.Model
{
    public partial class SettingsSharedDownloadSettings
    {
        public SettingsSharedDownloadSettings()
        {
            SettingsRsoSage50 = new HashSet<SettingsRsoSage50>();
            SettingsRsoXero = new HashSet<SettingsRsoXero>();
        }

        public int Id { get; set; }
        public string Path { get; set; }
        public string BackupPath { get; set; }
        public string FailurePath { get; set; }

        public virtual ICollection<SettingsRsoSage50> SettingsRsoSage50 { get; set; }
        public virtual ICollection<SettingsRsoXero> SettingsRsoXero { get; set; }
    }
}
