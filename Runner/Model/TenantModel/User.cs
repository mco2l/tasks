﻿using System;
using System.Collections.Generic;

namespace TaskRunner.Model
{
    public partial class User
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public bool IsAdmin { get; set; }
        public int? DelegateTo { get; set; }
        public string DelegateFromDate { get; set; }
        public string DelegateToDate { get; set; }
        public int? LockCount { get; set; }
    }
}
