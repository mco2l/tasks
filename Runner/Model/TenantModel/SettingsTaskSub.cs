﻿using System;
using System.Collections.Generic;

namespace TaskRunner.Model
{
    public partial class SettingsTaskSub
    {
        public int Id { get; set; }
        public int MainTaskId { get; set; }
        public int SubTaskId { get; set; }
        public int Order { get; set; }

        public virtual SettingsTask MainTask { get; set; }
        public virtual SettingsTask SubTask { get; set; }
    }
}
