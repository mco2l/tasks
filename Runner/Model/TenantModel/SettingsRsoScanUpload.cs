﻿using System;
using System.Collections.Generic;

namespace TaskRunner.Model
{
    public partial class SettingsRsoScanUpload
    {
        public int Id { get; set; }
        public int? TaskId { get; set; }
        public bool AutoDetection { get; set; }
        public bool AutoBuyerId { get; set; }
        public string BatchPath { get; set; }
        public long? MaxFileSize { get; set; }
        public string DefaultDocumentSystemName { get; set; }
        public string OverMaxSizeFilePath { get; set; }
        public string BackupPath { get; set; }
        public bool AutoCreateFolder { get; set; }
        public int? BatchListId { get; set; }
        public int RsosettingId { get; set; }

        public virtual SettingsSharedBatchList BatchList { get; set; }
        public virtual SettingsSharedRso Rsosetting { get; set; }
        public virtual SettingsTask Task { get; set; }
    }
}
