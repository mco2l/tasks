﻿using System;
using System.Collections.Generic;

namespace TaskRunner.Model
{
    public partial class SettingsSharedFieldMappingFields
    {
        public SettingsSharedFieldMappingFields()
        {
            //SettingsRsoScanDownload = new HashSet<SettingsRsoScanDownload>();
        }

        public int Id { get; set; }
        public int? MappingId { get; set; }
        public string FromField { get; set; }
        public string ToField { get; set; }
        public string DefaultValue { get; set; }
        //public virtual ICollection<SettingsRsoScanDownload> SettingsRsoScanDownload { get; set; }
        public virtual SettingsSharedFieldMapping SettingsSharedFieldMapping { get; set; }
    }
}
