﻿namespace TaskRunner.Model
{
    public partial class TaskTypes
    {
        public int TaskTypeId { get; set; }
        public string Description { get; set; }
        public string AssemblyName { get; set; }
        public string AssemblyType { get; set; }
        public string SettingsTable { get; set; }

        public virtual TaskTypeSettings TaskTypeSettings { get; set; }
    }
}
