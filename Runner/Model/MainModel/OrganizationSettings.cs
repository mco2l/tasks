﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace TaskRunner.Model
{
    public partial class OrganizationSettings
    {
        public int TenantId { get; set; }
        public string TenantName { get; set; }
        public string AccessKey { get; set; }
        public bool Disabled { get; set; }
        [NotMapped]
        public bool IsProcessing { get; set; }
    }
}
