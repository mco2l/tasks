﻿namespace TaskRunner.Model
{
    public partial class TaskTypeSettings
    {
        public int Id { get; set; }
        public int TaskTypeId { get; set; }
        public string SettingName { get; set; }
        public int Type { get; set; }
        public long? MinLength { get; set; }
        public long? MaxLength { get; set; }
        public string IsCore { get; set; }

        public virtual TaskTypes TaskType { get; set; }
    }
}
