﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskRunner.Core;
using TaskRunner.Data;
using TaskRunner.Model;

namespace TaskRunner
{
    public class Tenant : ITenant
    {
        private System.Timers.Timer timerPolling = null;
        private IocContainer iocContainer = new IocContainer();
        private string _masterDatabase = string.Empty;
        private int _tenantID = 0;

        public string MasterDatabase
        {
            set
            {
                _masterDatabase = value;
            }
        }

        public int TenantID
        {
            set
            {
                _tenantID = value;
            }
        }

        public void LoadTenant()
        {
            string dbPath = string.Concat(@"Filename=E:\Sources\Tasks\Database\Test\tenant", _tenantID, ".db");
            int totalInitialized = 0;

            using (var db = new TenantDBContext(dbPath))
            {

                //Read all Tasks set up for a tenant                
                Console.WriteLine("Tenant Tasks");
                foreach (var setting in db.SettingsTask)
                {
                    //Console.WriteLine("\t - Id - {0} TaskTypeId - {1}", setting.Id, setting.TaskTypeId);

                    //Validate tenant tasks & initialize task factories
                    InitializeTaskFactory(setting.TaskTypeId, setting.Id);
                    totalInitialized += 1;
                    //Start timer for tasks - if tasks valid and factories initialized           

                }

            }
        }

        /// <summary>
        /// Disables any tasks that do not meet minimum requirements i.e.assembly name, assembly type, interval or TotalRunPerTimeRange, etc...must be set.
        /// </summary>
        /// <returns></returns>
        private bool ValidateSettings()
        {
            bool ret = true;
            StringBuilder sb = new StringBuilder();

            //foreach (AdestTask task in settings.Tasks)
            //{
            //    if (!task.Disabled)
            //    {
            //        if (task.Interval == 0 && task.TotalRunPerTimeRange == 0)
            //        {
            //            ret = false;
            //            sb.AppendLine("The Interval and TotalRunPerTimeRange cannot both be set to 0. At least one of them must be set. Task Id: " + task.Id + " has been disabled.");
            //            task.Disabled = true;
            //        }
            //        if (string.IsNullOrEmpty(task.AssemblyName) || string.IsNullOrEmpty(task.AssemblyType))
            //        {
            //            ret = false;
            //            //LogHelper.WriteToEventViewer("TaskRunner", "The assembly name and assembly type must set. Task Id: " + task.Id + " has been disabled.", AdestLogMessageType.Error);
            //            task.Disabled = true;
            //        }
            //        if (string.IsNullOrEmpty(task.LogPath))
            //        {
            //            ret = false;
            //            //LogHelper.WriteToEventViewer("TaskRunner", "The assembly name and assembly type must set. Task Id: " + task.Id + " has been disabled.", AdestLogMessageType.Error);
            //            task.Disabled = true;
            //        }
            //    }
            //}

            if (sb.Length > 0)
                //LogHelper.WriteToEventViewer("TaskRunner", sb.ToString(), AdestLogMessageType.Error);

            sb = null;

            return ret;
        }

        //Initialize the factory file for the task type
        private bool InitializeTaskFactory(int taskTypeID, int taskID)
        {

            TaskTypes currentTask = GetTaskType(taskTypeID);
            try
            {
                ITask proxy = (ITask)iocContainer.Register(currentTask.AssemblyName, currentTask.AssemblyType, taskID.ToString());
                //proxy.InitializeTask(task);
                
                //LogHelper.WriteToEventViewer("TaskRunner", "Task: '" + currentTask.Description + "' - Id: " + currentTask.Id + " - Initialized successfully.", AdestLogMessageType.Information);
            }
            catch (Exception ex)
            {
                //Automatically disable this task if an exception occurs while attempting to create an instance of it.
                //currentTask.Disabled = true;
                //LogHelper.WriteToEventViewer("TaskRunner", "Task: '" + currentTask.Description + "' - Id: " + currentTask.Id + " - Failed to initialize.", ex, AdestLogMessageType.Error);
                //LogHelper.WriteToEventViewer("TaskRunner", "Task: '" + currentTask.Description + "' - Id: " + currentTask.Id + " - Disabled until issue has been resolved.", AdestLogMessageType.Warning);
            }
            finally
            {
                currentTask = null;
            }

            return true;
        }

        private TaskTypes GetTaskType(int taskTypeID)
        {
            TaskTypes currentTask; 
            using (var db = new mainContext())
            {
                currentTask = db.TaskTypes.First(a => a.TaskTypeId == taskTypeID);
            }
            return currentTask;
        }

        //private void FinalizeTaskFactories()
        //{
        //    //logEntry.ProcedureName = "FinalizeTaskFactories";
        //    foreach (AdestTask task in settings.Tasks.OrderByDescending(m => m.Id))
        //    {
        //        AdestTask currentTask = GetTask(task.Id);
        //        try
        //        {
        //            ITask proxy = (ITask)iocContainer.Container(currentTask.Id.ToString());
        //            //proxy.FinalizeTask();
        //            proxy = null;
        //            LogHelper.WriteToEventViewer("TaskRunner", "Task: '" + currentTask.Description + "' - Id: " + currentTask.Id + " - Finalized successfully.", AdestLogMessageType.Information);
        //        }
        //        catch (Exception ex)
        //        {
        //            LogHelper.WriteToEventViewer("TaskRunner", "Task: '" + currentTask.Description + "' - Id: " + currentTask.Id + " - Failed to finalize.", ex, AdestLogMessageType.Error);
        //        }
        //        finally
        //        {
        //            currentTask = null;
        //        }
        //    }
        //}

        private void ProcessTasks()
        {

            //try
            //{
            //    foreach (AdestTask taskSettings in settings.Tasks)
            //    {
            //        //verifies if the task is not herself a sub-task
            //        bool isSubTask = TaskIsSubtask(taskSettings, settings);

            //        //Flagging task to avoid subtasks having subtasks...
            //        taskSettings.IsSubTask = isSubTask;

            //        //Only run the Task if the task is not referenced as a SubTask
            //        if (!isSubTask)
            //        {
            //            if (!taskSettings.DisabledDueToFailure && taskSettings.TotalRunFailures >= taskSettings.MaxFailuresAllowed)
            //            {
            //                LogHelper.WriteToEventViewer("TaskRunner", "Disabling JobId: " + taskSettings.Id.ToString() + " as the number of failures allowed for this job has been reached i.e. " + taskSettings.MaxFailuresAllowed.ToString(), AdestLogMessageType.Information);
            //                taskSettings.TotalRunFailures = 0;
            //                taskSettings.DisabledDueToFailure = true;
            //                taskSettings.ElapsedTime = 0;
            //            }
            //            else
            //            {
            //                //increase the elapsed time every second for each individual task
            //                taskSettings.ElapsedTime += 1;
            //            }

            //            if (taskSettings.DisabledDueToFailure && taskSettings.IntervalFailureRetry > 0 && taskSettings.ElapsedTime >= taskSettings.IntervalFailureRetry) //re-enable a task that has been disabled.
            //            {
            //                LogHelper.WriteToEventViewer("TaskRunner", "Re-enabling JobId: " + taskSettings.Id.ToString() + " as the required retry delay interval has been reached: " + taskSettings.IntervalFailureRetry.ToString() + " seconds", AdestLogMessageType.Information);
            //                taskSettings.DisabledDueToFailure = false;
            //                taskSettings.ElapsedTime = taskSettings.Interval; //ensure that the task gets executed based on its original interval and doesn't waste time having to start from the beginning.
            //            }

            //            Debug.WriteLine("Task Id: " + taskSettings.Id + " - Elapsed Time: " + taskSettings.ElapsedTime + " - Interval: " + taskSettings.Interval);

            //            //validates whether or not a task can be processed.
            //            if (CanBeProcessed(taskSettings))
            //            {
            //                try
            //                {
            //                    if (System.Diagnostics.Debugger.IsAttached)
            //                    {
            //                        RunNow(taskSettings);
            //                    }
            //                    else
            //                    {

            //                        var result = RunTask(taskSettings);

            //                    }
            //                }
            //                catch (Exception ex)
            //                {
            //                    LogHelper.WriteToEventViewer("TaskRunner", "Task: '" + taskSettings.Description + "' - Id: " + taskSettings.Id + " - Failed to execute.", ex, AdestLogMessageType.Error);
            //                }
            //            }
            //            //lock (settings)
            //            //{
            //            //    if (task.ElapsedTime >= task.Interval)
            //            //        task.ElapsedTime = 0;
            //            //}
            //        }


            //    }
            //}
            //catch (Exception ex)
            //{
            //    LogHelper.WriteToEventViewer("TaskRunner", "An unhandled exception occurred while attempting to process one of the task.", ex, AdestLogMessageType.Error);
            //}
        }

        private async Task RunTask(object args)
        {
            var result = await Task.Run(() => RunNow(args));
        }

        private Action RunNow(object args)
        {

            ////logEntry.ProcedureName = "Run";

            //ITask taskFactory = null;
            //AdestTask currentTask = GetTask(((AdestTask)args).Id);
            //Exception returnedException = null;

            //try
            //{
            //    Debug.WriteLine("Task Id: " + currentTask.Id + " - AssemblyType: " + currentTask.AssemblyType + " - STARTED");

            //    //Make sure the IsProcessing flag is set so that the task doesn't get processed if it is still busy processing the orignal "same" task.
            //    lock (settings)
            //    {
            //        AdestTask task = settings.Tasks.Find(tj => tj.Id == currentTask.Id);
            //        settings.Tasks.Find(tj => tj.Id == currentTask.Id).IsProcessing = true;
            //        if (!task.HasRunOnStart && task.RunOnStart)
            //        {
            //            settings.Tasks.Find(tj => tj.Id == currentTask.Id).HasRunOnStart = true;
            //            settings.Tasks.Find(tj => tj.Id == currentTask.Id).ElapsedTime = 0;
            //            if (settings.Debug)
            //                WriteToDebugLog("Task id: " + currentTask.Id.ToString() + " - AssemblyType: " + currentTask.AssemblyType + " - Description: " + currentTask.Description + " - About to run on start.");
            //        }
            //    }

            //    taskFactory = (ITask)iocContainer.Container(currentTask.Id.ToString());

            //    if (!taskFactory.IsProcessing)
            //    {
            //        if (settings.Debug)
            //            WriteToDebugLog("Task id: " + currentTask.Id.ToString() + " - AssemblyType: " + currentTask.AssemblyType + " - Description: " + currentTask.Description + " - Start Run");


            //        taskFactory.RunTask(currentTask);


            //        if (settings.Debug)
            //            WriteToDebugLog("Task id: " + currentTask.Id.ToString() + " - AssemblyType: " + currentTask.AssemblyType + " - Description: " + currentTask.Description + " - Run Completed");
            //    }



            //    //Run SubTasks
            //    //If a 
            //    if (CurrentTaskHasSubTasks(currentTask) && !currentTask.IsSubTask)
            //    {
            //        ProcessSubTasks(currentTask);
            //    }

            //}
            //catch (Exception ex)
            //{
            //    returnedException = ex;
            //    lock (settings.Tasks) //Increase the TotalRunFailures
            //    {
            //        if (settings.Debug)
            //            WriteToDebugLog("Task id: " + currentTask.Id.ToString() + " - Failed to run - Exception: " + ex.ToString());

            //        LogHelper.WriteToEventViewer("TaskRunner", "Task: '" + currentTask.Description + "' - Id: " + currentTask.Id + " - Failed to run.", ex, AdestLogMessageType.Error);

            //        if (currentTask.TotalRunFailures <= currentTask.MaxFailuresAllowed)
            //        {
            //            if (settings.Debug)
            //                WriteToDebugLog("Task id: " + currentTask.Id.ToString() + " - TotalRunFailures: " + currentTask.TotalRunFailures.ToString() + " - MaxFailuresAllowed: " + currentTask.MaxFailuresAllowed.ToString());

            //            settings.Tasks.Find(tj => tj.Id == currentTask.Id).TotalRunFailures += 1;
            //        }
            //    }
            //}
            //finally
            //{
            //    //Must be reset here rather than in the loop calling the RunNow as the task is added to a thread pool, so must make sure the task is completed before resetting it!
            //    lock (settings)
            //    {
            //        settings.Tasks.Find(tj => tj.Id == currentTask.Id).IsProcessing = false;
            //        settings.Tasks.Find(tj => tj.Id == currentTask.Id).ElapsedTime = 0;
            //        Debug.WriteLine("Task Id: " + currentTask.Id + " - AssemblyType: " + currentTask.AssemblyType + " - FINISHED");
            //        Debug.WriteLine("*********************************************************");
            //    }
            //}

            //try
            //{
            //    if (!taskFactory.IsProcessing && taskFactory.IsDataProcessed)
            //    {
            //        IEmail mailer = null;
            //        Email email = null;

            //        if (currentTask.EmailOnSuccessId > 0 && taskFactory.IsSuccess) //Only email if data was processed successfully.
            //        {
            //            email = settings.Emails.FirstOrDefault(m => m.Id == currentTask.EmailOnSuccessId);
            //        }
            //        else if (currentTask.EmailOnFailureId > 0 && !taskFactory.IsSuccess && taskFactory.IsDataProcessed) //Only email if an error occured, irrelevant of whether data was processed or not.
            //        {
            //            email = settings.Emails.FirstOrDefault(m => m.Id == currentTask.EmailOnFailureId);
            //        }
            //        if (!object.ReferenceEquals(email, null) && (currentTask.EmailOnSuccessId > 0 || currentTask.EmailOnFailureId > 0))
            //        {
            //            string password = string.Empty;
            //            if (!string.IsNullOrEmpty(settings.Smtp.Password))
            //                password = EncryptionHelper.AdDecrypt(settings.Smtp.Password);

            //            Dictionary<string, string> inputTags = new Dictionary<string, string>();

            //            inputTags.Add("TaskId", currentTask.Id.ToString());
            //            inputTags.Add("TaskDescription", currentTask.Description);
            //            inputTags.Add("TaskStatus", taskFactory.IsSuccess.ToString());
            //            inputTags.Add("TaskStatusVerb", taskFactory.IsSuccess ? "Success" : "Failed");
            //            inputTags.Add("ExceptionMessage", object.ReferenceEquals(returnedException, null) ? string.Empty : returnedException.Message);
            //            inputTags.Add("ExceptionStackTrace", object.ReferenceEquals(returnedException, null) ? string.Empty : returnedException.StackTrace);
            //            inputTags.Add("Exception", object.ReferenceEquals(returnedException, null) ? string.Empty : returnedException.Message.ToString());

            //            if (taskFactory.AdditionalTags != null && taskFactory.AdditionalTags.Count > 0)
            //            {
            //                foreach (var additionalTag in taskFactory.AdditionalTags)
            //                {
            //                    inputTags.Add(additionalTag.Key, additionalTag.Value);
            //                }
            //            }

            //            List<string> attachments = new List<string>();

            //            if (taskFactory.IsSuccess)
            //            {
            //                attachments.Add(taskFactory.LogFilename);
            //            }
            //            else
            //            {
            //                attachments.Add(taskFactory.LogFilename);
            //                attachments.Add(taskFactory.LogErrorFilename);
            //            }

            //            mailer = new SmtpMail(settings.Smtp.Server, settings.Smtp.Port, settings.Smtp.Username, password, settings.Smtp.UseSSL, settings.Smtp.From, settings.Smtp.FromDisplayName, settings.Smtp.ReplyTo, settings.Smtp.DebugMode, settings.Smtp.DebugPath);
            //            mailer.Send(email.From, email.FromDisplayName, email.ReplyTo, email.To, email.Cc, email.Bcc, email.Subject, email.IsHtmlBody, email.Body, email.BodyFilename, attachments, inputTags);
            //            mailer = null;
            //        }
            //        //else //Won't log for now as this scenario should never happen but if it did, it would be logged every time which isn't ideal.
            //        //{
            //        //}
            //        email = null;
            //    }

            //    taskFactory = null;
            //}
            //catch (Exception ex)
            //{
            //    LogHelper.WriteToEventViewer("TaskRunner", "Task: '" + currentTask.Description + "' - Id: " + currentTask.Id + " - Failed to email.", ex, AdestLogMessageType.Error);
            //    currentTask = null;
            //    taskFactory = null;
            //}

            return null;

        }
    }
}
