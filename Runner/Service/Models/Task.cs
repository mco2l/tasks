﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskRunner.Service.Models
{
    internal class InternalTask
    {
        public int TenantId { get; set; }
        public string TenantDB { get; set; }
        public int Id { get; set; }
        public int TaskTypeId { get; set; }
        public string Description { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public long Interval { get; set; }
        public long TotalRunPerTimeRange { get; set; }
        public bool Disabled { get; set; }
        public bool EnableTracing { get; set; }
        public long MaxLogFiles { get; set; }
        public int EmailSuccessSettingId { get; set; }
        public int EmailFailureSettingId { get; set; }
        public bool RunOnStart { get; set; }
        public long? MaxFailuresAllowed { get; set; }
        public long? IntervalFailureRetry { get; set; }
        public bool IsProcessing { get; set; }
        public int TotalRunFailures { get; set; }
        public bool DisabledDueToFailure { get; set; }       
        public long ElapsedTime { get; set; }
        public bool HasRunOnStart { get; set; }
    }
}
