﻿
using System.Collections.Generic;

namespace TaskRunner.Service.Models
{
    internal class InternalTenant
    {
        public InternalTenant()
        {
            Tasks = new List<InternalTask>();
        }

        public int TenantId { get; set; }
        public string TenantName { get; set; }
        public bool IsProcessing { get; set; }

        public List<InternalTask> Tasks { get; set; }
    }
}
