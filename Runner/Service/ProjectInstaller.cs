﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace TaskRunner.Service
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : System.Configuration.Install.Installer
    {
        private bool debug = false;

        public ProjectInstaller()
        {
            InitializeComponent();
        }

        private string GetContextParameter(string key)
        {
            string value = string.Empty;

            try
            {
                value = Context.Parameters[key].ToString();
            }
            catch
            {
                //do nothing
            }

            return value;

        }

        private void WriteToLog(string message)
        {
            if (debug)
            {
                StreamWriter sw = new StreamWriter(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Adest\\Logs", "TaskRunner.Service.txt"), true);
                sw.WriteLine(message);
                sw.Close();
            }
        }

        private void serviceProcessInstaller_BeforeInstall(object sender, InstallEventArgs e)
        {
            bool isUserAccount = false;
            string account = string.Empty;
            string username = string.Empty;
            string password = string.Empty;

            //What type of credentials to use to run the service
            //The default is User

            WriteToLog("step1");
            account = GetContextParameter("account");
            WriteToLog("account: " + account);

            if (account.Length == 0)
                account = "user";

            //Decode the type of account to use
            switch (account.ToLowerInvariant())
            {
                case "user":
                    {
                        WriteToLog("step1.1");
                        serviceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.User;
                        isUserAccount = true;
                        break;
                    }
                case "localservice":
                    {
                        WriteToLog("step1.2");
                        serviceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalService;
                        break;
                    }
                case "localsystem":
                    {
                        WriteToLog("step1.3");
                        serviceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
                        break;
                    }
                case "networkservice":
                    {
                        WriteToLog("step1.4");
                        serviceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.NetworkService;
                        break;
                    }
            }

            WriteToLog("IsUserAccount: " + isUserAccount);

            //Should I use a user account?
            if (isUserAccount)
            {
                //User name and password
                username = GetContextParameter("user");
                password = GetContextParameter("password");

                WriteToLog("username:" + username);
                WriteToLog("password:" + password);

                //If we need to use a user account, set the user name and password()
                serviceProcessInstaller.Username = username;
                serviceProcessInstaller.Password = password;
            }
            else
            {
                serviceProcessInstaller.Password = null;
                serviceProcessInstaller.Username = null;
            }

        }
    }
}
