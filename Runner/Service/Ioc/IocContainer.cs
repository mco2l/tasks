﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TaskRunner.Service
{
    internal class IocContainer
    {
        private readonly Dictionary<string, object> _registeredObjects;

        public IocContainer()
        {
            _registeredObjects = new Dictionary<string, object>();
        }

        public object Register(string assemblyName, string assemblyType, string uniqueKey)
        {
            if (!_registeredObjects.ContainsKey(uniqueKey))
            {
                string filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, assemblyName);
                System.Reflection.Assembly registeredObject = System.Reflection.Assembly.LoadFrom(filePath);
                object factoryClass = Activator.CreateInstance(registeredObject.GetType(assemblyType));
                _registeredObjects.Add(uniqueKey, factoryClass);
            }
            return _registeredObjects[uniqueKey];
        }

        public object Container(string uniqueKey)
        {
            return _registeredObjects[uniqueKey];
        }

        public void Remove(string uniqueKey)
        {
            _registeredObjects.Remove(uniqueKey);
        }
    }
}
