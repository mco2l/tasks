﻿
using TaskRunner.Service.Models;
using TaskRunner.Data;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using TaskRunner.Core.Enums;
using TaskRunner.Core.Logging;
using TaskRunner.Model;
using TaskRunner.Core;
using System.Threading;
using NLog;

namespace TaskRunner.Service
{
    internal sealed class Application : IDisposable
    {
        private ADTS service = null;
        private System.Timers.Timer timerPolling = null;
        private IocContainer iocContainer = new IocContainer();
        public List<InternalTenant> tenants = new List<InternalTenant>();

        private static Logger logger = LogManager.GetCurrentClassLogger();

        public Application()
        {

        }

        public bool Start(ADTS serviceStart)
        {
            service = serviceStart;

            //TODO: Tenanant and task clean up i.e. remove tenants and tasks that are no longer in use

            //TODO: Replace logging with NLog

            //TODO: Read master and tenant database locations from a config file

            //TODO: RunOnStart

            //TODO: MaxFailuresAllowed

            //TODO: IntervalFailureRetry

            //TODO: TotalRunTimePerRange

            //Validate that there are tenants set up in master database & tenant databases are available
            //If No tenants set up or no valid tenants notify an admin, log exception in event viewer and stop service
            //If tenant set up and no tenant datbase notify admin but continue so that other tenants tasks are executed  
            if (ValidateMasterandTenant())
            {
                //Validate all assemblies exist and are available
                if (ValidateAssemblySettings())
                {

                    timerPolling = new System.Timers.Timer();
                    timerPolling.Elapsed += timerPolling_Elapsed;
                    timerPolling.Interval = 1000;
                    timerPolling.Start();

                    //TODO: Add & Start Timer to check for new tenants added
                }

            }

            return true;

        }

        void timerPolling_Elapsed(object sender, ElapsedEventArgs e)
        {
            Console.WriteLine("Elapsed timer");
            logger.Trace("Elapsed timer");
            timerPolling.Stop();
            ProcessTenants();
            timerPolling.Start();
        }

        private bool ValidateMasterandTenant()
        {
            int validTenants = 0;

            logger.Trace("Listing tenants and corresponding tasks set up");
            using (var db = new mainContext())
            {
                var tenants = db.OrganizationSettings.Where(t => !t.Disabled);
                foreach (var tenant in tenants)
                {
                    logger.Trace("Tenant Name - {0} ID - {1} ", tenant.TenantName, tenant.TenantId);

                    //Validate tenants db and if valid start tenant
                    if(!IsValidTenantDB(tenant.TenantId))
                    {
                        //TODO: Log invalid tenant
                    }  
                    else
                    {
                        LoadTenantAndTasks(tenant.TenantId, tenant.TenantName);
                        validTenants += 1;
                    }          
                }
            }

            return validTenants > 0 ? true : false;
        }

        private bool ValidateAssemblySettings()
        {
            //TODO: loop through all assemblies defined and ensure that they exist

            bool validated = true;

            return validated;
        }

        private bool IsValidTenantDB(int tenantId)
        {
            bool isValid = true;
            //Check tenant database exists and has settings
            if (!File.Exists(string.Concat(@"E:\Sources\Tasks\Database\Test\tenant", tenantId, ".db")))
            {
                //TODO: log missing tenant DB and email
                logger.Error("Database missing : " + tenantId);
                isValid = false;
            }

            return isValid;
        }

        private void LoadTenantAndTasks(int tenantID, string name)
        {
            InternalTenant tmp = new InternalTenant() { TenantId = tenantID, TenantName = name, IsProcessing = false };

            string dbPath = string.Concat(@"Filename=E:\Sources\Tasks\Database\Test\tenant", tenantID, ".db");

            using (var db = new TenantDBContext(dbPath))
            {

                //Read all Tasks set up for a tenant                
                logger.Trace("Tenant Tasks");
                foreach (var setting in db.SettingsTask)
                {
                    tmp.Tasks.Add(new Models.InternalTask()
                    {
                        TenantId = tenantID,
                        TenantDB = dbPath,
                        Id = setting.Id,
                        TaskTypeId = setting.TaskTypeId,
                        Description = setting.Description,
                        StartTime = setting.StartTime,
                        EndTime = setting.EndTime,
                        Interval = setting.Interval,
                        TotalRunPerTimeRange = setting.TotalRunPerTimeRange,
                        Disabled = setting.Disabled,
                        EnableTracing = setting.EnableTracing,
                        MaxLogFiles = setting.MaxLogFiles,
                        EmailSuccessSettingId = setting.EmailSuccessSettingId,
                        EmailFailureSettingId = setting.EmailFailureSettingId,
                        RunOnStart = setting.RunOnStart,
                        MaxFailuresAllowed = setting.MaxFailuresAllowed == 0 ? 3 : setting.MaxFailuresAllowed,
                        IntervalFailureRetry = setting.IntervalFailureRetry,
                        IsProcessing = false,
                        TotalRunFailures = 0,
                        DisabledDueToFailure = false,
                        ElapsedTime = 0,
                        HasRunOnStart = false
                    });
                }
            }

            tenants.Add(tmp);
            
        }

        private void ProcessTenants()
        {
            foreach (var tenant in tenants)
            {
                logger.Trace("Tenant Name - {0} ID - {1} ", tenant.TenantName, tenant.TenantId);
                tenant.IsProcessing = true;
                ProcessTenant(tenant);
            }

        }

        private void ProcessTenant(InternalTenant tenant)
        {
            int totalInitialized = 0;

            //Read all Tasks set up for a tenant                
            logger.Trace("Tenant Tasks");
            foreach (var setting in tenant.Tasks)
            {
                //Console.WriteLine("\t - Id - {0} TaskTypeId - {1}", setting.Id, setting.TaskTypeId);

                //Validate tenant tasks & initialize task factories
                if(InitializeTaskFactory(setting.TaskTypeId, tenant.TenantId, setting.Id, setting.Disabled)) totalInitialized += 1;                               
            }

            if (totalInitialized > 0) ProcessTasks(tenant);
        }

        //Initialize the factory file for the task type

        private bool InitializeTaskFactory(int taskTypeID, int tenantID, int taskID)
        {
            return InitializeTaskFactory(taskTypeID, tenantID, taskID, false);
        }

        private bool InitializeTaskFactory(int taskTypeID, int tenantID, int taskID, bool disabled)
        {
            bool initialized = false;
            if (disabled)
            {
                iocContainer.Remove(tenantID.ToString() + "_" + taskTypeID.ToString());
            }
            else
            {
                TaskTypes currentTask = GetTaskType(taskTypeID);
                try
                {
                    ITask proxy = (ITask)iocContainer.Register(currentTask.AssemblyName, currentTask.AssemblyType, tenantID.ToString() + "_" + taskTypeID.ToString());
                    initialized = true;
                }                
                catch (Exception ex)
                {
                    //TODO: Disable tenant task to automatically disable this task if an exception occurs while attempting to create an instance of it. Log exception.
                    logger.Error("InitializeTaskFactory : " + ex.ToString());
                }
                finally
                {
                    currentTask = null;
                }
            }

            return initialized;
        }

        private TaskTypes GetTaskType(int taskTypeID)
        {
            TaskTypes currentTask;
            using (var db = new mainContext())
            {
                currentTask = db.TaskTypes.First(a => a.TaskTypeId == taskTypeID);
            }
            return currentTask;
        }

        #region Tasks


        private void ProcessTasks(InternalTenant tenant)
        {
            //TODO: Handle sub tasks

            try
            {
                //InternalTenant tenant = tenants.First(a => a.TenantId == tenantId);

                    foreach (var taskSettings in tenant.Tasks)
                    {
                        //verifies if the task is not a sub-task
                        bool isSubTask = false;// TaskIsSubtask(taskSettings, settings);

                        //Flagging task to avoid subtasks having subtasks...
                        //taskSettings.IsSubTask = isSubTask;

                        //Only run the Task if the task is not referenced as a SubTask
                        if (!isSubTask)
                        {
                            if (!taskSettings.Disabled && taskSettings.TotalRunFailures >= taskSettings.MaxFailuresAllowed)
                            {
                                logger.Error("TaskRunner", "Disabling JobId: " + taskSettings.Id.ToString() + " as the number of failures allowed for this job has been reached i.e. " + taskSettings.MaxFailuresAllowed.ToString(), AdestLogMessageType.Information);
                                taskSettings.TotalRunFailures = 0;
                                taskSettings.DisabledDueToFailure = true;
                                taskSettings.ElapsedTime = 0;
                            }
                            else
                            {
                                //increase the elapsed time every second for each individual task
                                taskSettings.ElapsedTime += 1;
                            }

                            if (taskSettings.DisabledDueToFailure && taskSettings.IntervalFailureRetry > 0 && taskSettings.ElapsedTime >= taskSettings.IntervalFailureRetry) //re-enable a task that has been disabled.
                            {
                                logger.Error("TaskRunner", "Re-enabling JobId: " + taskSettings.Id.ToString() + " as the required retry delay interval has been reached: " + taskSettings.IntervalFailureRetry.ToString() + " seconds", AdestLogMessageType.Information);
                                taskSettings.DisabledDueToFailure = false;
                                taskSettings.ElapsedTime = taskSettings.Interval; //ensure that the task gets executed based on its original interval and doesn't waste time having to start from the beginning.
                            }

                            Debug.WriteLine("Task Id: " + taskSettings.Id + " - Elapsed Time: " + taskSettings.ElapsedTime + " - Interval: " + taskSettings.Interval);

                            //validates whether or not a task can be processed.
                        if (CanBeProcessed(taskSettings))
                        {
                            try
                            {
                                    
                                if (System.Diagnostics.Debugger.IsAttached)
                                {
                                    RunNow(taskSettings);
                                }
                                else
                                {

                                //var result = RunTask(taskSettings);
                                ThreadPool.QueueUserWorkItem(new WaitCallback(RunNow), taskSettings);

                                }
                            }
                            catch (Exception ex)
                            {
                                logger.Error("TaskRunner", "Task: '" + taskSettings.Description + "' - Id: " + taskSettings.Id + " - Failed to execute.", ex, AdestLogMessageType.Error);
                            }
                        }
                        //lock (settings)
                            //{
                            //    if (task.ElapsedTime >= task.Interval)
                            //        task.ElapsedTime = 0;
                            //}
                        }

                    }
            }
            catch (Exception ex)
            {
                logger.Error("TaskRunner", "An unhandled exception occurred while attempting to process one of the task.", ex, AdestLogMessageType.Error);
            }
        }

        private bool CanBeProcessed(InternalTask task)
        {
            bool disabledDueToFailure = task.DisabledDueToFailure;
            bool isProcessing = task.IsProcessing;
            bool isWithinTimeFame = IsWithinTimeRange(System.DateTime.Now, task.StartTime, task.EndTime);
            bool isReadyToProcess = (task.ElapsedTime >= task.Interval);
            bool runOnStart = task.RunOnStart && !task.HasRunOnStart;
            bool canBeProcessed = false;

            string outputMsg = string.Empty;
            outputMsg += "TaskID: " + task.Id.ToString();
            outputMsg += " - DisabledDueToFailure: " + disabledDueToFailure.ToString();
            outputMsg += " - Elapsed: " + task.ElapsedTime.ToString();
            outputMsg += " - Interval: " + task.Interval.ToString();
            outputMsg += " - IsWithinTimeFrame: " + isWithinTimeFame.ToString();
            outputMsg += " - IsReadyToProcess (job interval reached): " + isReadyToProcess.ToString();
            outputMsg += " - HasRunOnStart: " + task.HasRunOnStart.ToString();
            outputMsg += " - RunOnStart: " + task.RunOnStart.ToString();
            
            //if (settings.Debug)
            //    WriteToDebugLog("Task id: " + task.Id.ToString() + " - CanBeProcessed Checks: " + outputMsg);

            Debug.WriteLine(outputMsg);

            //WriteTraceLog("Checking if job should be run for " + outputMsg, pollingJob.JobID);

            //process file,database,etc... if first 3 conditions are met
            //1. not currently being processed
            //2. current time is within the defined time range
            //3. the elapsed time has reached the interval

            // if conditions are met, one last check remains to be done
            // check if it is a runonceperday job and if it is, make check
            // it hasn't been ran today. Make sure that the job isn't run due to initialize or too many errors detected failure.
            if ((!disabledDueToFailure && !isProcessing && isWithinTimeFame && isReadyToProcess) || (runOnStart))
            {
                canBeProcessed = true;
            }

            //if (settings.Debug)
            //    WriteToDebugLog("Task id: " + task.Id.ToString() + " - CanBeProcessed: " + canBeProcessed.ToString());


            //WriteTraceLog("canBeProcessed: " + canBeProcessed, pollingJob.JobID);

            return canBeProcessed;
        }

        public static bool IsWithinTimeRange(DateTime now, String start, String end)
        {

            string[] startTimeParts = start.Split(':');
            TimeSpan startTime = string.IsNullOrEmpty(start) ? new TimeSpan(0, 0, 0) : new TimeSpan(Convert.ToInt32(startTimeParts[0]), Convert.ToInt32(startTimeParts[1]), 0);
            string[] endTimeParts = end.Split(':');
            TimeSpan endTime = string.IsNullOrEmpty(end) ? new TimeSpan(0, 0, 0) : new TimeSpan(Convert.ToInt32(endTimeParts[0]), Convert.ToInt32(endTimeParts[1]), 0);

            bool midnight = (TimeSpan.Compare(startTime, endTime) > 0);
            TimeSpan timeOfDay = System.DateTime.Now.TimeOfDay;

            if (midnight)
            {
                return (TimeSpan.Compare(startTime, timeOfDay) <= 0 || TimeSpan.Compare(timeOfDay, endTime) <= 0);
            }
            else
            {
                return (TimeSpan.Compare(startTime, timeOfDay) <= 0 && TimeSpan.Compare(timeOfDay, endTime) <= 0);
            }

        }
        //private async Task RunTask(object args)
        //{
        //    var result = await Task.Run(() => RunNow(args));
        //}

        private void RunNow(object args)
        {            
            ITask taskFactory = null;
            InternalTask currentTask = (InternalTask)args;
            Exception returnedException = null;

            try
            {
                logger.Trace("Task Id: " + currentTask.Id + " - AssemblyType: " + currentTask.TaskTypeId + " - STARTED");

                //Make sure the IsProcessing flag is set so that the task doesn't get processed if it is still busy processing the orignal "same" task.
                lock (currentTask)
                {
                    currentTask.IsProcessing = true;
                    if (!currentTask.HasRunOnStart && currentTask.RunOnStart)
                    {
                        currentTask.HasRunOnStart = true;
                        currentTask.ElapsedTime = 0;
                    }
                }

                taskFactory = (ITask)iocContainer.Container(currentTask.TenantId.ToString() + "_" + currentTask.TaskTypeId.ToString());// currentTask.Id.ToString());

                if (!taskFactory.IsProcessing)
                {
                    taskFactory.RunTask(currentTask.TenantDB, currentTask.Id);
                }

                //TODO: Sub Tasks
                //////Run SubTasks
                //////If a 
                ////if (CurrentTaskHasSubTasks(currentTask) && !currentTask.IsSubTask)
                ////{
                ////    ProcessSubTasks(currentTask);
                ////}

            }
            catch (Exception ex)
            {
                returnedException = ex;
                lock (tenants) //Increase the TotalRunFailures
                {

                    if (currentTask.TotalRunFailures <= currentTask.MaxFailuresAllowed)
                    {
                        currentTask.TotalRunFailures += 1;
                    }
                }
            }
            finally
            {
                //Must be reset here rather than in the loop calling the RunNow as the task is added to a thread pool, so must make sure the task is completed before resetting it!
                lock (tenants)
                {
                    currentTask.IsProcessing = false;
                    currentTask.ElapsedTime = 0;
                    //Debug.WriteLine("Task Id: " + currentTask.Id + " - AssemblyType: " + currentTask.AssemblyType + " - FINISHED");
                    //Debug.WriteLine("*********************************************************");
                }
            }

            //try
            //{
            //    if (!taskFactory.IsProcessing && taskFactory.IsDataProcessed)
            //    {
            //        IEmail mailer = null;
            //        Email email = null;

            //        if (currentTask.EmailOnSuccessId > 0 && taskFactory.IsSuccess) //Only email if data was processed successfully.
            //        {
            //            email = settings.Emails.FirstOrDefault(m => m.Id == currentTask.EmailOnSuccessId);
            //        }
            //        else if (currentTask.EmailOnFailureId > 0 && !taskFactory.IsSuccess && taskFactory.IsDataProcessed) //Only email if an error occured, irrelevant of whether data was processed or not.
            //        {
            //            email = settings.Emails.FirstOrDefault(m => m.Id == currentTask.EmailOnFailureId);
            //        }
            //        if (!object.ReferenceEquals(email, null) && (currentTask.EmailOnSuccessId > 0 || currentTask.EmailOnFailureId > 0))
            //        {
            //            string password = string.Empty;
            //            if (!string.IsNullOrEmpty(settings.Smtp.Password))
            //                password = EncryptionHelper.AdDecrypt(settings.Smtp.Password);

            //            Dictionary<string, string> inputTags = new Dictionary<string, string>();

            //            inputTags.Add("TaskId", currentTask.Id.ToString());
            //            inputTags.Add("TaskDescription", currentTask.Description);
            //            inputTags.Add("TaskStatus", taskFactory.IsSuccess.ToString());
            //            inputTags.Add("TaskStatusVerb", taskFactory.IsSuccess ? "Success" : "Failed");
            //            inputTags.Add("ExceptionMessage", object.ReferenceEquals(returnedException, null) ? string.Empty : returnedException.Message);
            //            inputTags.Add("ExceptionStackTrace", object.ReferenceEquals(returnedException, null) ? string.Empty : returnedException.StackTrace);
            //            inputTags.Add("Exception", object.ReferenceEquals(returnedException, null) ? string.Empty : returnedException.Message.ToString());

            //            if (taskFactory.AdditionalTags != null && taskFactory.AdditionalTags.Count > 0)
            //            {
            //                foreach (var additionalTag in taskFactory.AdditionalTags)
            //                {
            //                    inputTags.Add(additionalTag.Key, additionalTag.Value);
            //                }
            //            }

            //            List<string> attachments = new List<string>();

            //            if (taskFactory.IsSuccess)
            //            {
            //                attachments.Add(taskFactory.LogFilename);
            //            }
            //            else
            //            {
            //                attachments.Add(taskFactory.LogFilename);
            //                attachments.Add(taskFactory.LogErrorFilename);
            //            }

            //            mailer = new SmtpMail(settings.Smtp.Server, settings.Smtp.Port, settings.Smtp.Username, password, settings.Smtp.UseSSL, settings.Smtp.From, settings.Smtp.FromDisplayName, settings.Smtp.ReplyTo, settings.Smtp.DebugMode, settings.Smtp.DebugPath);
            //            mailer.Send(email.From, email.FromDisplayName, email.ReplyTo, email.To, email.Cc, email.Bcc, email.Subject, email.IsHtmlBody, email.Body, email.BodyFilename, attachments, inputTags);
            //            mailer = null;
            //        }
            //        //else //Won't log for now as this scenario should never happen but if it did, it would be logged every time which isn't ideal.
            //        //{
            //        //}
            //        email = null;
            //    }

            //    taskFactory = null;
            //}
            //catch (Exception ex)
            //{
            //    LogHelper.WriteToEventViewer("TaskRunner", "Task: '" + currentTask.Description + "' - Id: " + currentTask.Id + " - Failed to email.", ex, AdestLogMessageType.Error);
            //    currentTask = null;
            //    taskFactory = null;
            //}

            //return null;

        }

    #endregion


    //private void StartTenantFactory(int tenantId)
    //{

    //    ////https://msdn.microsoft.com/en-us/library/dd537609(v=vs.110).aspx
    //    StartTenant(tenantId);

    //}

    //private async Task StartTenant(object args)
    //{
    //    var result = await Task.Run(() => StartTenantNow(args));

    //}

    //private Action StartTenantNow(object args)
    //{

    //    return null;

    //}

    public bool Stop()
        {
            bool ret = false;

            try
            {
                ret = true;
            }
            catch (Exception ex)
            {
                logger.Error("TaskRunner", "Stop", "An unhandled exception has occured while attempting to stop the service.", ex, AdestLogMessageType.Error);
            }

            if (ret)
            {
                logger.Error("TaskRunner", "Service stopped successfully", AdestLogMessageType.Information);
            }

            return ret;
        }

        
        #region IDisposable Members

        public void Dispose()
        {

            if (iocContainer != null)
                iocContainer = null;

            if (service != null)
                service = null;
        }

        #endregion
       
    }
}
