﻿using System;
using System.ServiceProcess;
using TaskRunner.Core.Enums;
using TaskRunner.Core.Logging;
using TaskRunner.Service;

namespace TaskRunner.Service
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun = null;

            if (System.Diagnostics.Debugger.IsAttached)
            {
                ADTS debugService = null;
                try
                {
                    debugService = new ADTS();
                    string debugServiceName = "c:\\" + debugService.ServiceName + "_service.debug";
                    System.IO.File.WriteAllText(debugServiceName, string.Empty);

                    debugService.Start(null);
                    while (System.IO.File.Exists(debugServiceName))
                    {
                        System.Threading.Thread.Sleep(10);
                    }
                    debugService.Stop();
                }
                catch (Exception ex)
                {
                    LogHelper.WriteToEventViewer("TaskRunner", "Main", "An unhandled exception has occured while attempting to start the service.", ex, AdestLogMessageType.Error);
                    //throw ex;                    
                }
            }
            else
            {
                try
                {
                    ServicesToRun = new System.ServiceProcess.ServiceBase[] { new ADTS() };
                    System.ServiceProcess.ServiceBase.Run(ServicesToRun);
                }
                catch (Exception ex)
                {
                    LogHelper.WriteToEventViewer("TaskRunner", "Main", "An unhandled exception has occured while attempting to start the service.", ex, AdestLogMessageType.Error);
                }
            }
        }
    }
}
