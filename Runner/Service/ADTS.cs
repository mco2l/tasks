﻿using System;
using System.ServiceProcess;

namespace TaskRunner.Service
{
    public partial class ADTS : ServiceBase
    {
        private Application application = null;

        public ADTS()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            Process(true);
        }

        internal void Start(string[] args)
        {
            OnStart(args);
        }

        protected override void OnStop()
        {
            Process(false);
            Dispose(true);
        }

        private void Process(bool startProcessing)
        {
            if (startProcessing)
            {
                if (application == null)
                {
                    application = new Application();
                    if (!application.Start(this))
                    {
                        application.Dispose();
                        application = null;
                        throw new Exception("Failed to start the service.");
                    }
                }
            }
            else
            {
                if (application != null)
                {
                    if (!application.Stop())
                    {
                        AppDomain app = AppDomain.CurrentDomain;
                        // There is thread afinity: the task depends on the identity of a physical operating system thread.
                        System.Threading.Thread.EndThreadAffinity();
                        AppDomain.Unload(app);
                    }
                }
            }
        }

    }
}
