﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskRunner.Data
{
    public class TenantDBContext : tenantDBContextBase
    {
        protected override string GetTenantDatabase
        {
            get
            {
                return DBPath;
            }
        }

        public string DBPath { get; set; }

        public TenantDBContext(string path )
        {
            DBPath = path;
        }
    }  
}
