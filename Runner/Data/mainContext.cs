﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using TaskRunner.Model;

namespace TaskRunner.Data
{
    public partial class mainContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            #warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
            optionsBuilder.UseSqlite(@"Filename=E:\Sources\Tasks\Database\Test\main.db");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<OrganizationSettings>(entity =>
            {
                entity.HasKey(e => e.TenantId)
                    .HasName("PK_OrganizationSettings");

                entity.Property(e => e.AccessKey)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 100)");

                entity.Property(e => e.Disabled).HasColumnType("BOOLEAN");

                entity.Property(e => e.TenantName)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 50)");
            });

            modelBuilder.Entity<TaskTypeSettings>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK_TaskTypeSettings");

                entity.Property(e => e.TaskTypeId).HasColumnName("TaskTypeId");

                entity.Property(e => e.IsCore).HasColumnType("BOOLEAN");

                entity.Property(e => e.MaxLength).HasColumnType("INTEGER (0, 0)");

                entity.Property(e => e.MinLength).HasColumnType("INTEGER (1, 50)");

                entity.Property(e => e.SettingName)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 50)");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnType("INTEGER")
                    .HasDefaultValue(1);
                
                entity.HasOne(d => d.TaskType)
                    .WithOne(p => p.TaskTypeSettings)
                    .HasForeignKey<TaskTypeSettings>(d => d.TaskTypeId)
                    .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<TaskTypes>(entity =>
            {
                entity.HasKey(e => e.TaskTypeId)
                    .HasName("PK_TaskTypes");

                entity.Property(e => e.AssemblyName)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 50)");

                entity.Property(e => e.AssemblyType)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 50)");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 50)");

                entity.Property(e => e.SettingsTable)
                    .IsRequired()
                    .HasColumnType("TEXT (40)");
            });
        }

        public virtual DbSet<OrganizationSettings> OrganizationSettings { get; set; }
        public virtual DbSet<TaskTypeSettings> TaskTypeSettings { get; set; }
        public virtual DbSet<TaskTypes> TaskTypes { get; set; }
    }
}