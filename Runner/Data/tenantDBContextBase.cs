﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using TaskRunner.Model;

namespace TaskRunner.Data
{
    public abstract class tenantDBContextBase : DbContext
    {
        protected abstract string GetTenantDatabase { get; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            #warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
            optionsBuilder.UseSqlite(GetTenantDatabase);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SettingsRsoMasterUpload>(entity =>
            {
                entity.ToTable("Settings_RSO_MasterUpload");

                entity.Property(e => e.BackupPath)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 255)");

                entity.Property(e => e.ClearBeforeUpload).HasColumnType("BOOLEAN");

                entity.Property(e => e.CsvSeparator).HasColumnType("TEXT (1, 1)");

                entity.Property(e => e.DataFilePath)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 255)");

                entity.Property(e => e.DataFilePattern).HasColumnType("TEXT (1, 20)");

                entity.Property(e => e.DataFilePatternExt).HasColumnType("TEXT (1, 10)");

                entity.Property(e => e.DataFilePatternMask).HasColumnType("TEXT (1, 20)");

                entity.Property(e => e.DbcommandText)
                    .HasColumnName("DBCommandText")
                    .HasColumnType("TEXT (1, 40)");

                entity.Property(e => e.DbcommandType)
                    .HasColumnName("DBCommandType")
                    .HasColumnType("TEXT (1, 40)");

                entity.Property(e => e.DbconnectionString)
                    .HasColumnName("DBConnectionString")
                    .HasColumnType("TEXT (1, 255)");

                entity.Property(e => e.ErrorPath)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 255)");

                entity.Property(e => e.IsSplitFiles).HasColumnType("BOOLEAN");

                entity.Property(e => e.RsosettingId).HasColumnName("RSOSettingId");

                entity.Property(e => e.SkipFirstLine).HasColumnType("BOOLEAN");

                entity.HasOne(d => d.FieldMapping)
                    .WithMany(p => p.SettingsRsoMasterUpload)
                    .HasForeignKey(d => d.FieldMappingId)
                    .OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.Rsosetting)
                    .WithMany(p => p.SettingsRsoMasterUpload)
                    .HasForeignKey(d => d.RsosettingId)
                    .OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.Task)
                    .WithMany(p => p.SettingsRsoMasterUpload)
                    .HasForeignKey(d => d.TaskId)
                    .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<SettingsRsoSage50>(entity =>
            {
                entity.ToTable("Settings_RSO_Sage50");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AttachmentPath)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 255)");

                entity.Property(e => e.Company)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 50)");

                entity.Property(e => e.DatabasePath)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 255)");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 20)");

                entity.Property(e => e.RsosettingId).HasColumnName("RSOSettingId");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 50)");

                entity.HasOne(d => d.DownloadSetting)
                    .WithMany(p => p.SettingsRsoSage50)
                    .HasForeignKey(d => d.DownloadSettingId)
                    .OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.HeaderFieldMapping)
                    .WithMany(p => p.SettingsRsoSage50HeaderFieldMapping)
                    .HasForeignKey(d => d.HeaderFieldMappingId)
                    .OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.LineFieldMapping)
                    .WithMany(p => p.SettingsRsoSage50LineFieldMapping)
                    .HasForeignKey(d => d.LineFieldMappingId)
                    .OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.Rsosetting)
                    .WithMany(p => p.SettingsRsoSage50)
                    .HasForeignKey(d => d.RsosettingId)
                    .OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.Task)
                    .WithMany(p => p.SettingsRsoSage50)
                    .HasForeignKey(d => d.TaskId);
            });

            modelBuilder.Entity<SettingsRsoScanDownload>(entity =>
            {
                entity.ToTable("Settings_RSO_ScanDownload");

                entity.Property(e => e.CreateBuyerFolder).HasColumnType("BOOLEAN");

                entity.Property(e => e.DownloadPath).HasColumnType("TEXT (1, 255)");

                entity.Property(e => e.RsosettingId).HasColumnName("RSOSettingId");

                entity.Property(e => e.UsePermaLink).HasColumnType("BOOLEAN");

                entity.HasOne(d => d.FieldMapping)
                    .WithMany(p => p.SettingsRsoScanDownloadFieldMapping)
                    .HasForeignKey(d => d.FieldMappingId);

                entity.HasOne(d => d.Rsosetting)
                    .WithMany(p => p.SettingsRsoScanDownload)
                    .HasForeignKey(d => d.RsosettingId);

                entity.HasOne(d => d.Task)
                    .WithMany(p => p.SettingsRsoScanDownload)
                    .HasForeignKey(d => d.TaskId);
            });

            modelBuilder.Entity<SettingsRsoScanUpload>(entity =>
            {
                entity.ToTable("Settings_RSO_ScanUpload");

                entity.Property(e => e.AutoBuyerId)
                    .IsRequired()
                    .HasColumnName("AutoBuyerID")
                    .HasColumnType("BOOLEAN");

                entity.Property(e => e.AutoCreateFolder).HasColumnType("BOOLEAN");

                entity.Property(e => e.AutoDetection).HasColumnType("BOOLEAN");

                entity.Property(e => e.BackupPath).HasColumnType("TEXT (1, 255)");

                entity.Property(e => e.BatchListId).HasColumnName("BatchListID");

                entity.Property(e => e.BatchPath).HasColumnType("TEXT (1, 255)");

                entity.Property(e => e.DefaultDocumentSystemName).HasColumnType("TEXT (1, 30)");

                entity.Property(e => e.OverMaxSizeFilePath).HasColumnType("TEXT (1, 255)");

                entity.Property(e => e.RsosettingId).HasColumnName("RSOSettingId");

                entity.HasOne(d => d.BatchList)
                    .WithMany(p => p.SettingsRsoScanUpload)
                    .HasForeignKey(d => d.BatchListId);

                entity.HasOne(d => d.Rsosetting)
                    .WithMany(p => p.SettingsRsoScanUpload)
                    .HasForeignKey(d => d.RsosettingId);

                entity.HasOne(d => d.Task)
                    .WithMany(p => p.SettingsRsoScanUpload)
                    .HasForeignKey(d => d.TaskId);
            });

            modelBuilder.Entity<SettingsRsoXero>(entity =>
            {
                entity.ToTable("Settings_RSO_Xero");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AppName)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 40)");

                entity.Property(e => e.Buyer)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 40)");

                entity.Property(e => e.ConsumerKey)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 40)");

                entity.Property(e => e.ConsumerSecret)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 40)");

                entity.Property(e => e.InvoiceLogEnabled).HasColumnType("BOOLEAN");

                entity.Property(e => e.InvoiceLogPath).HasColumnType("TEXT (1, 255)");

                entity.Property(e => e.ClearBeforeUpload).HasColumnType("BOOLEAN");

                entity.Property(e => e.OrganizationName)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 50)");

                entity.Property(e => e.RsosettingId).HasColumnName("RSOSettingId");

                entity.Property(e => e.UseXeroFailureSupplier).HasColumnType("BOOLEAN");

                entity.Property(e => e.XeroFailureSupplier).HasColumnType("TEXT (1, 40)");

                entity.HasOne(d => d.DebugLogSetting)
                    .WithMany(p => p.SettingsRsoXero)
                    .HasForeignKey(d => d.DebugLogSettingId);

                entity.HasOne(d => d.DownloadSetting)
                    .WithMany(p => p.SettingsRsoXero)
                    .HasForeignKey(d => d.DownloadSettingId)
                    .OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.Rsosetting)
                    .WithMany(p => p.SettingsRsoXero)
                    .HasForeignKey(d => d.RsosettingId)
                    .OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.Task)
                    .WithMany(p => p.SettingsRsoXero)
                    .HasForeignKey(d => d.TaskId);
            });

            modelBuilder.Entity<SettingsSage200Posting>(entity =>
            {
                entity.ToTable("Settings_Sage200_Posting");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Company)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 50)");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 20)");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 50)");

                entity.HasOne(d => d.AdestWebServiceSetting)
                    .WithMany(p => p.SettingsSage200Posting)
                    .HasForeignKey(d => d.AdestWebServiceSettingId)
                    .OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.DebugLogSetting)
                    .WithMany(p => p.SettingsSage200Posting)
                    .HasForeignKey(d => d.DebugLogSettingId);

                entity.HasOne(d => d.HeaderFieldMapping)
                    .WithMany(p => p.SettingsSage200PostingHeaderFieldMapping)
                    .HasForeignKey(d => d.HeaderFieldMappingId)
                    .OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.LineFieldMapping)
                    .WithMany(p => p.SettingsSage200PostingLineFieldMapping)
                    .HasForeignKey(d => d.LineFieldMappingId);

                entity.HasOne(d => d.Task)
                    .WithMany(p => p.SettingsSage200Posting)
                    .HasForeignKey(d => d.TaskId);
            });

            modelBuilder.Entity<SettingsSage50Posting>(entity =>
            {
                entity.ToTable("Settings_Sage50_Posting");

                entity.Property(e => e.Company)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 50)");

                entity.Property(e => e.DatabasePath)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 255)");

                entity.Property(e => e.LineFieldMappingId).HasColumnType("INTEGER (0)");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 20)");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 50)");

                entity.HasOne(d => d.AdestWebServiceSetting)
                    .WithMany(p => p.SettingsSage50Posting)
                    .HasForeignKey(d => d.AdestWebServiceSettingId)
                    .OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.DebugLogSetting)
                    .WithMany(p => p.SettingsSage50Posting)
                    .HasForeignKey(d => d.DebugLogSettingId)
                    .OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.HeaderFieldMapping)
                    .WithMany(p => p.SettingsSage50PostingHeaderFieldMapping)
                    .HasForeignKey(d => d.HeaderFieldMappingId)
                    .OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.LineFieldMapping)
                    .WithMany(p => p.SettingsSage50PostingLineFieldMapping)
                    .HasForeignKey(d => d.LineFieldMappingId);

                entity.HasOne(d => d.Task)
                    .WithMany(p => p.SettingsSage50Posting)
                    .HasForeignKey(d => d.TaskId);
            });

            modelBuilder.Entity<SettingsSharedAdestWebService>(entity =>
            {
                entity.ToTable("Settings_Shared_AdestWebService");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 20)");

                entity.Property(e => e.SearchCompany).HasColumnType("TEXT (1, 50)");

                entity.Property(e => e.Uri)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 255)");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 50)");
            });

            modelBuilder.Entity<SettingsSharedBatchList>(entity =>
            {
                entity.ToTable("Settings_Shared_BatchList");

                entity.Property(e => e.BuyersName).HasColumnType("TEXT (1, 50)");

                entity.Property(e => e.DocumentType).HasColumnType("TEXT (1, 40)");

                entity.Property(e => e.Path).HasColumnType("TEXT (1, 255)");
            });

            modelBuilder.Entity<SettingsSharedDebugLog>(entity =>
            {
                entity.ToTable("Settings_Shared_DebugLog");

                entity.Property(e => e.Enabled).HasColumnType("BOOLEAN");

                entity.Property(e => e.Path)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 255)");
            });

            modelBuilder.Entity<SettingsSharedDownloadSettings>(entity =>
            {
                entity.ToTable("Settings_Shared_DownloadSettings");

                entity.Property(e => e.BackupPath)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 255)");

                entity.Property(e => e.FailurePath)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 255)");

                entity.Property(e => e.Path)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 255)");
            });

            modelBuilder.Entity<SettingsSharedEmail>(entity =>
            {
                entity.ToTable("Settings_Shared_Email");

                entity.Property(e => e.Bcc)
                    .HasColumnName("BCC")
                    .HasColumnType("TEXT (40)");

                entity.Property(e => e.BodyTextFile).HasColumnType("TEXT (255)");

                entity.Property(e => e.Cc)
                    .HasColumnName("CC")
                    .HasColumnType("TEXT (40)");

                entity.Property(e => e.EmailFrom)
                    .IsRequired()
                    .HasColumnType("TEXT (40)");

                entity.Property(e => e.FromDisplayName)
                    .IsRequired()
                    .HasColumnType("TEXT (40)");

                entity.Property(e => e.IsHtmlBody).HasColumnType("BOOLEAN");

                entity.Property(e => e.Priority).HasColumnType("TEXT (20)");

                entity.Property(e => e.ReplyTo)
                    .IsRequired()
                    .HasColumnType("TEXT (40)");

                entity.Property(e => e.Sensitivity).HasColumnType("TEXT (40)");

                entity.Property(e => e.Subject)
                    .IsRequired()
                    .HasColumnType("TEXT (50)");

                entity.Property(e => e.EmailTo)
                    .IsRequired()
                    .HasColumnType("TEXT (40)");
            });

            modelBuilder.Entity<SettingsSharedFieldMapping>(entity =>
            {
                entity.ToTable("Settings_Shared_FieldMapping");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 50)");

            });

            modelBuilder.Entity<SettingsSharedFieldMappingFields>(entity =>
            {
                entity.ToTable("Settings_Shared_FieldMappingFields");

                entity.Property(e => e.DefaultValue).HasColumnType("TEXT (1, 50)");

                entity.Property(e => e.FromField)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 50)");

                entity.Property(e => e.ToField)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 50)");

                entity.HasOne(d => d.SettingsSharedFieldMapping)    
                .WithMany(p => p.SettingsSharedFieldMappingFields)    
                .HasForeignKey(d => d.MappingId);

            });

            modelBuilder.Entity<SettingsIndex>(entity =>
            {
                entity.ToTable("Settings_Index");

                entity.Property(e => e.Apiendpoint).HasColumnName("APIEndpoint");

                entity.Property(e => e.IndexDataFilePath).HasColumnType("TEXT (255)");

                entity.HasOne(d => d.ApiendpointNavigation)
                    .WithMany(p => p.SettingsIndexApiendpointNavigation)
                    .HasForeignKey(d => d.Apiendpoint);

                entity.HasOne(d => d.HeaderFieldMapping)
                    .WithMany(p => p.SettingsIndexHeaderFieldMapping)
                    .HasForeignKey(d => d.HeaderFieldMappingId);

                entity.HasOne(d => d.IdentityServerEndpointNavigation)
                    .WithMany(p => p.SettingsIndexIdentityServerEndpointNavigation)
                    .HasForeignKey(d => d.IdentityServerEndpoint);

                entity.HasOne(d => d.LineFieldMapping)
                    .WithMany(p => p.SettingsIndexLineFieldMapping)
                    .HasForeignKey(d => d.LineFieldMappingId);

                entity.HasOne(d => d.Task)
                .WithMany(p => p.SettingsIndex)
                .HasForeignKey(d => d.TaskId);

            });


            modelBuilder.Entity<SettingsSharedRso>(entity =>
            {
                entity.ToTable("Settings_Shared_RSO");

                entity.Property(e => e.ApiKey)
                    .IsRequired()
                    .HasColumnName("API_Key")
                    .HasColumnType("TEXT (1, 40)");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 20)");

                entity.Property(e => e.Uri)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 255)");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 50)");

                entity.Property(e => e.UsingLineTaxFields).HasColumnType("BOOLEAN");
            });

            modelBuilder.Entity<SettingsTask>(entity =>
            {
                entity.ToTable("Settings_Task");

                entity.Property(e => e.Description).HasColumnType("TEXT");

                entity.Property(e => e.Disabled).HasColumnType("BOOLEAN");

                entity.Property(e => e.EnableTracing).HasColumnType("BOOLEAN");

                entity.Property(e => e.EndTime)
                    .IsRequired()
                    .HasColumnType("TEXT (5)");

                entity.Property(e => e.RunOnStart).HasColumnType("BOOLEAN");

                entity.Property(e => e.StartTime)
                    .IsRequired()
                    .HasColumnType("TEXT (5)");

                entity.HasOne(d => d.EmailFailureSetting)
                    .WithMany(p => p.SettingsTaskEmailFailureSetting)
                    .HasForeignKey(d => d.EmailFailureSettingId)
                    .OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.EmailSuccessSetting)
                    .WithMany(p => p.SettingsTaskEmailSuccessSetting)
                    .HasForeignKey(d => d.EmailSuccessSettingId)
                    .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<SettingsTaskSub>(entity =>
            {
                entity.ToTable("Settings_Task_Sub");

                entity.HasOne(d => d.MainTask)
                    .WithMany(p => p.SettingsTaskSubMainTask)
                    .HasForeignKey(d => d.MainTaskId)
                    .OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.SubTask)
                    .WithMany(p => p.SettingsTaskSubSubTask)
                    .HasForeignKey(d => d.SubTaskId)
                    .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.DelegateFromDate).HasColumnType("DATE");

                entity.Property(e => e.DelegateToDate).HasColumnType("DATE");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 100)");

                entity.Property(e => e.IsAdmin).HasColumnType("BOOLEAN");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 20)");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasColumnType("TEXT (1, 50)");
            });
        }

        public virtual DbSet<SettingsRsoMasterUpload> SettingsRsoMasterUpload { get; set; }
        public virtual DbSet<SettingsRsoSage50> SettingsRsoSage50 { get; set; }
        public virtual DbSet<SettingsRsoScanDownload> SettingsRsoScanDownload { get; set; }
        public virtual DbSet<SettingsRsoScanUpload> SettingsRsoScanUpload { get; set; }
        public virtual DbSet<SettingsRsoXero> SettingsRsoXero { get; set; }
        public virtual DbSet<SettingsSage200Posting> SettingsSage200Posting { get; set; }
        public virtual DbSet<SettingsSage50Posting> SettingsSage50Posting { get; set; }
        public virtual DbSet<SettingsSharedAdestWebService> SettingsSharedAdestWebService { get; set; }
        public virtual DbSet<SettingsSharedBatchList> SettingsSharedBatchList { get; set; }
        public virtual DbSet<SettingsSharedDebugLog> SettingsSharedDebugLog { get; set; }
        public virtual DbSet<SettingsSharedDownloadSettings> SettingsSharedDownloadSettings { get; set; }
        public virtual DbSet<SettingsSharedEmail> SettingsSharedEmail { get; set; }
        public virtual DbSet<SettingsSharedFieldMapping> SettingsSharedFieldMapping { get; set; }
        public virtual DbSet<SettingsSharedFieldMappingFields> SettingsSharedFieldMappingFields { get; set; }
        public virtual DbSet<SettingsIndex> SettingsIndex { get; set; }
        public virtual DbSet<SettingsSharedRso> SettingsSharedRso { get; set; }
        public virtual DbSet<SettingsTask> SettingsTask { get; set; }
        public virtual DbSet<SettingsTaskSub> SettingsTaskSub { get; set; }
        public virtual DbSet<User> User { get; set; }
    }
}