﻿using System.IO;
using Newtonsoft.Json;

namespace TaskRunner.Task.AP.DTO
{
    class ApDocument
    {
        private string filePath;

        public string Extension { get; set; }

        public byte[] Document { get; set; }

        public string Name { get; set; }

        [JsonIgnore]
        public string FilePath
        {
            get
            {
                return filePath;
            }
            set
            {
                if (filePath == value)
                {
                    return;
                }

                filePath = value;
                if (!string.IsNullOrWhiteSpace(value))
                {
                    Name = Path.GetFileName(value);
                    Extension = Path.GetExtension(value);
                }
            }
        }
    }
}
