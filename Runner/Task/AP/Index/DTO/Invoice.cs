﻿//AP Invoice Data Transfer Object
using System;
using System.Collections.Generic;

namespace TaskRunner.Task.AP.DTO
{
    public class Invoice
    {

        public Invoice()
        {

        }

        public string CompanyName { get; set; }

        public string CompanyNumber { get; set; }

        public string SupplierCode { get; set; }

        public string SupplierName { get; set; }

        public string InvoiceNumber { get; set; }

        public DateTime? DocumentDate { get; set; }

        public string PONumber { get; set; }

        public int DocumentType { get; set; }

        public decimal? Net { get; set; }

        public decimal? VATRate { get; set; }

        public decimal? VAT { get; set; }

        public decimal? Total { get; set; }

        public string Currency { get; set; }

        public List<ApDocument> Documents { get; set; }

        public List<LineItem> LineItems { get; set; }
    }
}
