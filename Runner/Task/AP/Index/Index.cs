﻿///New assembly for Index task which will call the identity service endpoint to request a token allowing access to the index web api endpoint
using TaskRunner.Core;
using TaskRunner.Model;
using System;
using System.Linq;
using TaskRunner.Data;
using Microsoft.EntityFrameworkCore;
using System.IO;
using TaskRunner.Task.AP.DTO;
using System.Xml.Linq;
using System.Collections.Generic;

namespace TaskRunner.Task.AP
{
    public class Index : TaskBase, ITask
    {

        private SettingsIndex settings = null;
        private readonly IInvoiceService invoiceService;

        #region ITask Members

        bool ITask.IsDataProcessed
        {
            get { return IsDataProcessed; }
        }

        bool ITask.IsProcessing
        {
            get { return IsProcessing; }
        }

        bool ITask.IsSuccess
        {
            get { return IsSuccess; }
        }

        string ITask.LogErrorFilename
        {
            get { return LogErrorFilename; }
        }

        string ITask.LogFilename
        {
            get { return LogFilename; }
        }

        #endregion

        public void RunTask(string tenantDB, int taskID)
        {
            SetTaskSettings(tenantDB, taskID);

            try
            {
                IsProcessing = true;
                ProcessIndexFiles();

            }
            catch (Exception ex)
            {
                IsDataProcessed = true;
                WriteErrorLogFile("An unhandled exception occured.", "RunTask", ex);
                throw ex;
            }
            finally
            {
                IsProcessing = false;
            }

        }

        public void SetTaskSettings(string tenantDB, int taskID)
        {
            IsSuccess = false;
            IsDataProcessed = false;

            Initialize(tenantDB, taskID);

            using (var db = new TenantDBContext(tenantDB))
            {
                settings = db.SettingsIndex
                    .AsNoTracking()
                    .Include(rs => rs.IdentityServerEndpointNavigation)
                    .Include(i => i.ApiendpointNavigation)
                    .Include(fm => fm.HeaderFieldMapping)
                    .ThenInclude(a => a.SettingsSharedFieldMappingFields)
                    .Include(lm => lm.LineFieldMapping)
                    .ThenInclude(a => a.SettingsSharedFieldMappingFields)
                    .First(a => a.TaskId == taskID);
            }

        }

        #region Index Functions

        private void ProcessIndexFiles()
        {
            WriteLogFile("Checking for Index files to process in  : " + settings.IndexDataFilePath);

            try
            {
                if (Directory.Exists(settings.IndexDataFilePath))
                {
                    foreach (string file in Directory.GetFiles(settings.IndexDataFilePath, "*.*", SearchOption.TopDirectoryOnly))
                    {
                        if (!IsDataFileLocked(file)) ProcessIndexFile(file);
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void ProcessIndexFile(string indexFile)
        {
            WriteLogFile("Processing File  : " + indexFile);

            try
            {
                string[] lines = File.ReadAllLines(indexFile, System.Text.Encoding.Default);

                foreach (string line in lines)
                {
                    if (!string.IsNullOrEmpty(line.Trim()))
                    {
                        string processedLine = line.Trim();

                        if (processedLine.EndsWith("|"))
                            processedLine = line.Substring(0, line.Length - 1);

                        string[] data = processedLine.Split("|".ToCharArray(), StringSplitOptions.None);

                        if (data.Length > 0)
                        {
                            Invoice inv = MapInvoiceData(data);

                            //Call index Web API
                            PostInvoice(inv);

                        }
                        else
                        {

                        }
                    }
                }

            }
            catch (Exception ex)
            {

            }

        }

        /// <summary>
        /// Map Data to Invoice DTO
        /// </summary>
        private Invoice MapInvoiceData(string[] data)
        {
            Invoice inv = new Invoice();

            ////TODO: Load image in byte array - inv.ImageFile = GetRSOValue("ImageFile", data);
            //inv.DocumentType = GetDataValue("DocumentType", data,settings.HeaderFieldMapping.SettingsSharedFieldMappingFields);
            inv.SupplierCode = GetDataValue("SupplierCode", data, settings.HeaderFieldMapping.SettingsSharedFieldMappingFields);
            inv.InvoiceNumber = GetDataValue("InvoiceNumber", data, settings.HeaderFieldMapping.SettingsSharedFieldMappingFields);
            //inv.DocumentDate = GetDataValue("InvoiceDate", data,settings.HeaderFieldMapping.SettingsSharedFieldMappingFields);
            inv.PONumber = GetDataValue("PONumber", data,settings.HeaderFieldMapping.SettingsSharedFieldMappingFields);
            inv.Currency = GetDataValue("Currency", data, settings.HeaderFieldMapping.SettingsSharedFieldMappingFields);
            inv.VATRate = GetAmountDataValue("TaxRate", data, settings.HeaderFieldMapping.SettingsSharedFieldMappingFields);
            inv.Net = GetAmountDataValue("NetAmount", data, settings.HeaderFieldMapping.SettingsSharedFieldMappingFields);
            inv.VAT = GetAmountDataValue("TaxAmount", data, settings.HeaderFieldMapping.SettingsSharedFieldMappingFields);
            inv.Total = GetAmountDataValue("TotalAmount", data, settings.HeaderFieldMapping.SettingsSharedFieldMappingFields);

            string lineData = GetDataByTag(data, @"<LINEITEMS>");
            if (!string.IsNullOrEmpty(lineData))
                inv = PopulateLineItemData(inv, lineData);

            return inv;

        }

        //Populate invoice object line items with data from file
        //Map data to lineitem class using settngs MappingLineItemFields
        private Invoice PopulateLineItemData(Invoice inv, string lineData)
        {
            var xml = XDocument.Parse(lineData);
            var query = from c in xml.Descendants("LINEITEM")
                       select c;

            //Add each lineItem read from xml string to invoice DTO
            foreach (var lineItem in query)
            {
                var tempLines = lineItem.Elements("LINEITEMFIELD").Select(element => element.Value).ToArray<string>();
                inv.LineItems.Add(new LineItem
                {
                    ProductCode = GetDataValue("ProductCode", tempLines, settings.LineFieldMapping.SettingsSharedFieldMappingFields),
                    ProductDescription = GetDataValue("ProductDescription", tempLines, settings.LineFieldMapping.SettingsSharedFieldMappingFields),
                    Quantity = GetAmountDataValue("Quantity", tempLines, settings.LineFieldMapping.SettingsSharedFieldMappingFields),
                    UnitPrice = GetAmountDataValue("UnitPrice", tempLines, settings.LineFieldMapping.SettingsSharedFieldMappingFields),
                    Net = GetAmountDataValue("Net", tempLines, settings.LineFieldMapping.SettingsSharedFieldMappingFields),
                    Vat = GetAmountDataValue("Vat", tempLines, settings.LineFieldMapping.SettingsSharedFieldMappingFields),
                    VatRate = GetAmountDataValue("VatRate", tempLines, settings.LineFieldMapping.SettingsSharedFieldMappingFields),
                    VatCode = GetDataValue("VatCode", tempLines, settings.LineFieldMapping.SettingsSharedFieldMappingFields),
                    Segment1 = GetDataValue("Segment1", tempLines, settings.LineFieldMapping.SettingsSharedFieldMappingFields),
                    Segment2 = GetDataValue("Segment2", tempLines, settings.LineFieldMapping.SettingsSharedFieldMappingFields),
                    Segment3 = GetDataValue("Segment3", tempLines, settings.LineFieldMapping.SettingsSharedFieldMappingFields),
                    Segment4 = GetDataValue("Segment4", tempLines, settings.LineFieldMapping.SettingsSharedFieldMappingFields),
                    Segment5 = GetDataValue("Segment5", tempLines, settings.LineFieldMapping.SettingsSharedFieldMappingFields),
                    Segment6 = GetDataValue("Segment6", tempLines, settings.LineFieldMapping.SettingsSharedFieldMappingFields)
                });
            }

            return inv;
        }

        //Get the value from the string array based upon a mapped index or set a default value
        private string GetDataValue(string fieldName, string[] data, ICollection<SettingsSharedFieldMappingFields> fieldMapping)
        {
            string value = string.Empty;
            var mapResult = fieldMapping.FirstOrDefault(m => m.ToField.ToLower() == fieldName.ToLower());
            var rsoIndex = Object.ReferenceEquals(mapResult, null) ? -1 : System.Convert.ToInt16(mapResult.FromField);
            if (rsoIndex >= 0)
            {
                value = data[rsoIndex];
            }
            else
            {
                if (!Object.ReferenceEquals(mapResult, null))
                    value = fieldMapping.First(m => m.ToField.ToLower() == fieldName.ToLower()).DefaultValue;
            }

            return value;
        }

        private decimal GetAmountDataValue(string fieldName, string[] data, ICollection<SettingsSharedFieldMappingFields> fieldMapping)
        {
            string results = GetDataValue(fieldName, data, fieldMapping);
            return String.IsNullOrEmpty(results) ? 0 : System.Convert.ToDecimal(results);
        }

        //Checks for text data between 2 tags
        private string GetDataByTag(string[] data, string tagName)
        {
            string endTagName = tagName.Replace(@"<", @"</");

            foreach (string dataLine in data)
            {
                if (dataLine.IndexOf(tagName, StringComparison.OrdinalIgnoreCase) >= 0 && 
                    dataLine.IndexOf(endTagName, StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    return dataLine;
                }
            }

            return string.Empty;
        }

        private bool IsDataFileLocked(string indexFile)
        {
            try
            {
                FileStream fs = File.Open(indexFile, FileMode.Open, FileAccess.Read, FileShare.None);
                fs.Close();
                fs = null;
                return false;
            }
            catch (IOException ex)
            {
                WriteLogFile("An unhandled IO exception has occured when attempting to open the Readsoft data file (" + indexFile + ") exclusively. " + ex.Message);
                return true;
            }
            catch (Exception ex)
            {
                WriteLogFile("An unhandled exception has occured when attempting to open the Readsoft data file (" + indexFile + ") exclusively. " + ex.Message);
                throw ex;
            }
        }

        private void IndexAP()
        {
            try
            {

            }
            catch (Exception ex)
            {

            }
            finally
            {

            }

        }

        #endregion

        #region Posting to web api
        public string PostInvoiceStatus { get; set; }
        public string PostInvoiceResponseContent { get; set; }
        private void PostInvoice(Invoice inv)
        {

            invoiceService.Create(inv, settings.IdentityServerEndpointNavigation.Uri, settings.ApiendpointNavigation.Uri).ContinueWith(async task =>
            {
                PostInvoiceStatus = $"{(int)task.Result.StatusCode} - {task.Result.StatusCode.ToString()}";
                PostInvoiceResponseContent = JsonHelper.FormatJson(await task.Result.Content.ReadAsStringAsync());
            });
        }

        #endregion

    }
}
