﻿using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using IdentityModel.Client;
using Newtonsoft.Json.Linq;

namespace TaskRunner.Task.AP
{
    public abstract class RestClientBase
    {
       
        protected  Task<HttpResponseMessage> PostAsync<T>(T dto, string idsEndPoint, string endPoint) where T : class
        {
            // discover endpoints from metadata
            var disco =  DiscoveryClient.GetAsync(idsEndPoint).Result;

            // request token
            var tokenClient = new TokenClient(disco.TokenEndpoint, "MatchingHelperApp", "secret");
            var tokenResponse =  tokenClient.RequestClientCredentialsAsync("api.full_access");
           
            if (tokenResponse.Result.IsError)
            {
                //Console.WriteLine(tokenResponse.Error);
                return null;
            }

            // call api
            var client = new HttpClient();
            client.SetBearerToken(tokenResponse.Result.AccessToken);            

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, endPoint)
            {
                Content = new StringContent(JsonConvert.SerializeObject(dto, new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver(),
                    Converters = new JsonConverter[]
                 {
                        new StringEnumConverter(),
                 },
                    StringEscapeHandling = StringEscapeHandling.EscapeHtml
                }), Encoding.UTF8, "application/json")
            };

            var response = client.SendAsync(request);

            if (!response.Result.IsSuccessStatusCode)
            {
                //Console.WriteLine(response.StatusCode);
                var content = response.Result.Content.ReadAsStringAsync();
                return response;
            }
            else
            {
                return response;               
            }
            
        }
    }
}