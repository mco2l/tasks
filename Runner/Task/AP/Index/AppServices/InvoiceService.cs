﻿using System.Net.Http;
using System.Threading.Tasks;
using TaskRunner.Task.AP.DTO;

namespace TaskRunner.Task.AP
{
    public class InvoiceService : RestClientBase, IInvoiceService
    {
        public Task<HttpResponseMessage> Create(Invoice invoice, string idsEndPoint, string endPoint)
        {
            return PostAsync(invoice, idsEndPoint, endPoint);
        }
    }
}