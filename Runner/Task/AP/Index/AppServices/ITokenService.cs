﻿using System.Threading.Tasks;
using IdentityModel.Client;

namespace TaskRunner.Task.AP
{
    public interface ITokenService
    {
        Task<TokenResponse> GetToken(string endPoint);
    }
}