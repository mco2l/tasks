﻿using System.Threading.Tasks;
using IdentityModel.Client;

namespace TaskRunner.Task.AP
{
    public class TokenService : ITokenService
    {
        public async Task<TokenResponse> GetToken(string endPoint)
        {
            var discoveryClient = new DiscoveryClient(endPoint)
            {
                Policy =
                {
                    RequireHttps = true,
                }
            };
            var disco = await discoveryClient.GetAsync();
            //var tokenClient = new TokenClient(disco.TokenEndpoint, "TaskScheduler", "60ECCCA1-2189-4E85-81F2-F86E884EB19D");
            var tokenClient = new TokenClient(disco.TokenEndpoint, "MatchingHelperApp", "secret");
            return await tokenClient.RequestClientCredentialsAsync("api.full_access");
        }
    }
}