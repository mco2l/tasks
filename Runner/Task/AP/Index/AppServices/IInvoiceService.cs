﻿using System.Net.Http;
using System.Threading.Tasks;
using TaskRunner.Task.AP.DTO;

namespace TaskRunner.Task.AP
{
    public interface IInvoiceService
    {
        Task<HttpResponseMessage> Create(Invoice invoice, string idsEndPoint, string endPoint);
    }
}