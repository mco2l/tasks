﻿namespace TaskRunner.Task.RSO.Enums
{
    public enum RsoInvoicePropertyNameEnum
    {
        BuyerCode,
        BuyerName,
        SupplierCode,
        SupplierName,
        CreditInvoice,
        InvoiceNumber,
        InvoiceDate,
        OrderNumber,
        NetValue,
        VATRate,
        VATValue,
        GrossValue,
        Currency,
        CustomField1,
        CustomField2,
        CustomField3,
        CustomField4
    }

    public enum RsoMasterDataPropertyNameEnum
    {
        SupplierNumber,
        Name,
        Description,
        TaxRegistrationNumber,
        OrganizationNunmber,
        Street,
        PostalCode,
        City,
        CountryName,
        PaymentTerm,
        PaymentMethod,
        CurrencyCode,
        Id,
        Location,
        State,
        BankAccount,
        Blocked,
        Value,
        Code,
        ProjectNumber,
        Active
    }
}
