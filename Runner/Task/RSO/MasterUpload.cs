﻿using TaskRunner.Core;
using ReadSoft.Services.Client;
using ReadSoft.Services.Client.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using TaskRunner.Data;
using TaskRunner.Model;
using Microsoft.EntityFrameworkCore;
using TaskRunner.Core.Extensions;

namespace TaskRunner.Task.RSO
{
    public class MasterUpload : TaskBase, ITask
    {
        private long totalFound = 0;
        private string totalFoundList = string.Empty;

        private long totalUploaded = 0;
        private string totalUploadedList = string.Empty;
        private SettingsRsoMasterUpload settings;

        public void SetTaskSettings(string tenantDB, int taskID)
        {
            AddUpdateAdditionalTag("TotalFound", totalFound.ToString());
            AddUpdateAdditionalTag("TotalFoundList", totalFoundList.ToString());
            AddUpdateAdditionalTag("TotalUploaded", totalUploaded.ToString());
            AddUpdateAdditionalTag("TotalUploadedList", totalUploadedList.ToString());

            IsSuccess = false;
            IsDataProcessed = false;

            Initialize(tenantDB, taskID);

            using (var db = new TenantDBContext(tenantDB))
            {
                settings = db.SettingsRsoMasterUpload
                    .AsNoTracking()
                    .Include(rs => rs.Rsosetting)
                    .Include(fm => fm.FieldMapping)
                    .ThenInclude(a => a.SettingsSharedFieldMappingFields)
                    .First(a => a.TaskId == taskID);
            }
        }

        public void RunTask(string tenantDB, int taskID)
        {
            ClientConfiguration clientConfiguration = null;
            bool isLoggedIn = false;

            try
            {
                //if (settings.MasterDataType == MasterDataTypeEnum.Custom && string.IsNullOrEmpty(settings.CustomMasterDataTableName))
                //{
                //    throw new Exception("RsoMasterDataTableName element cannot be empty when the MasterDataType is set to Custom. Please assign a table name that's relevant to the data that you're trying to upload to RSO.");
                //}

                IsProcessing = true;

                WriteLogFile("Logging to RSO.");
                //TODO: Replace decryption with V10 component
                clientConfiguration = RsoAuthenticationService.Login(settings.Rsosetting.Uri, settings.Rsosetting.ApiKey, settings.Rsosetting.UserName, EncryptionHelper.AdDecrypt(settings.Rsosetting.Password));

                if (clientConfiguration != null)
                {
                    WriteLogFile("Logged in successfully to RSO.");
                    isLoggedIn = true;

                    WriteLogFile("Getting current customer details from RSO.");
                    Customer customer = RsoAccountService.GetCurrentCustomer(clientConfiguration);
                    if (customer == null)
                        throw new ArgumentNullException("Customer", "Cannot continue - RSO Current Customer cannot be null.");

                    WriteLogFile("Current customer details obtained successfully from RSO.");
                    WriteLogFile(string.Concat("Current customer Id: ", customer.Id.ToString()));
                    WriteLogFile(string.Concat("Current customer Name: ", customer.Name));
                    WriteLogFile("Getting all buyers for current customer from RSO.");

                    BuyerCollection buyerCollection = RsoAccountService.GetAllBuyersByCustomer(clientConfiguration, customer.Id);
                    if (buyerCollection == null)
                        throw new ArgumentNullException("BuyerCollection", "Cannot continue - RSO buyers for current customer cannot be null.");

                    if (buyerCollection.Count == 0)
                        throw new ArgumentOutOfRangeException("BuyerCollection", "Cannot continue - No buyers found for current customer.");

                    WriteLogFile("All buyers for current customer obtained successfully from RSO.");
                    WriteLogFile(string.Concat("Total Buyers for current customer: ", buyerCollection.Count.ToString()));

                    WriteLogFile("Loading field mapping list.");
                    List<RsoMasterDataMapping> mappingList = GetMapping();
                    WriteLogFile("Field mapping list loaded successfully.");

                    string pattern = string.Empty;
                    string patternMask = string.Empty;
                    string patternExt = string.Empty;
                    bool usePatternOnly = false;

                    if (settings.UploadType == 0)//UploadTypeEnum.File)
                    {
                        if (string.IsNullOrEmpty(settings.DataFilePattern))
                        {
                            patternMask = string.IsNullOrEmpty(settings.DataFilePatternMask) ? "*{0}" : settings.DataFilePatternMask;
                            patternExt = string.IsNullOrEmpty(settings.DataFilePatternExt) ? ".txt" : settings.DataFilePatternExt;
                            pattern = string.Concat(patternMask, patternExt);
                            usePatternOnly = false;
                        }
                        else
                        {
                            pattern = settings.DataFilePattern;
                            usePatternOnly = true;
                        }
                        WriteLogFile("Upload Type: File");
                        WriteLogFile(string.Concat("Pattern Format: ", pattern));
                    }
                    else
                    {
                        WriteLogFile("Upload Type: Database");
                    }

                    if (mappingList != null || mappingList.Count > 0)
                    {
                        if (buyerCollection.Count > 0)
                        {
                            foreach (Buyer buyer in buyerCollection.OrderBy(m => m.OrganizationNumber))
                            {
                                WriteLogFile(string.Concat("Buyer Id: ", buyer.Id.ToString()));
                                WriteLogFile(string.Concat("Buyer Name: ", buyer.Name));
                                WriteLogFile(string.Concat("Buyer ExternalId: ", buyer.ExternalId));

                                Dictionary<string, object> parameters = new Dictionary<string, object>();
                                bool buyerFound;

                                if (settings.UploadType == 0)//UploadTypeEnum.File)
                                {
                                    string patternValue = (usePatternOnly ? pattern : string.Format(pattern, Convert.ToInt32(buyer.ExternalId)));
                                    parameters.Add("FilePath", settings.DataFilePath);
                                    parameters.Add("Pattern", patternValue);
                                    WriteLogFile(string.Concat("FilePath: ", settings.DataFilePath.ToString()));
                                    WriteLogFile(string.Concat("Pattern: ", patternValue));
                                }
                                else
                                {
                                    parameters.Add("ConnectionType", settings.DbconnectionType);
                                    parameters.Add("ConnectionString", settings.DbconnectionString);
                                    parameters.Add("CommandType", settings.DbcommandType);
                                    parameters.Add("CommandText", settings.DbcommandText.ReplaceWhiteSpaces(" "));
                                    parameters.Add("PreCommandText", settings.DbcommandText.ReplaceWhiteSpaces(" "));
                                    parameters.Add("CsvSeparator", settings.CsvSeparator);
                                }

                                buyerFound = IsBuyerMatched(buyer);

                                if (!buyerFound)
                                {
                                    WriteLogFile("Buyer not found in ExternalIdFilter list. Skipping Process");
                                    continue;
                                }
                                else
                                {
                                    WriteLogFile(string.Concat("Buyer Found in ExternalIdFilter list: ", buyer.ExternalId.ToString()));
                                }

                                //Check if data is available from file or database and return it.
                                WriteLogFile(@"About to load data from file/data");
                                Dictionary<string, object> csvInfo = TaskHelpers.GetData(taskID, settings.UploadType, parameters);
                                WriteLogFile(@"Data from file/database loaded successfully.");

                                string csvData = string.Empty;

                                //Filename is returned by GetData(). When uploadtype is file returns first available 
                                //file, when database, it returns hardcoded value of ("DatabaseData" + CurrentTask.Id)
                                CurrentFilename = csvInfo["Filename"].ToString();
                                CurrentHashkey = csvInfo["HashKey"].ToString();

                                csvData = csvInfo["Data"].ToString();

                                totalFound += (int)(csvInfo["Total"]);
                                totalFoundList += "Buyer: " + buyer.Name + "(" + buyer.OrganizationNumber.ToString() + ") - Total " + (settings.UploadType == 0 ? "lines found" : "rows returned") + ": " + (int)(csvInfo["Total"]) + Environment.NewLine;

                                AddUpdateAdditionalTag("TotalFound", totalFound.ToString());
                                AddUpdateAdditionalTag("TotalFoundList", totalFoundList.ToString());

                                if (string.IsNullOrEmpty(CurrentFilename) && string.IsNullOrEmpty(csvData))
                                {
                                    //if no files or data, then skip remaining process.
                                    continue;
                                }

                                if (!string.IsNullOrEmpty(csvData) && TaskHelpers.IsHashFileExists(CurrentFilename, CurrentHashkey))
                                {
                                    IsDataProcessed = true;
                                    WriteLogFile(string.Concat("File: ", CurrentFilename));
                                    WriteLogFile(string.Concat("Hashkey: ", CurrentHashkey));

                                    string[] sampleDataArray = csvData.Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                                    if (sampleDataArray.Length > 0)
                                    {
                                        WriteLogFile("CsvData sample data:");
                                        WriteLogFile(sampleDataArray[0]);
                                        if (sampleDataArray.Length > 1)
                                            WriteLogFile(sampleDataArray[1]);
                                    }
                                    else //output the first 100 character if no \r\n is not found.
                                    {
                                        WriteLogFile(string.Concat("CsvData sample data (100 chars): ", csvData.Length > 100 ? csvData.Substring(0, 100) : csvData.Substring(0, csvData.Length)));
                                    }

                                    sampleDataArray = null;

                                    //TODO: UploadMasterData
                                    //UploadMasterData(settings.MasterDataType, clientConfiguration, buyer, csvData, mappingList);

                                    if (!settings.IsFileShared)
                                    {
                                        CreateHashKey();
                                        CleanUp();
                                    }
                                }
                                else //Delete the specific file (not shared) if its data is identical.
                                {
                                    if (settings.UploadType == 0)// UploadTypeEnum.File)
                                    {
                                        //Check if log is outputted as a mechanism to see if data was actually processed.
                                        if (!settings.IsFileShared)
                                        {
                                            WriteLogFile("No data was uploaded for buyer (ExternalId: " + buyer.ExternalId + ") as it is identical to the previous data that was uploaded.");
                                            CleanUp(); //clean up files but don't create hashkey as it is done above.
                                        }
                                        else
                                        {
                                            WriteLogFile("No data was uploaded for buyer (ExternalId: " + buyer.ExternalId + ") in the shared file as it is identical to the previous data that was uploaded.");
                                        }
                                    }
                                }
                            } //end of buyerCollection loop

                            //When using a shared file among multiple buyers, only create the hash key file, move 
                            //the file to the backup folder or deleted it once all the buyers have been processed
                            if (settings.UploadType == 0 && settings.IsFileShared)
                            {
                                CreateHashKey();
                                CleanUp();
                            }
                        }
                        else
                        {
                            //?? Upload using customer Id instead of buyer's id.
                        }
                    }
                    else
                    {
                        //WriteLogFile("Cannot continue - No mapping fields appears to have been defined.");
                        WriteErrorLogFile("RunTask", "Mapping Error", new Exception("Cannot continue - No mapping fields appears to have been defined."));
                        throw new Exception("Failed to login to RSO. Please check APIKey, Username and Password.");
                    }
                }
                else
                {
                    //WriteLogFile("Failed to login to RSO. Please check APIKey, Username and Password.");
                    WriteErrorLogFile("RunTask", "Login Error", new Exception("Failed to login to RSO. Please check APIKey, Username and Password."));
                    throw new Exception("Failed to login to RSO. Please check APIKey, Username and Password.");
                }
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                IsDataProcessed = true; //Set this to true to ensure that whether data was processed or not, it will log logging off from RSO, etc...
                WriteErrorLogFile("An unhandled exception occured.", "RunTask", ex);
                if (!string.IsNullOrEmpty(CurrentFilename))
                {
                    WriteLogFile("Moving erroneous file to error folder.");
                    TaskHelpers.MoveFileToErrorPath(settings.ErrorPath, CurrentFilename, CurrentHashkey);
                    WriteLogFile("Erroneous file moved to error folder.");
                }
                throw ex; //Throw the error so that it notifies the task scheduler of a major error and it will disable the relevant job after x failures i.e. 3.
            }
            finally
            {
                if (isLoggedIn && clientConfiguration != null)
                {
                    try
                    {
                        if (IsDataProcessed) WriteLogFile("Logging off from RSO.");
                        RsoAuthenticationService.Logoff(clientConfiguration);
                        if (IsDataProcessed) WriteLogFile("Logged off successfully from RSO.");
                        if (IsDataProcessed) WriteLogFile(string.Empty);
                    }
                    catch (Exception ex)
                    {
                        WriteErrorLogFile("RunTask", "An unhandled exception occurred while attempting to sign off from RSO.", ex);
                    }
                }
                settings = null;
                clientConfiguration = null;
                IsProcessing = false;
            }
        }

        //TODO: Field Mapping LineSeparator & ReplaceByDefault
        //TODO: Replace Adest.BusinessObjects
        private List<RsoMasterDataMapping> GetMapping()
        {
            List<RsoMasterDataMapping> mappingList = new List<RsoMasterDataMapping>();

            //Convert from Settings.Mapping to Rso.RsoMasterDataMapping;
            mappingList = (from m in settings.FieldMapping.SettingsSharedFieldMappingFields
                           select new RsoMasterDataMapping()
                           {
                               CsvFieldIndex = System.Convert.ToInt16(m.FromField),
                               RsoPropertyName = (Enums.RsoMasterDataPropertyNameEnum)Enum.Parse(typeof(Enums.RsoMasterDataPropertyNameEnum), m.ToField),
                               LineSeparator = settings.CsvSeparator, //m.LineSeparator,
                               DefaultValue = m.DefaultValue,
                               //ReplaceByDefault = m.ReplaceByDefault
                           }).ToList<RsoMasterDataMapping>();

            return mappingList;
        }

        private void CleanUp()
        {
            try
            {
                if (settings.UploadType == 0 && !string.IsNullOrEmpty(CurrentFilename))
                {
                    WriteLogFile("Cleaning up.");
                    if (!string.IsNullOrEmpty(settings.BackupPath))
                    {
                        WriteLogFile(string.Concat("Backing up file: ", CurrentFilename, " to ", settings.BackupPath));
                        TaskHelpers.MoveFileToBackupPath(settings.BackupPath, CurrentFilename, CurrentHashkey);
                        WriteLogFile("File backed up successfully.");
                    }
                    else //Delete file if backup is not set up.
                    {
                        WriteLogFile("Deleting the file: " + CurrentFilename);
                        TaskHelpers.DeleteFile(CurrentFilename);
                        WriteLogFile("File deleted successfully.");
                    }
                    WriteLogFile("Clean up completed.");
                }
            }
            catch (Exception ex)
            {
                WriteErrorLogFile("CleanUp", "An unhandled exception occured during the clean up operation.", ex);
                throw ex;
            }
        }

        private void CreateHashKey()
        {
            //Create the hashtag file
            WriteLogFile("Creating hashkey flag file");
            TaskHelpers.CreateHashFile(CurrentFilename, CurrentHashkey);
            WriteLogFile("Hashkey flag file created.");
        }

        private bool IsBuyerMatched(Buyer buyer)
        {
            bool isMatched = false;

            //TODO: Implement ExternalIdFilter

            //if (string.IsNullOrEmpty(settings.ExternalIdFilter)) //if no filter defined in config file, import data for all defined buyers in rso.
            isMatched = true;
            //else
            //{
            //    string[] rsoBuyerFilter = settings.ExternalIdFilter.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            //    string id = string.Empty;
            //    id = rsoBuyerFilter.FirstOrDefault(m => m.ToUpper() == buyer.ExternalId.ToUpper());

            //    if (id != null)
            //        isMatched = true;
            //    else
            //        isMatched = false;

            //    rsoBuyerFilter = null;
            //}

            return isMatched;
        }
    }
}
