﻿using TaskRunner.Core;
using ReadSoft.Services.Client;
using ReadSoft.Services.Client.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TaskRunner.Data;
using Microsoft.EntityFrameworkCore;
using TaskRunner.Model;
using TaskRunner.Core.Extensions;

namespace TaskRunner.Task.RSO
{
    public class ScanUpload : TaskBase, ITask
    {
        private SettingsRsoScanUpload settings = null;

        private int TotalScanUploaded = 0;
        private string TotalScanUploadedList = string.Empty;

        private int TotalScanUploadedFail = 0;        
        private string TotalScanUploadedFailList = string.Empty;

        private string ScanUploadFailureFileList = string.Empty;

        private string TotalOverMaxSizeList = string.Empty;

        #region ITask Members

        public void SetTaskSettings(string tenantDB, int taskID)
        {
            //Initialize values used by Additional ags
            TotalScanUploaded = 0;
            TotalScanUploadedList = string.Empty;
            TotalScanUploadedFail = 0;
            TotalScanUploadedFailList = string.Empty;
            ScanUploadFailureFileList = string.Empty;

            //Initialize AdditionalTags
            AddUpdateAdditionalTag("TotalScanUploaded", TotalScanUploaded.ToString());
            AddUpdateAdditionalTag("TotalScanUploadedList", TotalScanUploadedList);
            AddUpdateAdditionalTag("TotalScanUploadedFail", TotalScanUploaded.ToString());
            AddUpdateAdditionalTag("TotalScanUploadedFailList", TotalScanUploadedList);
            AddUpdateAdditionalTag("ScanUploadFailureFileList ", ScanUploadFailureFileList);

            IsSuccess = false;
            IsDataProcessed = false;            

            Initialize(tenantDB,taskID);

            using (var db = new TenantDBContext(tenantDB))
            {
                settings = db.SettingsRsoScanUpload
                    .AsNoTracking()
                    .Include(rs => rs.Rsosetting)
                    .First(a => a.TaskId == taskID);
            }
        }

        public void RunTask(string tenantDB, int taskID)
        {
            SetTaskSettings(tenantDB, taskID);

            ClientConfiguration clientConfiguration = null;
            bool isLoggedIn = false;

            try
            {
                IsProcessing = true;

                //WriteLogToMemory("Logging to RSO.");
                //TODO: Replace decryption with V10 component
                clientConfiguration = RsoAuthenticationService.Login(settings.Rsosetting.Uri, settings.Rsosetting.ApiKey, settings.Rsosetting.UserName, EncryptionHelper.AdDecrypt(settings.Rsosetting.Password));

                if (clientConfiguration != null)
                {
                    //WriteLogToMemory("Logged in successfully to RSO.");
                    isLoggedIn = true;

                    //WriteLogToMemory("Getting current customer details from RSO.");
                    Customer customer = RsoAccountService.GetCurrentCustomer(clientConfiguration);
                    if (customer == null)
                        throw new ArgumentNullException("Customer", "Cannot continue - RSO Current Customer cannot be null.");

                    //WriteLogToMemory("Current customer details obtained successfully from RSO.");
                    //WriteLogToMemory(string.Concat("Current customer Id: ", customer.Id.ToString()));
                    //WriteLogToMemory(string.Concat("Current customer Name: ", customer.Name));
                    //WriteLogToMemory("Getting all buyers for current customer from RSO.");

                    BuyerCollection buyerCollection = RsoAccountService.GetAllBuyersByCustomer(clientConfiguration, customer.Id);
                    if (buyerCollection == null)
                        throw new ArgumentNullException("BuyerCollection", "Cannot continue - RSO buyer list for current customer cannot be null.");

                    Dictionary<string, DocumentTypeCollection> documentTypeCollectionList = new Dictionary<string, DocumentTypeCollection>();

                    if (buyerCollection.Count == 0 || settings.AutoBuyerId)
                    {
                        DocumentTypeCollection documentTypeCollection = RsoAccountService.GetCustomerDocumentTypes(clientConfiguration, customer.Id);

                        if (documentTypeCollection == null)
                            throw new ArgumentNullException("DocumentTypeCollection", "Cannot continue - RSO document types list for customer: " + customer.Name + " cannot be null.");

                        if (documentTypeCollection.Count == 0)
                            throw new ArgumentNullException("DocumentTypeCollection", "Cannot continue - RSO document types list for customer: " + customer.Name + " cannot be empty.");

                        documentTypeCollectionList.Add(customer.Id, documentTypeCollection);
                    }
                    else
                    {
                        foreach (var buyer in buyerCollection)
                        {
                            DocumentTypeCollection documentTypeCollection = RsoAccountService.GetBuyerDocumentTypes(clientConfiguration, buyer.Id);

                            if (documentTypeCollection == null)
                                throw new ArgumentNullException("DocumentTypeCollection", "Cannot continue - RSO document types list for buyer: " + buyer.Name + " cannot be null.");

                            if (documentTypeCollection.Count == 0)
                                throw new ArgumentNullException("DocumentTypeCollection", "Cannot continue - RSO document types list for buyer: " + buyer.Name + " cannot be empty.");

                            documentTypeCollectionList.Add(buyer.Id, documentTypeCollection);
                        }
                    }

                    //This will never apply when dealing with buyers rather an a customer as an error is thrown immediately if no document type is found for a specific buyer.
                    if (documentTypeCollectionList.Count == 0)
                        throw new Exception("Cannot continue - There were no RSO Document Collection List detected for " + (buyerCollection.Count > 0 ? ("the customer " + customer.Name + ".") : " any of the buyers."));

                    if (settings.AutoCreateFolder)
                    {
                        AutoCreateFolders(clientConfiguration, customer, buyerCollection, documentTypeCollectionList);
                    }

                    if (settings.AutoDetection)
                    {
                        AutoDetectBatchProcessing(clientConfiguration, customer, buyerCollection, documentTypeCollectionList);
                    }
                    else
                    {
                        ManualBatchProcessing();
                    }
                    if (TotalScanUploadedFail == 0) IsSuccess = true;
                }
                else
                {
                    WriteErrorLogFile("RunTask", "Login Error", new Exception("Failed to login to RSO. Please check APIKey, Username and Password."));
                }
            }
            catch (Exception ex)
            {
                //Ensure all memory logs are outputted if not previously outputted.
                WriteErrorLogFile("An unhandled exception occured.", "RunTask", ex);
                throw ex; //Throw the error so that it notifies the task scheduler of a major error and it will disable the relevant job after x failures i.e. 3.
            }
            finally
            {
                if (isLoggedIn && clientConfiguration != null)
                {
                    try
                    {
                        if (string.IsNullOrEmpty(LogText.ToString()) || LogTextOutputted) WriteLogFile("Logging off from RSO.");
                        RsoAuthenticationService.Logoff(clientConfiguration);
                        if (string.IsNullOrEmpty(LogText.ToString()) || LogTextOutputted) WriteLogFile("Logged off successfully from RSO.");
                        if (string.IsNullOrEmpty(LogText.ToString()) || LogTextOutputted) WriteLogFile(string.Empty);
                    }
                    catch (Exception ex)
                    {
                        WriteErrorLogFile("RunTask", "An unhandled exception occurred while attempting to sign off from RSO.", ex);
                    }
                }
                clientConfiguration = null;
                IsProcessing = false;
            }
        }

        bool ITask.IsDataProcessed
        {
            get { return IsDataProcessed; }
        }

        bool ITask.IsProcessing
        {
            get { return IsProcessing; }
        }

        bool ITask.IsSuccess
        {
            get { return IsSuccess; }
        }

        string ITask.LogErrorFilename
        {
            get { return LogErrorFilename; }
        }

        string ITask.LogFilename
        {
            get { return LogFilename; }
        }

        #endregion

        private void AutoCreateFolders(ClientConfiguration clientConfiguration, Customer customer, BuyerCollection buyerCollection, Dictionary<string, DocumentTypeCollection> documentTypeCollectionList)
        {
            string batchPath = settings.BatchPath;
            bool isMultiBuyer = buyerCollection.Count > 0 ? true : false;
            string defaultDocumentType = settings.DefaultDocumentSystemName;
            string documentType = string.Empty;
            string buyerName = string.Empty;

            if (!Directory.Exists(batchPath))
            {
                try
                {
                    WriteLogFile("About to create BatchPath folder: " + batchPath);
                    Directory.CreateDirectory(batchPath);
                    WriteLogFile("BatchPath folder created successfully.");
                }
                catch (IOException ex)
                {
                    WriteErrorLogFile("AutoCreateFolders", "An unhandled IO exception occured while attempting to create the batch path: " + batchPath + " - Exception: " + ex.ToString());
                }
                catch (Exception ex)
                {
                    WriteErrorLogFile("AutoCreateFolders", "An unhandled exception occured while attempting to create the batch path: " + batchPath + " - Exception: " + ex.ToString());
                }
            }

            if (isMultiBuyer && !settings.AutoBuyerId)
            {
                foreach (var buyer in buyerCollection)
                {
                    string batch = Path.Combine(batchPath, buyer.Name);
                    if (!Directory.Exists(batch))
                    {
                        try
                        {
                            WriteLogFile("About to create buyer folder: " + buyer.Name + " in " + batchPath);
                            Directory.CreateDirectory(batch);
                            WriteLogFile("Folder created successfully.");
                        }
                        catch (IOException ex)
                        {
                            WriteErrorLogFile("AutoCreateFolders", "An unhandled IO exception occured while attempting to create the folder: " + buyer.Name + " in " + batchPath + " - Exception: " + ex.ToString());
                        }
                        catch (Exception ex)
                        {
                            WriteErrorLogFile("AutoCreateFolders", "An unhandled exception occured while attempting to create the folder: " + buyer.Name + " in " + batchPath + " - Exception: " + ex.ToString());
                        }
                    }

                    foreach (var documentTypeCollection in documentTypeCollectionList[buyer.Id])
                    {
                        documentType = documentTypeCollection.SystemName;
                        string subBatch = Path.Combine(batch, documentType);
                        if (!Directory.Exists(subBatch))
                        {
                            try
                            {
                                WriteLogFile("About to create document type folder: " + documentType + " in " + subBatch);
                                Directory.CreateDirectory(subBatch);
                                WriteLogFile("Folder created successfully.");
                            }
                            catch (IOException ex)
                            {
                                WriteErrorLogFile("AutoCreateFolders", "An unhandled IO exception occured while attempting to create the folder: " + documentType + " in " + subBatch + " - Exception: " + ex.ToString());
                            }
                            catch (Exception ex)
                            {
                                WriteErrorLogFile("AutoCreateFolders", "An unhandled exception occured while attempting to create the folder: " + documentType + " in " + subBatch + " - Exception: " + ex.ToString());
                            }
                        }
                    }
                }
            }
            else
            {
                foreach (var documentTypeCollection in documentTypeCollectionList[customer.Id])
                {
                    documentType = documentTypeCollection.SystemName;
                    string batch = Path.Combine(batchPath, documentType);
                    if (!Directory.Exists(batch))
                    {
                        try
                        {
                            WriteLogFile("About to create document type folder: " + documentType + " in " + batch);
                            Directory.CreateDirectory(batch);
                            WriteLogFile("Folder created successfully.");
                        }
                        catch (IOException ex)
                        {
                            WriteErrorLogFile("AutoCreateFolders", "An unhandled IO exception occured while attempting to create the folder: " + documentType + " in " + batch + " - Exception: " + ex.ToString());
                        }
                        catch (Exception ex)
                        {
                            WriteErrorLogFile("AutoCreateFolders", "An unhandled exception occured while attempting to create the folder: " + documentType + " in " + batch + " - Exception: " + ex.ToString());
                        }
                    }
                }
            }
        }

        private void ManualBatchProcessing()
        {
            //loop through the batches defined in config and use properties from 
            //definition line i.e. isbuyer, buyername, document type, 
            //default document type
        }

        private void AutoDetectBatchProcessing(ClientConfiguration clientConfiguration, Customer customer, BuyerCollection buyerCollection, Dictionary<string, DocumentTypeCollection> documentTypeCollectionList)
        {
            string batchPath = settings.BatchPath;
            bool isMultiBuyer = buyerCollection.Count > 0 && !settings.AutoBuyerId ? true : false;
            string defaultDocumentType = settings.DefaultDocumentSystemName;
            string documentType = string.Empty;
            string buyerName = string.Empty;
            bool isMultiPage;

            //WriteLogToMemory(string.Concat("Batch Path           : ", batchPath));
            //WriteLogToMemory(string.Concat("Processing           : ", isMultiBuyer ? "Buyer(s)" : "Company"));
            //WriteLogToMemory(string.Concat("Default Document Type: ", defaultDocumentType));
            //WriteLogToMemory(string.Concat("CustomerId           : ", customer.Id));
            //WriteLogToMemory(string.Concat("ExternalId           : ", customer.ExternalId));
            //WriteLogToMemory(string.Concat("Total Buyers         : ", buyerCollection.Count));

            if (isMultiBuyer) //Process buyer(s)
            {
                string[] buyerFolders = Directory.GetDirectories(batchPath);
                if (buyerFolders.Length > 0) //Will have a sub-folder for every buyer and/or another sub-folder within each buyer sub-folder for one or more document type.
                {
                    foreach (string buyerFolder in buyerFolders)
                    {
                        DirectoryInfo di = new DirectoryInfo(buyerFolder);
                        isMultiPage = false;

                        if (di.Name.EndsWith("_Multi", StringComparison.InvariantCultureIgnoreCase))
                        {
                            buyerName = di.Name.Substring(0, di.Name.Length - "_Multi".Length);
                            isMultiPage = true;
                        }
                        else
                        {
                            buyerName = di.Name;
                        }

                        Buyer buyer = buyerCollection.FirstOrDefault(m => m.Name.ToUpper() == buyerName.ToUpper());
                        
                        di = null;

                        documentType = string.Empty;

                        if (buyer != null)
                        {
                            string[] documentTypeFolders = Directory.GetDirectories(buyerFolder);
                            if (documentTypeFolders.Length > 0) //multi document type
                            {
                                foreach (string documentTypeFolder in documentTypeFolders)
                                {
                                    isMultiPage = false;
                                    di = new DirectoryInfo(documentTypeFolder);
                                    
                                    string folderName = di.Name.ToUpper();

                                    if (folderName.EndsWith("_Multi", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        folderName = di.Name.Substring(0, di.Name.Length - "_Multi".Length);
                                        isMultiPage = true;
                                    }
                                    else
                                    {
                                        folderName = di.Name;
                                    }

                                    DocumentType documentTypeObject = documentTypeCollectionList[buyer.Id].FirstOrDefault(m => m.SystemName.ToUpper() == folderName);

                                    if (!object.ReferenceEquals(documentTypeObject, null))
                                    {
                                        documentType = documentTypeObject.SystemName;
                                        di = null;
                                        //if default document type is provided and document type is not found, use the default.
                                        documentType = string.IsNullOrEmpty(documentType) ? defaultDocumentType : documentType;
                                        UploadFilesToRSO(clientConfiguration, documentTypeFolder, customer, buyer, documentType, isMultiPage);
                                    }
                                    //DO NOT LOG FOR NOW - In case it generates a file every 1 minutes or so, it could cause hundreds of files to be created for the same error. Should this be logged?? Mayb
                                    //it needs to be fixed before continuing any further.
                                    //else
                                    //{
                                    //    WriteErrorLogFile("AutoDetectBatchProcessing", "DocumentType " + folderName + " was not found in the Document Type list returned from RSO. IsMulti: " + isMultiPage.ToString());
                                    //}
                                }
                            }
                            else //buyer with one document type
                            {
                                //In this scenario, we will always use the default document type as it will
                                //either be set to something or it will be empty.
                                documentType = defaultDocumentType;
                                UploadFilesToRSO(clientConfiguration, buyerFolder, customer, buyer, documentType, isMultiPage);
                            }
                        }
                        else
                        {
                            WriteErrorLogFile("AutoDetectBatchProcessing", "The buyer " + buyerName + " was not found in the buyers list returned from RSO.");
                        }
                    }
                }
                else
                {
                    WriteErrorLogFile("AutoDetectBatchProcessing", "No buyer batch found.", new Exception("No buyer batch found in: " + batchPath + ". When multiple buyers are defined in RSO, one batch must exists for each existing buyer. The batch name must match the buyer's name."));
                }
            }
            else //Process a company
            {
                string companyFolder = settings.BatchPath;
                WriteLogFile("Checking for document type folders in: " + companyFolder);
                string[] documentTypeFolders = Directory.GetDirectories(companyFolder);
                WriteLogFile("Folders found: " + documentTypeFolders.Length);
                isMultiPage = false;

                if (documentTypeFolders.Length > 0) //multi document type
                {
                    foreach (string documentTypeFolder in documentTypeFolders)
                    {
                        WriteLogFile("Folder: " + documentTypeFolder);
                        isMultiPage = companyFolder.EndsWith("_Multi", StringComparison.InvariantCultureIgnoreCase);
                        DirectoryInfo di = new DirectoryInfo(documentTypeFolder);
                        di = new DirectoryInfo(documentTypeFolder);
                        documentType = documentTypeCollectionList[customer.Id].FirstOrDefault(m => m.SystemName.ToUpper() == di.Name.ToUpper()).SystemName;
                        di = null;
                        //if default document type is provided and document type is not found, use the default.
                        documentType = string.IsNullOrEmpty(documentType) ? defaultDocumentType : documentType;
                        WriteLogFile("DocumentType: " + documentType);
                        UploadFilesToRSO(clientConfiguration, documentTypeFolder, customer, null, documentType, isMultiPage);
                    }
                }
                else //buyer with one document type
                {
                    //In this scenario, we may as well always use the default document type as it will
                    //either be set to something or it will be empty.
                    documentType = defaultDocumentType;
                    isMultiPage = companyFolder.EndsWith("_Multi", StringComparison.InvariantCultureIgnoreCase);
                    UploadFilesToRSO(clientConfiguration, companyFolder, customer, null, documentType, isMultiPage);
                }
            }
        }

        private void UploadFilesToRSO(ClientConfiguration clientConfiguration, string folder, Customer customer, Buyer buyer, string documentType, bool isMultiPage)
        {
            string buyerId = buyer != null ? buyer.Id : customer.Id;
            string buyerName = buyer != null ? buyer.Name : customer.Name;

            FileInfo[] fis = null;

            try
            {
                fis = new DirectoryInfo(folder).GetFilesByExtensions(new string[] { ".pdf", ".tif" }).ToArray();
            }
            catch (IOException ex)
            {
                WriteErrorLogFile("An unhandled IO exception occured trying to get the list of files for the following folder: " + folder, "Failed to access folder", ex);
                return;
            }
            catch (Exception ex)
            {
                WriteErrorLogFile("An unhandled exception occured trying to get the list of files for the following folder: " + folder, "Failed to access folder", ex);
                return;
            }

            if (fis.Length > 0)
            {

                IsDataProcessed = true;
                WriteLogFile("Starting file upload process to RSO for Buyer: " + buyerName + " - Document Type: " + documentType);

                int localTotalUploaded = 0;
                int localTotalUploadedFail = 0;

                foreach (FileInfo fi in fis)
                {
                    //Check for file size and if it's greater than 50mb
                    if (settings.MaxFileSize > 0 && ((fi.Length /1024) > (settings.MaxFileSize * 1000)))
                    {
                        localTotalUploadedFail += 1;

                        if (buyerId.ToString() == customer.Id) //if the buyerid is the same as the customerid, it was set above and it means we're dealing with a customer rather than a buyer.
                        {
                            ScanUploadFailureFileList += "Customer: " + customer.Name + " - Type: " + documentType + " - Filename: " + fi.FullName + Environment.NewLine;
                            WriteErrorLogFile("UploadFileToRSO", "File size over max file size to upload. File: " + fi.FullName + " - Size: " + fi.Length + " - CustomerId: " + buyerId);
                        }
                        else
                        {
                            ScanUploadFailureFileList += "Buyer: " + buyer.Name + "(" + buyer.OrganizationNumber.ToString() + ") - Type: " + documentType + " - Filename: " + fi.FullName + Environment.NewLine;
                            WriteErrorLogFile("UploadFileToRSO", "File size over max file size to upload. File: " + fi.FullName + " to RSO. BuyerId: " + buyerId);
                        }

                        //Move to folder for large files
                        if (!string.IsNullOrEmpty(settings.OverMaxSizeFilePath))
                        {
                            MoveFileToMaxSizeFolder(fi.FullName);
                        }

                        break;
                    }

                    Stream stream = null;
                    try
                    {
                        stream = File.Open(fi.FullName, FileMode.Open, FileAccess.Read, FileShare.Read);
                        var fileClient = new FileServiceClient(clientConfiguration);

                        WriteLogFile("Uploading file: " + fi.FullName + " to RSO.");
                        var result = fileClient.UploadImage2(Path.GetFileName(fi.FullName), customer.Id, "", buyerId, documentType, isMultiPage ? "MultipleDocumentsPerFile" : "OneDocumentPerFile", stream);
                        WriteLogFile("Uploaded file: " + fi.FullName + " to RSO.");

                        stream.Close();

                        fileClient = null;
                        stream = null;

                        if (!Object.ReferenceEquals(result, null) && result.Value)
                        {
                            localTotalUploaded += 1;

                            if (buyerId.ToString() != customer.Id) //if the buyerid is the same as the customerid, it was set above and it means we're dealing with a customer rather than a buyer.
                            {
                                WriteLogFile("File: " + fi.FullName + " uploaded successfully to RSO for Customer: " + customer.Name + " - Buyer: " + buyerName + " - Document Type: " + documentType, true, true);
                            }
                            else
                            {
                                WriteLogFile("File: " + fi.FullName + " uploaded successfully to RSO for Customer: " + customer.Name + " - Document Type: " + documentType, true, true);
                            }

                            if (!string.IsNullOrEmpty(settings.BackupPath))
                            {
                                MoveFileToBackupFolder(fi.FullName);
                            }
                            else
                            {
                                DeleteFile(fi.FullName);
                            }
                        }
                        else
                        {
                            localTotalUploadedFail += 1;
                            if (buyerId.ToString() == customer.Id) //if the buyerid is the same as the customerid, it was set above and it means we're dealing with a customer rather than a buyer.
                            {
                                ScanUploadFailureFileList += "Customer: " + customer.Name + " - Type: " + documentType + " - Filename: " + fi.FullName + Environment.NewLine;
                                WriteErrorLogFile("UploadFileToRSO", "Fail to upload file to RSO.", new Exception("Fail to upload the file to RSO: " + fi.FullName + " - CustomerId: " + buyerId + " - DocumentType: " + documentType + " - IsMultiPagePerDoc: " + (isMultiPage ? "Yes" : "No")));
                            }
                            else
                            {
                                ScanUploadFailureFileList += "Buyer: " + buyer.Name + "(" + buyer.OrganizationNumber.ToString() + ") - Type: " + documentType + " - Filename: " + fi.FullName + Environment.NewLine;
                                WriteErrorLogFile("UploadFileToRSO", "Fail to upload file to RSO.", new Exception("Fail to upload the file to RSO: " + fi.FullName + " - BuyerId: " + buyerId + " - DocumentType: " + documentType + " - IsMultiPagePerDoc: " + (isMultiPage ? "Yes" : "No")));
                            }
                        }
                    }
                    catch (IOException ex)
                    {
                        localTotalUploadedFail += 1;
                        if (buyerId.ToString() == customer.Id) //if the buyerid is the same as the customerid, it was set above and it means we're dealing with a customer rather than a buyer.
                        {
                            ScanUploadFailureFileList += "Customer: " + customer.Name + " - Type: " + documentType + " - Filename: " + fi.FullName + Environment.NewLine;
                            WriteErrorLogFile("UploadFileToRSO", "An unhandled IO exception occured trying to upload the file: " + fi + " - CustomerId: " + buyerId + " - DocumentType: " + documentType + " - IsMultiPagePerDoc: " + (isMultiPage ? "Yes" : "No"),ex);
                        }
                        else
                        {
                            ScanUploadFailureFileList += "Buyer: " + buyer.Name + "(" + buyer.OrganizationNumber.ToString() + ") - Type: " + documentType + " - Filename: " + fi.FullName + Environment.NewLine;
                            WriteErrorLogFile("UploadFileToRSO", "An unhandled IO exception occured trying to upload the file: " + fi + " to RSO. BuyerId: " + buyerId + " - DocumentType: " + documentType + " - IsMultiPagePerDoc: " + (isMultiPage ? "Yes" : "No"), ex);
                        }
                    }
                    catch (Exception ex)
                    {
                        localTotalUploadedFail += 1;
                        if (buyerId.ToString() == customer.Id) //if the buyerid is the same as the customerid, it was set above and it means we're dealing with a customer rather than a buyer.
                        {
                            ScanUploadFailureFileList += "Customer: " + customer.Name + " - Type: " + documentType + " - Filename: " + fi.FullName + Environment.NewLine;
                            WriteErrorLogFile("UploadFileToRSO", "An unhandled IO exception occured trying to upload the file: " + fi + " - CustomerId: " + buyerId + " - DocumentType: " + documentType + " - IsMultiPagePerDoc: " + (isMultiPage ? "Yes" : "No"), ex);
                        }
                        else
                        {
                            ScanUploadFailureFileList += "Buyer: " + buyer.Name + "(" + buyer.OrganizationNumber.ToString() + ") - Type: " + documentType + " - Filename: " + fi.FullName + Environment.NewLine;
                            WriteErrorLogFile("UploadFileToRSO", "An unhandled IO exception occured trying to upload the file: " + fi + " to RSO. BuyerId: " + buyerId + " - DocumentType: " + documentType + " - IsMultiPagePerDoc: " + (isMultiPage ? "Yes" : "No"), ex);
                        }
                    }
                }

                TotalScanUploaded += localTotalUploaded;
                TotalScanUploadedFail += localTotalUploadedFail;
                if (buyerId.ToString() == customer.Id) //if the buyerid is the same as the customerid, it was set above and it means we're dealing with a customer rather than a buyer.
                {
                    TotalScanUploadedList += "Customer: " + customer.Name + " - Type: " + documentType + " - Total Uploaded: " + localTotalUploaded.ToString() + Environment.NewLine;
                    TotalScanUploadedFailList += "Customer: " + customer.Name + " - Type: " + documentType + " - Total Upload Failure: " + localTotalUploadedFail.ToString() + Environment.NewLine;
                }
                else
                {
                    TotalScanUploadedList += "Buyer: " + buyer.Name + "(" + buyer.OrganizationNumber.ToString() + ") - Type: " + documentType + " - Total Uploaded: " + localTotalUploaded.ToString() + Environment.NewLine;
                    TotalScanUploadedFailList += "Buyer: " + buyer.Name + "(" + buyer.OrganizationNumber.ToString() + ") - Type: " + documentType + " - Total Upload Failure: " + localTotalUploadedFail.ToString() + Environment.NewLine;
                }                    

                AddUpdateAdditionalTag("TotalScanUploaded", TotalScanUploaded.ToString());
                AddUpdateAdditionalTag("TotalScanUploadedList", TotalScanUploadedList);

                AddUpdateAdditionalTag("TotalScanUploadedFail", TotalScanUploaded.ToString());
                AddUpdateAdditionalTag("TotalScanUploadedFailList", TotalScanUploadedList);

                AddUpdateAdditionalTag("ScanUploadFailureFileList ", ScanUploadFailureFileList);
                if (buyerId.ToString() == customer.Id)
                {
                    WriteLogFile("File upload process to RSO completed for Customer: " + customer.Name + " - Document Type: " + documentType);
                }
                else
                {
                    WriteLogFile("File upload process to RSO completed for Buyer: " + buyerName + " - Document Type: " + documentType);
                }                    
            }
        }

        private void DeleteFile(string filename)
        {
            try
            {
                WriteLogFile("Deleting successfully uploaded file: " + filename);
                File.Delete(filename);
                WriteLogFile("File Deleted successfully.");
            }
            catch (IOException ex)
            {
                WriteErrorLogFile("DeleteFile", "An unhandled IO exception occured while attempting to delete a file.", ex);
            }
            catch (Exception ex)
            {
                WriteErrorLogFile("DeleteFile", "An unhandled exception occured while attempting to delete a file.", ex);
            }
        }

        private void MoveFileToBackupFolder(string filename)
        {
            //string targetFilename = filename.ReplaceCaseInsensitive(settings.ScanUploadSettings.BatchPath, settings.ScanUploadSettings.BackupPath);
            //targetFilename = targetFilename.TrimStart(new char[] { '\\' });
            string targetFolder = Path.GetDirectoryName(filename).Replace(settings.BatchPath, "");
            targetFolder = Path.Combine(settings.BackupPath, targetFolder.Replace("\\", ""));
            string targetFilename = Path.Combine(targetFolder, Path.GetFileName(filename));

            if (!Directory.Exists(targetFolder))
            {
                try
                {
                    WriteLogFile("About to create backup folder: " + targetFolder);
                    Directory.CreateDirectory(targetFolder);
                    WriteLogFile("Backup folder created successfully.");
                }
                catch (IOException ex)
                {
                    WriteErrorLogFile("UploadFilesToRSO", "An unhandled exception occured while attempting to create the backup folder: " + settings.BackupPath, ex);
                }
                catch (Exception ex)
                {
                    WriteErrorLogFile("UploadFilesToRSO", "An unhandled IO exception occured while attempting to create the backup folder: " + settings.BackupPath, ex);
                }
            }
            try
            {
                WriteLogFile("Moving file: " + filename + " to backup folder: " + Path.GetDirectoryName(targetFilename));

                //Ensure uniqueness of file in backup folder.
                if (File.Exists(targetFilename))
                    targetFilename = Path.Combine(Path.GetDirectoryName(targetFilename),
                        Path.GetFileNameWithoutExtension(targetFilename)) +
                        "_" +
                        DateTime.Now.ToString().Replace(@"/", string.Empty).Replace(":", string.Empty).Replace(" ", string.Empty) + Path.GetExtension(targetFilename);

                File.Move(filename, targetFilename);

                WriteLogFile("File moved successfully.");
            }
            catch (IOException ex)
            {
                WriteErrorLogFile("UploadFilesToRSO", "An unhandled IO exception occured while attempting to move the successfully uploaded file to backup folder.", ex);
            }
            catch (Exception ex)
            {
                WriteErrorLogFile("UploadFilesToRSO", "An unhandled exception occured while attempting to move the successfully uploaded file to backup folder.", ex);
            }
        }

        private void MoveFileToMaxSizeFolder(string filename)
        {
            //string targetFilename = filename.ReplaceCaseInsensitive(settings.ScanUploadSettings.BatchPath, settings.ScanUploadSettings.OverMaxFileSizePath);
            //targetFilename = targetFilename.TrimStart(new char[] { '\\' });
            string targetFolder = Path.GetDirectoryName(filename).Replace(settings.BatchPath, "");
            targetFolder = Path.Combine(settings.OverMaxSizeFilePath, targetFolder.Replace("\\",""));
            string targetFilename = Path.Combine(targetFolder, Path.GetFileName(filename));

            if (!Directory.Exists(targetFolder))
            {
                try
                {
                    WriteLogFile("About to create max size folder: " + targetFolder);
                    Directory.CreateDirectory(targetFolder);
                    WriteLogFile("Max Size folder created successfully.");
                }
                catch (IOException ex)
                {
                    WriteErrorLogFile("UploadFilesToRSO", "An unhandled exception occured while attempting to create the max size folder: " + settings.OverMaxSizeFilePath, ex);
                }
                catch (Exception ex)
                {
                    WriteErrorLogFile("UploadFilesToRSO", "An unhandled IO exception occured while attempting to create the max size folder: " + settings.OverMaxSizeFilePath, ex);
                }
            }
            try
            {
                WriteLogFile("Moving file: " + filename + " to max size folder: " + Path.GetDirectoryName(targetFilename));

                //Ensure uniqueness of file in folder.
                if (File.Exists(targetFilename))
                    targetFilename = Path.Combine(Path.GetDirectoryName(targetFilename),
                        Path.GetFileNameWithoutExtension(targetFilename)) +
                        "_" +
                        DateTime.Now.ToString().Replace(@"/", string.Empty).Replace(":", string.Empty).Replace(" ", string.Empty) + Path.GetExtension(targetFilename);

                File.Move(filename, targetFilename);

                WriteLogFile("File moved successfully.");
            }
            catch (IOException ex)
            {
                WriteErrorLogFile("UploadFilesToRSO", "An unhandled IO exception occured while attempting to move the file to max size folder.", ex);
            }
            catch (Exception ex)
            {
                WriteErrorLogFile("UploadFilesToRSO", "An unhandled exception occured while attempting to move the file to max size folder.", ex);
            }
        }

    }
}
