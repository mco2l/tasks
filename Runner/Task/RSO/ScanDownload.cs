﻿using TaskRunner.Core;
using TaskRunner.Model;
using Microsoft.EntityFrameworkCore;
using ReadSoft.Services.Client;
using ReadSoft.Services.Client.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using TaskRunner.Data;

namespace TaskRunner.Task.RSO
{
    public class ScanDownload : TaskBase, ITask
    {
        private int TotalScanDownloaded = 0;
        private string TotalScanDownloadedList = string.Empty;

        private int TotalScanDownloadedFail = 0;
        private string TotalScanDownloadedFailList = string.Empty;
        private SettingsRsoScanDownload settings = null;

        #region ITask Members

        bool ITask.IsDataProcessed
        {
            get { return IsDataProcessed; }
        }

        bool ITask.IsProcessing
        {
            get { return IsProcessing; }
        }

        bool ITask.IsSuccess
        {
            get { return IsSuccess; }
        }

        string ITask.LogErrorFilename
        {
            get { return LogErrorFilename; }
        }

        string ITask.LogFilename
        {
            get { return LogFilename; }
        }

        #endregion

        public void SetTaskSettings(string tenantDB, int taskID)
        {
            //Initialize values used by Additional ags
            TotalScanDownloaded = 0;
            TotalScanDownloadedList = string.Empty;
            TotalScanDownloadedFail = 0;
            TotalScanDownloadedFailList = string.Empty;

            //Initialize AdditionalTags
            AddUpdateAdditionalTag("TotalScanDownloaded", TotalScanDownloaded.ToString());
            AddUpdateAdditionalTag("TotalScanDownloadedList", TotalScanDownloadedList);
            AddUpdateAdditionalTag("TotalScanDownloadedFail", TotalScanDownloadedFail.ToString());
            AddUpdateAdditionalTag("TotalScanDownloadedFailList", TotalScanDownloadedFailList);

            IsSuccess = false;
            IsDataProcessed = false;

            Initialize(tenantDB, taskID);

            using (var db = new TenantDBContext(tenantDB))
            {
                settings = db.SettingsRsoScanDownload
                    .AsNoTracking()
                    .Include(rs => rs.Rsosetting)
                    .Include(fm => fm.FieldMapping)                   
                    .ThenInclude(a => a.SettingsSharedFieldMappingFields)
                    .First(a => a.TaskId == taskID);
            }
        }

        public void RunTask(string tenantDB, int taskID)
        {
            SetTaskSettings(tenantDB, taskID);

            ClientConfiguration clientConfiguration = null;
            Customer customer = null;

            bool isLoggedIn = false;            

            try
            {
                
                IsProcessing = true;

                //TODO: Replace decryption with V10 component

                clientConfiguration = RsoAuthenticationService.Login(settings.Rsosetting.Uri, settings.Rsosetting.ApiKey, settings.Rsosetting.UserName, EncryptionHelper.AdDecrypt(settings.Rsosetting.Password));

                if (clientConfiguration != null)
                {
                    isLoggedIn = true;

                    WriteLogFile("Requesting RSO Customer Info.");
                    customer = RsoAccountService.GetCurrentCustomer(clientConfiguration);
                    WriteLogFile("RSO Customer Info returned successfully: " + customer.Name);

                    OutputSystem outputSystem = GetOutputSystem(clientConfiguration, customer);
                    if (outputSystem.Name.ToLowerInvariant() != "api")
                    {
                        throw new Exception("Unsupported RSO Target System has been set in RSO. API is the only Target System supported at this time.");
                    }

                    //Looping through the buyers and using the buyer's id does not work as it always returns 0 when calling GetNumberOfOutputDocuments.
                    //Must download to a temp location and create folder based on information contained in metadata.
                    DownloadInvoices(settings, clientConfiguration, customer, outputSystem.SupportedImageTypes);

                    if (TotalScanDownloadedFail == 0) IsSuccess = true;
                }
                else
                {
                    WriteErrorLogFile("RunTask", "Login Error", new Exception("Failed to login to RSO. Please check APIKey, Username and Password."));
                }
            }
            catch (Exception ex)
            {
                //Set this to true to ensure that whether data was processed 
                //or not, it will log logging off from RSO, etc...
                IsDataProcessed = true;
                WriteErrorLogFile("An unhandled exception occured.", "RunTask", ex);

                //if (!string.IsNullOrEmpty(CurrentFilename))
                //{
                //    WriteLogFile("Moving erroneous file to error folder.");
                //    TaskHelpers.MoveFileToErrorPath(settings.MasterUploadSettings.DataFileSettings.ErrorPath, CurrentFilename, CurrentHashkey);
                //    WriteLogFile("Erroneous file moved to error folder.");
                //}
                throw ex; //Throw the error so that it notifies the task scheduler of a major error and it will disable the relevant job after x failures i.e. 3.
            }
            finally
            {
                if (isLoggedIn && clientConfiguration != null)
                {
                    try
                    {
                        if (IsDataProcessed) WriteLogFile("Logging off from RSO.");
                        RsoAuthenticationService.Logoff(clientConfiguration);
                        if (IsDataProcessed) WriteLogFile("Logged off successfully from RSO.");
                        if (IsDataProcessed) WriteLogFile(string.Empty);
                    }
                    catch (Exception ex)
                    {
                        WriteErrorLogFile("RunTask", "An unhandled exception occurred while attempting to sign off from RSO.", ex);
                    }
                }
                settings = null;
                clientConfiguration = null;
                IsProcessing = false;
            }
        }

        private void DownloadInvoices(SettingsRsoScanDownload scanDownloadSettings, ClientConfiguration clientConfiguration, Customer customer, int supportedImageTypes)
        {
            int numberOfOutputDocuments = RsoDocumentService.GetNumberOfOutputDocuments(clientConfiguration, customer.Id);

            if (numberOfOutputDocuments > 0)
            {
                BuyerCollection buyerCollection = null;

                if (scanDownloadSettings.CreateBuyerFolder)
                {
                    WriteLogFile("CreateBuyerFolder: True");
                    WriteLogFile("Requesting all buyers.");

                    buyerCollection = RsoAccountService.GetAllBuyersByCustomer(clientConfiguration, customer.Id);

                    if (buyerCollection == null)
                        throw new ArgumentNullException("BuyerCollection", "Cannot continue - RSO buyers for current customer cannot be null.");

                    if (buyerCollection.Count == 0)
                        throw new ArgumentOutOfRangeException("BuyerCollection", "Cannot continue - No buyers found for current customer.");

                    WriteLogFile("Total buyers downloaded: " + buyerCollection.Count.ToString());
                }

                WriteLogFile("Invoices/Credit Notes download process from RSO - Started");
                WriteLogFile("Total output documents available: " + numberOfOutputDocuments.ToString());

                foreach (var documentReference in RsoDocumentService.GetOutputDocuments(clientConfiguration, customer.Id))
                {
                    string buyerName = string.Empty; //only used when CreateBuyerFolder is true.
                    string rsoDocumentId = documentReference.DocumentId;

                    if (scanDownloadSettings.CreateBuyerFolder)
                    {
                        buyerName = buyerCollection.FirstOrDefault(m => m.Id == documentReference.BuyerId).Name;
                    }

                    WriteLogFile("Downloading document reference with RSO DocumentId: " + documentReference.DocumentId);
                    Document document = null;

                    //Using second level try catch will allow us to skip one "corrupt" operation and move on to the next.
                    try
                    {
                        document = RsoDocumentService.GetDocument(clientConfiguration, documentReference.DocumentId);
                        string rsoTrackId = document.TrackId;

                        WriteLogFile("Document reference with RSO DocumentId: " + documentReference.DocumentId + " downloaded successfully.");

                        string extension = GetExtension(supportedImageTypes);
                        string documentImageFilename = Path.GetFileNameWithoutExtension(document.Filename) + extension;
                        documentImageFilename = Path.Combine(scanDownloadSettings.DownloadPath, buyerName, "Images", documentImageFilename);

                        if (!CreateFolder(Path.GetDirectoryName(documentImageFilename)))
                        {
                            //If it failed to create any image folder or sub-folder, do not continue!
                            return;
                        }

                        DocumentFormat documentFormat = DocumentFormat.Default;

                        switch (extension.ToUpperInvariant())
                        {
                            case ".TIF":
                            case ".TIFF":
                                documentFormat = DocumentFormat.Tif;
                                break;
                            case ".PDF":
                                documentFormat = DocumentFormat.Pdf;
                                break;
                        }

                        bool docDownloaded = false;
                        if (scanDownloadSettings.UsePermaLink)
                        {
                            WriteLogFile("Document/image file with RSO DocumentId: " + documentReference.DocumentId + " UsePermaLink set to true");

                            documentImageFilename = documentImageFilename.Replace(extension, ".html");

                            String content = GeneratePermaLinkHtml(document.Permalink);
                            System.IO.File.WriteAllText(documentImageFilename, content);

                            WriteLogFile("Document/image file with RSO DocumentId: " + documentReference.DocumentId + " html with permalink created successfully. PermaLink: " + document.Permalink);
                            docDownloaded = true;
                        }
                        else
                        {
                            WriteLogFile("Downloading document/image file with RSO DocumentId: " + documentReference.DocumentId);
                            if (RsoDocumentService.GetDocumentFile(clientConfiguration, documentReference.DocumentId, documentFormat, documentImageFilename))
                            {
                                docDownloaded = true;
                                WriteLogFile("Document/image file with RSO DocumentId: " + documentReference.DocumentId + " downloaded successfully. Filename: " + documentImageFilename);
                            }
                        }

                        if (docDownloaded)
                        {
                            string csvOutput = BuildCSVData(document, documentImageFilename, documentReference);

                            if (!string.IsNullOrEmpty(csvOutput))
                            {
                                //BuyerName is passed in case we need to create a subFolder based on buyer's name when CreateBuyerFolder is true.
                                if (SaveCSVDataToFile(csvOutput, buyerName))
                                {
                                    WriteLogFile("About to update the document downloaded status in RSO.");
                                    RsoDocumentService.DocumentDownloaded(clientConfiguration, documentReference.DocumentId);
                                    WriteLogFile("Document downloaded status updated successfully in RSO.");

                                    //document type, invoice/credit note number
                                    TotalScanDownloaded += 1;
                                    TotalScanDownloadedList += "RSO trackId: " + rsoTrackId + Environment.NewLine;
                                }
                                else
                                {
                                    WriteLogFile("Failed to save csv data: " + csvOutput + " to file.");
                                    TotalScanDownloadedFail += 1;
                                    TotalScanDownloadedList += "RSO trackId: " + rsoTrackId + Environment.NewLine;
                                }
                            }
                            else
                            {
                                WriteLogFile("Failed to download or output document/image file with RSO DocumentId: " + documentReference.DocumentId);
                                TotalScanDownloadedFail += 1;
                                TotalScanDownloadedList += "RSO trackId: " + rsoTrackId + Environment.NewLine;
                            }
                        }
                        else
                        {
                            WriteLogFile("No csv output was generated from the provided RSO data.");
                            TotalScanDownloadedFail += 1;
                            TotalScanDownloadedList += "RSO trackId: " + rsoTrackId + Environment.NewLine;
                        }
                    }
                    catch (Exception ex)
                    {
                        TotalScanDownloadedFail += 1;
                        WriteErrorLogFile("RunJob", "An unhandled exception occured while attempting to download/save/process the document reference or document image from the RSO service.", ex);
                        TotalScanDownloadedList += "RSO DocumentId: " + rsoDocumentId + Environment.NewLine;
                    }
                    finally
                    {
                        document = null;
                    }
                }

                AddUpdateAdditionalTag("TotalScanDownloaded", TotalScanDownloaded.ToString());
                AddUpdateAdditionalTag("TotalScanDownloadedList", TotalScanDownloadedList);

                AddUpdateAdditionalTag("TotalScanDownloadedFail", TotalScanDownloadedFail.ToString());
                AddUpdateAdditionalTag("TotalScanDownloadedFailList", TotalScanDownloadedFailList);

                WriteLogFile("Invoices/Credit Notes download process from RSO - Finished");
            }
        }

        private String GeneratePermaLinkHtml(string permaLink)
        {
            string content = string.Empty;
            try
            {
                StringBuilder HTMLcontent = new StringBuilder();
                HTMLcontent.Append("<!DOCTYPE HTML>");
                HTMLcontent.Append(@"<html lang=""en-US"">");
                HTMLcontent.Append("<head>");
                HTMLcontent.Append(@"<meta charset=""UTF-8"">");
                HTMLcontent.Append(@"<meta http-equiv=""refresh"" content=""1;url=");
                HTMLcontent.Append(permaLink);
                HTMLcontent.Append('"');
                HTMLcontent.Append('>');
                HTMLcontent.Append('<');
                HTMLcontent.Append("script type=");
                HTMLcontent.Append('"');
                HTMLcontent.Append(@"text/javascript");
                HTMLcontent.Append('>');
                HTMLcontent.Append(@"window.location.href = ");
                HTMLcontent.Append('"');
                HTMLcontent.Append(permaLink);
                HTMLcontent.Append('"');
                HTMLcontent.Append("</script>");
                HTMLcontent.Append("<title>Page Redirection</title>");
                HTMLcontent.Append("</head>");
                HTMLcontent.Append("<body>");
                HTMLcontent.Append("If you are not redirected automatically, follow the");
                HTMLcontent.Append('<');
                HTMLcontent.Append("a href='");
                HTMLcontent.Append(permaLink);
                HTMLcontent.Append("'>link to the image</a>");
                HTMLcontent.Append("</body>");
                HTMLcontent.Append("</html>");
                content = HTMLcontent.ToString();
            }
            catch (Exception ex)
            {
                WriteErrorLogFile("GeneratePermaLinkHtml", "GeneratePermaLinkHtml", ex);
            }

            return content;
        }

        private OutputSystem GetOutputSystem(ClientConfiguration clientConfiguration, Customer customer)
        {
            WriteLogFile("Requesting RSO Output Configuration Info.");
            OutputConfiguration3 outputConfiguration3 = RsoAccountService.GetAccountOutputConfiguration3(clientConfiguration, customer.Id);
            WriteLogFile("RSO Output Configuration Info returned successfully.");

            WriteLogFile("Checking if a Target System system is selected.");
            OutputSystem outputSystem = outputConfiguration3.OutputSystems.FirstOrDefault(m => m.IsSelected);

            if (object.ReferenceEquals(outputSystem, null))
                throw new Exception("The Target System in RSO has not been selected. Please make sure to select the API as the Target System and set the file type to the relevant file format i.e. PDF or Tif.");
            else
            {
                WriteLogFile("Selected Output System: " + outputSystem.Name);
                return outputSystem;
            }
        }

        private void ValidateSettings()
        {
            string exceptionMessage = string.Empty;
            
            if (string.IsNullOrEmpty(settings.DownloadPath))
            {
                exceptionMessage += ("The Download Path cannot be empty." + Environment.NewLine);
            }

            string dataPath = settings.DownloadPath;
            if (!Directory.Exists(dataPath))
            {
                try
                {
                    Directory.CreateDirectory(dataPath);
                }
                catch (Exception ex)
                {
                    exceptionMessage += ("Failed to create the Download path: " + dataPath + " : " + ex.Message + Environment.NewLine);
                }
            }

            if (!string.IsNullOrEmpty(exceptionMessage))
                throw new Exception(exceptionMessage);
        }

        private string GetExtension(int supportedImageTypes)
        {
            switch (supportedImageTypes)
            {
                case 1:
                    return ".tif";
                case 2:
                    return ".pdf";
                default:
                    throw new Exception("Unsupported SupportedImageTypes was provided: " + supportedImageTypes.ToString() + " - The only supported image formats are .tif(1) and .pdf(2).");
            }
        }

        private bool SaveCSVDataToFile(string csvOutput, string buyerName)
        {
            string dataFilename = System.Guid.NewGuid().ToString() + ".txt";
            dataFilename = Path.Combine(settings.DownloadPath, buyerName, "Data", dataFilename);

            if (!CreateFolder(Path.GetDirectoryName(dataFilename)))
            {
                //If it failed to create any data folder or sub-folder, do not continue!
                return false;
            }

            try
            {
                using (FileStream fs = new FileStream(dataFilename, FileMode.Create))
                {
                    UTF8Encoding utf8Encoding = new UTF8Encoding();
                    byte[] arr = utf8Encoding.GetBytes(csvOutput);
                    fs.Write(arr, 0, arr.Length);
                    arr = null;
                    fs.Flush();
                }
                return true;
            }
            catch (IOException ex)
            {
                WriteErrorLogFile("SaveCSVDataToFile", "An unhandled IO exception occured while attempting to save the csv data to file. csv data: " + csvOutput + " - csv filename: " + dataFilename, ex);
                return false;
            }
            catch (Exception ex)
            {
                WriteErrorLogFile("SaveCSVDataToFile", "An unhandled exception occured while attempting to save the csv data to file. csv data: " + csvOutput + " - csv filename: " + dataFilename, ex);
                return false;
            }
        }

        private string BuildCSVData(Document document, string documentImageFilename, DocumentReference documentReference,  string extractHistory = null)
        {
            string csvHeaders = BuildReadsoftHeaderCSVString(document, documentImageFilename);
            string csvLineItems = BuildReadsoftLineItemCSVString(document);
            string csvTaxTableItems = BuildReadsoftTaxTableItemsCSVString(document);
            string csvCodingLineItems = BuildReadsoftCodingLineItemCSVString(document);
            string csvHistoryItems = BuildReadSoftHistoryCSVString(document, documentReference, extractHistory);

            string csvOutput = string.Empty;

            if (!string.IsNullOrEmpty(csvHeaders))
            {
                csvOutput = csvHeaders +
                           (!string.IsNullOrEmpty(csvLineItems) ? csvLineItems : string.Empty) +
                           (!string.IsNullOrEmpty(csvTaxTableItems) ? csvTaxTableItems : string.Empty) +
                           (!string.IsNullOrEmpty(csvCodingLineItems) ? csvCodingLineItems : string.Empty) +
                           (!string.IsNullOrEmpty(csvHistoryItems) ? csvHistoryItems : string.Empty);
            }

            return csvOutput;
        }

        private string BuildReadSoftHistoryCSVString(Document document, DocumentReference documentReference, string extractHistory)
        {
            string csvOutputString = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(extractHistory))
                {
                    if(extractHistory.ToLower() == "true")
                    {
                        List<Dictionary<string, string>> historyItems = new List<Dictionary<string, string>>();

                        historyItems = GetHistoryItems(document, documentReference);

                        if (historyItems.Any())
                        {

                            foreach (Dictionary<string, string> histItem in historyItems)
                            {
                                string csvHistory = "<HISTORYITEM>";

                                foreach (KeyValuePair<string, string> histItemValue in histItem)
                                {
                                    csvHistory += "<HISTORYITEMVALUE>" + histItemValue.Value + "</HISTORYITEMVALUE>";
                                }

                                csvHistory += "</HISTORYITEM>";
                                csvOutputString += csvHistory;
                            }

                            csvOutputString = "<HISTORYITEMS>" + csvOutputString + "</HISTORYITEMS>";
                            csvOutputString += "|";
                        }
                    }
                   
                }

                return csvOutputString;
            }
            catch (Exception ex)
            {
                WriteErrorLogFile("BuildReadSoftHistoryCSVString", "An unhandled IO exception occurred while attempting to build readsoft history item csv string.", ex);
                csvOutputString = string.Empty;
                throw ex;
            }
        }

        private string BuildReadsoftHeaderCSVString(Document document, string fileName)
        {
            string csvOutputString = string.Empty;

            try
            {
                Dictionary<string, string> headerValues = new Dictionary<string, string>();

                headerValues.Add("Image", fileName);
                headerValues = GetHeaderValues(document, headerValues);

                SortedList<int, string> csvOutputFieldSortedIndexList = new SortedList<int, string>();

                csvOutputFieldSortedIndexList.Add(settings.ImageLocationIndex, "Image");

                foreach (var mappingField in settings.FieldMapping.SettingsSharedFieldMappingFields)
                {
                    if (!csvOutputFieldSortedIndexList.ContainsKey(System.Convert.ToInt16(mappingField.ToField)))
                    {
                        csvOutputFieldSortedIndexList.Add(System.Convert.ToInt16(mappingField.ToField), mappingField.FromField);
                    }
                }

                //foreach (var mappingField in settings.FieldMapping)
                //{
                //    //if (!csvOutputFieldSortedIndexList.ContainsKey(System.Convert.ToInt16(mappingField.ToField)))
                //    //{
                //    //    csvOutputFieldSortedIndexList.Add(System.Convert.ToInt16(mappingField.ToField), mappingField.FromField);
                //    //}
                //}

                foreach (int fieldIndex in csvOutputFieldSortedIndexList.Keys)
                {
                    string propertyName = csvOutputFieldSortedIndexList[fieldIndex];

                    if (headerValues.ContainsKey(propertyName))
                        csvOutputString += (headerValues[csvOutputFieldSortedIndexList[fieldIndex]] + "|");
                }

                csvOutputFieldSortedIndexList = null;
            }
            catch (Exception ex)
            {
                WriteErrorLogFile("BuildReadsoftHeaderCSVString", "An unhandled IO exception occurred while attempting to build readsoft header csv string.", ex);
                throw ex;
            }

            return csvOutputString;
        }

        private string BuildReadsoftLineItemCSVString(Document document)
        {
            string csvOutputString = string.Empty;

            try
            {
                List<Dictionary<string, string>> lineItems = new List<Dictionary<string, string>>();

                lineItems = GetLineItems(document);

                if (lineItems.Count > 0)
                {
                    foreach (Dictionary<string, string> lineItem in lineItems)
                    {
                        string csvLineItem = "<LINEITEM>";
                        foreach (KeyValuePair<string, string> lineItemValue in lineItem)
                        {
                            csvLineItem += ("<LINEITEMFIELD>" + lineItemValue.Value + "</LINEITEMFIELD>");
                        }
                        csvLineItem += "</LINEITEM>";
                        csvOutputString += csvLineItem;
                    }
                }

                csvOutputString = "<LINEITEMS>" + csvOutputString + "</LINEITEMS>";
                csvOutputString += "|";

                return csvOutputString;
            }
            catch (Exception ex)
            {
                WriteErrorLogFile("BuildReadsoftLineItemCSVString", "An unhandled IO exception occurred while attempting to build readsoft line item csv string.", ex);
                csvOutputString = string.Empty;
                throw ex;
            }
        }

        private string BuildReadsoftTaxTableItemsCSVString(Document document)
        {
            string csvOutputString = string.Empty;

            csvOutputString = "<TaxTable>" + csvOutputString + "</TaxTable>";
            csvOutputString += "|";

            return csvOutputString;
        }

        private string BuildReadsoftCodingLineItemCSVString(Document document)
        {
            string csvOutputString = string.Empty;

            try
            {
                List<Dictionary<string, string>> codingLineItems = new List<Dictionary<string, string>>();

                codingLineItems = GetCodingLineItems(document);

                if (codingLineItems.Count > 0)
                {
                    foreach (Dictionary<string, string> lineItem in codingLineItems)
                    {
                        string csvLineItem = "<CODINGFIELD>";
                        foreach (KeyValuePair<string, string> lineItemValue in lineItem)
                        {
                            //csvLineItem += ("<CODINGLINEITEMFIELD>" + lineItemValue.Value.Replace("&", "&amp;") + "</CODINGLINEITEMFIELD>");
                            string lineValue = lineItemValue.Value;

                            csvLineItem += ("<CODINGLINEITEMFIELD>" + HttpUtility.HtmlEncode(lineValue) + "</CODINGLINEITEMFIELD>");
                        }
                        csvLineItem += "</CODINGFIELD>";
                        csvOutputString += csvLineItem;
                    }
                }

                csvOutputString = "<CODINGFIELDS>" + csvOutputString + "</CODINGFIELDS>";
                csvOutputString += "|";

                //string encodedXml = HttpUtility.HtmlEncode(csvOutputString);

                return csvOutputString;
            }
            catch (Exception ex)
            {
                WriteErrorLogFile("BuildReadsoftLineItemCSVString", "An unhandled IO exception occurred while attempting to build readsoft line item csv string.", ex);
                csvOutputString = string.Empty;
                throw ex;
            }
        }

        /// <summary>
        /// Gets header values from RSO document & Parties (i.e. supplier and buyer)
        /// </summary>
        /// <param name="document"></param>
        /// <param name="headerValues"></param>
        /// <returns></returns>
        private Dictionary<string, string> GetHeaderValues(Document document, Dictionary<string, string> headerValues)
        {
            try
            {
                if (document.Parties.Count > 0)
                {
                    headerValues.Add("BuyerCode", document.Parties[0].ExternalId);
                    headerValues.Add("BuyerName", document.Parties[0].Name);
                    WriteLogFile("GetHeaderValues: BuyerCode(ExternalId): " + document.Parties[0].ExternalId + ", BuyerName: " + document.Parties[0].Name);
                }

                if (document.Parties.Count > 1)
                {
                    headerValues.Add("SupplierCode", document.Parties[1].ExternalId);
                    headerValues.Add("SupplierName", document.Parties[1].Name);
                    WriteLogFile("GetHeaderValues: Supplier Code(ExternalId): " + document.Parties[1].ExternalId + ", SupplierName: " + document.Parties[1].Name);
                }

                foreach (HeaderField headerField in document.HeaderFields)
                {
                    if (headerValues.ContainsKey(headerField.Name))
                        headerValues[headerField.Name] = headerField.Text;
                    else
                        headerValues.Add(headerField.Name, headerField.Text);

                    if (headerField.Name == "CreditInvoice")
                    {
                        headerValues[headerField.Name] = (Convert.ToBoolean(headerField.Text) ? "Credit Note" : "Invoice");
                    }
                }
            }
            catch (Exception ex)
            {
                WriteErrorLogFile("GetHeaderValues", "An unhandled exception occurred while attempting to get the header values.", ex);
                TotalScanDownloadedFail += 1;
                throw ex;
            }

            return headerValues;
        }

        /// <summary>
        /// Gets line item values from RSO document
        /// </summary>
        /// <param name="document"></param>
        /// <returns></returns>
        private List<Dictionary<string, string>> GetLineItems(Document document)
        {
            List<Dictionary<string, string>> lineItems = new List<Dictionary<string, string>>();

            try
            {
                if (document.Tables.Count > 0)
                {
                    foreach (TableRow tableRow in document.Tables[0].TableRows)
                    {
                        Dictionary<string, string> lineItemValues = new Dictionary<string, string>();
                        foreach (ItemField itemField in tableRow.ItemFields)
                        {
                            if (lineItemValues.ContainsKey(itemField.Name))
                                lineItemValues[itemField.Name] = itemField.Text;
                            else
                                lineItemValues.Add(itemField.Name, itemField.Text);
                        }
                        lineItems.Add(lineItemValues);
                    }
                }
            }
            catch (Exception ex)
            {
                WriteErrorLogFile("GetLineItems", "An unhandled exception occurred while attempting to get the line items values.", ex);
                TotalScanDownloadedFail += 1;
                throw ex;
            }

            return lineItems;
        }

        private List<Dictionary<string, string>> GetCodingLineItems(Document document)
        {
            List<Dictionary<string, string>> codingLineItems = new List<Dictionary<string, string>>();

            try
            {
                if (document.AccountingInformation.CodingLines.Count > 0)
                {
                    foreach (CodingLine codingLine in document.AccountingInformation.CodingLines)
                    {
                        Dictionary<string, string> lineItemValues = new Dictionary<string, string>();

                        //Hardcode some of the fixed RSO Fields.
                        lineItemValues.Add("AccountCode", codingLine.AccountCode);
                        lineItemValues.Add("AccountDetail", codingLine.AccountDetail);
                        lineItemValues.Add("AccountType", object.ReferenceEquals(codingLine.AccountType, null) ? string.Empty : codingLine.AccountType.ToString()); //always null, need to find out why from Readsoft.
                        lineItemValues.Add("Amount", object.ReferenceEquals(codingLine.Amount, null) ? string.Empty : codingLine.Amount.ToString());

                        foreach (CodingField itemField in codingLine.CodingFields)
                        {
                            if (lineItemValues.ContainsKey(itemField.Type))
                                lineItemValues[itemField.Type] = itemField.Text;
                            else
                                lineItemValues.Add(itemField.Type, itemField.Text);
                        }
                        codingLineItems.Add(lineItemValues);
                    }
                }
            }
            catch (Exception ex)
            {
                WriteErrorLogFile("GetLineItems", "An unhandled exception occurred while attempting to get the line items values.", ex);
                TotalScanDownloadedFail += 1;
                throw ex;
            }

            return codingLineItems;
        }

        /// <summary>
        /// Gets history item values ordered by timestamp from RSO document
        /// </summary>
        /// <param name="document"></param>
        /// <returns></returns>
        private List<Dictionary<string, string>> GetHistoryItems(Document document, DocumentReference documentReference)
        {
            List<Dictionary<string, string>> historyItems = new List<Dictionary<string, string>>();

            try
            {
                if (document.History.Any())
                {
                    var historyItemsOrderedByTimestamp = document.History.OrderBy(o => o.Timestamp);

                    foreach (var historyItem in historyItemsOrderedByTimestamp)
                    {
                        Dictionary<string, string> historyItemValues = new Dictionary<string, string>();
                        historyItemValues.Add("Id", document.Id.ToString());
                        historyItemValues.Add("DocMasterID", documentReference.DocumentId);
                        historyItemValues.Add("TrackId", document.TrackId);
                        historyItemValues.Add("Filename", document.Filename);
                        historyItemValues.Add("Timestamp", historyItem.Timestamp.ToString());
                        historyItemValues.Add("Status", historyItem.Status.ToString());
                        historyItemValues.Add("StatusAsInt", historyItem.StatusAsInt.ToString());                    
                        historyItemValues.Add("UserFullName", historyItem.UserFullName.ToString());

                        historyItems.Add(historyItemValues);
                    }
                }
            }
            catch (Exception ex)
            {
                WriteErrorLogFile("GetHistoryItems", "An unhandled exception occurred while attempting to get the history item values.", ex);
                TotalScanDownloadedFail += 1;
                throw ex;
            }

            return historyItems;
        }

    }
}
