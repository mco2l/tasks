﻿using ReadSoft.Services.Client;
using ReadSoft.Services.Client.Entities;
using System;

namespace TaskRunner.Task.RSO
{
    public sealed class RsoAccountService
    {
        public static Customer GetCurrentCustomer(ClientConfiguration clientConfiguration)
        {
            try
            {
                AccountServiceClient asc = new AccountServiceClient(clientConfiguration);
                return asc.GetCurrentCustomer();
            }
            catch (Exception ex)
            {
                throw new Exception("An unhandled exception occured while attempting to get the RSO Customer details.", ex);
            }
        }

        public static BuyerCollection GetAllBuyersByCustomer(ClientConfiguration clientConfiguration, string customerId)
        {
            try
            {
                AccountServiceClient asc = new AccountServiceClient(clientConfiguration);
                BuyerCollection buyerCollection = asc.GetAllBuyersByCustomer(customerId);
                return buyerCollection;
            }
            catch (Exception ex)
            {
                throw new Exception("An unhandled exception occured while attempting to get the all the RSO buyers for customerId: " + customerId, ex);
            }
        }

        public static DocumentTypeCollection GetCustomerDocumentTypes(ClientConfiguration clientConfiguration, string customerId)
        {
            try
            {
                AccountServiceClient asc = new AccountServiceClient(clientConfiguration);
                ExtractionConfiguration ec = asc.GetAccountExtractionConfiguration(customerId);
                DocumentTypeCollection dtc = ec.SelectedDocumentTypes;
                ec = null;
                asc = null;
                return dtc;
            }
            catch (Exception ex)
            {
                throw new Exception("An unhandled exception occured while attempting to get the available document types for customerId: " + customerId, ex);
            }
        }

        public static DocumentTypeCollection GetBuyerDocumentTypes(ClientConfiguration clientConfiguration, string buyerId)
        {
            try
            {
                AccountServiceClient asc = new AccountServiceClient(clientConfiguration);
                ExtractionConfiguration ec = asc.GetBuyerExtractionConfiguration(buyerId);
                DocumentTypeCollection dtc = ec.SelectedDocumentTypes;
                ec = null;
                asc = null;
                return dtc;
            }
            catch (Exception ex)
            {
                throw new Exception("An unhandled exception occured while attempting to get the available document types for buyerId: " + buyerId, ex);
            }
        }

        public static OutputConfiguration3 GetAccountOutputConfiguration3(ClientConfiguration clientConfiguration, string id)
        {
            try
            {
                AccountServiceClient asc = new AccountServiceClient(clientConfiguration);
                OutputConfiguration3 outputConfiguration3 = asc.GetAccountOutputConfiguration3(id);
                return outputConfiguration3;
            }
            catch (Exception ex)
            {
                throw new Exception("An unhandled exception occured while attempting to get the RSO Output Configuration details.", ex);
            }
        }

    }
}
