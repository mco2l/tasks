﻿using ReadSoft.Services.Client;
using ReadSoft.Services.Client.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace TaskRunner.Task.RSO
{
    public sealed class RsoAuthenticationService
    {
        public static ClientConfiguration Login(string serviceUri, string apiKey, string username, string password)
        {
            try
            {
                // Login details obtained from ReadSoft.
                Guid applicationKey = Guid.Parse(apiKey);

                // The host name of the ReadSoft Online service.
                Uri serviceAddress = new Uri(serviceUri);

                // When we authenticate ourselves towards ReadSoft Online, ReadSoft Online will give us a
                // login token which we include in further calls to the ReadSoft Online API. This token comes in
                // a cookie, so we use the cookie container to store it.
                var cookieContainer = new CookieContainer();

                // The client configuration contains information neccessary to connect to ReadSoft Online, such as
                // the application ID and the host address of the API to connect to.            
                ClientConfiguration clientConfiguration = ClientConfiguration.Create(applicationKey, serviceAddress, cookieContainer);

                // Authenticate ourselves towards ReadSoft Online (log in).
                var authenticationServiceClient = new AuthenticationServiceClient(clientConfiguration);
                var authenticationResult = authenticationServiceClient.Authenticate(new AuthenticationCredentials
                {
                    AuthenticationType = AuthenticationType.SetCookie,
                    UserName = username,
                    Password = password
                });

                if (authenticationResult.Status != AuthenticationStatus.Success)
                {
                    // Authentication has failed. This may be due to incorrect login details.
                    return null;
                }

                return clientConfiguration;
            }
            catch (Exception ex)
            {
                throw new Exception("An unhandled exception occured while attempting to login " + username + " to the RSO service.", ex);
            }
        }

        public static AuthenticationResult Logoff(ClientConfiguration clientConfiguration)
        {
            try
            {
                var authenticationServiceClient = new AuthenticationServiceClient(clientConfiguration);
                return authenticationServiceClient.SignOut();
            }
            catch (Exception ex)
            {
                throw new Exception("An unhandled exception occured while attempting to sign out the currently logged in user from the RSO service.", ex);
            }
        }
    }
}
