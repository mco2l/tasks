﻿using System;
using TaskRunner.Task.RSO.Enums;

namespace TaskRunner.Task.RSO
{
    public sealed class RsoMasterDataMapping
    {
        public RsoMasterDataMapping()
        {
            CsvFieldIndex = -1;
        }

        public int CsvFieldIndex { get; set; }

        public string CsvFieldIndexString
        {
            get { return CsvFieldIndex.ToString(); }
            set { CsvFieldIndex = string.IsNullOrEmpty(value) ? -1 : Convert.ToInt32(value); }
        }

        public RsoMasterDataPropertyNameEnum RsoPropertyName { get; set; }

        public string RsoPropertyNameString
        {
            get { return RsoPropertyName.ToString(); }
            set { RsoPropertyName = (RsoMasterDataPropertyNameEnum)Enum.Parse(typeof(RsoMasterDataPropertyNameEnum), value); }
        }

        public string LineSeparator { get; set; }

        public string DefaultValue { get; set; }

        public string ReplaceByDefault { get; set; }
    }
}
