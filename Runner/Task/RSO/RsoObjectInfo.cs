﻿using TaskRunner.Core;
using ReadSoft.Services.Client.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaskRunner.Core.Enums;

namespace TaskRunner.Task.RSO
{
    public class RsoObjectInfo
    {
        public string ObjectName { get; set; }
        public string CollectionName { get; set; }
        public Type ObjectType { get; set; }
        public Type CollectionType { get; set; }

        public RsoObjectInfo(MasterDataTypeEnum masterDataTypeEnum)
        {            
            switch (masterDataTypeEnum)
            {
                case MasterDataTypeEnum.NominalCode:
                    ObjectName = "GeneralLedgerAccount";
                    CollectionName = "GeneralLedgerAccountCollection";
                    ObjectType = typeof(GeneralLedgerAccount);
                    CollectionType = typeof(GeneralLedgerAccountCollection);
                    break;
                case MasterDataTypeEnum.Supplier:
                    ObjectName = "Supplier";
                    CollectionName = "SupplierCollection";
                    ObjectType = typeof(Supplier);
                    CollectionType = typeof(SupplierCollection);
                    break;
                case MasterDataTypeEnum.Currency:
                    ObjectName = "Currency";
                    CollectionName = "CurrencyCollection";
                    ObjectType = typeof(Currency);
                    CollectionType = typeof(CurrencyCollection);
                    break;
                case MasterDataTypeEnum.SupplierBank:
                    ObjectName = "SupplierBankAccount";
                    CollectionName = "SupplierBankAccountCollection";
                    ObjectType = typeof(SupplierBankAccount);
                    CollectionType = typeof(SupplierBankAccountCollection);
                    break;
                case MasterDataTypeEnum.Project:                    
                    ObjectName = "Project";
                    CollectionName = "ProjectCollection";
                    ObjectType = typeof(Project);
                    CollectionType = typeof(ProjectCollection);
                    break;
                case MasterDataTypeEnum.Custom:
                    ObjectName = "MasterDataObject";
                    CollectionName = "MasterDataObjectCollection";
                    ObjectType = typeof(MasterDataObject);
                    CollectionType = typeof(MasterDataObjectCollection);
                    break;
                default:
                    throw new Exception("Unknow Master Data Type provided i.e. " + masterDataTypeEnum.ToString());
            }
        }
    }
}
