﻿using ReadSoft.Services.Client;
using ReadSoft.Services.Client.Entities;
using System.IO;

namespace TaskRunner.Task.RSO
{
    public sealed class RsoDocumentService
    {
        public static int GetNumberOfOutputDocuments(ClientConfiguration clientConfiguration, string customerId)
        {
            DocumentServiceClient dsc = new DocumentServiceClient(clientConfiguration);
            return dsc.GetNumberOfOutputDocuments(customerId).Value;
        }

        public static DocumentReferenceCollection GetOutputDocuments(ClientConfiguration clientConfiguration, string customerId)
        {
            DocumentServiceClient dsc = new DocumentServiceClient(clientConfiguration);
            return dsc.GetOutputDocuments(customerId);
        }

        public static Document GetDocument(ClientConfiguration clientConfiguration, string documentId)
        {
            DocumentServiceClient dsc = new DocumentServiceClient(clientConfiguration);
            return dsc.GetDocument(documentId);
        }

        public static bool GetDocumentFile(ClientConfiguration clientConfiguration, string documentId, ReadSoft.Services.Client.Entities.DocumentFormat documentFormat, string filename)
        {
            byte[] documentFileBuffer = GetDocumentFile(clientConfiguration, documentId, documentFormat);
            using (FileStream fs = new FileStream(filename, FileMode.Create))
            {
                fs.Write(documentFileBuffer, 0, documentFileBuffer.Length);
                fs.Flush();
            };
            return true;
        }

        public static byte[] GetDocumentFile(ClientConfiguration clientConfiguration, string documentId, ReadSoft.Services.Client.Entities.DocumentFormat documentFormat)
        {
            byte[] buffer = new byte[4096];
            byte[] documentFileBuffer = null;

            DocumentServiceClient dsc = new DocumentServiceClient(clientConfiguration);
            Stream stream = dsc.GetDocumentFile(documentId, documentFormat.ToString());
            using (var responseStream = stream)
            {
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    int count = 0;
                    do
                    {
                        count = responseStream.Read(buffer, 0, buffer.Length);
                        memoryStream.Write(buffer, 0, count);
                    }
                    while (count != 0);
                    documentFileBuffer = memoryStream.ToArray();
                }
            }
            return documentFileBuffer;
        }

        public static void DocumentDownloaded(ClientConfiguration clientConfiguration, string documentId)
        {
            DocumentServiceClient dsc = new DocumentServiceClient(clientConfiguration);
            dsc.DocumentDownloaded(documentId);
        }

        //Reject document that didn't get to Xero
        public static void DocumentRejected(ClientConfiguration clientConfiguration, string documentId, string message)
        {
            DocumentServiceClient dsc = new DocumentServiceClient(clientConfiguration);
            dsc.DocumentRejected(documentId, message);
        }
    }
}
