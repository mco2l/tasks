CREATE TABLE Settings_Index (
    Id                     INTEGER       PRIMARY KEY,
    TaskId                 INTEGER       REFERENCES Settings_Task (Id),
    IdentityServerEndpoint INTEGER       REFERENCES Settings_Shared_AdestWebService (Id),
    APIEndpoint            INTEGER       REFERENCES Settings_Shared_AdestWebService (Id),
    HeaderFieldMappingId   INTEGER       REFERENCES Settings_Shared_FieldMapping (Id),
    LineFieldMappingId     INTEGER       REFERENCES Settings_Shared_FieldMapping (Id),
    DownloadSettingId      INTEGER (255) REFERENCES Settings_Shared_DownloadSettings (Id) 
);