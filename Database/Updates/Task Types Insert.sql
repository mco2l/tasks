INSERT INTO TaskTypes (
                          SettingsTable,
                          AssemblyType,
                          AssemblyName,
                          Description,
                          TaskTypeId
                      )
                      VALUES (
                          'Settings_Sage200_Posting',
                          'Adest.TaskRunner.Sage200.Post',
                          'Adest.TaskRunner.Sage200.dll',
                          'Sage 200 Task',
                          1
                      ),
                      (
                          'Settings_RSO_MasterUpload',
                          'TaskRunner.Task.RSO.MasterUpload',
                          'TaskRunner.Task.RSO.dll',
                          'RSO Master Data Upload Task',
                          2
                      ),
                      (
                          'Settings_RSO_ScanUpload',
                          'TaskRunner.Task.RSO.ScanUpload',
                          'TaskRunner.Task.RSO.dll',
                          'RSO Scan Upload Task',
                          3
                      ),
                      (
                          'Settings_RSO_ScanDownload',
                          'TaskRunner.Task.RSO.ScanDownload',
                          'TaskRunner.Task.RSO.dll',
                          'RSO Scan Download Task',
                          4
                      ),
                      (
                          'Settings_RSO_Sage50',
                          'Adest.TaskRunner.RSOSage50.Post',
                          'Adest.TaskRunner.RSOSage50.dll',
                          'Post RSO Download to Sage 50 Task',
                          5
                      ),
                      (
                          'Settings_Sage50_Posting',
                          'Adest.TaskRunner.AdestSage50.Post',
                          'Adest.TaskRunner.AdestSage50.dll',
                          'Sage 50 Task',
                          6
                      ),
                      (
                          'Settings_RSO_Xero',
                          'Adest.TaskRunner.RSOXero.Post',
                          'Adest.TaskRunner.RSOXero.dll',
                          'Adest RSO to Xero Task',
                          7
                      ),
                      (
                          'Settings_Index',
                          'TaskRunner.Task.AP.Index',
                          'TaskRunner.Task.AP.Index.dll',
                          'AP Index Task',
                          8
                      );
