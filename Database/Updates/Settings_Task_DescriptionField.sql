
PRAGMA foreign_keys = 0;

CREATE TABLE sqlitestudio_temp_table AS SELECT *
                                          FROM Settings_Task;

DROP TABLE Settings_Task;

CREATE TABLE Settings_Task (
    Id                    INTEGER    PRIMARY KEY
                                     NOT NULL,
    TaskTypeId            INTEGER    NOT NULL,
    Description           TEXT (100),
    StartTime             TEXT (5)   NOT NULL,
    EndTime               TEXT (5)   NOT NULL,
    Interval              INTEGER    NOT NULL,
    TotalRunPerTimeRange  INTEGER    NOT NULL,
    Disabled              BOOLEAN,
    EnableTracing         BOOLEAN,
    MaxLogFiles           INTEGER    NOT NULL,
    EmailSuccessSettingId INTEGER    NOT NULL
                                     REFERENCES Settings_Shared_Email (Id),
    EmailFailureSettingId INTEGER    REFERENCES Settings_Shared_Email (Id) 
                                     NOT NULL,
    RunOnStart            BOOLEAN,
    MaxFailuresAllowed    INTEGER,
    IntervalFailureRetry  INTEGER
);

INSERT INTO Settings_Task (
                              Id,
                              TaskTypeId,                              
                              StartTime,
                              EndTime,
                              Interval,
                              TotalRunPerTimeRange,
                              Disabled,
                              EnableTracing,
                              MaxLogFiles,
                              EmailSuccessSettingId,
                              EmailFailureSettingId,
                              RunOnStart,
                              MaxFailuresAllowed,
                              IntervalFailureRetry
                          )
                          SELECT Id,
                                 TaskTypeId,                                
                                 StartTime,
                                 EndTime,
                                 Interval,
                                 TotalRunPerTimeRange,
                                 Disabled,
                                 EnableTracing,
                                 MaxLogFiles,
                                 EmailSuccessSettingId,
                                 EmailFailureSettingId,
                                 RunOnStart,
                                 MaxFailuresAllowed,
                                 IntervalFailureRetry
                            FROM sqlitestudio_temp_table;

DROP TABLE sqlitestudio_temp_table;

PRAGMA foreign_keys = 1;
