PRAGMA foreign_keys = 0;

CREATE TABLE sqlitestudio_temp_table AS SELECT *
                                          FROM Settings_RSO_ScanDownload;

DROP TABLE Settings_RSO_ScanDownload;

CREATE TABLE Settings_RSO_ScanDownload (
    Id                 INTEGER       PRIMARY KEY,
    TaskId             INTEGER       REFERENCES Settings_Task (Id),
    DownloadPath       TEXT (1, 255),
    ImageLocationIndex INTEGER       NOT NULL,
    CreateBuyerFolder  BOOLEAN,
    UsePermaLink       BOOLEAN,
    RSOSettingId       INTEGER       REFERENCES Settings_Shared_RSO (Id),
    FieldMappingId     INTEGER       REFERENCES Settings_Shared_FieldMapping (Id) 
                                     NOT NULL
                                     REFERENCES Settings_Shared_FieldMappingFields (MappingId) 
);

INSERT INTO Settings_RSO_ScanDownload (
                                          Id,
                                          TaskId,
                                          DownloadPath,
                                          ImageLocationIndex,
                                          CreateBuyerFolder,
                                          UsePermaLink,
                                          RSOSettingId,
                                          FieldMappingId
                                      )
                                      SELECT Id,
                                             TaskId,
                                             DownloadPath,
                                             ImageLocationIndex,
                                             CreateBuyerFolder,
                                             UsePermaLink,
                                             RSOSettingId,
                                             FieldMappingId
                                        FROM sqlitestudio_temp_table;

DROP TABLE sqlitestudio_temp_table;

PRAGMA foreign_keys = 1;