--
-- File generated with SQLiteStudio v3.1.0 on Wed Jul 27 10:23:09 2016
--
-- Text encoding used: System
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: OrganizationSettings
CREATE TABLE OrganizationSettings (TenantId INTEGER PRIMARY KEY NOT NULL, TenantName TEXT (1, 50) NOT NULL, AccessKey TEXT (1, 100) NOT NULL, Disabled BOOLEAN, HostName TEXT (1, 100) NOT NULL);

-- Table: TaskTypes
CREATE TABLE TaskTypes (TaskTypeId INTEGER PRIMARY KEY NOT NULL, Description TEXT (1, 50) NOT NULL, AssemblyName TEXT (1, 50) NOT NULL, AssemblyType TEXT (1, 50) NOT NULL, SettingsTable TEXT (40) NOT NULL);

-- Table: TaskTypeSettings
CREATE TABLE TaskTypeSettings (Id INTEGER PRIMARY KEY NOT NULL, TaskTypeId INTEGER REFERENCES TaskTypes (TaskTypeId) NOT NULL, SettingName TEXT (1, 50) NOT NULL, Type INTEGER (0, 0) NOT NULL, MinLength INTEGER (1, 50), MaxLength INTEGER (0, 0), IsCore BOOLEAN);

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
