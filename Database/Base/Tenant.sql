--
-- File generated with SQLiteStudio v3.1.0 on Wed Aug 10 16:48:27 2016
--
-- Text encoding used: System
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: User
DROP TABLE IF EXISTS User;
CREATE TABLE User (UserId INTEGER PRIMARY KEY, UserName TEXT (1, 50) NOT NULL, Password TEXT (1, 20) NOT NULL, Email TEXT (1, 100) NOT NULL, IsAdmin BOOLEAN, DelegateTo INTEGER, DelegateFromDate DATE, DelegateToDate DATE, LockCount INTEGER);

-- Table: Settings_Shared_FieldMapping
DROP TABLE IF EXISTS Settings_Shared_FieldMapping;
CREATE TABLE Settings_Shared_FieldMapping (Id INTEGER PRIMARY KEY, Description TEXT (1, 50) NOT NULL);

-- Table: Settings_Shared_DownloadSettings
DROP TABLE IF EXISTS Settings_Shared_DownloadSettings;
CREATE TABLE Settings_Shared_DownloadSettings (Id INTEGER PRIMARY KEY NOT NULL, Path TEXT (1, 255) NOT NULL, BackupPath TEXT (1, 255) NOT NULL, FailurePath TEXT (1, 255) NOT NULL);

-- Table: Settings_Shared_Email
DROP TABLE IF EXISTS Settings_Shared_Email;
CREATE TABLE Settings_Shared_Email (Id INTEGER PRIMARY KEY NOT NULL, EmailTo TEXT (40) NOT NULL, CC TEXT (40), BCC TEXT (40), Subject TEXT (50) NOT NULL, BodyTextFile TEXT (255), IsHtmlBody BOOLEAN, Priority TEXT (20), Sensitivity TEXT (40), EmailFrom TEXT (40), FromDisplayName TEXT (40), ReplyTo TEXT (40) NOT NULL);

-- Table: Settings_Shared_FieldMappingFields
DROP TABLE IF EXISTS Settings_Shared_FieldMappingFields;
CREATE TABLE Settings_Shared_FieldMappingFields (Id INTEGER PRIMARY KEY, MappingId INTEGER REFERENCES Settings_Shared_FieldMapping (Id), FromField TEXT (1, 50) NOT NULL, ToField TEXT (1, 50) NOT NULL, DefaultValue TEXT (1, 50));

-- Table: Settings_UploadCSVToTable
DROP TABLE IF EXISTS Settings_UploadCSVToTable;
CREATE TABLE Settings_UploadCSVToTable (Id INTEGER PRIMARY KEY NOT NULL, TaskId INTEGER REFERENCES Settings_Task (Id) NOT NULL, AdestWebServiceSettingId INTEGER REFERENCES Settings_Shared_AdestWebService (Id), MapName TEXT (40), UploadType TEXT (10) NOT NULL, ClearBeforeUpload BOOLEAN, CsvSeparator TEXT (2), DataFilePath TEXT (255), DataFilePattern TEXT (20), DataFilePatternExt TEXT (10), DataFilePatternMask TEXT (20), BackupPath TEXT (255), ErrorPath TEXT (255), SkipFirstLine BOOLEAN, DatabaseId INTEGER REFERENCES Settings_Shared_Database (Id));

-- Table: Settings_Shared_RSO
DROP TABLE IF EXISTS Settings_Shared_RSO;
CREATE TABLE Settings_Shared_RSO (Id INTEGER PRIMARY KEY, API_Key TEXT (1, 40) NOT NULL, UserName TEXT (1, 50) NOT NULL, Password TEXT (1, 20) NOT NULL, Uri TEXT (1, 255) NOT NULL, UsingLineTaxFields BOOLEAN);

-- Table: Settings_Task_Sub
DROP TABLE IF EXISTS Settings_Task_Sub;
CREATE TABLE Settings_Task_Sub (Id INTEGER PRIMARY KEY, MainTaskId INTEGER REFERENCES Settings_Task (Id) NOT NULL, SubTaskId INTEGER REFERENCES Settings_Task (Id) NOT NULL, "Order" INTEGER NOT NULL);

-- Table: Settings_Shared_Sage50
DROP TABLE IF EXISTS Settings_Shared_Sage50;
CREATE TABLE Settings_Shared_Sage50 (Id INTEGER PRIMARY KEY NOT NULL, Company TEXT (50) NOT NULL, DatabasePath TEXT (255) NOT NULL, UserName TEXT (50) NOT NULL, Password TEXT (50) NOT NULL);

-- Table: Settings_Task
DROP TABLE IF EXISTS Settings_Task;
CREATE TABLE Settings_Task (Id INTEGER PRIMARY KEY NOT NULL, TaskTypeId INTEGER NOT NULL, StartTime TEXT (5) NOT NULL, EndTime TEXT (5) NOT NULL, Interval INTEGER NOT NULL, TotalRunPerTimeRange INTEGER NOT NULL, Disabled BOOLEAN, EnableTracing BOOLEAN, MaxLogFiles INTEGER NOT NULL, EmailSuccessSettingId INTEGER NOT NULL REFERENCES Settings_Shared_Email (Id), EmailFailureSettingId INTEGER REFERENCES Settings_Shared_Email (Id) NOT NULL, RunOnStart BOOLEAN, MaxFailuresAllowed INTEGER, IntervalFailureRetry INTEGER);

-- Table: Settings_Shared_DebugLog
DROP TABLE IF EXISTS Settings_Shared_DebugLog;
CREATE TABLE Settings_Shared_DebugLog (Id INTEGER NOT NULL PRIMARY KEY, Enabled BOOLEAN, Path TEXT (1, 255) NOT NULL);

-- Table: Settings_Shared_Database
DROP TABLE IF EXISTS Settings_Shared_Database;
CREATE TABLE Settings_Shared_Database (Id INTEGER PRIMARY KEY NOT NULL, ConnectionType TEXT (10) NOT NULL, ConnectionString TEXT (255) NOT NULL, CommandType TEXT (10) NOT NULL, PreCommandText TEXT (500), CommandText TEXT (500) NOT NULL);

-- Table: Settings_RSO_ScanDownload
DROP TABLE IF EXISTS Settings_RSO_ScanDownload;
CREATE TABLE Settings_RSO_ScanDownload (Id INTEGER PRIMARY KEY, TaskId INTEGER REFERENCES Settings_Task (Id), DownloadPath TEXT (1, 255), ImageLocationIndex INTEGER NOT NULL, CreateBuyerFolder BOOLEAN, UsePermaLink BOOLEAN, RSOSettingId INTEGER REFERENCES Settings_Shared_RSO (Id), FieldMappingId INTEGER REFERENCES Settings_Shared_FieldMapping (Id) NOT NULL);

-- Table: Settings_RSO_MasterUpload
DROP TABLE IF EXISTS Settings_RSO_MasterUpload;
CREATE TABLE Settings_RSO_MasterUpload (Id INTEGER PRIMARY KEY, TaskId INTEGER REFERENCES Settings_Task (Id) NOT NULL, UploadType INTEGER NOT NULL, ClearBeforeUpload BOOLEAN, CsvSeparator TEXT (1, 1), DataFilePath TEXT (1, 255) NOT NULL, DataFilePattern TEXT (1, 20), DataFilePatternExt TEXT (1, 10), DataFilePatternMask TEXT (1, 20), BackupPath TEXT (1, 255) NOT NULL, ErrorPath TEXT (1, 255) NOT NULL, SkipFirstLine BOOLEAN, IsSplitFiles BOOLEAN, DatabaseId INTEGER REFERENCES Settings_Shared_Database (Id), RSOSettingId INTEGER REFERENCES Settings_Shared_RSO (Id) NOT NULL, FieldMappingId INTEGER REFERENCES Settings_Shared_FieldMapping (Id) NOT NULL);

-- Table: Settings_RSO_Sage50
DROP TABLE IF EXISTS Settings_RSO_Sage50;
CREATE TABLE Settings_RSO_Sage50 (ID INTEGER PRIMARY KEY NOT NULL, TaskId INTEGER REFERENCES Settings_Task (Id), Sage50Id INTEGER REFERENCES Settings_Shared_Sage50 (Id) NOT NULL, AttachmentPath TEXT (1, 255) NOT NULL, RSOSettingId INTEGER NOT NULL REFERENCES Settings_Shared_RSO (Id), HeaderFieldMappingId INTEGER REFERENCES Settings_Shared_FieldMapping (Id) NOT NULL, LineFieldMappingId INTEGER NOT NULL REFERENCES Settings_Shared_FieldMapping (Id), DownloadSettingId INTEGER NOT NULL REFERENCES Settings_Shared_DownloadSettings (Id));

-- Table: Settings_RSO_ScanUpload
DROP TABLE IF EXISTS Settings_RSO_ScanUpload;
CREATE TABLE Settings_RSO_ScanUpload (Id INTEGER PRIMARY KEY, TaskId INTEGER REFERENCES Settings_Task (Id), AutoDetection BOOLEAN, AutoBuyerID BOOLEAN NOT NULL, BatchPath TEXT (1, 255), MaxFileSize INTEGER, DefaultDocumentSystemName TEXT (1, 30), OverMaxSizeFilePath TEXT (1, 255), BackupPath TEXT (1, 255), AutoCreateFolder BOOLEAN, BatchListID INTEGER REFERENCES Settings_Shared_BatchList (Id), RSOSettingId INTEGER REFERENCES Settings_Shared_RSO (Id));

-- Table: Settings_Shared_BatchList
DROP TABLE IF EXISTS Settings_Shared_BatchList;
CREATE TABLE Settings_Shared_BatchList(Id INTEGER PRIMARY KEY, Path TEXT (1, 255), DocumentType TEXT (1, 40), BuyersName TEXT (1, 50));

-- Table: Settings_RSO_Xero
DROP TABLE IF EXISTS Settings_RSO_Xero;
CREATE TABLE Settings_RSO_Xero (ID INTEGER PRIMARY KEY NOT NULL, TaskId INTEGER REFERENCES Settings_Task (Id), OrganizationName TEXT (1, 50) NOT NULL, ConsumerKey TEXT (1, 40) NOT NULL, ConsumerSecret TEXT (1, 40) NOT NULL, AppName TEXT (1, 40) NOT NULL, Buyer TEXT (1, 40) NOT NULL, UseXeroFailureSupplier BOOLEAN, XeroFailureSupplier TEXT (1, 40), DownloadSettingId INTEGER REFERENCES Settings_Shared_DownloadSettings (Id) NOT NULL, RSOSettingId INTEGER REFERENCES Settings_Shared_RSO (Id) NOT NULL, DebugLogSettingId INTEGER REFERENCES Settings_Shared_DebugLog (Id), InvoiceLogEnabled BOOLEAN, InvoiceLogPath TEXT (1, 255), ClearBeforeUpload BOOLEAN);

-- Table: Settings_Shared_AdestWebService
DROP TABLE IF EXISTS Settings_Shared_AdestWebService;
CREATE TABLE Settings_Shared_AdestWebService (Id INTEGER PRIMARY KEY, Uri TEXT (1, 255) NOT NULL, UserName TEXT (1, 50) NOT NULL, Password TEXT (1, 20) NOT NULL, SearchCompany TEXT (1, 50));

-- Table: Settings_Sage200_Posting
DROP TABLE IF EXISTS Settings_Sage200_Posting;
CREATE TABLE Settings_Sage200_Posting (ID INTEGER PRIMARY KEY NOT NULL, TaskId INTEGER REFERENCES Settings_Task (Id), Company TEXT (1, 50) NOT NULL, UserName TEXT (1, 50) NOT NULL, Password TEXT (1, 20) NOT NULL, AdestWebServiceSettingId INTEGER REFERENCES Settings_Shared_AdestWebService (Id) NOT NULL, DebugLogSettingId INTEGER REFERENCES Settings_Shared_DebugLog (Id), HeaderFieldMappingId INTEGER REFERENCES Settings_Shared_FieldMapping (Id) NOT NULL, LineFieldMappingId INTEGER REFERENCES Settings_Shared_FieldMapping (Id));

-- Table: Settings_Sage50_Posting
DROP TABLE IF EXISTS Settings_Sage50_Posting;
CREATE TABLE Settings_Sage50_Posting (Id INTEGER PRIMARY KEY, TaskId INTEGER REFERENCES Settings_Task (Id), Sage50Id INTEGER REFERENCES Settings_Shared_Sage50 (Id) NOT NULL, AdestWebServiceSettingId INTEGER REFERENCES Settings_Shared_AdestWebService (Id) NOT NULL, DebugLogSettingId INTEGER NOT NULL REFERENCES Settings_Shared_DebugLog (Id), HeaderFieldMappingId INTEGER REFERENCES Settings_Shared_FieldMapping (Id) NOT NULL, LineFieldMappingId INTEGER (0) REFERENCES Settings_Shared_FieldMapping (Id));

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
