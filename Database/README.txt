Each developer will have their own local databases to allow for trying things out, changes ect without impacting others
Base databases scripted and source controlled in git 
If a developer is working on a database change & is happy with the change they should:
	- Ensure they are working with the latest schema
	- Add their change
	- Test it
	- Script their changes object (including exists, create or alter statements where necessary).
	- Add script to repository if script does not exist otherwise replace existing script for object.	


For each release the updates are merged with the Base, therefore creating the development base for the next version.

Until we see a better/more automated way to notify all developers of script changes and as Marie receives notification of all comitts she will notify developers of database changes via email.

